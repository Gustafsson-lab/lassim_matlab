Installation of the SBPOP Package:
==================================

- Start MATLAB
- The first time you install a new version of the SBPOP package, please run 
  the script: "installSBPOPpackageInitial". This will compile all needed 
  libraries and executables for your system.
- If you do not install it for the first time, then just execute the script
  "installSBPOPpackage". Alternatively you just need to add the SBPOP 
  Package folder and all subfolders to the MATLAB path.
- If your system is allowed to store the MATLAB path between MATLAB 
  sessions, you do not need to call these installation scripts again. If 
  your MATLAB path is not stored, then you will need to do that each time
  you start MATLAB and want to use the SBPOP Package.

Handling of NLME tools:
=======================

If you want to use NLME tools (MONOLIX and/or NONMEM), then please do the following:

- Open the file "SETUP_NLME_TOOLS.m"
- Follow the instructions in the file, which essentially means to provide the 
  paths and names of the executables for NONMEM and MONOLIX


