function [] = PopPK_checkData( dataPath )
% [SYNTAX]
% [] = PopPK_checkData( dataPath )
% dataPath: Path to the dataset in the general data format
%
% [OUTPUT]
% If at least one of the required columns is not present an error will be
% shown. Warnings might be shown for other detected things. No claim on
% completeness of checks is done!

% Sanity check if general data format is correct
SBPOPcheckGeneralDataFormat(dataPath);
