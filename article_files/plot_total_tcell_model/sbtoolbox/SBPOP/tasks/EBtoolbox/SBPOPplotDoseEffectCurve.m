function SBPOPplotDoseEffectCurve(effect, dosevector, paramchangevector, ylabeltext, titletext)
% SBPOPplotDoseEffectCurve: This function plots a dose/effect curve for
% given input arguments.
%
% USAGE:
% ======
% SBPOPplotDoseEffectCurve(effect, dosevector, paramchangevector, ylabeltext, titletext)
%
% effect: matrix with determined effect values (columns correspond to
%   different parameter settings (parameterchangevector) and rows correspond
%    to different dose amount settings (dosevector)).
% dosevector: vector with different dosing amounts, corresponding to  the
%   effect matrix.
% paramchangevector: vector with values for an additional parameter that
%   was varied.
% ylabeltext: text to plot on the y-axis
% titletext: text to use as figure title

% Information:
% ============
% Copyright � 2012 Novartis Pharma AG
% 
% This program is Free Open Source Software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program. If not, see <http://www.gnu.org/licenses/>.

[dummy,p] = max(var(effect')');
figure;
for k = 1:length(paramchangevector)
    plot(dosevector, effect(:, k),'-', 'LineWidth', 2);
    text(dosevector(p), effect(p, k), num2str(paramchangevector(k)),'EdgeColor','black','BackgroundColor','white');
    hold on
end
set(gca,'XScale','log');
hlhlx = title(titletext,'Interpreter','none');
set(hlhlx,'Interpreter','none');
xlim([min(dosevector) max(dosevector)]);
xlabel('Dose');
hlhlx = ylabel(ylabeltext,'Interpreter','none');
set(hlhlx,'Interpreter','none');

