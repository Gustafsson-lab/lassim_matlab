function [popt] = SBPOPoptimizeLigandTARGETkinetics(model,pnames_in,pvalues0,referenceValues_in,varargin)
% SBPOPoptimizeLigandTARGETkinetics: This function will estimate parameters
% of the standard model to adjust the kinetics of a target (ligand) to
% desired properties (alpha and beta halflife)
%
% These desired properties are:
%   - alpha-halftime of target in plasma             (referenceValue(1)) 
%   - beta-halftime of target in plasma              (referenceValue(2)) 
% 
% IMPORTANT:
% ==========
% This function assumes the use of the standard model and by default the use of
% "T1_plasma" as the statename for the target in the plasma compartment.
% The user can define a different name for the target state in plasma, which is
% useful if a second soluble target is available in the model which also should 
% be fitted.
% The user can provide the parameters to be estimated and initial guesses. 
%
% The results are summarized in the MATLAB command window.
%
% USAGE:
% ======
% [popt] = SBPOPoptimizeLigandTARGETkinetics(model,pnames,pvalues0,referenceValues)
% [popt] = SBPOPoptimizeLigandTARGETkinetics(model,pnames,pvalues0,referenceValues,parametervector)
% [popt] = SBPOPoptimizeLigandTARGETkinetics(model,pnames,pvalues0,referenceValues,parametervector,targetparam)
% [popt] = SBPOPoptimizeLigandTARGETkinetics(model,pnames,pvalues0,referenceValues,parametervector,targetparam,weigthts)
% [popt] = SBPOPoptimizeLigandTARGETkinetics(model,pnames,pvalues0,referenceValues,parametervector,targetparam,weigthts,timevector)
%
% model: SBmodel or MEX simulation model
% pnames: cell-array with parameters to estimate
% pvalues0: vector with initial guesses for the parameters
% referenceValues: vector with 2 entries:
%   referenceValues(1): desired alpha-halftime of target in plasma (NOT ALLOWED TO BE ZERO) 
%   referenceValues(2): desired beta-halftime of target in plasma (NOT ALLOWED TO BE ZERO) 
% parametervector: full parameter vector for model simulation. If not
%   given, the parameters defined in the model are used. The definition of
%   this parameter vector makes most sense if the function is called on a
%   MEX simulation function, rather than an SBmodel. If empty vector is
%   passed then the parameter vector in the model will be used.
% targetparam: name of the target state variable to consider (there might
%   be several targets in the model)
% weights: vector with weights for the residuals for the two different
%   objectives, defined in the referenceValues vector. For small halflives
%   it might happen that the alpha halflife depends on the beta halflife.
%   In these cases a higher weight for the beta halflife should be chosen
%   to get this at least onto the desired value.
% timevector: timevector for simulation during optimization (needs to
%   capture both the fast and the slow phase of target distribution and
%   elimination.
%
% DEFAULT VALUES:
% ===============
% parametervector: nominal vector defined in the model
% targetparam: 'T1_plasma'
% timevector: 0 to referenceValues(2)*10 in 5000 steps
%
% Output Arguments:
% =================
% popt: vector with optimized parameter values

% Information:
% ============
% Copyright � 2012 Novartis Pharma AG
% 
% This program is Free Open Source Software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program. If not, see <http://www.gnu.org/licenses/>.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Make parameter names and reference values 
% available to the cost function
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
warning off;
global pnames referenceValues MEXmodel weights opttimevector plotFlag targetparam parameternames parametervector
warning on;
pnames = pnames_in;
referenceValues = referenceValues_in;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Handle variable input arguments
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ts = 0;
te = referenceValues(2)*10;
opttimevector = [ts:(te-ts)/5000:te];
targetparam = 'T1_plasma';
weights = [1 1];
[parameternames,parametervector_nominal] = SBparameters(model); % get values stored in the model
parametervector = parametervector_nominal;
if nargin >= 5,
    parametervector = varargin{1};
    if isempty(parametervector),
        parametervector = parametervector_nominal;
    else
        if length(parametervector) ~= length(SBparameters(model)),
            error('The length of the provided parameter vector does not match the number of parameters in the model.');
        end
    end
end
if nargin >= 6,
    targetparam = varargin{2};
end
if nargin >= 7,
    weights = varargin{3};
end
if nargin >= 8,
    opttimevector = varargin{4};
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Get MEX model
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if isSBmodel(model),
    [MEXmodel, MEXmodelfullpath] = makeTempMEXmodelSBPD(model);
else 
    MEXmodel = model;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define parameter bounds
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
plb = zeros(1,length(pvalues0));       % lower bounds
pub = 1000*pvalues0;                   % upper bounds

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Run the optimization
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
OPTOPTIONS.lowbounds = plb;
OPTOPTIONS.highbounds = pub;
OPTOPTIONS.maxfunevals = 200;
OPTOPTIONS.silent = 1;
plotFlag = 0;
[popt, FVAL] = simplexSB(@target_T_a_b, pvalues0, OPTOPTIONS);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Check "optimal" parameters and plot a check
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
REF_T12_a         = referenceValues(1);   % Target Alpha half time
REF_T12_b         = referenceValues(2);   % Target Beta half time
plotFlag = 1;
[cost,residuals] = target_T_a_b(popt);
disp(' ');
disp('Optimized Target kinetics:');
disp('==========================');
disp(sprintf('achieved cost = %g',cost));
disp(sprintf('achieved_T12_a = %g',residuals(1) + REF_T12_a))
disp(sprintf('achieved_T12_b = %g',residuals(2) + REF_T12_b))
disp('Optimal parameters:');
for k=1:length(pnames),
    disp(sprintf('\t%s = %g',pnames{k},popt(k)));
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Remove MEX model if created here
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if isSBmodel(model),
    clear mex; delete(MEXmodelfullpath);
end
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Cost function for optimization of the parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [cost,res] = target_T_a_b(x)
% target_T_a_b: cost function for the optimization of given model parameters
% with the goal to achieve a certain (and provided):
%
%   - Alpha half-time of target
%   - Beta half-time of target

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% get the global variables
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
global pnames referenceValues MEXmodel weights opttimevector plotFlag targetparam parameternames parametervector

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Get the reference values
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
REF_T12_a   = referenceValues(1);
REF_T12_b   = referenceValues(2);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Perform experiment to determine the alpha 
% and beta halflife of the target in plasma 
% for given parameters to be optimized
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% set initial concentration of drug in plasma to a value
icvector = makeinicondvectorSBPD(MEXmodel,{targetparam},1000);
% set the parameters to be optimized
parametervector_sim = updateparamvecSBPD(parameternames, parametervector, pnames, x);
% simulate                                     
options = [];
options.abstol = 1e-10;
options.reltol = 1e-10;
simdata = feval(MEXmodel, opttimevector, icvector, parametervector_sim,options);
% Get the interesting data from simulation results (timevector and drug concentration in plasma)
time          = simdata.time;
Target_plasma = simdata.statevalues(:,getstateindicesSBPD(MEXmodel,targetparam));

% Compute the target alpha half time
t1_indx = 10;
T1_a = time(1);
T2_a = time(t1_indx);
Y1_a = Target_plasma(1);
Y2_a = Target_plasma(t1_indx);
T12_a = log(0.5)*(T2_a - T1_a) / (log(Y2_a) - log(Y1_a));

% Compute the target beta half time
t2_indx = 100;
T1_b = time(end-t2_indx);
T2_b = time(end);
Y1_b = Target_plasma(end-t2_indx);
Y2_b = Target_plasma(end);
T12_b = log(0.5)*(T2_b - T1_b) / (log(Y2_b) - log(Y1_b));

if plotFlag,
    figure; clf;
    plot(time,Target_plasma); hold on; drawnow;  
    plot(T1_a,Y1_a,'ro');
    plot(T2_a,Y2_a,'go');
    plot(T1_b,Y1_b,'rx');
    plot(T2_b,Y2_b,'gx');
    set(gca,'YScale','log');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Determine the residuals
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
res(1) = T12_a - REF_T12_a;
res(2) = T12_b - REF_T12_b;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Determine the costfunction 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
weightedres = [res(1)/REF_T12_a  res(2)/REF_T12_b].*weights(:)';
cost = sum(weightedres.^2);
return


