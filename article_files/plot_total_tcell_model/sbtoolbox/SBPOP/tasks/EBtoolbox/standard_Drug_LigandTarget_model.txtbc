********** MODEL NAME
standard_Drug_LigandTarget_model

% Information:
% ============
% Copyright � 2012 Novartis Pharma AG
% 
% This program is Free Open Source Software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program. If not, see <http://www.gnu.org/licenses/>.

********** MODEL NOTES
General 3 compartment (ligand target) model (plasma, disease tissue, 
peripheral tissue) containing drug (mAb, nanobody, aptamer, etc.) kinetics, 
target kinetics, drug/target binding kinetics, target dimerization, binding 
protein kinetics and binding to target.

Model assumptions are indicated in comment text.

It is modular ... just cut out what you do not need.
    
D:          drug (monoclonal antibody, nanobody, aptamer, ...)
T1:         target 1 (ligand)
D_T1:       target 1 bound to D
T1_D_T1:    2times target 1 bound to D (one T1 on each arm)
T1_T1:      dimerized target 1
BP:         target binding soluble protein
T1_BP:      target 1 bound to BP

Possible improvements of the model:
===================================
- Adding a second target T2
- Adding a receptor R1 to which the target can bind 
    - only in tissue compartment but possible also in all compartments
- Adding binding of drug to target dimer
- ...

********** MODEL STATE INFORMATION
% Definition of initial conditions for all the species. 
% Mostly they are zero and thus would not need to be defined. However,
% in this section the connection between species and their compartments is 
% done, using the additional information {isSpecie:compartmentname:unittype},
% where "compartmentname" is the name of the parameter or variable, defining 
% the volume of the compartment and "unittype" is "amount" or "concentration".
% This additional information is crucial for correct simulation and thus needs
% to be provided for each state variable!

D_plasma(0)              = 0   {isSpecie:volume_plasma:concentration}        % (nM) concentration of D in plasma
D_itst_tissue(0)         = 0   {isSpecie:volume_itst_tissue:concentration}   % (nM) concentration of D in interstitial space around disease compartment
D_itst_peri(0)           = 0   {isSpecie:volume_itst_peri:concentration}     % (nM) concentration of D in interstitial space around peripheral compartment

T1_plasma(0)             = 0   {isSpecie:volume_plasma:concentration}        % (nM) concentration of T1 in plasma
T1_itst_tissue(0)        = 0   {isSpecie:volume_itst_tissue:concentration}   % (nM) concentration of T1 interstitial space around disease compartment
T1_itst_peri(0)          = 0   {isSpecie:volume_itst_peri:concentration}     % (nM) concentration of T1 in interstitial space around peripheral compartment

D_T1_plasma(0)           = 0   {isSpecie:volume_plasma:concentration}        % (nM) concentration of D/T1 complex in plasma
D_T1_itst_tissue(0)      = 0   {isSpecie:volume_itst_tissue:concentration}   % (nM) concentration of D/T1 complex in interstitial space around disease compartment
D_T1_itst_peri(0)        = 0   {isSpecie:volume_itst_peri:concentration}     % (nM) concentration of D/T1 complex in interstitial space around peripheral compartment 

T1_D_T1_plasma(0)        = 0   {isSpecie:volume_plasma:concentration}        % (nM) concentration of T1/D/T1 complex in plasma
T1_D_T1_itst_tissue(0)   = 0   {isSpecie:volume_itst_tissue:concentration}   % (nM) concentration of T1/D/T1 complex in interstitial space around disease compartment
T1_D_T1_itst_peri(0)     = 0   {isSpecie:volume_itst_peri:concentration}     % (nM) concentration of T1/D/T1 complex in interstitial space around peripheral compartment 

T1_T1_plasma(0)          = 0   {isSpecie:volume_plasma:concentration}        % (nM) concentration of dimerized T1 in plasma
T1_T1_itst_tissue(0)     = 0   {isSpecie:volume_itst_tissue:concentration}   % (nM) concentration of dimerized T1 in interstitial space around disease compartment
T1_T1_itst_peri(0)       = 0   {isSpecie:volume_itst_peri:concentration}     % (nM) concentration of dimerized T1 in interstitial space around peripheral compartment 

BP_plasma(0)             = 0   {isSpecie:volume_plasma:concentration}        % (nM) concentration of T1 binding soluble protein in plasma
BP_itst_tissue(0)        = 0   {isSpecie:volume_itst_tissue:concentration}   % (nM) concentration of T1 binding soluble protein in interstitial space around disease compartment
BP_itst_peri(0)          = 0   {isSpecie:volume_itst_peri:concentration}     % (nM) concentration of T1 binding soluble protein in interstitial space around peripheral compartment 

T1_BP_plasma(0)          = 0   {isSpecie:volume_plasma:concentration}        % (nM) concentration of T1/BP complex in plasma
T1_BP_itst_tissue(0)     = 0   {isSpecie:volume_itst_tissue:concentration}   % (nM) concentration of T1/BP complex in interstitial space around disease compartment
T1_BP_itst_peri(0)       = 0   {isSpecie:volume_itst_peri:concentration}     % (nM) concentration of T1/BP complex in interstitial space around peripheral compartment 


********** MODEL PARAMETERS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define general parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
weight                     = 70        % (kg) Weight of standard person (needed to determine dose in nmol from mg/kg)
volume_body                = 74.8757  {isCompartment:}             % (L) Total body volume
volume_itst                = 17.4572  {isCompartment:volume_body}  % (L) Total volume of interstitial space
volume_blood               = 5.5      {isCompartment:volume_body}  % (L) Total volume of blood
volume_plasma              = 2.68345  {isCompartment:volume_body}  % (L) Plasma volume (corresponds to "interstitial space" in blood)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define disease tissue related parameters (project specific)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
volume_tissue              = 1        {isCompartment:volume_body}  % (L) Volume of disease tissue 
volume_itst_tissue         = 1        {isCompartment:volume_body}  % (L) Volume of interstitial space around disease tissue

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define lymph flow related rate constant
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ASSUMPTION: Rate constants for lymph flow related clearance from interstitial 
%             space into plasma compartments are the same for all species. Therefore,
%             we define a single parameter vor them.
kL                         = 0         % (1/day) Rate constant for lymph flow related clearance from interstitial space into plasma

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define D only related parameters (project specific)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
F                          = 1         % (.) Bioavailability
molecularweight_D          = 150000    % (Da) Molecular weight of D
kPS_D_total                = 0         % (L/day) D total surface area exchange coefficient
kCL_D_plasma               = 0         % (1/day) Rate constant for clearance of D from the plasma compartment

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define T1 only related parameters (project specific)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
molecularweight_T1         = 10000     % (Da) Molecular weight of T1 (only used for unit conversion for plotting, etc.)
ksyn_T1_plasma             = 0         % (nmol/day) Synthesis rate of T1 in plasma (amount/time units)
ksyn_T1_itst_tissue        = 0         % (nmol/day) Synthesis rate of T1 in interstitial space around disease tissue
ksyn_T1_itst_peri          = 0         % (nmol/day) Synthesis rate of T1 in interstitial space around peripheral compartment
kPS_T1_total               = 0         % (L/day) Target T1 total surface area exchange coefficient
kCL_T1_plasma              = 0         % (1/day) Rate constant for clearance of T1 from the plasma compartment
kCL_T1_itst_tissue         = 0         % (1/day) Rate constant for clearance of T1 from the interstitial space around disease tissue
kCL_T1_itst_peri           = 0         % (1/day) Rate constant for clearance of T1 from the interstitial space around peripheral compartment
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define D / T1 binding parameters (project specific)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
kon_D_T1                   = 0         % (1/(nM*day)) Binding rate constant drug and T1
KD_D_T1                    = 0         % (nM) Dissociation constant D and T1 koff_D_T1: KD_D_T1 * kon_D_T1

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define parameters related to T1 dimerization (project specific)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
kon_T1_T1                  = 0         % (1/(nM*day)) Target T1-dimerization rate constant
KD_T1_T1                   = 0         % (nM) Target T1-Dimer-Dissociation constant koff_T1_T1: KD_T1_T1 * kon_T1_T1

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define parameters related to soluble target T1 binding protein BP (project specific)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
kPS_BP_total               = 0         % (L/day) BP total surface area exchange coefficient
kon_T1_BP                  = 0         % (1/(nM*day)) Binding rate constant T1 and BP
KD_T1_BP                   = 0         % (nM) Dissociation constant T1/BP complex koff_T1_BP: KD_T1_BP * kon_T1_BP

********** MODEL VARIABLES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculate the disease dependent volumes for the peripheral compartment
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
volume_peri                = volume_body-volume_tissue-volume_blood        {isCompartment:volume_body}  % (L) Peripheral compartment volume
volume_itst_peri           = volume_itst-volume_itst_tissue-volume_plasma  {isCompartment:volume_body}  % (L) Volume of interstitial space around peripheral compartment

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculate D only related parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
kPS_D_plasma_itst_tissue   = volume_tissue/volume_body*kPS_D_total    % (L/day) D surface area exchange coefficient between plasma and interstitial tissue around disease tissue
kPS_D_plasma_itst_peri     = volume_peri/volume_body*kPS_D_total      % (L/day) D surface area exchange coefficient between plasma and interstitial tissue around peripheral compartment
% Assume same clearance rate constant in all compartments
kCL_D_itst_tissue          = kCL_D_plasma                             % (1/day) Rate constant for clearance of D from the interstitial space around disease tissue
kCL_D_itst_peri            = kCL_D_plasma                             % (1/day) Rate constant for clearance of D from the interstitial space around peripheral compartment

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculate T1 only related parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
kPS_T1_plasma_itst_tissue  = volume_tissue/volume_body*kPS_T1_total   % (L/day) Target T1 surface area exchange coefficient between plasma and interstitial tissue around disease tissue
kPS_T1_plasma_itst_peri    = volume_peri/volume_body*kPS_T1_total     % (L/day) Target T1 surface area exchange coefficient between plasma and interstitial tissue around peripheral compartment

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculate T1/D binding related parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ASSUMPTION: same binding kinetics for D with T1 and D/T1 complex to T1
kon_T1_D_T1                = kon_D_T1                                 % (1/(nM*day)) Binding rate constant D/T1 complex to T1
KD_T1_D_T1                 = KD_D_T1                                  % (nM) Dissociation constant D/T1 complex and T1 koff_T1_D_T1: KD_T1_D_T1 * kon_T1_D_T1

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculate D/T1 complex related parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ASSUMPTION: same kinetic rate constants for exchange, lymph flow, and  
%             clearance reactions as for the free D
kPS_D_T1_plasma_itst_tissue    = kPS_D_plasma_itst_tissue             % (L/day) D/T1 complex surface area exchange coefficient between plasma and interstitial tissue around disease tissue
kPS_D_T1_plasma_itst_peri      = kPS_D_plasma_itst_peri               % (L/day) D/T1 complex surface area exchange coefficient between plasma and interstitial tissue around peripheral compartment
kCL_D_T1_plasma                = kCL_D_plasma                         % (1/day) Rate constant for clearance of D/T1 from the plasma compartment
kCL_D_T1_itst_tissue           = kCL_D_itst_tissue                    % (1/day) Rate constant for clearance of D/T1 from the interstitial tissue around disease tissue
kCL_D_T1_itst_peri             = kCL_D_itst_peri                      % (1/day) Rate constant for clearance of D/T1 from the interstitial tissue around peripheral compartment

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculate T1/D/T1 complex related parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ASSUMPTION: same kinetic rate constants for exchange, lymph flow, and  
%             clearance reactions as for the free D
kPS_T1_D_T1_plasma_itst_tissue = kPS_D_plasma_itst_tissue             % (L/day) T1/D/T1 complex surface area exchange coefficient between plasma and interstitial tissue around disease tissue
kPS_T1_D_T1_plasma_itst_peri   = kPS_D_plasma_itst_peri               % (L/day) T1/D/T1 complex surface area exchange coefficient between plasma and interstitial tissue around peripheral compartment
kCL_T1_D_T1_plasma             = kCL_D_plasma                         % (1/day) Rate constant for clearance of T1/D/T1 from the plasma compartment
kCL_T1_D_T1_itst_tissue        = kCL_D_itst_tissue                    % (1/day) Rate constant for clearance of T1/D/T1 from the interstitial tissue around disease tissue
kCL_T1_D_T1_itst_peri          = kCL_D_itst_peri                      % (1/day) Rate constant for clearance of T1/D/T1 from the interstitial tissue around peripheral compartment

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculate T1 dimerization related parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ASSUMPTION: same kinetic rate constants for exchange, lymph flow, and  
%             clearance reactions as for the T1 alone 
kPS_T1_T1_plasma_itst_tissue   = kPS_T1_plasma_itst_tissue            % (L/day) Dimerized T1 surface area exchange coefficient between plasma and interstitial tissue around disease tissue
kPS_T1_T1_plasma_itst_peri     = kPS_T1_plasma_itst_peri              % (L/day) Dimerized T1 surface area exchange coefficient between plasma and interstitial tissue around peripheral compartment
kCL_T1_T1_plasma               = kCL_T1_plasma                        % (1/day) Rate constant for clearance of dimerized T1 from the plasma compartment
kCL_T1_T1_itst_tissue          = kCL_T1_itst_tissue                   % (1/day) Rate constant for clearance of dimerized T1 from the interstitial tissue around disease tissue
kCL_T1_T1_itst_peri            = kCL_T1_itst_peri                     % (1/day) Rate constant for clearance of dimerized T1 from the interstitial tissue around peripheral compartment

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculate parameters related to soluble T1 binding protein BP (project specific)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
kPS_BP_plasma_itst_tissue      = volume_tissue/volume_body*kPS_BP_total     % (L/day) BP surface area exchange coefficient between plasma and interstitial tissue around disease tissue
kPS_BP_plasma_itst_peri        = volume_peri/volume_body*kPS_BP_total       % (L/day) BP surface area exchange coefficient between plasma and interstitial tissue around peripheral compartment

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Unit conversion factors
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
FACTOR_mg_per_kg_2_nmol        = weight/molecularweight_D*1e6               % (nmol/(mg/kg)) unit conversion factor for dosing input

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Unit conversions for plotting and checking
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
D_plasma_amount                = D_plasma * volume_plasma
D_itst_tissue_amount           = D_itst_tissue * volume_itst_tissue
D_itst_peri_amount             = D_itst_peri*volume_itst_peri
T1_plasma_amount               = T1_plasma * volume_plasma
T1_itst_tissue_amount          = T1_itst_tissue * volume_itst_tissue
T1_itst_peri_amount            = T1_itst_peri*volume_itst_peri

********** MODEL REACTIONS
% Please note that all reactions should be in amount/time units => nmol/day

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% D only reactions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% D input into plasma compartment (INPUT1 in (mg/kg)/day)
=> D_plasma : v_D_input                                       % (nmol/day) 
	vf = F * FACTOR_mg_per_kg_2_nmol*INPUT1

% Passive exchange of D between plasma and interstitial space around disease compartment (diffusion type of mechanism)
D_plasma <=> D_itst_tissue : v_D_plasma_itst_tissue_ex        % (nmol/day) 
	vf = kPS_D_plasma_itst_tissue*D_plasma
    vr = kPS_D_plasma_itst_tissue*D_itst_tissue

% Passive exchange of D between plasma and interstitial space around peripheral compartment (diffusion type of mechanism)
D_plasma <=> D_itst_peri : v_D_plasma_itst_peripheral_ex      % (nmol/day) 
	vf = kPS_D_plasma_itst_peri*D_plasma
    vr = kPS_D_plasma_itst_peri*D_itst_peri

% Active transport of D due to lymph flow from interstitial space around disease tissue into plasma compartment
D_itst_tissue => D_plasma : v_D_itst_tissue_plasma_lymph      % (nmol/day) 
	vf = (kL*D_itst_tissue)*volume_itst_tissue

% Active transport of D due to lymph flow from interstitial space around peripheral compartment into plasma compartment
D_itst_peri => D_plasma : v_D_itst_peri_plasma_lymph          % (nmol/day)
    vf = (kL*D_itst_peri)*volume_itst_peri 

% Clearance of D from plasma
D_plasma =>  : v_D_plasma_cl                                    % (nmol/day)
	vf = (kCL_D_plasma*D_plasma)*volume_plasma

% Clearance of D from interstitial tissue around disease compartment
D_itst_tissue => : v_D_itst_tissue_cl                           % (nmol/day)
    vf = (kCL_D_itst_tissue*D_itst_tissue)*volume_itst_tissue

% Clearance of D from interstitial tissue around peripheral compartment
D_itst_peri => : v_D_itst_peri_cl                               % (nmol/day)
    vf = (kCL_D_itst_peri*D_itst_peri)*volume_itst_peri

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Target T1 only reactions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
% Target T1 synthesis in plasma (constant production rate)
=> T1_plasma : v_T1_plasma_syn                                        % (nmol/day)
    vf = ksyn_T1_plasma

% Target T1 synthesis in interstitial space around disease tissue (constant production rate)
=> T1_itst_tissue : v_T1_itst_tissue_syn                              % (nmol/day)
    vf = ksyn_T1_itst_tissue

% Target T1 synthesis in interstitial space around peripheral tissue (constant production rate)
=> T1_itst_peri : v_T1_itst_peri_syn                                  % (nmol/day)
    vf = ksyn_T1_itst_peri

% Passive exchange of T1 between plasma and interstitial space around disease compartment (diffusion type of mechanism)
T1_plasma <=> T1_itst_tissue : v_T1_plasma_itst_tissue_ex             % (nmol/day) 
	vf = kPS_T1_plasma_itst_tissue*T1_plasma
    vr = kPS_T1_plasma_itst_tissue*T1_itst_tissue

% Passive exchange of T1 between plasma and interstitial space around peripheral compartment (diffusion type of mechanism)
T1_plasma <=> T1_itst_peri : v_T1_plasma_itst_peripheral_ex            % (nmol/day)
	vf = kPS_T1_plasma_itst_peri*T1_plasma
    vr = kPS_T1_plasma_itst_peri*T1_itst_peri
    
% Active transport of T1 due to lymph flow from interstitial space around disease tissue into plasma compartment
T1_itst_tissue => T1_plasma : v_T1_itst_tissue_plasma_lymph            % (nmol/day)
	vf = (kL*T1_itst_tissue)*volume_itst_tissue

% Active transport of T1 due to lymph flow from interstitial space around peripheral compartment into plasma compartment
T1_itst_peri => T1_plasma : v_T1_itst_peri_plasma_lymph                % (nmol/day)
	vf = (kL*T1_itst_peri)*volume_itst_peri
    
% Clearance of T1 from plasma
T1_plasma =>  : v_T1_plasma_cl                                        % (nmol/day)
	vf = (kCL_T1_plasma*T1_plasma)*volume_plasma

% Clearance of T1 from interstitial tissue around disease compartment
T1_itst_tissue => : v_T1_itst_tissue_cl                               % (nmol/day)
    vf = (kCL_T1_itst_tissue*T1_itst_tissue)*volume_itst_tissue

% Clearance of T1 from interstitial tissue around peripheral compartment
T1_itst_peri => : v_T1_itst_peri_cl                                   % (nmol/day)
    vf = (kCL_T1_itst_peri*T1_itst_peri)*volume_itst_peri

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% D / Target T1 binding reactions (single binding)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Binding of D with T1 in plasma compartment
D_plasma+T1_plasma <=> D_T1_plasma : v_D_T1_plasma_reac          % (nmol/day)
	vf = (kon_D_T1*D_plasma*T1_plasma)*volume_plasma
    vr = (kon_D_T1*KD_D_T1*D_T1_plasma)*volume_plasma

% Binding of D with T1 in interstitial tissue around disease tissue
D_itst_tissue+T1_itst_tissue <=> D_T1_itst_tissue : v_D_T1_itst_tissue_reac % (nmol/day)
	vf = (kon_D_T1*D_itst_tissue*T1_itst_tissue)*volume_itst_tissue
    vr = (kon_D_T1*KD_D_T1*D_T1_itst_tissue)*volume_itst_tissue

% Binding of D with T1 in interstitial tissue around peripheral tissue
D_itst_peri+T1_itst_peri <=> D_T1_itst_peri : v_D_T1_itst_peri_reac % (nmol/day)
	vf = (kon_D_T1*D_itst_peri*T1_itst_peri)*volume_itst_peri
    vr = (kon_D_T1*KD_D_T1*D_T1_itst_peri)*volume_itst_peri

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% D_T1 / Target T1 binding reactions (double binding (bivalent drug))
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
       
% Binding of second T1 to D_T1 in plasma compartment    
D_T1_plasma+T1_plasma <=> T1_D_T1_plasma : v_T1_D_T1_plasma_reac
	vf = (kon_T1_D_T1*D_T1_plasma*T1_plasma)*volume_plasma
    vr = (kon_T1_D_T1*KD_T1_D_T1*T1_D_T1_plasma)*volume_plasma

% Binding of second T1 to D_T1 in interstitial space around disease tissue
D_T1_itst_tissue+T1_itst_tissue <=> T1_D_T1_itst_tissue : v_T1_D_T1_itst_tissue_reac % (nmol/day)
	vf = (kon_T1_D_T1*D_T1_itst_tissue*T1_itst_tissue)*volume_itst_tissue
    vr = (kon_T1_D_T1*KD_T1_D_T1*T1_D_T1_itst_tissue)*volume_itst_tissue

% Binding of second T1 to D_T1 in interstitial space around peripheral tissue
D_T1_itst_peri+T1_itst_peri <=> T1_D_T1_itst_peri : v_T1_D_T1_itst_peri_reac % (nmol/day)
	vf = (kon_T1_D_T1*D_T1_itst_peri*T1_itst_peri)*volume_itst_peri
    vr = (kon_T1_D_T1*KD_T1_D_T1*T1_D_T1_itst_peri)*volume_itst_peri

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% D/T1 complex transport and clearance reactions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Passive exchange of D/T1 complex between plasma and interstitial space around disease compartment (diffusion type of mechanism)
D_T1_plasma <=> D_T1_itst_tissue : v_D_T1_plasma_itst_tissue_ex % (nmol/day)
	vf = kPS_D_T1_plasma_itst_tissue*D_T1_plasma
    vr = kPS_D_T1_plasma_itst_tissue*D_T1_itst_tissue
 
% Passive exchange of D/T1 complex between plasma and interstitial space around peripheral compartment (diffusion type of mechanism)
D_T1_plasma <=> D_T1_itst_peri : v_D_T1_plasma_itst_peripheral_ex % (nmol/day)
	vf = kPS_D_T1_plasma_itst_peri*D_T1_plasma
    vr = kPS_D_T1_plasma_itst_peri*D_T1_itst_peri
    
% Active transport of D/T1 complex due to lymph flow from interstitial space around disease tissue into plasma compartment
D_T1_itst_tissue => D_T1_plasma : v_D_T1_itst_tissue_plasma_lymph      % (nmol/day) 
	vf = (kL*D_T1_itst_tissue)*volume_itst_tissue

% Active transport of D/T1 complex due to lymph flow from interstitial space around peripheral compartment into plasma compartment
D_T1_itst_peri => D_T1_plasma : v_D_T1_itst_peri_plasma_lymph          % (nmol/day)
    vf = (kL*D_T1_itst_peri)*volume_itst_peri 
   
% Clearance of D/T1 complex from plasma
D_T1_plasma =>  : v_D_T1_plasma_cl                                    % (nmol/day)
	vf = (kCL_D_T1_plasma*D_T1_plasma)*volume_plasma

% Clearance of D/T1 complex from interstitial tissue around disease compartment
D_T1_itst_tissue => : v_D_T1_itst_tissue_cl                           % (nmol/day)
    vf = (kCL_D_T1_itst_tissue*D_T1_itst_tissue)*volume_itst_tissue

% Clearance of D/T1 complex from interstitial tissue around peripheral compartment
D_T1_itst_peri => : v_D_T1_itst_peri_cl                               % (nmol/day)
    vf = (kCL_D_T1_itst_peri*D_T1_itst_peri)*volume_itst_peri
   
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% T1/D/T1 complex transport and clearance reactions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
% Passive exchange of T1/D/T1 complex between plasma and interstitial space around disease compartment (diffusion type of mechanism)
T1_D_T1_plasma <=> T1_D_T1_itst_tissue : v_T1_D_T1_plasma_itst_tissue_ex % (nmol/day)
	vf = kPS_T1_D_T1_plasma_itst_tissue*T1_D_T1_plasma
    vr = kPS_T1_D_T1_plasma_itst_tissue*T1_D_T1_itst_tissue
 
% Passive exchange of T1/D/T1 complex between plasma and interstitial space around peripheral compartment (diffusion type of mechanism)
T1_D_T1_plasma <=> T1_D_T1_itst_peri : v_T1_D_T1_plasma_itst_peripheral_ex % (nmol/day)
	vf = kPS_T1_D_T1_plasma_itst_peri*T1_D_T1_plasma
    vr = kPS_T1_D_T1_plasma_itst_peri*T1_D_T1_itst_peri
    
% Active transport of T1/D/T1 complex due to lymph flow from interstitial space around disease tissue into plasma compartment
T1_D_T1_itst_tissue => T1_D_T1_plasma : v_T1_D_T1_itst_tissue_plasma_lymph      % (nmol/day) 
	vf = (kL*T1_D_T1_itst_tissue)*volume_itst_tissue

% Active transport of T1/D/T1 complex due to lymph flow from interstitial space around peripheral compartment into plasma compartment
T1_D_T1_itst_peri => T1_D_T1_plasma : v_T1_D_T1_itst_peri_plasma_lymph          % (nmol/day)
    vf = (kL*T1_D_T1_itst_peri)*volume_itst_peri 

% Clearance of T1/D/T1 complex from plasma
T1_D_T1_plasma => : v_T1_D_T1_plasma_cl                                    % (nmol/day)
	vf = (kCL_T1_D_T1_plasma*T1_D_T1_plasma)*volume_plasma

% Clearance of T1/D/T1 complex from interstitial tissue around disease compartment
T1_D_T1_itst_tissue => : v_T1_D_T1_itst_tissue_cl                           % (nmol/day)
    vf = (kCL_T1_D_T1_itst_tissue*T1_D_T1_itst_tissue)*volume_itst_tissue

% Clearance of T1/D/T1 complex from interstitial tissue around peripheral compartment
T1_D_T1_itst_peri => : v_T1_D_T1_itst_peri_cl                               % (nmol/day)
    vf = (kCL_T1_D_T1_itst_peri*T1_D_T1_itst_peri)*volume_itst_peri
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Target T1 dimerization reactions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    

% Dimerization of T1 in plasma
T1_plasma + T1_plasma <=> T1_T1_plasma : v_T1_T1_plasma_reac                       % (nmol/day)
	vf = (kon_T1_T1*T1_plasma*T1_plasma)*volume_plasma
    vr = (kon_T1_T1*KD_T1_T1*T1_T1_plasma)*volume_plasma

% Dimerization of T1 in interstitial tissue around disease compartment
T1_itst_tissue+T1_itst_tissue <=> T1_T1_itst_tissue : v_T1_T1_itst_tissue_reac     % (nmol/day)
	vf = (kon_T1_T1*T1_itst_tissue*T1_itst_tissue)*volume_itst_tissue
    vr = (kon_T1_T1*KD_T1_T1*T1_T1_itst_tissue)*volume_itst_tissue

% Dimerization of T1 in interstitial tissue around peripheral compartment
T1_itst_peri+T1_itst_peri <=> T1_T1_itst_peri : v_T1_T1_itst_peri_reac             % (nmol/day)
	vf = (kon_T1_T1*T1_itst_peri*T1_itst_peri)*volume_itst_peri
    vr = (kon_T1_T1*KD_T1_T1*T1_T1_itst_peri)*volume_itst_peri

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Dimerized T1 transport and clearance reactions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%      

% Passive exchange of T1 dimer between plasma and interstitial space around disease compartment (diffusion type of mechanism)
T1_T1_plasma <=> T1_T1_itst_tissue : v_T1_T1_plasma_itst_tissue_ex % (nmol/day)
	vf = kPS_T1_T1_plasma_itst_tissue*T1_T1_plasma
    vr = kPS_T1_T1_plasma_itst_tissue*T1_T1_itst_tissue
 
% Passive exchange of T1 dimer complex between plasma and interstitial space around peripheral compartment (diffusion type of mechanism)
T1_T1_plasma <=> T1_T1_itst_peri : v_T1_T1_plasma_itst_peripheral_ex % (nmol/day)
	vf = kPS_T1_T1_plasma_itst_peri*T1_T1_plasma
    vr = kPS_T1_T1_plasma_itst_peri*T1_T1_itst_peri
    
% Active transport of T1 dimer complex due to lymph flow from interstitial space around disease tissue into plasma compartment
T1_T1_itst_tissue => T1_T1_plasma : v_T1_T1_itst_tissue_plasma_lymph      % (nmol/day) 
	vf = (kL*T1_T1_itst_tissue)*volume_itst_tissue

% Active transport of T1 dimer complex due to lymph flow from interstitial space around peripheral compartment into plasma compartment
T1_T1_itst_peri => T1_T1_plasma : v_T1_T1_itst_peri_plasma_lymph          % (nmol/day)
    vf = (kL*T1_T1_itst_peri)*volume_itst_peri 

% Clearance of T1 dimer complex from plasma
T1_T1_plasma =>  : v_T1_T1_plasma_cl                                    % (nmol/day)
	vf = (kCL_T1_T1_plasma*T1_T1_plasma)*volume_plasma

% Clearance of T1 dimer complex from interstitial tissue around disease compartment
T1_T1_itst_tissue => : v_T1_T1_itst_tissue_cl                           % (nmol/day)
    vf = (kCL_T1_T1_itst_tissue*T1_T1_itst_tissue)*volume_itst_tissue

% Clearance of T1 dimer complex from interstitial tissue around peripheral compartment
T1_T1_itst_peri => : v_T1_T1_itst_peri_cl                               % (nmol/day)
    vf = (kCL_T1_T1_itst_peri*T1_T1_itst_peri)*volume_itst_peri
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Soluble protein binding reactions to target T1, exchange and transport reactions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
% ASSUMPTION: the soluble binding protein BP and all complexes containing it are 
%             not secreted since BP is not produced in the model this is a 
%             necessary assumption to achieve homeostasis 
% ASSUMPTION: its ok to neglect exchange and transport of BP/T1 complexes. 
%             After all: to few data available to really estimate all parameters
%             (unidentifiable!)

% Passive exchange of BP between plasma and interstitial space around disease compartment (diffusion type of mechanism)
BP_plasma <=> BP_itst_tissue : v_BP_plasma_itst_tissue_ex        % (nmol/day) 
	vf = kPS_BP_plasma_itst_tissue*BP_plasma
    vr = kPS_BP_plasma_itst_tissue*BP_itst_tissue

% Passive exchange of BP between plasma and interstitial space around peripheral compartment (diffusion type of mechanism)
BP_plasma <=> BP_itst_peri : v_BP_plasma_itst_peripheral_ex      % (nmol/day) 
	vf = kPS_BP_plasma_itst_peri*BP_plasma
    vr = kPS_BP_plasma_itst_peri*BP_itst_peri

% Active transport of BP due to lymph flow from interstitial space around disease tissue into plasma compartment
BP_itst_tissue => BP_plasma : v_BP_itst_tissue_plasma_lymph      % (nmol/day) 
	vf = (kL*BP_itst_tissue)*volume_itst_tissue

% Active transport of BP due to lymph flow from interstitial space around peripheral compartment into plasma compartment
BP_itst_peri => BP_plasma : v_BP_itst_peri_plasma_lymph          % (nmol/day)
    vf = (kL*BP_itst_peri)*volume_itst_peri 

% Binding of soluble protein to T1 in plasma
T1_plasma + BP_plasma <=> T1_BP_plasma : v_T1_BP_plasma_reac                       % (nmol/day)
	vf = (kon_T1_BP*T1_plasma*BP_plasma)*volume_plasma
    vr = (kon_T1_BP*KD_T1_BP*T1_BP_plasma)*volume_plasma

% Binding of soluble protein to T1 in interstitial tissue around disease compartment
T1_itst_tissue+BP_itst_tissue <=> T1_BP_itst_tissue : v_T1_BP_itst_tissue_reac     % (nmol/day)
	vf = (kon_T1_BP*T1_itst_tissue*BP_itst_tissue)*volume_itst_tissue
    vr = (kon_T1_BP*KD_T1_BP*T1_BP_itst_tissue)*volume_itst_tissue

% Binding of soluble protein to T1 in interstitial tissue around peripheral compartment
T1_itst_peri+BP_itst_peri <=> T1_BP_itst_peri : v_T1_BP_itst_peri_reac             % (nmol/day)
	vf = (kon_T1_BP*T1_itst_peri*BP_itst_peri)*volume_itst_peri
    vr = (kon_T1_BP*KD_T1_BP*T1_BP_itst_peri)*volume_itst_peri

********** MODEL FUNCTIONS
********** MODEL EVENTS
********** MODEL MATLAB FUNCTIONS

