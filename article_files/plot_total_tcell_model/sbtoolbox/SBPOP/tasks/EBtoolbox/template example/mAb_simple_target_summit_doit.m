%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This is an example script that performs standard tasks to be done for 
% target summits. The work is based in the "standard" 3 compartment model,
% provided in the SBPOP framework and on the target summit work for the
% TARGETX target.
% The currently implemented model only features a single target (which is a
% ligand) and no target binding to a receptor. Depending on the project to
% be performed the model would need to be changed (receptor as target,
% several targets, etc.).
%
% IMPORTANT:
% ==========
% This script serves as an EXAMPLE for how to use some of the implemented
% functions. It also documents somehow the thoughts behind the work. It
% might serve in parts as a template for doing own work. But you might
% still need to write your own scripts.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clc; clear all; close all; format short g;

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Obtain a copy of the "standard" model, import it, and add initial parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
SBPOPcreateDRUGligandTargetModel('TARGETX_mAb_model');
edit('TARGETX_mAb_model.txtbc');
model = SBmodel('TARGETX_mAb_model.txtbc');
% Looking at this "TARGETX_mAb_model.txtbc" will be the first step. We need to
% modify the model to fit the requirements for the task on the TARGETX. You
% can either a) set these values in the text model, b) update the imported
% model with the new values of parameters, or c) obtain the model parameter
% vector and use the function "updateparamvecSBPD" to keep parametervector
% and model separated. In the following we chose to update the parameters
% in the model using the SBparameters function:
%
%% 1) The volume of the disease tissue and the volume of the corresponding
% interstitial space need to be set. Human values for these can be obtained
% using the function "SBPOPtissuedata".
SBPOPtissuedata
%% 1a) For the TARGETX project the tissue is the intestine (small+large) 
model = SBparameters(model,'volume_tissue',0.6960+0.4043);      % set the volume of the disease tissue 
model = SBparameters(model,'volume_itst_tissue',0.1558+0.0907); % set the volume of the corresponding interstitial space 
%% 2) The molecular weight of the TARGETX ligand needs to be set (it is 11kDa)
model = SBparameters(model,'molecularweight_T1',11000);
%% 3) Finally, we know (from the project team) that TARGETX can
% dimerize with a KD of 2uM. We have no idea about the kon but we assume
% 10 and perform afterwards a sensitivity analysis on this value.
model = SBparameters(model,'KD_T1_T1',2000); % Set the KD for target dimerization
model = SBparameters(model,'kon_T1_T1',10); % Set the guessed kon for target dimerization

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% We have now a model, parameterized for the TARGETX work. The next step
% is to fit the mAb kinetics to implement desired halflives and mAb
% distribution in the tissue. Also the target kinetics to fit the target
% halflives and finally the synthesis rates of the target to fit the
% observed target concentrations in the different compartments.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Define the desired mAb and target properties
% If unknown for the mAb typically 2days/20days for alpha/beta halflife is assumed
T05alphaBetaDrug = [2 20];              % (days) alpha/beta halflife of drug
% If unknown the assumption is that in steady-state (infinite infusion
% time) the mAb concentration in the tissue will be 20% of the
% concentration in the plasma
SSratio_percent_drug = 20;              % (percent) ratio between ss drug concentration in tissue and plasma
% If unknown for a small soluble protein as target typically 2 hours is
% assumed as beta halflife. This will be subjected to a sensitivity
% analysis later on to see the effect of other halflives in the required
% dosing amount and schedule. The alpha halflife can often not be chosen
% freely but is dependent on the beta halflife. We aim at 5min to 10min.
T05alphaBetaTarget = [5 120]*(1/60/24); % (days) alpha/beta halflife of target (5/120 minutes)
% The steady-state concentration of target in disease tissue was unknown in
% this project. Only the ss concentration in plasma was known. Here we use
% the worst-case concentration, corresponding to Asian patients. 
Tss_plasma = 0.11028;   % (nmol) Steady-state conc in plasma

%% To speed up our optimizations we generate a MEX simulation function
SBPDmakeMEXmodel(model,'MEX_model_4_initial_fits');
% get the models parametervector
[parameternames_model,paramvalues_model0] = SBparameters('MEX_model_4_initial_fits');

%% Fit the drug kinetics to achieve desired alpha and beta halflife and tissue distribution for the mAb
% The fitted parameters are the mAb clearance, mAb surface exchange coefficient 
% and lymph flow rate constant. Since the mAb has a molecular weight of 150
% kDa it will not be cleared by renal clearance. Here the assumption is
% that it is cleared in all 3 compartments with the same clearance rate
% constant. Therefor the clearance in tissue and peripheral tissue has been
% assigned the value of the clearance in plasma in the model. So changing
% plasma clearance changes the drug clearance in the other compartments to
% the same value.
pnamesopt = {'kCL_D_plasma', 'kPS_D_total', 'kL'}; 
pvalues0 = [0.1 .5 0.1]; % initial guesses
referenceValues = [T05alphaBetaDrug SSratio_percent_drug]; % desired properties
weights = [1 1 1];
popt = SBPOPoptimizeDRUGkinetics('MEX_model_4_initial_fits',pnamesopt,pvalues0,referenceValues,paramvalues_model0,weights); % do the optimization
% finally add the optimized parameters the parameter vector
paramvalues_model1 = updateparamvecSBPD(parameternames_model,paramvalues_model0,pnamesopt,popt);
% Please check the output in the matlab command window. You can see that
% the fit was successful and that the objectives have been achieved.

%% Fit the target kinetics to achieve desired alpha and beta halflife
% The target molecule TARGETX has a molecular weight of ~11kDa. This means
% it is far below the threshold of ~70kDa from which on the molecule is not
% anymore cleared by the kidneys. This means, we can assume that the
% renal clearance in the plasma compartment considerable outweighs the
% clearance of TARGETX in the tissue compartments. Therefor we assume the
% clearance of TARGETX in tissue and peripheral tissue to be zero and fit
% only the clearance in the plasma. Additionally, we fit the surface
% exchange coefficient for the target.
pnamesopt   = {'kCL_T1_plasma', 'kPS_T1_total'};
pvalues0 = [10 200]; % initial guesses
referenceValues = T05alphaBetaTarget;
weights = [1 10]; % the weight for the beta halflife is increased on purpose to get a better fit for it (alpha less important than beta)
popt = SBPOPoptimizeLigandTARGETkinetics('MEX_model_4_initial_fits',pnamesopt,pvalues0,referenceValues,paramvalues_model1,'T1_plasma',weights);
% finally add the optimized parameters the parameter vector
paramvalues_model2 = updateparamvecSBPD(parameternames_model,paramvalues_model1,pnamesopt,popt);
% Please check the output in the matlab command window. You can see that
% the final objectiove function (achieved_cost) is relatively high. But you can also
% see that the desired beta halflife (0.083) has been fitted very well and thus 
% the result is acceptable. For small hallives we have encountered that
% alpha and beta can not be chosen completely independent, and it is better
% to get a correct beta than only a correct alpha halflife.

%% Fit the target synthesis rate to obtain desired target steady-state concentrations
% From literature we know that the concentration of TARGETX in sick patients
% can be as high as 0.11028 nM in the plasma. We have no idea about the
% concentration in the disease tissue. So here we do only optimize the
% synthesis rates in order to achieve the desired steady-state plasma
% concentration of TARGETX. An additional assumption is that the synthesis
% rate in plasma and the peripheral tissue is negligible in comparison to
% the disease tissue (only estimate synthesis in the disease tissue).
pnamesopt   = {'ksyn_T1_itst_tissue'};
pvalues0 = [1]; % initial guesses
referenceValues = [Tss_plasma 1 1]; % neglected values set to "1" ... "0" is not allowed but value does not matter anyway
weights = [1 0 0]; % neglect tissue and peripheral tissue concentrations (we dont know anything about them anyway)
popt = SBPOPoptimizeLigandTARGETsynthesis('MEX_model_4_initial_fits',pnamesopt,pvalues0,referenceValues,paramvalues_model2,{'T1_plasma','T1_itst_tissue','T1_itst_peri'},weights);
% finally add the optimized parameters the parameter vector
paramvalues_model3 = updateparamvecSBPD(parameternames_model,paramvalues_model2,pnamesopt,popt);
% Please check the output in the matlab command window. You can see that
% the fit was successful and that the objectives have been achieved.

%% Get the steady-state of the now parameterized mAb and target model
simdata = SBPDsimulate('MEX_model_4_initial_fits',[0:1:20],[],parameternames_model,paramvalues_model3);
ss = simdata.statevalues(end,:);

%% Generate a new SBmodel which contains the optimized parameters and the
% determined steady-state as initial conditions.
model_fitted = SBparameters(model,parameternames_model,paramvalues_model3);
model_fitted = SBinitialconditions(model_fitted,ss);

%% At this point we can remove the MEX model since it has served its purpose
% and we do not want to keep it longer (reducing confusion). The
% reason for having it in the first place is the fact that MEX simulation
% functions are much faster to simulate (crucial for optimization). The
% compilation of such a model takes some time and using the mexmodel made
% it possible to only compile once instead of three times.
clear mex; delete('MEX_model_4_initial_fits.mexw32');

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% The model has now been parameterized in terms of the mAb and target
% kinetics, the mAb steady-state distribution and the target steady-state
% concentrations. The next step will be to apply a dosing schedule. We will
% consider an IV infusion every 14 days (20 times) with a dose of 10mg/kg
% each time. 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Please have a look at the base model (TARGETX_mAb_model.txtbc)
edit TARGETX_mAb_model.txtbc
% If you scroll down to the first defined reaction you can see that the
% identifier "INPUT1" has been used to define that the drug is administered
% to the plasma compartment. How this does happen is defined in a separate
% dosing scheme. Here in this example we use the dosing scheme
% "IV_dosing_14.dos".

%% Look at the dosing scheme
edit IV_dosing_14.dos

%% We can import this dosing scheme to MATLAB using:
dosing = SBPOPdosing('IV_dosing_14.dos')

%% Since we chose infusion into the plasma compartment we might want to
% update the bioavailability parameter in the model
model_IV = SBparameters(model_fitted,'F',1); 

%% In order to be able to simulate a dosing scheme on a model we need to
% prepare the model for it:
moddos = mergemoddosSBPOP(model_IV,dosing);
% The "moddos" model is now an SBmodel which has been prepared in order to
% be able to simulate dosing events.

%% Simulate the IV 14-day dosing schedule
SBPOPsimdosing(moddos,dosing,[0:0.1:360])         
% Just browse through the plots. You will probably see that the drug does
% not bind the target. This is due to the fact that the kon_D_T1 parameter
% is still 0. For the sake of this examlpe we are going to set:
% kon_D_T1=10 (1/(nM*day)) and KD_D_T1=0.05nM (in the next section)

%% Set drug/target binding parameters and simulate the dosing scheme
moddos_kon_KD_modified = SBparameters(moddos,{'kon_D_T1','KD_D_T1'},[10 0.05])
SBPOPsimdosing(moddos_kon_KD_modified,dosing,[0:0.1:360])
% Have a look at the state "T1_plasma". this is the free target
% concentration in the plasma in nM units. In this work here we assume that
% the therapy goal is achieved if the concentration of the free TARGETX in
% plasma drops below a concentration of 0.013nM. This means that the
% 10mg/kg treatment here, clearly is achieving the therapy goal.

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% We fitted the model and tested it. And it seemed to work fine. Now
% we want to determine dose response curves for different values of
% mAb/TARGETX affinity. Again we want to use the 14 day IV infusion dosing
% schedule. Again we will use a kon value of 10 (1/(nM*day))
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Add the kon value to the model_IV
model_IV = SBparameters(model_IV,'kon_D_T1',10);
% Load again the dosing schedule
dosing = SBPOPdosing('IV_dosing_14.dos');
% Define the doses to be considered for the dose response calculations
dose_INPUT1 = logspace(-3,2,10); 
% Define the KD values to consider
KD_D_T1 = [0.01 0.02 0.05 0.1 0.2 0.5 1 2 5 10];
% Define the timevector to use for the simulations
timevector = [0:0.01:360];

%% The following section performs the dose response calculations based on
% the definitions above
%
% prepare model for dosing application and generate mex model (here at this
% stage of the example we creata a hidden temporary mex file).
moddos = mergemoddosSBPOP(model_IV,dosing);
[MEXmodel, MEXmodelfullpath] = makeTempMEXmodelSBPD(moddos);
% Define variable to store the data that are needed to determine the
% efficacy of the treatment. "max_free_T1_plasma" will be a matrix that
% contains for each KD and dose combination the maximum value of free
% TARGETX in the plasma when the treatment behavior is in steady-state.
max_free_T1_plasma = [];
for k1 =1:length(dose_INPUT1), k1
    newDosing = SBPOPdoseupdatevalue(dosing,'INPUT1',dose_INPUT1(k1));
    for k2 = 1:length(KD_D_T1)
        % Simulate the model with the parameter changes 
        simresult = SBPOPsimdosing(MEXmodel,newDosing,timevector,[],{'KD_D_T1'},KD_D_T1(k2));
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Get the free target in plasma 
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        T1_plasma = simresult.statevalues(:,getstateindicesSBPD(MEXmodel,'T1_plasma'));
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Determine the maximum value of relative free target during treatment
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        max_free_T1_plasma_k1_k2 = getlocalmaxSB(T1_plasma);
        % Take second last value (seen from plot some lines above)
        max_free_T1_plasma(k1,k2) = max_free_T1_plasma_k1_k2(end-2);
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    end
end

%% Now plot the dose response curves in the plasma
% The dashed horizontal line indicates the therapy goal. We can see that 
% a dose of 10mg/kg together with a KD of 0.1nM allow to achieve the
% therapy goal.
SBPOPplotDoseEffectCurve(max_free_T1_plasma, dose_INPUT1, KD_D_T1, 'Free target [nM]', 'Target in plasma (KD_D_T1 as parameter)');
xlim = get(gca,'Xlim');
ylim = get(gca,'Ylim'); 
plot(xlim,[0.013 0.013],'k--');

%% The next plot allows to plot the different affinities over the different
% doses, whereby each line corresponds to the same achieved therapy effect.
% The therapy goal is highlighted red. This plot shows that (at least) in
% the plasma even a smaller KD than 0.01nM would be better. 
effectLevels = [0.07352 0.02757 0.0068925];
targetEffectLevel = 0.013785;
SBPOPplotDoseParamCurve(max_free_T1_plasma, dose_INPUT1, KD_D_T1, 'Target in plasma (effect levels as parameter)',effectLevels,targetEffectLevel);
% Sidenote: For the real TARGETX project we also considered tissue
% concentrations and it quickly became obvious that KD~0.1 nM was a
% reasonable target affinity.

%% Final words: This was a very simple example of how the new SBPOP
% functions can be used in a target summit work. You will still have to
% code a little bit yourself (and model to0) but at least this here should
% serve as an explanation on how you can use the different functions and
% speed up the work. 