%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Just an example / template script for graphical exploration of PD
% Note, this code will not run as is - it is more a reminder of things to do
% and not to have to type everything from scratch.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Setup
clc; clear all; close all; restoredefaultpath();
path2SBPOP          = 'SBPOP PACKAGE';
oldpath             = pwd; cd(path2SBPOP);      installSBPOPpackageInitial();           cd(oldpath);

%% Load data (generalized dataset format)
data = SBPOPloadCSVdataset('../Data/data.csv');

%% Define covariates to be considered
covNames = {'WT0','XYZ0'};
catNames = {'STUDY' 'TRT'};

%% Continuous PD readouts
NAMES               = {'XYZ'};
BASELINENAMES       = {'XYZ0'};
COVARIATES          = [covNames catNames];

TIMEPOINT_CHANGE    = 100;
PD_IMPROVEMENT      = [-20 -40];  % in percent

options             = [];
options.filename    = '../Output/Graphical_Exploration/01_PD_data_continuous';
options.fontsize    = 14;

SBPOPgraphicalExplorationContinuousPD(data,NAMES,BASELINENAMES,COVARIATES,TIMEPOINT_CHANGE,PD_IMPROVEMENT,options)

%% Some custom data cleaning ...
dataClean = data;

%% Continuous PD readouts - cleaned
NAMES               = {'XYZ'};
BASELINENAMES       = {'XYZ0'};
COVARIATES          = [covNames catNames];

TIMEPOINT_CHANGE    = 100;
PD_IMPROVEMENT      = [-20 -40];  % in percent

options             = [];
options.filename    = '../Output/Graphical_Exploration/02_PD_data_continuous_cleaned';
options.fontsize    = 14;

SBPOPgraphicalExplorationContinuousPD(dataClean,NAMES,BASELINENAMES,COVARIATES,TIMEPOINT_CHANGE,PD_IMPROVEMENT,options)

%% Summary statistics
SBPOPexploreSummaryStats(dataX,covNames,catNames,'../Output/Graphical_Exploration/05_Summary_Statistics')

%% Covariate exploration
SBPOPexploreCovariateCorrelations(dataX,covNames,catNames,'../Output/Graphical_Exploration/06_Covariate_Exploration')

%% Export cleaned and combined dataset for further PKPD modeling
SBPOPexportCSVdataset(dataX,'../Data/data_cleaned.csv')

