function [ parameters, parameters_TRANS ] = SBPOPgetParametersMedian( projectfolder )
% SBPOPgetParametersMedian: Reads the parameter estimates from the bootstraps 
% from a PDmedian project folder. Returned are the transformed and the 
% untransformed parameters as datasets.
%
% Only the estimated parameters are returned. Fixed ones are not considered.
%
% USAGE:
% ======
% [ parameters, parameters_TRANS ] = SBPOPgetParametersMedian( projectfolder )
%
% projectfolder:        string with the path where the model results are stored 
%                       (e.g.: logfiles folder, run_results.data, ...)

% Information:
% ============
% Copyright (c) 2012 Novartis Pharma AG
% 
% This program is Free Open Source Software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program. If not, see <http://www.gnu.org/licenses/>.

%% Read tun results
run_results = load([projectfolder '/run_results.data'],'-mat'); run_results = run_results.run_results;

%% Read in parameters from bootstraps and transform them
parameters_bootstrap_all = [];
parameters_bootstrap_all_TRANS = [];
for k=1:length(run_results.run_information),
    parameters                          = run_results.run_information.OUTPUTopt{k}.X;
    parameters_bootstrap_all            = [parameters_bootstrap_all; parameters];
    parameters_trans                    = transformXparametersSBPOP(parameters,run_results.parameterInformation.trans(find(run_results.parameterInformation.estimate)));
    parameters_bootstrap_all_TRANS      = [parameters_bootstrap_all_TRANS; parameters_trans];
end

parameters = mat2datasetSB(parameters_bootstrap_all,run_results.parameterInformation.names(find(run_results.parameterInformation.estimate)));
parameters_TRANS = mat2datasetSB(parameters_bootstrap_all_TRANS,run_results.parameterInformation.names(find(run_results.parameterInformation.estimate)));



