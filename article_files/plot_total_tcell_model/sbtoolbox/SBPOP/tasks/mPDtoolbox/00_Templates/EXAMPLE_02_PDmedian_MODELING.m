%% SCRIPT 02: for median modeling (no data included ... just code)
%
% The dataset for the optimization as been prepared in Graphical_Exploration_Example.m
% and is stored in ../Data/data_median_optimization.csv
%
% Here in this example we will do it for a single model only ...
%
% Assumption: A PK model has been fitted previously and is available for
% sampling of PK parameters. In case the PK model needs covariates, these
% are taken from the model ... the names in the optimization dataset need
% to be the same! FIT_PK could also be omitted if a KPD, etc. model is
% estimated. 

%% ===Setup
clc; clear all; close all; restoredefaultpath();
path2SBPOP          = '../../../SBPOP_REV_1289';
oldpath             = pwd; cd(path2SBPOP);      installSBPOPpackageInitial();           cd(oldpath);

%% ===Load data 
data                = SBPOPloadCSVdataset('../Data/data_median_optimization.csv');

%% ===Create dosing schemes for treatment arms that are in the dataset
% The median modeling assumes nominal dosing.
% All dosing schemes need to be based on the same structure!
% Dosings do not need to be in the dataset ... that will be matched later.

% Check the TRT groups that are in the dataaset
% ---------------------------------------------
unique(data.TRT)

% Define the dosings for the TRT groups
% -------------------------------------
% PLACEBO  0
dosing_0            = SBPOPcreateDOSING({'INFUSION','BOLUS'},   {0,0},              {0,0},                      {0.0833,[]});
dosing_22011        = SBPOPcreateDOSING({'INFUSION','BOLUS'},   {[0.1 0.1]*1000,0}, {[0 21],0},                 {0.0833,[]});
dosing_22012        = SBPOPcreateDOSING({'INFUSION','BOLUS'},   {[1 1]*1000,0},     {[0 21],0},                 {0.0833,[]});
dosing_22013        = SBPOPcreateDOSING({'INFUSION','BOLUS'},   {[10 10]*1000,0},   {[0 21],0},                 {0.0833,[]});
dosing_623011       = SBPOPcreateDOSING({'INFUSION','BOLUS'},   {0,75*1000},        {0,[0 1 2 3 4 8 12 16]*7},  {0.0833,[]});
dosing_623012       = SBPOPcreateDOSING({'INFUSION','BOLUS'},   {0,150*1000},       {0,[0 1 2 3 4 8 12 16]*7},  {0.0833,[]});

% Create a dosingInformation structure
% ------------------------------------
dosingInformation               = [];                         
dosingInformation.TRT           = [0            22011               22012           22013           623011          623012       ];
dosingInformation.dosings       = {dosing_0,    dosing_22011,       dosing_22012,   dosing_22013,   dosing_623011,  dosing_623012};
dosingInformation.name          = {'Placebo',   '0.1mg/kg',         '1mg/kg',       '10mg/kg',      '75mg',         '150mg'};
% Weight based contains 2 rows ... one for the first dosing (IV) and one for the second (SC)!
% There could be more columns, depending on the number of inputs in the dosing schemes
dosingInformation.weightBased   = [     0               1               1                   1           0                 0      
                                        0               0               0                   0           0                 0      ];

%% ===Define optimization options

options                      = [];

options.maxtime              = Inf;
options.maxiter              = Inf;
options.maxfunevals          = 400;
options.OPTIMIZER            = 'simplexSB';
options.N_BOOTSTRAP          = 120;
options.N_PROCESSORS         = 8; 

%% ===MODEL: MODEL_01_MNO20_40
% Data to be used for fitting defined by what is present in the dataset.
% Dosings need to be defined for each TRT but can also be defined for non present TRTs in the data
dataUse = data;
dataUse(dataUse.TRT==22011,:) = [];
dataUse(dataUse.TRT==22012,:) = [];
dataUse(dataUse.TRT==22013,:) = [];

% Load the model to fit
modelInformation                = [];
modelInformation.model          = SBmodel('../Models/PKPD_model_MNO.txt');
modelInformation.modelOutput    = {'RR_median_MNO20','RR_median_MNO40'};
modelInformation.FIT_PK         = '../Models/PK_MODEL_FIT'; % could be undefined and then additional parameters provided in parameterInformation

% Define parameters to estimate (names taken from the selected model)
parameterInformation            = [];
parameterInformation.names      = {'kout'    'EC50'    'EMAX'    'h'    'PLACEBO'};
parameterInformation.values0    = [ 0.0612    20000     0.316     2.5    0.10    ];
parameterInformation.estimate   = [  1        1         1         0      1       ];

% Define information for data - allow for stratification
dataInformation                 = [];
dataInformation.data            = dataUse;
dataInformation.names           = {'MNO20','MNO40'}; % Matches the modelOutput in the same order
dataInformation.type            = 'categorical'; % Does RR fitting. 'continuous' does median fitting
dataInformation.stratify        = {'','AGE0'}; % Stratification by covariates or none ('')

% Define project folder
projectfolder                   = '../Models/MEDIAN_MODELING/MODEL_01_MNO20_40';

SBPOPrunMEDIANoptimization(projectfolder,modelInformation,dataInformation,parameterInformation,dosingInformation,options);


%% ===MODEL: MODEL_02_XYZVAS
dataUse = data;

% Load the model to fit
modelInformation                = [];
modelInformation.model          = SBmodel('../Models/PKPD_model_XYZVAS.txt');
modelInformation.modelOutput    = {'XYZVAS'};
modelInformation.FIT_PK         = '../Models/PK_MODEL_FIT'; % could be undefined and then additional parameters provided in parameterInformation

% Define parameters to estimate (names taken from the selected model)
parameterInformation            = [];
parameterInformation.names      = {'kout'    'EC50'    'EMAX'    'h'    'PLACEBO' 'BASELINE'};
parameterInformation.values0    = [ 0.0612    15000     0.5      2.5    0.10         6.5];
parameterInformation.estimate   = [  1        1         1         0      1            1];

% Define information for data - allow for stratification
dataInformation                 = [];
dataInformation.data            = data;
dataInformation.names           = {'XYZVAS'};   % Matches the modelOutput in the same order
dataInformation.type            = 'continuous'; % Does median fitting
dataInformation.stratify        = {'','AGE0'};  % This will run several different models ... stratified by covariates
% Define project folder
projectfolder                   = '../Models/MEDIAN_MODELING/MODEL_02_XYZVAS';

SBPOPrunMEDIANoptimization(projectfolder,modelInformation,dataInformation,parameterInformation,dosingInformation,options);
