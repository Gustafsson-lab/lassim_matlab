function [dataFitMedian,dataRemovedNaNSettings] = getMedianModelingDataStructSBPOP(data,names,type,varargin)
% [DESCRIPTION]
% Function generates a datatructure that contains information about either
% responder rates in TRT groups or median values of readouts in TRT groups.
%
% NOMINAL_TIME column is used for binning and needs to be available in the
% provided data. 
%
% The result can be used for plotting or fitting of the RR/median responses.
%
% Handling of continuous covariates:
% ----------------------------------
% For the median modeling approach it is possible to specify a PK model
% fit. If this model depends on continuous covariates, it does make sense
% to obtain information about the median of these covariates in the
% generated structure. When simulating the different treatment groups the
% correct settings for the median covariates can be used.
%
% Handling of categorical covariates:
% -----------------------------------
% While continuous covariates are easy to handle (by their median),
% categorial covariates are not really feasible to handle. However, since
% the median modeling approach allows for stratification, categorical
% covariates can be considered if only a single category is left.
% This means that if a single category is left (by TRT group), then this
% one will be recorded. If, however, multiple categories are left, the
% reference value will be used and a warning displayed.
%
% [SYNTAX]
% [dataFitMedian,dataRemovedNaNSettings] = getMedianModelingDataStructSBPOP(data,names,type)
% [dataFitMedian,dataRemovedNaNSettings] = getMedianModelingDataStructSBPOP(data,names,type,covNames)
% [dataFitMedian,dataRemovedNaNSettings] = getMedianModelingDataStructSBPOP(data,names,type,covNames,catNames,referenceCAT)
% [dataFitMedian,dataRemovedNaNSettings] = getMedianModelingDataStructSBPOP(data,names,type,covNames,catNames,referenceCAT,regressionPARA,regressionDATA)
%
% [INPUT]
% data:         dataset in wide format.  
%               ID, TRT, NOMINAL time and the columns specified in "names"
%               need to be present at least
% names:        Cell-array with the names of readouts to consider.
%               Categorical and continuous can not be mixed. Categorical
%               are limited to values of 0 and 1. 
% type:         string defining what to do. "categorical" will assume
%               categorical data and calculate responder rates.
%               "continuous" will calculate medians for the readouts.
%
% covNames:     Names of continuous covariates to generate median values
% catNames:     Names of categorical covariates to generate values for
%               In case of single category this value is taken, otherwise 
%               the reference value
% referenceCAT: Reference values for categorical covariates
% regressionPARA: Cell-array with regression parameter names in model
% regressionDATA: Cell-array with regression data column names in dataset
%
% [OUTPUT]
% dataFitMedian: Matlab structure with the following contents:
%     dataFitMedian.NAMES             = names; % Names of readouts
%     dataFitMedian.TRT               = [];    % TRT codes in data
%     dataFitMedian.NT                = {};    % All NOMINAL_TIME values in TRT group
%     dataFitMedian.N                 = [];    % Total number of subjects in TRT group
%     dataFitMedian.N_NT              = {};    % Number of subjects at NOMINAL_TIME with measurement per TRT
%     dataFitMedian.DATA              = {};    % The data ... one element per TRT group, in each element one 
%                                                row per names element and as many columns as nominal times in TRT group
%                                                Responder rates (in %) for categorical, and medians for continuous readouts 
%     dataFitMedian.DATA_STDERR       = {};    % Standard errors for the DATA to be used in cost function for median fitting
%     dataFitMedian.DATA_STDERR_LOG   = {};    % Standard errors for the log transformed DATA to be used in cost function for median fitting
%                                                NaN in case of categorical readouts
%     dataFitMedian.covNames          = {};    % Cell-array with names of continuous covariates for which medians are calculated
%     dataFitMedian.covValues         = [];    % Matrix (row: covariate in order of covNames) with median continuous covariate values for TRT groups
%
%     dataFitMedian.catNames          = {};    % Cell-array with names of categorical covariates for which values are determined
%     dataFitMedian.catValues         = [];    % Matrix (row: covariate in order of catNames) with values for TRT groups
%                                              % Reference group if multiple categories - value if single category 
%
% dataRemovedNaNSettings: Dataset with removed records from provided
% datasets. All records are removed for which at least one of the "names"
% has a NaN entry
%
% [ASSUMPTIONS]
%
% [AUTHOR]
% Henning Schmidt, henning.schmidt@novartis.com
%
% [DATE]
% 21st April 2014
%
% [PLATFORM]
% Windows, Unix, MATLAB
%
% [TOOLBOXES USED]
% Statistics Toolbox

% Information:
% ============
% Copyright (c) 2012 Novartis Pharma AG
%
% This program is Free Open Source Software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program. If not, see <http://www.gnu.org/licenses/>.

%% Handle variable input arguments
covNames = {};
catNames = {};
referenceCAT = [];
regressionPARA = {};
regressionDATA = {};
if nargin == 3,
elseif nargin == 4,
    covNames = varargin{1};
elseif nargin == 6,
    covNames = varargin{1};
    catNames = varargin{2};
    referenceCAT = varargin{3};
elseif nargin == 8,
    covNames = varargin{1};
    catNames = varargin{2};
    referenceCAT = varargin{3};
    regressionPARA = varargin{4};
    regressionDATA = varargin{5};
else
    error('Incorrect number of input arguments.');
end

%% Check input something
if isempty(regressionPARA),
    regressionPARA = {};
end

if isempty(regressionDATA),
    regressionDATA = {};
end

if ischar(regressionPARA),
    regressionPARA = {regressionPARA};
end
if ischar(regressionDATA),
    regressionDATA = {regressionDATA};
end

if ischar(names),
    names = {names};
end
if ischar(covNames),
    covNames = {covNames};
end
if ischar(catNames),
    catNames = {catNames};
end

if length(catNames) ~= length(referenceCAT),
    error('Length of "catNames" and "referenceCAT" is not matching.');
end

if length(regressionPARA) ~= length(regressionDATA),
    error('Length of "regressionPARA" and "regressionDATA" is not matching.');
end


%% Remove all records which contain NaN values in one of the names columns
dataClean = data;
for k=1:length(names),
    dataRemovedNaNSettings = dataClean(isnan(dataClean.(names{k})),:);
    dataClean(isnan(dataClean.(names{k})),:) = [];
end

%% If type=categorical, then require only two type of elements (0 and 1) in the names columns
if strcmpi(type,'categorical'),
    for k=1:length(names),
        X = sort(unique(dataClean.(names{k})));
        if length(X) > 2,
            error('When considering categorical data (responder rates), the "names" columns only are allowed to contain 0 or 1.');
        end
        if max(X)>1 || min(X)<0,
            error('When considering categorical data (responder rates), the "names" columns only are allowed to contain 0 or 1.');
        end
    end
end

%% Handle "categorical" type
if strcmpi(type,'categorical'),
    % Initialize output structure
    dataFitMedian                   = [];
    dataFitMedian.NAMES             = names; % Names of readouts
    dataFitMedian.TRT               = [];    % TRT codes in data
    dataFitMedian.NT                = {};    % all NOMINAL_TIME values in TRT
    dataFitMedian.N                 = [];    % total number of subjects in TRT
    dataFitMedian.N_NT              = {};    % number of subjects at NOMINAL_TIME with measurement per TRT
    dataFitMedian.DATA              = {};    % data for fitting per TRT and NOMINAL_TIME 
    dataFitMedian.DATA_STDERR       = {};    % stderr for the data per TRT and NOMINAL_TIME
    dataFitMedian.DATA_STDERR_LOG   = {};    % stderr for the log transformed data per TRT and NOMINAL_TIME
    
    % Determine all available TRT groups
    allTRT                          = unique(dataClean.TRT);     % All treatment groups
    dataFitMedian.TRT               = allTRT(:)';               % Collect info in output structure
    
    % Cycle through all TRT groups and collect the information
    for k=1:length(allTRT),
        % Get data for TRT group only
        datak                       = dataClean(dataClean.TRT==allTRT(k),:);
        
        % Get all NOMINAL_TIME values for TRT group
        allNT                       = unique(datak.NOMINAL_TIME);
        dataFitMedian.NT{k}         = allNT;                % Collect info in output structure
        
        % Get total number of subjects per TRT group
        N                           = length(unique(datak.ID));
        dataFitMedian.N(k)          = N;                    % Collect info in output structure
        
        % Initialize some variables to collect information for each nominal
        % time point
        N_RESPONSE_NT               = NaN(length(names),length(allNT)); % Sum of responders based on the categorical data (1=response, 0=no response)
        N_NT                        = []; % Number of patients in TRT group at NT
        
        % Cycle through the nominal time points and collect information
        for k2=1:length(allNT),
            datak2                  = datak(datak.NOMINAL_TIME==allNT(k2),:);
            
            if ~isempty(datak2),
                N_NT(k2)                    = length(unique(datak2.ID));
                % Get number of responders for each NAMES in current TRT and NT
                for k3=1:length(names),
                    N_RESPONSE_NT(k3,k2)    = sum(datak2.(names{k3})==1);
                end
            else
                N_NT(k2)                    = 0;
                for k3=1:length(names),
                    N_RESPONSE_NT(k3,k2)    = NaN;
                end
            end
        end
        
        % Calculate Responder Rates in percent for TRT group over
        % NOMINAL_TIME. RAW RR ... in the sense of no imputation!
        % Also calculate standard error for RR
        RR                          = NaN(length(names),length(allNT));
        STDERR_RR                   = NaN(length(names),length(allNT));
        for k3=1:length(names),
            p                       = N_RESPONSE_NT(k3,:)./N_NT;
            RR(k3,:)                = 100*p;
            STDERR_RR(k3,:)         = max(100*sqrt(p.*(1-p)./N_NT),1);
        end
        
        % Collect information
        dataFitMedian.N_NT{k}            = N_NT;
        dataFitMedian.DATA{k}            = RR;
        dataFitMedian.DATA_STDERR{k}     = STDERR_RR;
        % Log transformed undefined for RR!
        dataFitMedian.DATA_STDERR_LOG{k} = NaN(length(names),length(allNT));
    end
    
    
elseif strcmpi(type,'continuous'),
    %% Handle "continuous" type
    % Initialize output structure
    dataFitMedian                   = [];
    dataFitMedian.NAMES             = names; % Names of readouts
    dataFitMedian.TRT               = [];    % TRT codes in data
    dataFitMedian.NT                = {};    % all NOMINAL_TIME values in TRT
    dataFitMedian.N                 = [];    % total number of subjects in TRT
    dataFitMedian.N_NT              = {};    % number of subjects at NOMINAL_TIME with measurement per TRT
    dataFitMedian.DATA              = {};    % data for fitting per TRT and NOMINAL_TIME - this will be the median values
    dataFitMedian.DATA_STDERR       = {};    % stderr for the data per TRT and NOMINAL_TIME
    dataFitMedian.DATA_STDERR_LOG   = {};    % stderr for the log transformed data per TRT and NOMINAL_TIME

    % Determine all available TRT groups
    allTRT                          = unique(dataClean.TRT);     % All treatment groups
    dataFitMedian.TRT               = allTRT(:)';                % Collect info in output structure
    
    % Cycle through all TRT groups and collect the information
    for k=1:length(allTRT),
        % Get data for TRT group only
        datak                       = dataClean(dataClean.TRT==allTRT(k),:);
        
        % Get all NOMINAL_TIME values for TRT group
        allNT                       = unique(datak.NOMINAL_TIME);
        dataFitMedian.NT{k}         = allNT;                % Collect info in output structure
        
        % Get total number of subjects per TRT group
        N                           = length(unique(datak.ID));
        dataFitMedian.N(k)          = N;                    % Collect info in output structure
        
        % Initialize some variables to collect information for each nominal time point
        MEDIAN_RESPONSE_NT          = NaN(length(names),length(allNT)); 
        STDERR_RESPONSE_NT          = NaN(length(names),length(allNT)); 
        STDERR_LOG_RESPONSE_NT      = NaN(length(names),length(allNT)); 
        N_NT                        = []; % Number of patients in TRT group at NT
        
        % Cycle through the nominal time points and collect information
        for k2=1:length(allNT),
            datak2                  = datak(datak.NOMINAL_TIME==allNT(k2),:);
            
            if ~isempty(datak2),
                N_NT(k2)            = length(unique(datak2.ID));
                % Get number of responders for each NAMES in current TRT and NT
                for k3=1:length(names),
                    MEDIAN_RESPONSE_NT(k3,k2)     = nanmedian(datak2.(names{k3}));
                    STDERR_RESPONSE_NT(k3,k2)     = nanstd(datak2.(names{k3}))/sqrt(N_NT(k2));
                    STDERR_LOG_RESPONSE_NT(k3,k2) = nanstd(log(datak2.(names{k3})))/sqrt(N_NT(k2));
                end
            else
                N_NT(k2)                    = 0;
                for k3=1:length(names),
                    MEDIAN_RESPONSE_NT(k3,k2)     = NaN;
                    STDERR_RESPONSE_NT(k3,k2)     = NaN;                    
                    STDERR_LOG_RESPONSE_NT(k3,k2) = NaN;                    
                end
            end
        end
        
        % Handle 0 STDERROR things
        % If zero then set to median of others. If all zero then set all to 1
        % Do it rowwise
        for k2=1:size(STDERR_RESPONSE_NT,1)
            row = STDERR_RESPONSE_NT(k2,:);
            % Check if all zero
            if sum(abs(row))==0,
                row = ones(1,length(row));
            else
                row(row==0) = median(row(row~=0));
            end
            STDERR_RESPONSE_NT(k2,:) = row;
        end
        
        % Do same for STDERR_LOG_RESPONSE_NT
        for k2=1:size(STDERR_LOG_RESPONSE_NT,1)
            row = STDERR_LOG_RESPONSE_NT(k2,:);
            % Check if all zero
            if sum(abs(row))==0,
                row = ones(1,length(row));
            else
                row(row==0) = median(row(row~=0));
            end
            STDERR_LOG_RESPONSE_NT(k2,:) = row;
        end
        
        % Collect information
        dataFitMedian.N_NT{k}            = N_NT;
        dataFitMedian.DATA{k}            = MEDIAN_RESPONSE_NT;
        dataFitMedian.DATA_STDERR{k}     = STDERR_RESPONSE_NT;
        dataFitMedian.DATA_STDERR_LOG{k} = STDERR_LOG_RESPONSE_NT;
    end
else
    error('Incorrect "type" definition.');
end

%% Finally, need to add the covariate information into the median data structure
% Handling of continuous covariates:
% ----------------------------------
% For the median modeling approach it is possible to specify a PK model
% fit. If this model depends on continuous covariates, it does make sense
% to obtain information about the median of these covariates in the
% generated structure. When simulating the different treatment groups the
% correct settings for the median covariates can be used.
%
% Handling of categorical covariates:
% -----------------------------------
% While continuous covariates are easy to handle (by their median),
% categorial covariates are not really feasible to handle. However, since
% the median modeling approach allows for stratification, categorical
% covariates can be considered if only a single category is left.
% This means that if a single category is left (by TRT group), then this
% one will be recorded. If, however, multiple categories are left, the
% reference value will be used and a warning displayed.


%% Handle continuous covariates
% Median for continuous covariates

% Check if all covariates in dataset that are passed in covNames
% If not, then provide an error!
errorText = '';
for k=1:length(covNames),
    if isempty(strmatchSB(covNames{k},get(data,'VarNames'),'exact')),
        errorText = sprintf('%sCovariate "%s" not in dataset. Please provide it!\n',errorText,covNames{k});
    end
end
if ~isempty(errorText),
    error(errorText);
end

% Get TRT groups
allTRT                      = unique(dataClean.TRT);

% Initialize variable to store median values
medianCovValues             = NaN(length(covNames),length(allTRT));

% Cycle through all TRT groups and get median CovValues
for k=1:length(allTRT),
    % Get TRT group
    datak = dataClean(dataClean.TRT==allTRT(k),:);
    % Get first rows only
    allID = unique(datak.ID);
    data2k = dataset();
    for k2=1:length(allID),
        datak2 = datak(datak.ID==allID(k2),:);
        data2k = [data2k; datak2(1,:)];
    end
    % Get median covariate values for TRT group
    for k2=1:length(covNames),
        medianCovValues(k2,k) = nanmedian(data2k.(covNames{k2}));
    end
end

% Add to structure
dataFitMedian.covNames = covNames;
dataFitMedian.covValues = medianCovValues;

%% Handle categorical covariates
% Reference if multiple cat categories
% Value if single category

% Check if all covariates in dataset that are passed in catNames
% If not, then provide an error!
errorText = '';
for k=1:length(catNames),
    if isempty(strmatchSB(catNames{k},get(data,'VarNames'),'exact')),
        errorText = sprintf('%sCovariate "%s" not in dataset. Please provide it!\n',errorText,catNames{k});
    end
end
if ~isempty(errorText),
    error(errorText);
end

% Get TRT groups
allTRT                      = unique(dataClean.TRT);

% Initialize variable to store median values
catValues                   = NaN(length(catNames),length(allTRT));

% Cycle through all TRT groups and get median CovValues
for k=1:length(allTRT),
    % Get TRT group
    datak = dataClean(dataClean.TRT==allTRT(k),:);
    % Get first rows only
    allID = unique(datak.ID);
    data2k = dataset();
    for k2=1:length(allID),
        datak2 = datak(datak.ID==allID(k2),:);
        data2k = [data2k; datak2(1,:)];
    end
    % Get cat values for TRT group
    for k2=1:length(catNames),
        % Check if single category
        x = unique(data2k.(catNames{k2}));
        if length(x) == 1,
            catValues(k2,k) = x;
        else
            disp(sprintf('Multiple categories present for "%s" => using reference value for this covariate.',catNames{k2}));
            catValues(k2,k) = referenceCAT(k2);
        end
    end
end

% Add to structure
dataFitMedian.catNames = catNames;
dataFitMedian.catValues = catValues;

%% Handle regression parameters
% Get unique median of regression values from dataset

% Check if all regressionDATA in dataset 
% If not, then provide an error!
errorText = '';
for k=1:length(regressionDATA),
    if isempty(strmatchSB(regressionDATA{k},get(data,'VarNames'),'exact')),
        errorText = sprintf('%sRegression data "%s" not in dataset. Please provide it!\n',errorText,regressionDATA{k});
    end
end
if ~isempty(errorText),
    error(errorText);
end

% Get TRT groups
allTRT                      = unique(dataClean.TRT);

% Initialize variable to store median values
medianRegressionValues      = NaN(length(regressionDATA),length(allTRT));

% Cycle through all TRT groups and get median Regression values
for k=1:length(allTRT),
    % Get TRT group
    datak = dataClean(dataClean.TRT==allTRT(k),:);
    % Get first rows only
    allID = unique(datak.ID);
    data2k = dataset();
    for k2=1:length(allID),
        datak2 = datak(datak.ID==allID(k2),:);
        data2k = [data2k; datak2(1,:)];
    end
    % Get median covariate values for TRT group
    for k2=1:length(regressionDATA),
        medianRegressionValues(k2,k) = nanmedian(data2k.(regressionDATA{k2}));
    end
end

% Add to structure
dataFitMedian.regressionNames = regressionPARA;
dataFitMedian.regressionValues = medianRegressionValues;


