function [Xopt,OUTPUTopt] = optimizeModelMedianSBPOP(kBOOTSTRAP,projectfolder_stratified,mexModel,modelInformation,dataFit,parameterInformation,dosingInformation,options)
% optimizeModelMedianSBPOP: Auxiliary and interface function between SBPOPrunMEDIANoptimization and the 
% costfunction costFunctionMedianSBPOP. For each bootstrap this function is evaluated and handles the parameter optimization
% of a single run.
%
% No further documentation, since auxiliary to SBPOPrunMEDIANoptimization

% Information:
% ============
% Copyright (c) 2012 Novartis Pharma AG
% 
% This program is Free Open Source Software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program. If not, see <http://www.gnu.org/licenses/>.

%% Set global parameters needed for optimization
global simulationTRTinfo 

%% Get moddos ... use a dosing object from dosingInformation - any can be taken since all are supposed to have the same structure
moddos = mergemoddosSBPOP(modelInformation.model,dosingInformation.dosings{1});

%% Generate simulation information for the different TRT groups that are present in the dataset
% Important: there can be as many dosing TRT definitions as one wants but only TRT groups that are present in the data 
% are considered for estimation.
simulationTRTinfo = [];

% Cycle through the data TRTs
for kTRT_data = 1:length(dataFit.TRT),
    
    % Find the index of the corresponding TRT in the dosingInformation
    ixTRT_dosing                                              = find(dosingInformation.TRT==dataFit.TRT(kTRT_data));
    
    % Construct simulationTRTinfo structure. Each element for one TRT group
    simulationTRTinfo(kTRT_data).TRT                        = dataFit.TRT(kTRT_data);                          % TRT code
    simulationTRTinfo(kTRT_data).TRTNAME                    = dosingInformation.name{ixTRT_dosing};            % Corresponding TRT NAME from dosingInformation
    simulationTRTinfo(kTRT_data).N                          = dataFit.N(kTRT_data);                            % Number of patients in TRT group
    simulationTRTinfo(kTRT_data).SIMTIME                    = dataFit.NT{kTRT_data}(:)';                       % Nominal times to use for simulation    
    simulationTRTinfo(kTRT_data).DOSING                     = dosingInformation.dosings{ixTRT_dosing};         % Dosing object for trial
    simulationTRTinfo(kTRT_data).WEIGHTBASED                = dosingInformation.weightBased(:,ixTRT_dosing);   % Flag if weight based dosing (1) or not (0)
    simulationTRTinfo(kTRT_data).REFERENCE_DATA             = dataFit.DATA{kTRT_data};                         % Reference data for cost function determination
    simulationTRTinfo(kTRT_data).REFERENCE_DATA_STDERR      = dataFit.DATA_STDERR{kTRT_data};                  % Reference stderr data for weighting of cost function 
    simulationTRTinfo(kTRT_data).REFERENCE_DATA_STDERR_LOG  = dataFit.DATA_STDERR_LOG{kTRT_data};              % Reference stderr on log transformed data for weighting of cost function 
    simulationTRTinfo(kTRT_data).modelInformation           = modelInformation;                                % Pass the model information
    simulationTRTinfo(kTRT_data).moddos                     = moddos;                                          % Pass moddos for readout of variables later
    simulationTRTinfo(kTRT_data).mexModel                   = mexModel;                                        % Pass the name of the mex model
    simulationTRTinfo(kTRT_data).parameterInformation       = parameterInformation;                            % Pass the parameter information
    simulationTRTinfo(kTRT_data).options                    = options;                                         % Optimizer options
    simulationTRTinfo(kTRT_data).TRTweighting               = dosingInformation.TRTweighting(ixTRT_dosing);    % Optimizer options
    simulationTRTinfo(kTRT_data).regressionNames            = dataFit.regressionNames;                         % Add regression parameters
    simulationTRTinfo(kTRT_data).regressionValues           = dataFit.regressionValues(:,kTRT_data);           % Add regression parameters
    
    % Check if WEIGHTBASED dosing in this TRT group and in this case define the median weight
    if sum(simulationTRTinfo(kTRT_data).WEIGHTBASED) > 0,
        % Get the median weight for weight based dosing
        simulationTRTinfo(kTRT_data).WEIGHT                 = dataFit.covValues(strmatchSB(dataFit.covNames,'WT0','exact'),kTRT_data);
    else
        % Assign NaN
        simulationTRTinfo(kTRT_data).WEIGHT                 = NaN;                                         
    end
    
    % If FIT_PK is defined, then sample PK parameters - also taking covariates as well into account as feasible with this approach.
    if ~isempty(modelInformation.FIT_PK),
        % Do not take into account uncertainty (FLAG: 3)
        % Sample 1 time only
        % Consider covariates
        PKparam = SBPOPsampleNLMEfitParam(modelInformation.FIT_PK,3,1,dataFit.covNames,dataFit.covValues(:,kTRT_data)',dataFit.catNames,dataFit.catValues(:,kTRT_data)');
        % Need to check in the model and remove the PK parameters that are
        % not present in the model
        PKparamNotInModel = setdiff(PKparam.parameterNames,SBparameters(moddos));
        ix_remove_PKparam = [];
        for kPKparam=1:length(PKparamNotInModel),
            ix_remove_PKparam = [ix_remove_PKparam strmatchSB(PKparamNotInModel{kPKparam},PKparam.parameterNames)];
        end
        PKparam.parameterNames(ix_remove_PKparam) = [];
        PKparam.parameterValuesPopulation(ix_remove_PKparam) = [];
        simulationTRTinfo(kTRT_data).PKparamNames        = PKparam.parameterNames;
        simulationTRTinfo(kTRT_data).PKparamValues       = PKparam.parameterValuesPopulation;
    else
        simulationTRTinfo(kTRT_data).PKparamNames        = {};
        simulationTRTinfo(kTRT_data).PKparamValues       = [];
    end
end

%% Initialize log text
simulationTRTinfo(1).logText = '';

%% Handle the interface to the optimization function
% 1) Obtain initial guesses for the parameters to be estimated
% 2) Transform the parameters if desired (based on parameterInformation.trans)
% 3) Perform the optimization using the costfunction
% 4) Backtransforming the parameters if needed (based on parameterInformation.trans)
% 5) Evaluation of the costfunction for the optimal parameters to obtain the following information
%       - optimal cost
%       - parameter names (all, including the not optimized ones)
%       - parameter values (all, including the not optimized ones)
%       - TRT group code
%       - DV
%       - PRED
%       - TIME
% 6) Save the log text for the estimation 

% 1) Determine initial guess vector X0 based on provided parameter values and flags about which parameters to estimate
X0 = parameterInformation.values0(parameterInformation.estimate==1);

% 2) Transform the initial guesses based on parameter transformation
X0_trans = transformXparametersSBPOP(X0,parameterInformation.trans(find(parameterInformation.estimate)));

% 3) Run the optimization
Xopt_trans = feval(options.OPTIMIZER,'costFunctionMedianSBPOP',X0_trans,options);

% 4) Inverse transform the optimal parameters
Xopt = invtransformXparametersSBPOP(Xopt_trans,parameterInformation.trans(find(parameterInformation.estimate)));

% 5) Run the costfunction at optimized point to obtain some additional information
[costOpt, OUTPUTopt]   = costFunctionMedianSBPOP(Xopt_trans,1);

% 6) Save log text
fid = fopen(sprintf([projectfolder_stratified '/logfiles/RUN_%d.log'],kBOOTSTRAP),'w');
fprintf(fid,'%s',simulationTRTinfo(1).logText);
fclose(fid);



