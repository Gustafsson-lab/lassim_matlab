function [ output ] = SBPOPrunMEDIANoptimization(projectfolder,modelInformation,dataInformation,parameterInformation,dosingInformation,varargin)
% SBPOPrunMEDIANoptimization: Performs the median optimization, allowing to
% use a structural PK, PKPD, KPD, etc model and estimate its parameters to
% fit the median of the considered readouts in each treatment groups (if
% continuous readouts) - or the responder rates in each treatment groups
% (if categorical readout).
%
% Costfunction: 
%   - Weighted sum of squares
%   - Weighting by (stderr(mean observation))^2
%   - Allowing for user defined weight per TRT group which is multiplied to 
%     the cost function for the TRT groups
%
% USAGE:
% ======
% SBPOPrunMEDIANoptimization(projectfolder,modelInformation,dataInformation,parameterInformation,dosingInformation)        
% SBPOPrunMEDIANoptimization(projectfolder,modelInformation,dataInformation,parameterInformation,dosingInformation,options)        
%
% projectfolder:        string with the path where to store the model runs / logfiles / results
%
% modelInformation:     structure with information about the structural model
%   modelInformation.model:         Structural model as SBmodel object
%   modelInformation.modelOutput:   Cell-array with names of SBmodel variables that are the outputs 
%                                   of the model and compared to the data
%   modelInformation.FIT_PK:        Project path to a potential PK model (NONMEM or MONOLIX), allowing to take parameters 
%                                   (population mean parameters adjusted by median covariate information in 
%                                   considered treatment groups). Useful when performing PKPD modeling using the median 
%                                   approach. The structural model needs to contain the parameters and the structural elements
%                                   from the FIT_PK model. Can be left empty ''. In this case the non-estimated parameters 
%                                   from the model are defined only in the structural SBmodel. (default: '')
%   modelInformation.regressionPARA:Cell-array, defining the parameters to define by regression variables. Same order as in 
%                                   dataInformation.regressionDATA.
% 
% dataInformation:      structure with information about the data to be used for fitting the model
%   dataInformation.data:           MATLAB dataset in a wide format. Needs to contain the columns ID, TRT, and NOMINAL_TIME.
%                                   Additionally columns containing covariates of interest and the data readouts of interest.
%                                   Such a dataset is easiest generated using the function "SBPOPcreateDatasetMedianOptimization".
%   dataInformation.names:          Cell-array with names of the columns that contain the data to be used for fitting (same order
%                                   as the corresponding model outputs in "modelInformation.modelOutput").
%   dataInformation.type:           'categorical': does responder-rate fitting, 'continuous': does median fitting
%   dataInformation.stratify:       Cell array with stratification settings. Element '' means no stratification. The other 
%                                   elements need to be names of covariates that are in the dataset - either categorical or 
%                                   continuous. Dataset is then split in half based on median value and estimation repeated for 
%                                   upper and lower stratified dataset. (default: {''})!
%   dataInformation.regressionDATA: Cell-array with names of regression parameters in the data to pass to the model.
%                                   Same order as in modelInformation.regressionPARA.
%
% parameterInformation: structure with information about the parameters to be estimated
%   parameterInformation.names:     Cell-array with parameter names to be estimated
%   parameterInformation.values0:   Vector with initial guesses for the estimation (default: taken from the model)
%                                   (default: values in SBmodel)
%   parameterInformation.estimate:  Vector with entries 1 (estimated) and 0 (not estimated - kept on initial guess)
%                                   (default: all estimated)
%   parameterInformation.trans:     Cell-array with transformation information of parameter search space ('N': normal 'L': lognormal)
%                                   (default: all 'L' - ensuring positive values)
%
% dosingInformation:	structure with information about the nominal treatment group dosings
%   dosingInformation.TRT:          Vector with treatment group identifiers to be used in optimization
%                                   TRT groups are optimized if dosings AND data are available - otherwise ignored!
%                                   It will be checked that for each TRT in the data a dosing definition is present. Otherwise error!
%   dosingInformation.dosings:      Cell-array defining nominal dosings for each treatment group
%   dosingInformation.name:         Cell-array defining names for each treatment group
%   dosingInformation.weightBased:  Matrix with as many rows as dosing inputs in the model and as many columns as treatment groups.
%                                   A "0" entry defines fixed dosing and a "1" entry defines weight based dosing.
%                                   If weight based dosing is used, the dataInformation.data dataset needs to contain a WT0 column.
%   dosingInformation.TRTweighting: Vector with cost function weights for each TRT group. By default all "1".
%                                   Can be used to increase the weight of specific TRT groups is desired.
%
% options:              structure with optional information regarding the parameter estimation
%   options.maxtime:                Maximum time of single optimization run (default: Inf)
%   options.maxiter:                Maximum number of iterations in single optimization run (default: Inf)
%   options.maxfunevals:            Maximum number of costfunction evaluations in single optimization run (default: 100*number of estimated parameters)
%   options.OPTIMIZER:              String with name of optimization function to be used (default: 'simplexSB')
%   options.N_BOOTSTRAP:            Number of bootstraps to perform (default: 50)
%                                   If N_BOOTSTRAP>1 then data is resampled
%   options.LOG_TRANSFORM:          Flag defining log transformation of the data (1: log transformed, 0: untransformed (default))
%                                   This has no impact on the dataset. The transformation is handled internally.
%   options.SEED:                   Random seed to be used (default: 123456)
%   options.N_PROCESSORS:           Number of processors to use for parallelization (default: 1)

% Information:
% ============
% Copyright (c) 2012 Novartis Pharma AG
% 
% This program is Free Open Source Software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program. If not, see <http://www.gnu.org/licenses/>.

%% Define default output
output = [];

%% Handle variable input arguments
options = [];
if nargin==5,
elseif nargin == 6,
    options = varargin{1};
else
    error('Incorrect number of input arguments.');
end

%% Check and process input arguments
[projectfolder,modelInformation,dataInformation,parameterInformation,dosingInformation,options,COVCAT_INFORMATION] = ...
    checkProcessMedianModelingInputsSBPOP(projectfolder, ...
    modelInformation, dataInformation,parameterInformation,dosingInformation,options);

%% Check log transform and categorical ... only allow log transform of data in case
% of continuous readouts
if strcmpi(dataInformation.type,'categorical') && options.LOG_TRANSFORM==1,
    error('Log transformed data can not be used for categorical readouts.');
end

%% Request processors
killMATLABpool = 0;
if options.N_PROCESSORS>1,
    try
        if matlabpool('size') == 0,
            eval(sprintf('matlabpool %d',options.N_PROCESSORS));
            killMATLABpool = 1;
        end
    catch
    end
end

%% Set the SEED
setseedSBPOP(123456)

%% Determine number of estimated parameters
NparamEstimated = sum(parameterInformation.estimate);

%% Set high and low bounds for estimation of parameters
% Set high and low bounds for the optimization to +Inf and -Inf to allow
% for consideration of the whole range for estimation (transformation of
% parameters will be used)
options.lowbounds            = -Inf*ones(1,NparamEstimated);
options.highbounds           = Inf*ones(1,NparamEstimated);

%% Create the main project folder
% Additional folders might be created later on
if exist(projectfolder)==7,
    result = input('The project folder already exists. Do you want to overwrite it? (Y,N): ','s');
    if strcmpi(result,'y'),
        while exist(projectfolder) == 7
            try rmdir(projectfolder,'s'); catch end
            pause(0.5)
        end
    else
        disp('SBPOPrunMEDIANoptimization aborted. Please change the name of the projectfolder and continue.');
        return
    end
end
% Create project folder
mkdir(projectfolder);

%% Create mexModel for the simulation and determination of the cost function
% It is created in the folder where the function is invoked ... this is easier when handling parallel computation.
% At the end of the optimization it is removed again.
moddos                      = mergemoddosSBPOP(modelInformation.model,dosingInformation.dosings{1});
[xdummyx,mexModel]                = fileparts(tempname);
SBPDmakeMEXmodel(moddos,mexModel);

%%  Cycle through the different stratifications

for kStratify=1:length(dataInformation.stratify),
    stratificationSetting = dataInformation.stratify{kStratify};
    % Empty ('') means no stratification
    if isempty(stratificationSetting),
        stratificationSetting = '';
    end
    
    % Decide if no stratification or stratification ... in the latter case
    % two runs need to be made. One lower and one upper
    if isempty(stratificationSetting),
        stratificationRanges = {''};
    else
        % Assume dealing with continuous then use median of categorical
        % with 2 values ... then use mean.
        stratificationRanges = {'lower','upper'};
    end
    
    %% Cycle through stratification ranges and handle them independently
    for kStratifyRange=1:length(stratificationRanges),
        stratificationRange = stratificationRanges{kStratifyRange};
        
        %% Create subfolder for stratified run in project folder
        if ~isempty(stratificationSetting),
            projectfolder_stratified = [projectfolder '/' stratificationSetting '_' stratificationRange];
        else
            projectfolder_stratified = [projectfolder '/ALLDATA'];
        end            
        mkdir(projectfolder_stratified);
        
        %% Create logfiles folder
        mkdir([projectfolder_stratified '/logfiles']);
        
        %% Create stratified dataset
        [dataStratified,stratificationTHRESHOLD] = createStratifiedDatasetSBPOP(dataInformation.data,'ID',stratificationSetting,stratificationRange);

        % Report what it is doing
        if isempty(stratificationSetting),
            disp('Handling the ALLDATA case (no stratification).');
        else
            if strcmpi(stratificationRange,'lower'),
                disp(sprintf('Handling a stratified case: %s <= %g',stratificationSetting,stratificationTHRESHOLD));
            else
                disp(sprintf('Handling a stratified case: %s > %g',stratificationSetting,stratificationTHRESHOLD));
            end
        end
        
        %% Get memory to store results for single runs
        % We need to check later what information we want to store ... for now its
        % as it is.
        RUN_ALL             = NaN(options.N_BOOTSTRAP,1);
        OUTPUTopt_ALL       = cell(options.N_BOOTSTRAP,1);
        
        % Run the estimations - allow for parallel execution if desired
        parfor kBOOTSTRAP=1:options.N_BOOTSTRAP,
            
            % Resample data if N_BOOTSTRAP>1
            if options.N_BOOTSTRAP>1,
                % Use "ID" as unique subject identifier
                % Use "TRT" as grouping variable to keep structure for TRT
                dataresample = resampleDatasetSBPOP(dataStratified,'ID','TRT');
            else
                dataresample = dataStratified; % Do not resample if N_BOOTSTRAP=1
            end
                        
            % Convert to datastructure for fitting
            % Include median continuous covariate information in the structure and categorical covariate information
            % for the covariates that the PK model requires (if specified)
            % Also WT0 needs to be present if weight based dosing in dosing schemes.
            [dataFit,~] = getMedianModelingDataStructSBPOP(dataresample,dataInformation.names,dataInformation.type,COVCAT_INFORMATION.covNames,COVCAT_INFORMATION.catNames,COVCAT_INFORMATION.referenceCAT,modelInformation.regressionPARA,dataInformation.regressionDATA);

            % Run the optimization
            [Xopt,OUTPUTopt] = optimizeModelMedianSBPOP(kBOOTSTRAP,projectfolder_stratified,mexModel,modelInformation,dataFit,parameterInformation,dosingInformation,options);
                            
            % Collect results of optimization
            RUN_ALL(kBOOTSTRAP)           = kBOOTSTRAP;
            OUTPUTopt_ALL{kBOOTSTRAP}     = OUTPUTopt;
        end
        
        % Combine results in structure
        run_results                             = [];
        
        % Add all input arguments to results structure
        run_results.projectfolder               = projectfolder;
        run_results.projectfolder_stratified    = projectfolder_stratified;
        run_results.modelInformation            = modelInformation;
        run_results.dataInformation             = dataInformation;
        run_results.parameterInformation        = parameterInformation;
        run_results.dosingInformation           = dosingInformation;
        run_results.options                     = options;
        
        % Add additional information that is run independent
        run_results.dataOriginalStratified              = dataStratified;
        run_results.dataOriginalStratifiedMedian        = getMedianModelingDataStructSBPOP(dataStratified,dataInformation.names,dataInformation.type,COVCAT_INFORMATION.covNames,COVCAT_INFORMATION.catNames,COVCAT_INFORMATION.referenceCAT,modelInformation.regressionPARA,dataInformation.regressionDATA);
        run_results.stratificationSetting.NAME          = stratificationSetting;
        run_results.stratificationSetting.RANGE         = stratificationRange;
        run_results.stratificationSetting.THRESHOLD     = stratificationTHRESHOLD;
        
        % Add info to a dataset that are different from run to run
        run_results.run_information                 = dataset();
        run_results.run_information.RUN             = RUN_ALL;
        run_results.run_information.OUTPUTopt       = OUTPUTopt_ALL;
        
        % Save result dataset in LOGfolder path
        save([projectfolder_stratified '/run_results.data'],'run_results');
        
        % Post-process results for single fit
        SBPOPassessConvergenceMedian(projectfolder_stratified)
        SBPOPcontributionTRTcostMedian(projectfolder_stratified)
        SBPOPbootstrapFitsMedian(projectfolder_stratified)
        SBPOPuncertaintyDistributionMedian(projectfolder_stratified)
        if options.N_BOOTSTRAP > 1,
            SBPOPvpcMedianBootstrapParam(projectfolder_stratified)
            SBPOPvpcMedianSampledParam(projectfolder_stratified,min(5*options.N_BOOTSTRAP,1000))
        end
    end % kStratifyRange
end % kStratify

%% Delete mex model
clear mex
delete([mexModel '.' mexext]);

% Release processors
try
    if matlabpool('size')>1 && killMATLABpool==1,
        matlabpool close
    end
catch
end
