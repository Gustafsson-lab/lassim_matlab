function [datanew] = SBPOPcleanBLOQdata(data,FLAG_LLOQ,filename)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Handle LLOQ in the desired way 
%
% FLAG_LLOQ = 0: remove LLOQ data
% FLAG_LLOQ = 1: use CENS=1 and add LLOQ into DV
% FLAG_LLOQ = 2: use CENS=0, remove all but first LLOQ value, set DV to LLOQ/2
%
% Do this for all records that have LLOQ information available.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if nargin==2,
    filename = '';
end

%% Copy dataset
datanew     = data;

%% Find all NAMEs which have LLOQ information
handleNames = unique(datanew.NAME(~isnan(datanew.LLOQ)));

%% Create a CENS column in the dataset (all 0)
datanew.CENS = zeros(length(datanew),1);

%% Find all records that are BLOQ for all names that have LLOQ information
ixBLOQ = [];
for k=1:length(handleNames),
    ixBLOQ = [ixBLOQ(:); find(datanew.DV < datanew.LLOQ & strcmp(datanew.NAME,handleNames{k}))];
end

%% Update CENS column with 1 for the BLOQ records
datanew.CENS(ixBLOQ) = 1;

%% Handle the different approaches for BLOQ handling
if FLAG_LLOQ==0,
    % REMOVE all BLOQ data
    % Get the BLOQ data
    dataBLLOQ = datanew(datanew.CENS==1,:);
    % Remove the BLOQ data
    datanew(datanew.CENS==1,:) = [];
    % Save the removed data in the output path
    if ~isempty(filename),
        SBPOPexportCSVdataset(dataBLLOQ,[filename '.csv']);
    end
    disp('The following BLOQ data were removed:');
    disp('=====================================');
    dataBLLOQ
    
elseif FLAG_LLOQ==1,
    % Use CENS as in Monolix with LLOQ in DV
    datanew.DV(ixBLOQ) = datanew.LLOQ(ixBLOQ);
    text = sprintf('The BLOQ PK data were kept and the DV values were set to LLOQ, CENS=1');
    disp(text);
    if ~isempty(filename),
        fid = fopen([filename '.txt'],'w');
        fprintf(fid,'%s\n',text);
        fclose(fid);
    end
    
elseif FLAG_LLOQ==2,
    % Set DV below LLOQ values to 0.5*LLOQ and remove subsequent ones
    % A CENS column will be present in the dataset but not used (all 0)
    % Find LLOQ values
    
    % Set all DV values with CENS=1 to LLOQ/2
    datanew.DV(datanew.CENS==1) = datanew.LLOQ(datanew.CENS==1)/2;
    % Now we only need to remove the consecutive LLOQ records for each readout
   
    dataRemoved = dataset();
    dataDVset   = dataset();
    datanew2    = datanew;
    
    % Cycle through each NAME with LLOQ info
    for k00=1:length(handleNames),
        % Get NAME data
        dataNAME   = datanew2(strcmp(datanew2.NAME,handleNames{k00}),:);
        % Get data without NAME
        dataNONAME = datanew2(~strcmp(datanew2.NAME,handleNames{k00}),:);
        % Get empty dataset to collect handled NAME data
        dataNAMEhandled = dataset();
        
        % Cycle through each subject in data with NAME
        allID = unique(dataNAME.ID);
        
        for k=1:length(allID),
            datak       = dataNAME(dataNAME.ID==allID(k),:);
            % Check if BLOQ available
            ix_BLOQ = find(datak.CENS);
            if ~isempty(ix_BLOQ),
                % See if consecutive readouts available - if yes then remove all subsequent ones
                delta = [NaN; diff(ix_BLOQ)];
                ix_consequtive = ix_BLOQ(delta==1);
                % Save records to be removed
                dataRemoved = [dataRemoved; datak(ix_consequtive,:)];
                % Save records for which DV set to LLOQ/2
                dataDVset = [dataDVset; datak(ix_BLOQ(find(delta~=1)),:)];
                % Remove records
                datak(ix_consequtive,:) = [];
            end
            % Combine again the NAME data after handling
            dataNAMEhandled = [dataNAMEhandled; datak];
        end
        
        % Combine again dataNAMEhandled with dataNONAME
        datanew2 = [dataNONAME; dataNAMEhandled];
    end
    % Sort
    datanew = sortrows(datanew2,{'ID','TIME','TYPE','SUBTYPE'});
    % Reset CENS column to 0
    datanew.CENS(1:end) = 0;
    if ~isempty(filename),
        % Save the removed data in the output path
        SBPOPexportCSVdataset(dataRemoved,[filename '_REMOVED.csv']);
        SBPOPexportCSVdataset(dataDVset,[filename '_LLOQ2.csv']);
    end
    disp('The following BLOQ data were removed:');
    disp('=====================================');
    dataRemoved
    disp('The following BLOQ data obtained DV=LLOQ/2:');
    disp('===========================================');
    dataDVset
end
