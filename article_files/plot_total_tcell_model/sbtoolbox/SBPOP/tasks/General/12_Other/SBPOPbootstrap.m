function [] = SBPOPbootstrap(projectPath,bootstrapSettings)
% SBPOPbootstrap: runs a bootstrap analysis on the provided NLME project folder.
%
% USAGE:
% ======
% SBPOPbootstrap(projectPath,bootstrapSettings)
%
% projectPath:      Path to the NONMEM or MONOLIX project that should be bootstrapped
%
% bootstrapSettings: structure with the following elements
%   bootstrapSettings.path                  Path where to store the bootstrap projects (default: './BOOTSTRAPPATH')
%   bootstrapSettings.outputpath            Path for storage of results (default: './BOOTSTRAPPATH') 
%   bootstrapSettings.NSAMPLES              Number of bootstrap samples (default: 100)
%   bootstrapSettings.GROUP                 String with group name to use for stratification (some column
%                                           name in the dataset that contains categorical information).
%                                           Can be set to '' (empty) if stratification not desired (default: '')
%   bootstrapSettings.N_PROCESSORS_PAR:     Number of parallel model runs (default: 1)
%   bootstrapSettings.N_PROCESSORS_SINGLE:  Number of processors to parallelize single run (if NONMEM and MONOLIX allow for it) (default: 1)

% Information:
% ============
% Copyright (c) 2012 Novartis Pharma AG
%
% This program is Free Open Source Software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program. If not, see <http://www.gnu.org/licenses/>.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Handle bootstrap settings
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
try bootstrappath           = bootstrapSettings.path;                   catch, bootstrappath         = './BOOTSTRAPPATH';    end
try NSAMPLES                = bootstrapSettings.NSAMPLES;               catch, NSAMPLES              = 100;                  end
try GROUP                   = bootstrapSettings.GROUP;                  catch, GROUP                 = '';                   end
try outputpath              = bootstrapSettings.outputpath;             catch, outputpath            = bootstrappath;        end
try N_PROCESSORS_PAR        = bootstrapSettings.N_PROCESSORS_PAR;       catch, N_PROCESSORS_PAR      = 1;                    end
try N_PROCESSORS_SINGLE     = bootstrapSettings.N_PROCESSORS_SINGLE;    catch, N_PROCESSORS_SINGLE   = 1;                    end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Check project type
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if isNONMEMfitSBPOP(projectPath),
    TOOL = 'NONMEM';
    projectHeader = parseProjectHeaderNONMEMSBPOP(projectPath);
    projectfile = 'project.nmctl';
elseif isMONOLIXfitSBPOP(projectPath),
    TOOL = 'MONOLIX';
    projectHeader = parseProjectHeaderMONOLIXSBPOP(projectPath);
    projectfile = 'project.mlxtran';
else
    error('Incorrect content of ''projectPath''');
end
   
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Create bootstrap project folder
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
oldpath = pwd;
try, rmdir(bootstrappath,'s'); catch, end
mkdir(bootstrappath); 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Create TEMPLATE project in bootstrappath
% - Copying the projectPath without RESULTS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
mkdir([bootstrappath '/TEMPLATE']);
copyfile(projectPath,[bootstrappath '/TEMPLATE'])
try rmdir([bootstrappath '/TEMPLATE/RESULTS'],'s'); catch end
mkdir([bootstrappath '/TEMPLATE/RESULTS']);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Updating template project file with new data path and filename
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
oldpath = pwd();
cd([bootstrappath '/TEMPLATE']);
content = fileread(projectfile);
% Replace the data path with './data.csv'
content = strrep(content,projectHeader.DATA{1},'./data.csv');
% Handle monolix thing extra
[p,f,e] = fileparts(projectHeader.DATA{1});
content = strrep(content,[p '/'],'.');
content = strrep(content,p,'.');
content = strrep(content,[f e],'data.csv');
% Write out new control file
fid     = fopen(projectfile,'w');
fprintf(fid,'%s',content);
fclose(fid);
cd(oldpath);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Loading the original data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
oldpath = pwd(); cd(projectPath);
dataCSV = SBPOPloadCSVdataset(projectHeader.DATA{1});
cd(oldpath);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Check that 'ID' and 'GROUP' present as columns in the data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
varNames = get(dataCSV,'VarNames');

ix = strmatchSB('ID',varNames,'exact');
if isempty(ix),
    error('No ID column present in the dataset.');
end

if ~isempty(GROUP),
    ix = strmatchSB(GROUP,varNames,'exact');
    if isempty(ix),
        error('The specified GROUP is not present as column in the dataset.');
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Create model with nominal dataset - call it "_00" and use as reference
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
copyfile([bootstrappath '/TEMPLATE'],[bootstrappath '/MODEL_00'])
SBPOPexportCSVdataset(dataCSV,[bootstrappath '/MODEL_00/data.csv']);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Create all bootstrap models
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Get processors
N_PROCESSORS_NEEDED = min(N_PROCESSORS_PAR,NSAMPLES);
killMATLABpool = 0;
if N_PROCESSORS_NEEDED>1,
    try
        if matlabpool('size') == 0,
            eval(sprintf('matlabpool %d',N_PROCESSORS_NEEDED));
            killMATLABpool = 1;
        end
    catch
    end
end
% Create models
parfor k=1:NSAMPLES,
    % Define path for model
    modelpath = [bootstrappath sprintf('/MODEL_%s',preFillCharSB(k,length(num2str(NSAMPLES)),'0'))];
    % Copy the template
    copyfile([bootstrappath '/TEMPLATE'],modelpath);
    % Resample the dataset
    dataCSV_resampled = resampleDatasetSBPOP(dataCSV,'ID',GROUP);
    % Export resampled dataset to folder
    SBPOPexportCSVdataset(dataCSV_resampled,[modelpath '/data.csv']);
end
% Release processors
try
    if matlabpool('size')>1 && killMATLABpool==1,
        matlabpool close
    end
catch
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Remove TEMPLATE folder
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
try rmdir([bootstrappath '/TEMPLATE'],'s'); catch end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Run all the models
% Do not produce GoF Plots for all fits!
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
SBPOPrunNLMEprojectFolder(bootstrappath,N_PROCESSORS_PAR,N_PROCESSORS_SINGLE,1);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Generate standard tables at defined location and get RESULTS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
RESULTS = SBPOPfitanalysisProjectsFolderInfo(bootstrappath,outputpath);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Get results
% For fixed effects, random effects, correlations, covariates, error model
% parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Get parameter names
fef_names = RESULTS(1).rawParameterInfo.fixedEffects.names;
ref_names = RESULTS(1).rawParameterInfo.randomEffects.names;
cor_names = RESULTS(1).rawParameterInfo.correlation.names;
cov_names = RESULTS(1).rawParameterInfo.covariate.names;
err_names = RESULTS(1).rawParameterInfo.errorParameter.names;

% Get nominal values
fef_value_nominal = RESULTS(1).rawParameterInfo.fixedEffects.values;
ref_value_nominal = RESULTS(1).rawParameterInfo.randomEffects.values;
cor_value_nominal = RESULTS(1).rawParameterInfo.correlation.values;
cov_value_nominal = RESULTS(1).rawParameterInfo.covariate.values;
err_value_nominal = RESULTS(1).rawParameterInfo.errorParameter.values;

% Get nominal standard errors
fef_stderr_nominal = RESULTS(1).rawParameterInfo.fixedEffects.stderr;
ref_stderr_nominal = RESULTS(1).rawParameterInfo.randomEffects.stderr;
cor_stderr_nominal = RESULTS(1).rawParameterInfo.correlation.stderr;
cov_stderr_nominal = RESULTS(1).rawParameterInfo.covariate.stderr;
err_stderr_nominal = RESULTS(1).rawParameterInfo.errorParameter.stderr;

% Get bootstrap resulting values
fef_value_resampled = NaN(length(RESULTS)-1,length(fef_value_nominal));
ref_value_resampled = NaN(length(RESULTS)-1,length(ref_value_nominal));
cor_value_resampled = NaN(length(RESULTS)-1,length(cor_value_nominal));
cov_value_resampled = NaN(length(RESULTS)-1,length(cov_value_nominal));
err_value_resampled = NaN(length(RESULTS)-1,length(err_value_nominal));

for k=2:length(RESULTS),
    if ~isempty(RESULTS(k).rawParameterInfo),
        if ~isempty(RESULTS(k).rawParameterInfo.fixedEffects.values),
            fef_value_resampled(k-1,:) = RESULTS(k).rawParameterInfo.fixedEffects.values;
        end
        if ~isempty(RESULTS(k).rawParameterInfo.randomEffects.values),
            ref_value_resampled(k-1,:) = RESULTS(k).rawParameterInfo.randomEffects.values;
        end
        if ~isempty(RESULTS(k).rawParameterInfo.correlation.values),
            cor_value_resampled(k-1,:) = RESULTS(k).rawParameterInfo.correlation.values;
        end
        if ~isempty(RESULTS(k).rawParameterInfo.covariate.values),
            cov_value_resampled(k-1,:) = RESULTS(k).rawParameterInfo.covariate.values;
        end
        if ~isempty(RESULTS(k).rawParameterInfo.errorParameter.values),
            err_value_resampled(k-1,:) = RESULTS(k).rawParameterInfo.errorParameter.values;
        end
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Remove all crashed fits (defined by NaN)
% Nominal fit not allowed to be crashed
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ix_crashed = find(isnan([RESULTS(2:end).OBJ]));
fef_value_resampled(ix_crashed,:) = [];
ref_value_resampled(ix_crashed,:) = [];
cor_value_resampled(ix_crashed,:) = [];
cov_value_resampled(ix_crashed,:) = [];
err_value_resampled(ix_crashed,:) = [];
NSAMPLES_NOT_CRASHED = size(fef_value_resampled,1);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Generate output for results
% A figure with subplots will be done for each category.
%  - Plotting histogram
%  - Highlighting the 5% and 95% CI based on bootstraps
%  - Plotting nominal point estimate and 5%/95% of CI
%  - Textual representation of RSE based on nominal and bootstrap
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Prepare output file
filename = [outputpath '/bootstrap_plot_results'];
startNewPrintFigureSBPOP(filename);

names_all           = {fef_names,ref_names,cor_names,cov_names,err_names};
value_nominal_all   = {fef_value_nominal, ref_value_nominal, cor_value_nominal, cov_value_nominal, err_value_nominal};
stderr_nominal_all  = {fef_stderr_nominal, ref_stderr_nominal,cor_stderr_nominal,cov_stderr_nominal,err_stderr_nominal};
value_resampled_all = {fef_value_resampled,ref_value_resampled,cor_value_resampled,cov_value_resampled,err_value_resampled};

for kk=1:length(names_all),
    
    names           = names_all{kk};
    value_nominal   = value_nominal_all{kk};
    stderr_nominal  = stderr_nominal_all{kk};
    value_resampled = value_resampled_all{kk};
    
    % Define number of bins
    NBINS           = ceil(max(NSAMPLES_NOT_CRASHED/50,10));
    
    if length(names) > 0,
        % Remove parameters with stderr=0 (not estimated)
        ix = find(stderr_nominal==0);
        names(ix) = [];
        value_nominal(ix) = [];
        stderr_nominal(ix) = [];
        value_resampled(:,ix) = [];
        
        figure(kk); clf
        ncols = ceil(sqrt(length(names)));
        nrows = ceil(length(names)/ncols);
        for k=1:length(names),
            subplot(nrows,ncols,k);
            % Plot histogram
            [n,x] = hist(value_resampled(:,k),NBINS);
            bar(x,n/NSAMPLES_NOT_CRASHED); hold on
            XLim = get(gca,'XLim');
            YLim = get(gca,'YLim');
            set(gca,'YLim',YLim);
            set(gca,'XLim',XLim);
            % Plot median based on bootstrap
            q = quantile(value_resampled(:,k),[0.05,0.5,0.95]);
            plot(q(2)*[1 1],YLim,'-','LineWidth',3,'color',[0 0.7 0.7])
            % Plot median based on nominal fit
            plot(value_nominal(k)*[1 1],YLim,'r--','LineWidth',3);

            % Plot 90% CI for bootstrap results
            plot(q(1)*[1 1],YLim,'-','LineWidth',2,'color',[0 0.7 0.7])
            plot(q(3)*[1 1],YLim,'-','LineWidth',2,'color',[0 0.7 0.7])

            % Plot 90% CI for nominal fit results
            plot((value_nominal(k)-1.96*stderr_nominal(k))*[1 1],YLim,'r--','LineWidth',2)
            plot((value_nominal(k)+1.96*stderr_nominal(k))*[1 1],YLim,'r--','LineWidth',2)
                    
            % Increase YLim by 30%
            YLim = [0 YLim(2)*1.3];
            set(gca,'YLim',YLim);
            
            % Title
            title(sprintf('%s',names{k}),'FontSize',14,'Interpreter','none');
            % Determine CI for nominal and bootstrap
            CI_nominal = [(value_nominal(k)-1.96*stderr_nominal(k)) (value_nominal(k)+1.96*stderr_nominal(k))];
            textInfo = sprintf('Value, [90%% CI]:\nBT: %1.3g,[%1.3g,%1.3g]\nEST: %1.3g,[%1.3g,%1.3g]',q(2),q(1),q(3),value_nominal(k),CI_nominal(1),CI_nominal(2));
            text(XLim(2),YLim(2),textInfo,'FontSize',10,'VerticalAlign','top','HorizontalAlign','right','Interpreter','none');
            if k==1,
                legend(sprintf('Distribution (N=%d)',NSAMPLES_NOT_CRASHED),'Bootstrap median, 90% CI','Model estimate, 90% CI','Location','SouthEast');
            end
            grid off;
            set(gca,'FontSize',12)
        end
        printFigureSBPOP(gcf,filename);
    end
end

convert2pdfSBPOP(filename)
close all
