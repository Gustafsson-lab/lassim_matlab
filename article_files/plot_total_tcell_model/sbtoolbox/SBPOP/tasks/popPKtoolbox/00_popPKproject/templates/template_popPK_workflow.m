%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Template for population PK analysis using SBPOP with NONMEM or MONOLIX
%
% No real documentation of the popPK workflow. Just a collection of code
% pieces to get the popPK workflow started.
%
% In order to obtain a good introduction into the popPK workflow, please
% have a look at the following paper (and its supplementary material):
%
% Schmidt H, Radivojevic A (2014) Enhancing population pharmacokinetic
% modeling efficiency and quality using an integrated workflow, Journal of
% Pharmacokinetics and Pharmacodynamics.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% === Initial preparations for the popPK workflow

clc;                        % Clear command window
clear all;                  % Clear workspace from all defined variables
close all;                  % Close all figures
restoredefaultpath();       % Clear all user installed toolboxes

% Installation of SBPOP
PATH_SBPOP              = '~/STUFF/SBPOP PACKAGE EXPORT'; oldpath = pwd(); cd(PATH_SBPOP); installSBPOPpackageInitial; cd(oldpath);

% SBPAR needed for parallel execution in MATLAB. If your cluster is setup
% that you can obtain nodes by calling "matlabpool N", then you do not need
% SBPAR and can remove this line!
PATH_SBPAR              = '~/STUFF/SBPAR'; oldpath = pwd(); cd(PATH_SBPAR); installSBPAR; cd(oldpath);

%% === Loading, checking and converting the original dataset

% Load the data in the generalized dataset format
dataGeneral = SBPOPloadCSVdataset('../Data/DataSet_Example_x.csv');

% Check consistency of data
SBPOPcheckGeneralDataFormat(dataGeneral);
 
%% Convert the general dataset format to an augmented format

covariateInfo = {
    % NAME              USENAME      
    'Gender'            'SEX'
    'Age'               'AGE0'
    'Bodyweight'        'WT0'
    'Height'            'HT0'
    'BMI'               'BMI0'
};
data = SBPOPconvertGeneralDataFormat(dataGeneral,covariateInfo);

%% Save the augmented format

SBPOPexportCSVdataset(data,'../Data/DataSet_Example_x_Augmented.csv');
 
%% Define the covariate names

covNames = {'AGE0'    'HT0'      'WT0'     'BMI0'};
catNames = {'SEX'     'STUDY'    'TRT'};


%% === Graphical exploration 

optionsGraphicalExploration                     = [];
optionsGraphicalExploration.color               = 1;
optionsGraphicalExploration.outputPath          = '../Output/01_DataExploration_uncleaned_data/';
 
SBPOPexplorePopPKdata(data,covNames,catNames,optionsGraphicalExploration)
 
%% Custom data exploration
 
%% === Data Cleaning 

catImputationValues                     = [  1        9999      9999];

removeSUBJECT  = {
    % Unique subject identifier     Reason for removal of subject
    'X3401_0100_0009'               'Confirmed very different PK as for all other subjects.'
};
 
removeREC = {
    % Record number in dataset      Reason for removal
     268                            'Confirmed outlier'
     860                            'Confirmed outlier'
     1548                           'Confirmed outlier'
     2002                           'Confirmed outlier - might be trough sampled post dose'
};
 
Nobs = 1;
 
optionsDataCleaning                 = [];
 
optionsDataCleaning.outputPath      = '../Output/02_DataCleaning/';
 
%   - FLAG_LLOQ = 1: use CENS=1 and add LLOQ into DV 
%   - FLAG_LLOQ = 2: use CENS=0, remove all but first LLOQ value, set DV to LLOQ/2
optionsDataCleaning.FLAG_LLOQ       = 0; % Remove all LLOQ data
 
dataCleaned = SBPOPcleanPopPKdata(data,removeSUBJECT,removeREC,Nobs,covNames,catNames,catImputationValues,optionsDataCleaning);
  
%% Repeat the general graphical exploration of data on the cleaned dataset

optionsGraphicalExploration                     = [];
optionsGraphicalExploration.color               = 1;
optionsGraphicalExploration.outputPath          = '../Output/03_DataExploration_cleaned_data/';
 
SBPOPexplorePopPKdata(dataCleaned,covNames,catNames,optionsGraphicalExploration)
 
%% === Conversion of data to popPK analysis dataset

modelingDatasetFile = '../Data/popPK_modeling_NLME.csv';
 
dataheaderNLME = SBPOPconvert2popPKdataset(dataCleaned,covNames,catNames,modelingDatasetFile);
 
%% === PopPK model building

%% Setting up the options for the parameter estimation software

optionsNLME                                 = [];                      

optionsNLME.parameterEstimationTool         = 'NONMEM';
optionsNLME.N_PROCESSORS_PAR                = 10;
optionsNLME.N_PROCESSORS_SINGLE             = 5;

optionsNLME.algorithm.SEED                  = 123456;                  
optionsNLME.algorithm.K1                    = 500;                    
optionsNLME.algorithm.K2                    = 200;                     
optionsNLME.algorithm.NRCHAINS              = 1;                       

% Set NONMEM specific options
optionsNLME.algorithm.METHOD                = 'SAEM';
optionsNLME.algorithm.ITS                   = 1;
optionsNLME.algorithm.ITS_ITERATIONS        = 10;
optionsNLME.algorithm.IMPORTANCESAMPLING    = 1;
optionsNLME.algorithm.IMP_ITERATIONS        = 5;

% Set MONOLIX specific options
optionsNLME.algorithm.LLsetting             = 'linearization';     

%% Generation of a first model to determine reasonable initial guesses

modeltest                           = [];                               
modeltest.numberCompartments        = 2;                            
modeltest.errorModels               = 'comb1';  
modeltest.saturableClearance        = 1;         
modeltest.lagTime                   = 0;                              
modeltest.FACTOR_UNITS              = 1;
%                                       CL    Vc    Q1    Vp1    Q2    Vp2    Fiv    Fabs1    ka    Tlag_input1    VMAX    KM
modeltest.POPvalues0                = [ 5     10    5     100    5     100    1      1        1     0.5            10      10];
modeltest.POPestimate               = [ 1     1     1     1      1     1      0      0        1     1               1       1];
modeltest.IIVestimate               = [ 1     1     1     1      1     1      0      0        1     1               1       1];
 
optionsModelSpace                   = [];
optionsModelSpace.buildModelsOnly   = 1;
 
optionsNLME.parameterEstimationTool = 'MONOLIX';

SBPOPbuildPopPKModelSpace('MODEL_0_INITIAL_GUESSES', modeltest, modelingDatasetFile, dataheaderNLME, optionsNLME, optionsModelSpace);

% This model can be loaded into the Monolix GUI and evaluated to assess the
% initial guesses, K1, K2, etc. Information gathered in this way can be
% used in the following step to have better informed initial guesses.

%% === Base model building
modeltest                           = [];                             
modeltest.numberCompartments        = [2];                          
modeltest.errorModels               = {'const' 'comb1'};        
modeltest.saturableClearance        = [0];                   
modeltest.lagTime                   = [0]; 
modeltest.FACTOR_UNITS              = 1;
%                                       CL    Vc    Q1    Vp1    Q2    Vp2    Fiv    Fabs1    ka    Tlag_input1    VMAX    KM
modeltest.POPvalues0                = [ 10    100   10    500    10    500    1      1        1     0.5            10      10];
modeltest.POPestimate               = [ 1     1     1     1      1     1      0      0        1     1               0       0];
modeltest.IIVestimate               = [ 1     1     1     1      1     1      0      0        1     1               0       0];
modeltest.IIVvalues0                = [ 0.5   0.5   0.5   0.5    0.5   0.5    0.5    0.5      0.5   0.5             0.5     0.5];

% This is how to run it using MONOLIX
optionsNLME.parameterEstimationTool = 'MONOLIX';
SBPOPbuildPopPKModelSpace('MODEL_1_BASE_MONOLIX', modeltest, modelingDatasetFile, dataheaderNLME, optionsNLME);

% This is how to run using NONMEM
optionsNLME.parameterEstimationTool = 'NONMEM';
optionsNLME.algorithm.METHOD        = 'SAEM';
SBPOPbuildPopPKModelSpace('MODEL_1_BASE_NONMEM', modeltest, modelingDatasetFile, dataheaderNLME, optionsNLME);

%% Compare 2 and 3 cpt models as example
projectFolders          = {'../Models/MODEL_1_BASE_MONOLIX/FITMODEL_1_BASE_MONOLIX_003','../Models/MODEL_1_BASE_MONOLIX/FITMODEL_1_BASE_MONOLIX_007'};
dosing                  = SBPOPcreateDOSING({'BOLUS','INFUSION'},{100 100},{0 48:24:500},{0.001 2});
obsTimes                = [0:0.5:600];
options                 = [];
options.N_PROCESSORS    = 10;
SBPOPcomparePopPKmodels(projectFolders,FACTOR_UNITS,dosing,obsTimes,options);

%% Robustness analysis of BASE model
modeltest                           = [];                             
modeltest.numberCompartments        = 2;                          
modeltest.errorModels               = {'comb1'};        
modeltest.saturableClearance        = 0;                   
modeltest.lagTime                   = 0; 
modeltest.FACTOR_UNITS              = 1;
%                                       CL    Vc    Q1    Vp1    Q2    Vp2    Fiv    Fabs1    ka    Tlag_input1    VMAX    KM
modeltest.POPvalues0                = [ 23    209   23    2100   10    500    1      1        2.08  0.5            10      10];
modeltest.POPestimate               = [ 1     1     1     1      1     1      0      0        1     1               0       0];
modeltest.IIVestimate               = [ 1     1     1     1      1     1      0      0        1     1               0       0];
modeltest.IIVvalues0                = [0.5    0.5   0.5   0.5    0.5   0.5    0.5    0.5      0.5   0.5             0.5   0.5];
 
optionsModelSpace                   = [];
optionsModelSpace.Ntests            = 10;
optionsModelSpace.std_noise_setting = 0.5;
 
% This is how to run it using MONOLIX
optionsNLME.parameterEstimationTool         = 'MONOLIX';
SBPOPbuildPopPKModelSpace('MODEL_2_BASE_ROBUSTNESS_MONOLIX', modeltest, modelingDatasetFile, dataheaderNLME, optionsNLME, optionsModelSpace);
 
% This is how to run using NONMEM
optionsNLME.parameterEstimationTool         = 'NONMEM';
optionsNLME.algorithm.METHOD                = 'SAEM';
SBPOPbuildPopPKModelSpace('MODEL_2_BASE_ROBUSTNESS_NONMEM', modeltest, modelingDatasetFile, dataheaderNLME, optionsNLME, optionsModelSpace);

%% === Covariate model building
modeltest                           = [];                             
modeltest.numberCompartments        = 2;                          
modeltest.errorModels               = {'comb1'};        
modeltest.saturableClearance        = 0;                   
modeltest.lagTime                   = 0; 
modeltest.FACTOR_UNITS              = 1;
%                                       CL    Vc    Q1    Vp1    Q2    Vp2    Fiv    Fabs1    ka    Tlag_input1    VMAX    KM
modeltest.POPvalues0                = [ 23    209   23    2100   10    500    1      1        2.08  0.5            10      10];
modeltest.POPestimate               = [ 1     1     1     1      1     1      0      0        1     1               0       0];
modeltest.IIVestimate               = [ 1     1     1     1      1     1      0      0        1     1               0       0];
modeltest.IIVvalues0                = [0.5    0.5   0.5   0.5    0.5   0.5    0.5    0.5      0.5   0.5             0.5   0.5];
 
modeltest.covariateModels           = {''
                                       '{CL,SEX,AGE0,WT0}, {Vc,SEX,AGE0,WT0}, {Q1,SEX,AGE0,WT0}, {Vp1,SEX,AGE0,WT0}'
                                      };
 
% This is how to run it using MONOLIX
optionsNLME.parameterEstimationTool         = 'MONOLIX';
SBPOPbuildPopPKModelSpace('MODEL_3_COVARIATE_MONOLIX', modeltest, modelingDatasetFile, dataheaderNLME, optionsNLME);
 
% This is how to run using NONMEM
optionsNLME.parameterEstimationTool         = 'NONMEM';
optionsNLME.algorithm.METHOD                = 'SAEM';
SBPOPbuildPopPKModelSpace('MODEL_3_COVARIATE_NONMEM', modeltest, modelingDatasetFile, dataheaderNLME, optionsNLME);

%% Refinement of covariate model
modeltest                           = [];                             
modeltest.numberCompartments        = 2;                          
modeltest.errorModels               = {'comb1'};        
modeltest.saturableClearance        = 0;                   
modeltest.lagTime                   = 0; 
modeltest.FACTOR_UNITS              = 1;
%                                       CL    Vc    Q1    Vp1    Q2    Vp2    Fiv    Fabs1    ka    Tlag_input1    VMAX    KM
modeltest.POPvalues0                = [ 23    209   23    2100   10    500    1      1        2.08  0.5            10      10];
modeltest.POPestimate               = [ 1     1     1     1      1     1      0      0        1     1               0       0];
modeltest.IIVestimate               = [ 1     1     1     1      1     1      0      0        1     1               0       0];
modeltest.IIVvalues0                = [0.5    0.5   0.5   0.5    0.5   0.5    0.5    0.5      0.5   0.5             0.5   0.5];
 
modeltest.covariateModels           = {''
                                       '{CL,SEX,AGE0,WT0}, {Vc,WT0}'
                                       '{CL,SEX,AGE0,WT0}, {Vc,SEX,AGE0,WT0}, {Q1,SEX,AGE0,WT0}, {Vp1,SEX,AGE0,WT0}'
                                      };
                                  
% This is how to run it using MONOLIX
optionsNLME.parameterEstimationTool         = 'MONOLIX';
SBPOPbuildPopPKModelSpace('MODEL_4_COVARIATE_REFINEMENT_MONOLIX', modeltest, modelingDatasetFile, dataheaderNLME, optionsNLME);
 
% This is how to run using NONMEM
optionsNLME.parameterEstimationTool         = 'NONMEM';
optionsNLME.algorithm.METHOD                = 'SAEM';
SBPOPbuildPopPKModelSpace('MODEL_4_COVARIATE_REFINEMENT_NONMEM', modeltest, modelingDatasetFile, dataheaderNLME, optionsNLME);

%% Assessing "clinical relevance" of identified covariates
FIT_PATH            = '../Models/MODEL_4_COVARIATE_REFINEMENT_MONOLIX/FITMODEL_4_COVARIATE_REFINEMENT_MONOLIX_003';
options             = [];
options.filename    = '../Output/FitAnalysis/MODEL_4_COVARIATE_REFINEMENT_MONOLIX/Covariate_Assessment_fullCovariateModel';
SBPOPcovariateAssessmentUncertainty(FIT_PATH, modelingDatasetFile,options)
 
FIT_PATH            = '../Models/MODEL_4_COVARIATE_REFINEMENT_MONOLIX/FITMODEL_4_COVARIATE_REFINEMENT_MONOLIX_002';
options             = [];
options.filename    = '../Output/FitAnalysis/MODEL_4_COVARIATE_REFINEMENT_MONOLIX/Covariate_Assessment_reducedCovariateModel';
SBPOPcovariateAssessmentUncertainty(FIT_PATH, modelingDatasetFile,options)
 
%% Robustness analysis of COVARIATE model
modeltest                           = [];                             
modeltest.numberCompartments        = 2;                          
modeltest.errorModels               = {'comb1'};        
modeltest.saturableClearance        = 0;                   
modeltest.lagTime                   = 0; 
modeltest.FACTOR_UNITS              = 1;
%                                       CL    Vc    Q1    Vp1    Q2    Vp2    Fiv    Fabs1    ka    Tlag_input1    VMAX    KM
modeltest.POPvalues0                = [ 23    209   23    2100   10    500    1      1        2.08  0.5            10      10];
modeltest.POPestimate               = [ 1     1     1     1      1     1      0      0        1     1               0       0];
modeltest.IIVestimate               = [ 1     1     1     1      1     1      0      0        1     1               0       0];
modeltest.IIVvalues0                = [0.5    0.5   0.5   0.5    0.5   0.5    0.5    0.5      0.5   0.5             0.5   0.5];
 
modeltest.covariateModels           = '{CL,SEX,AGE0,WT0}, {Vc,WT0}';
 
optionsModelSpace                   = [];
optionsModelSpace.Ntests            = 20;
optionsModelSpace.std_noise_setting = 0.5;
optionsModelSpace.createGOFplots    = 1;
 
% This is how to run it using MONOLIX
optionsNLME.parameterEstimationTool         = 'MONOLIX';
SBPOPbuildPopPKModelSpace('MODEL_5_COVARIATE_ROBUSTNESS_MONOLIX', modeltest, modelingDatasetFile, dataheaderNLME, optionsNLME, optionsModelSpace);

% This is how to run using NONMEM
optionsNLME.parameterEstimationTool         = 'NONMEM';
optionsNLME.algorithm.METHOD                = 'SAEM';
SBPOPbuildPopPKModelSpace('MODEL_5_COVARIATE_ROBUSTNESS_NONMEM', modeltest, modelingDatasetFile, dataheaderNLME, optionsNLME, optionsModelSpace);

%% === Covariance model building
modeltest                           = [];                             
modeltest.numberCompartments        = 2;                          
modeltest.errorModels               = {'comb1'};        
modeltest.saturableClearance        = 0;                   
modeltest.lagTime                   = 0; 
modeltest.FACTOR_UNITS              = 1;
%                                       CL    Vc    Q1    Vp1    Q2    Vp2    Fiv    Fabs1    ka    Tlag_input1    VMAX    KM
modeltest.POPvalues0                = [ 23    209   23    2100   10    500    1      1        2.08  0.5            10      10];
modeltest.POPestimate               = [ 1     1     1     1      1     1      0      0        1     1               0       0];
modeltest.IIVestimate               = [ 1     1     1     1      1     1      0      0        1     1               0       0];
modeltest.IIVvalues0                = [0.5    0.5   0.5   0.5    0.5   0.5    0.5    0.5      0.5   0.5             0.5   0.5];
modeltest.covariateModels           = '{CL,SEX,AGE0,WT0}, {Vc,WT0}';
 
modeltest.covarianceModels          = {''
                                       '{ka,CL,Vc,Q1,Vp1}'
                                       '{CL,Vc,Q1,Vp1}'
                                       '{CL,Vc},{Q1,Vp1}'
                                       '{CL,Vc}'
                                       };
 
optionsModelSpace                   = [];
optionsModelSpace.Ntests            = 5;
optionsModelSpace.std_noise_setting = 0.5;
optionsModelSpace.createGOFplots    = 1;
 
% This is how to run it using MONOLIX
optionsNLME.parameterEstimationTool         = 'MONOLIX';
SBPOPbuildPopPKModelSpace('MODEL_6_COVARIANCE_ROBUSTNESS_MONOLIX', modeltest, modelingDatasetFile, dataheaderNLME, optionsNLME, optionsModelSpace);
 
% This is how to run using NONMEM
optionsNLME.parameterEstimationTool         = 'NONMEM';
optionsNLME.algorithm.METHOD                = 'SAEM';
SBPOPbuildPopPKModelSpace('MODEL_6_COVARIANCE_ROBUSTNESS_NONMEM', modeltest, modelingDatasetFile, dataheaderNLME, optionsNLME, optionsModelSpace);

%% === Select final model
modeltest                           = [];
modeltest.numberCompartments        = 2;                          
modeltest.errorModels               = {'comb1'};        
modeltest.saturableClearance        = 0;                   
modeltest.lagTime                   = 0; 
modeltest.FACTOR_UNITS              = 1;
%                                       CL    Vc    Q1    Vp1    Q2    Vp2    Fiv    Fabs1    ka    Tlag_input1    VMAX    KM
modeltest.POPvalues0                = [ 23    209   23    2100   10    500    1      1        2.08  0.5            10      10];
modeltest.POPestimate               = [ 1     1     1     1      1     1      0      0        1     1               0       0];
modeltest.IIVestimate               = [ 1     1     1     1      1     1      0      0        1     1               0       0];
modeltest.IIVvalues0                = [0.5    0.5   0.5   0.5    0.5   0.5    0.5    0.5      0.5   0.5             0.5   0.5];
modeltest.covariateModels           = '{CL,SEX,AGE0,WT0}, {Vc,WT0}';
modeltest.covarianceModels          = '{ka,CL,Vc,Q1,Vp1}';
 
% This is how to run it using MONOLIX
optionsNLME.parameterEstimationTool         = 'MONOLIX';
SBPOPbuildPopPKModelSpace('MODEL_7_FINAL_MONOLIX', modeltest, modelingDatasetFile, dataheaderNLME, optionsNLME);
 
% This is how to run using NONMEM
optionsNLME.parameterEstimationTool         = 'NONMEM';
optionsNLME.algorithm.METHOD                = 'SAEM';
SBPOPbuildPopPKModelSpace('MODEL_7_FINAL_NONMEM', modeltest, modelingDatasetFile, dataheaderNLME, optionsNLME);
 
%% === Generation of VPCs for final model
 
%% Generate stratified VPC 
dataVPC                     = SBPOPloadCSVdataset(modelingDatasetFile);

projectFolder               = '../Models/MODEL_7_FINAL_MONOLIX/FITMODEL_7_FINAL_MONOLIX_001';
 
FACTOR_UNITS                = 1;

options                     = [];
options.filename            = '../Output/FitAnalysis/MODEL_7_FINAL_MONOLIX/VPC';
options.logY                = 1;
options.showDataQuantiles   = 1;
options.bins_mean           = [ 0.083 0.25  0.5    1    2    4    8   12       23.917   24   48       95.917   96  168 191.92       287.92       383.92       479.92       480.08 480.5          481          482          484          488       503.92       599.92          696          792  960];
options.bins_lookaround     = 0.05*ones(1,length(options.bins_mean));
options.nTimePoints         = 1000;
options.N_PROCESSORS        = 10;

SBPOPcreatePopPKstratifiedVPC(projectFolder,modeltest.FACTOR_UNITS,dataVPC,covNames,catNames,options)

%% Generate stratified VPC - dose normalized
dataVPC                     = SBPOPloadCSVdataset(modelingDatasetFile);
dataVPC(dataVPC.TIME<0,:)   = [];
dataVPC.DV(dataVPC.TYPE==1) = dataVPC.DV(dataVPC.TYPE==1)./dataVPC.DOSE(dataVPC.TYPE==1);
dataVPC.AMT(dataVPC.TYPE==0) = 1;

projectFolder               = '../Models/MODEL_7_FINAL_MONOLIX/FITMODEL_7_FINAL_MONOLIX_001';

options                     = [];
options.filename            = '../Output/FitAnalysis/MODEL_7_FINAL_MONOLIX/VPC_dose_normalized';
options.logY                = 1;
options.showDataQuantiles   = 1;
options.bins_mean           = [ 0.083 0.25  0.5    1    2    4    8   12       23.917   24   48       95.917   96  168 191.92       287.92       383.92       479.92       480.08 480.5          481          482          484          488       503.92       599.92          696          792  960];
options.bins_lookaround     = 0.05*ones(1,length(options.bins_mean));
options.nTimePoints         = 500;
options.N_PROCESSORS        = 10;

options.groupName           = 'STUDY';
 
SBPOPcreatePopPKstratifiedVPC(projectFolder,modeltest.FACTOR_UNITS,dataVPC,covNames,catNames,options)
 
 

