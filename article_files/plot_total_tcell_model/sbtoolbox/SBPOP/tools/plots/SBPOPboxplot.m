function [ h ] = SBPOPboxplot( perc, axisij, varargin )
% This function is a wrapper for the MATLAB function "boxplot" with identical input arguments
% (varargin). Additionall, the percentiles argument is to be provided and should contain the values
% for the percentiles to which the whiskers are to be plotted.
%
% The DIFFERENCES are:
% - The whiskers are plotted to span 95% of the data (2.5 to 97.5 percentiles)
% - Plotting of outliers is omitted
% - The following assumptions are made
%     - If the first argument is a double matrix then do not allow the second 
%       one to be a group vector.
%     - If the first argument is a vector, check if the second is a vector and 
%       if it is, then assume its a group vector
%
% [ h ] = SBPOPboxplot( perc, location, varargin )
%
% perc:         percentiles to use for whiskers (example: perc = [2.5 97.5])
% axisij:       =0 if not axisij plot, =1 if axisij plot
% varargin:     normal boxplot matlab arguments

% Define the percentiles to be used for plotting


% Handle first and second input argument and determine the percentiles of the 
% data to be plotted ... 
if nargin == 1,
    plotDataMatrix = varargin{1}; % To be used to determine 95% CI for whiskers
    % Determine data based percentiles for the 2.5 and 97.5
    percentiles = prctile(plotDataMatrix,perc);
else
    arg1 = varargin{1};
    arg2 = varargin{2};
    [n1,m1] = size(arg1);
    try
        [n2,m2] = size(arg2);
    catch
        n2 = NaN;
        m2 = NaN;
    end
    
    % Check if arg1 a double matrix
    if n1>1 && m1>1,
        % Its a double matrix. Do not allow second to be a double vector
        if isnumeric(arg2),
            error('SBPOPboxplot: Group variable not allowed if matrix first input argument.');
        end
        plotDataMatrix = arg1;
        % Determine data based percentiles for the percentile_low and percentile_high
        percentiles = prctile(plotDataMatrix,perc);
    elseif n1==1 && m1==1,
        % Its a scalar double => do not allow
        error('SBPOPboxplot: Scalar double as first argument is not allowed.');
    else
        % arg1 is a vector ... make it column
        arg1 = arg1(:);
        
        if ~isnumeric(arg2),
            % If second argument not a vector / double then get percentiles from this here
            percentiles = prctile(arg1,perc)';
        else
            % Assume arg2 being group vector and determine percentiles per group
            allGroup = unique(arg2);
            percentiles = [];
            for k=1:length(allGroup),
                ix = find(arg2==allGroup(k));
                percentiles = [percentiles prctile(arg1(ix),perc)'];
            end
        end
    end
end

% Plot the standard boxplot
h = boxplot(varargin{:},'symbol','');

% Adjust whiskers to the defined percentiles of the data

% CASE 1
hw = flipud(findobj(gca,'Tag','Whisker'));
% Replace whiskers with 95% CI values
for kw = 1:length(hw),
    % Check if XData or YData
    XData = get(hw(kw),'XData');
    if XData(1)~=XData(2),
        set(hw(kw),'XData',percentiles(:,kw)');
    else
        set(hw(kw),'YData',percentiles(:,kw)');
    end        
end

% CASE 2 'Upper Whisker'
hw = flipud(findobj(gca,'Tag','Upper Whisker'));
% Replace whiskers with 95% CI values
for kw = 1:length(hw),
    XData = get(hw(kw),'XData');
    YData = get(hw(kw),'YData');
    if XData(1)~=XData(2),
        XData(2) = max(percentiles(:,kw));
        set(hw(kw),'XData',XData);
    else
        YData(2) = max(percentiles(:,kw));
        set(hw(kw),'YData',YData);
    end
end

% CASE 2 'Upper Adjacent Value'
hw = flipud(findobj(gca,'Tag','Upper Adjacent Value'));
% Replace whiskers with 95% CI values
for kw = 1:length(hw),
    XData = get(hw(kw),'XData');
    YData = get(hw(kw),'YData');
    if XData(1)==XData(2),
        XData = [1 1]*max(percentiles(:,kw));
        set(hw(kw),'XData',XData);
    else
        YData = [1 1]*max(percentiles(:,kw));
        set(hw(kw),'YData',YData);
    end
end

% CASE 2 'Lower Whisker'
hw = flipud(findobj(gca,'Tag','Lower Whisker'));
% Replace whiskers with 95% CI values
for kw = 1:length(hw),
    XData = get(hw(kw),'XData');
    YData = get(hw(kw),'YData');
    if XData(1)~=XData(2),
        XData(1) = min(percentiles(:,kw));
        set(hw(kw),'XData',XData);
    else
        YData(1) = min(percentiles(:,kw));
        set(hw(kw),'YData',YData);
    end
end

% CASE 2 'Lower Adjacent Value'
hw = flipud(findobj(gca,'Tag','Lower Adjacent Value'));
% Replace whiskers with 95% CI values
for kw = 1:length(hw),
    XData = get(hw(kw),'XData');
    YData = get(hw(kw),'YData');
    if XData(1)==XData(2),
        XData = [1 1]*min(percentiles(:,kw));
        set(hw(kw),'XData',XData);
    else
        YData = [1 1]*min(percentiles(:,kw));
        set(hw(kw),'YData',YData);
    end
end

% Plot text about ticks into the figure
XLim = get(gca,'XLim');
YLim = get(gca,'YLim');
if ~axisij,
    text(XLim(2),YLim(1),sprintf('Boxplot ticks: %g%%,25%%,50%%,75%%,%g%%',perc(1),perc(2)),'FontSize',10,'HorizontalAlign','right','VerticalAlign','bottom');
else
    text(XLim(2),YLim(2),sprintf('Boxplot ticks: %g%%,25%%,50%%,75%%,%g%%',perc(1),perc(2)),'FontSize',10,'HorizontalAlign','right','VerticalAlign','bottom');
end