function SBPOPrunNONMEMproject(projectPath,NPROCESSORS,NO_GOF_PLOTS)
% SBPOPrunNONMEMproject: runs a specified NONMEM project
%
% Essentially this function is just a simple wrapper.
%
% USAGE:
% ======
% SBPOPrunNONMEMproject(projectPath)
% SBPOPrunNONMEMproject(projectPath,NPROCESSORS)
% SBPOPrunNONMEMproject(projectPath,NPROCESSORS,NO_GOF_PLOTS)
%
% projectPath:   path to the .nmctl NONMEM project file
% NPROCESSORS:   Number of processors if use of parallel (default: 1)
% NO_GOF_PLOTS:             =0: Create GoF plots for all runs (default), =1: No Gof plots
%
% Control NONMEM run from commandline:
% ====================================
% CTRL-J: Console iteration printing on/off 
% CTRL-K: Exit analysis at any time, which completes its output, and goes
%         on to next mode or estimation method
% CTRL-E: Exit program gracefully at any time
% CTRL-T: Monitor the progress of each individual during an estimation by
%         toggling ctrl-T. Wait 15 seconds or more to observe a subject’s
%         ID, and individual objective function value. It is also good to
%         test that the problem did not hang if a console output had not
%         been observed for a long while

% Information:
% ============
% Copyright (C) 2012 Novartis Pharma AG
% 
% This program is Free Open Source Software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program. If not, see <http://www.gnu.org/licenses/>.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Change in to project path 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
oldpath = pwd;
cd(projectPath);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Handle variable input arguments
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if nargin == 1,
    NPROCESSORS = 1;
    NO_GOF_PLOTS = 0;
elseif nargin == 2,
    NO_GOF_PLOTS = 0;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Load information about the MONOLIX PATH
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[PATH_MONOLIX,PATH_NONMEM,PATH_NONMEM_PAR] = getNLMEtoolInfoSBPOP();
if isempty(PATH_NONMEM) && NPROCESSORS==1,
    error('Path to NONMEM executable not defined in SETUP_PATHS_TOOLS.m');
end
if isempty(PATH_NONMEM_PAR) && NPROCESSORS>1,
    error('Path to NONMEM parallel executable not defined in SETUP_PATHS_TOOLS.m');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Run NONMEM
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if NPROCESSORS == 1,
    eval(sprintf('!%s project.nmctl project.nmlog',PATH_NONMEM));
else
    eval(sprintf('!%s %d project.nmctl project.nmlog',PATH_NONMEM_PAR,NPROCESSORS));
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Change back to old path
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
cd(oldpath);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Cleanup
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
try
    cleanNONMEMprojectFolderSBPOP(projectPath);
catch
    cd(oldpath);
    error('NONMEM run created a problem. Please check.');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Postprocess ...
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
try
    SBPOPplotConvergenceNONMEM(projectPath)
	close all
catch
    disp('Problem with plotting');
    disp(lasterr);    
end
try
    SBPOPreportNONMEMresults(projectPath)
catch
    disp('Problem with reporting');
    disp(lasterr);    
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Generate information for GOF plots
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

try
    PROJECTINFO     = parseProjectHeaderNONMEMSBPOP(projectPath);
    
    % outputNumber: Defined by metadata "OUTPUTS"
    outputNumberALL = [1:length(PROJECTINFO.OUTPUTS)];
    outputNamesALL  = PROJECTINFO.OUTPUTS;
    
    % data:
    oldpath         = pwd();
    cd(projectPath)
    data            = SBPOPloadCSVdataset(PROJECTINFO.DATA{1});
    cd(oldpath)
    
    % covNames:
    covNames        = PROJECTINFO.COVNAMES;
    
    % catNames:
    catNames        = PROJECTINFO.CATNAMES;
catch
    warning('Problem with obtaining information for GOF plots.');
    disp(lasterr);    
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Do GOF plots
% For each output one folder in RESULTS
% If not switched off
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if ~NO_GOF_PLOTS,
    try
        for k=1:length(outputNumberALL),
            outputNumber            = outputNumberALL(k);
            options                 = [];
            options.basefilename    = sprintf([projectPath '/RESULTS/GOF_OUTPUT_%d_%s/GOF_%d_%s_'],outputNumber,outputNamesALL{k},outputNumber,outputNamesALL{k});
            SBPOPfitanalysisGeneralPlots(outputNumber,data,projectPath,covNames,catNames,options)
        end
    catch
        warning('Problem with GOF plots.');
        disp(lasterr);
    end
end
