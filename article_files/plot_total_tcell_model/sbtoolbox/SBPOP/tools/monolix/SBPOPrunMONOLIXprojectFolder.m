function [] = SBPOPrunMONOLIXprojectFolder(modelProjectsFolder,N_PROCESSORS_PAR,N_PROCESSORS_SINGLE,NO_GOF_PLOTS)
% [DESCRIPTION]
% This functions runs all the Monolix projects in the specified folder.
% Parallel computation is supported in two different ways. Parallel execution 
% of models at the same time (par for loop) and also allowing each single model
% run to be parallelized (if the MONOLIX installation allows for it).
%
% [SYNTAX]
% [] = SBPOPrunMONOLIXprojectFolder(modelProjectsFolder)
% [] = SBPOPrunMONOLIXprojectFolder(modelProjectsFolder,N_PROCESSORS)
% [] = SBPOPrunMONOLIXprojectFolder(modelProjectsFolder,N_PROCESSORS,N_PROCESSORS_SINGLE)
% [] = SBPOPrunMONOLIXprojectFolder(modelProjectsFolder,N_PROCESSORS,N_PROCESSORS_SINGLE,NO_GOF_PLOTS)
%
% [INPUT]
% modelProjectsFolder:      Path to a folder with Monolix project folders
%                           to be run. Folder names are arbitrary, but a
%                           project.mlxtran file needs to be present in
%                           each folder.
% N_PROCESSORS_PAR:         Number of processors for parallel model evaluation (default: 1)
% N_PROCESSORS_SINGLE:      Number of processors for parallelization of single model run (default: 1)
% NO_GOF_PLOTS:             =0: Create GoF plots for all runs (default), =1: No Gof plots
%
% If N_PROCESSORS_PAR>1 then parallel nodes are requested via the matlabpool
% command and N_PROCESSORS_PAR models will be run in parallel.
%
% [OUTPUT]
% No output! The function just runs the Monolix projects. All results are
% written by Monolix to the relevant output folders ("RESULTS").
%
% [AUTHOR]
% Henning Schmidt, henning.schmidt@novartis.com
%
% [DATE]
% 2nd March, 2013

% Information:
% ============
% Copyright (c) 2012 Novartis Pharma AG
% 
% This program is Free Open Source Software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program. If not, see <http://www.gnu.org/licenses/>.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Handle variable input arguments
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if nargin == 1,
    N_PROCESSORS_PAR    = 1;
    N_PROCESSORS_SINGLE = 1;
    NO_GOF_PLOTS = 0;
elseif nargin == 2,
    N_PROCESSORS_SINGLE = 1;
    NO_GOF_PLOTS = 0;
elseif nargin == 3,
    NO_GOF_PLOTS = 0;
end
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Get the projects to run in the folder
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
projects = dir([modelProjectsFolder '/*']);
% Remove . and ..
ix_dot = strmatchSB('.',{projects.name});
projects(ix_dot) = [];
% Remove files
projects(find(~[projects.isdir])) = [];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Request processors
% Request min(N_PROCESSORS,length(projects))
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
N_PROCESSORS_NEEDED = min(N_PROCESSORS_PAR,length(projects));

killMATLABpool = 0;
if N_PROCESSORS_NEEDED>1,
    try
        if matlabpool('size') == 0,
            eval(sprintf('matlabpool %d',N_PROCESSORS_NEEDED));
            killMATLABpool = 1;
        end
    catch
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Run the models
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
warning off
oldpath = pwd();
parfor k=1:length(projects),
    fprintf('Running project %d of %d ...\n',k,length(projects));
    pathfolder = [modelProjectsFolder '/' projects(k).name];
    if isMONOLIXfitSBPOP(pathfolder),
        SBPOPrunMONOLIXproject(pathfolder,N_PROCESSORS_SINGLE,NO_GOF_PLOTS);
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Release processors
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
try
    if matlabpool('size')>1 && killMATLABpool==1,
        matlabpool close
    end
catch
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Done!
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fprintf('\nEstimations READY!\n\n');
