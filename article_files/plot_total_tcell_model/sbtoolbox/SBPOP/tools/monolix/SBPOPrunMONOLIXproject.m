function SBPOPrunMONOLIXproject(projectPath,N_PROCESSORS,NO_GOF_PLOTS)
% SBPOPrunMONOLIXproject: runs a specified Monolix project
%
% Essentially this function is just a simple wrapper.
%
% It requires the path to the standalone version of MONOLIX!
%
% USAGE:
% ======
% SBPOPrunMONOLIXproject(projectPath)
% SBPOPrunMONOLIXproject(projectPath,N_PROCESSORS)
% SBPOPrunMONOLIXproject(projectPath,N_PROCESSORS,NO_GOF_PLOTS)
%
% projectPath:              path to the .mlxtran Monolix project file
% NPROCESSORS:              Number of processors if use of parallel (default: 1)
%                           This argumentis not used yet!
% NO_GOF_PLOTS:             =0: Create GoF plots for all runs (default), =1: No Gof plots

% Information:
% ============
% Copyright (c) 2012 Novartis Pharma AG
% 
% This program is Free Open Source Software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program. If not, see <http://www.gnu.org/licenses/>.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Handle variable input arguments
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if nargin == 1,
    NPROCESSORS = 1;
    NO_GOF_PLOTS = 0;
elseif nargin == 2,
    NO_GOF_PLOTS = 0;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Change in to project path 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
oldpath = pwd;
cd(projectPath);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Load information about the MONOLIX PATH
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[PATH_MONOLIX,PATH_NONMEM,PATH_NONMEM_PAR] = getNLMEtoolInfoSBPOP();
if isempty(PATH_MONOLIX),
    error('Path to MONOLIX standalone version not defined in SETUP_PATHS_TOOLS.m');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Run the MONOLIX project
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if isunix,
    system([PATH_MONOLIX ' -p ./project.mlxtran -nowin -f run']);
else
    fullProjectPath = pwd();
    PATH_MONOLIX_BAT = fileparts(PATH_MONOLIX);
    cd(PATH_MONOLIX_BAT);
    systemcall = sprintf('Monolix.bat -p "%s/project.mlxtran" -nowin -f run',fullProjectPath);
    system(systemcall);
    cd(fullProjectPath);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Convert the results.ps to results.pdf
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
try
    convert2pdfSBPOP('RESULTS/results.ps')
catch
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Generate information for GOF plots
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

try
    PROJECTINFO     = parseProjectHeaderMONOLIXSBPOP('.');
    
    % outputNumber: Defined by metadata "OUTPUTS"
    outputNumberALL = [1:length(PROJECTINFO.OUTPUTS)];
    outputNamesALL  = PROJECTINFO.OUTPUTS;
    
    % data:
    data            = SBPOPloadCSVdataset(PROJECTINFO.DATA{1});
    
    % covNames:
    covNames        = PROJECTINFO.COVNAMES;
    
    % catNames:
    catNames        = PROJECTINFO.CATNAMES;
catch
    warning('Problem with obtaining information for GOF plots.');
    disp(lasterr);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Do GOF plots
% For each output one folder in RESULTS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if ~NO_GOF_PLOTS,
    try
        for k=1:length(outputNumberALL),
            outputNumber            = outputNumberALL(k);
            options                 = [];
            options.basefilename    = sprintf(['./RESULTS/GOF_OUTPUT_%d_%s/GOF_%d_%s_'],outputNumber,outputNamesALL{k},outputNumber,outputNamesALL{k});
            SBPOPfitanalysisGeneralPlots(outputNumber,data,'.',covNames,catNames,options)
        end
    catch
        warning('Problem with GOF plots.');
        disp(lasterr);        
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Change back to previous path
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
cd(oldpath)



