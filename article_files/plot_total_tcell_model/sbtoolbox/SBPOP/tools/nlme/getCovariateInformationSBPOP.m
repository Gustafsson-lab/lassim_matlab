function [covNames,catNames,referenceCAT] = getCovariateInformationSBPOP( projectPath )
% [NAME]
% getCovariateNamesSBPOP
%
% [DESCRIPTION]
% Returns the names of the categorical and continuous covariates that are
% used in a NONMEM or MONOLIX model that is defined by the "projectPath"
% input argument. Additionally, it returns the values that are used as
% reference categories for the categorical covariates (identified by a
% covariate coefficient of "0").
% 
% [SYNTAX]
% [covNames,catNames] = getCovariateNamesSBPOP( projectPath )
%
% [INPUT]
% projectPath: path to the NLME project folder. NONMEM and MONOLIX projects
%              need to have been generated with SBPOP.
%
% [OUTPUT]
% covNames:     Cell-array with names of continuous covariates used in the model
% catNames:     Cell-array with names of categorical covariates used in the model
% referenceCAT: Vector with reference values for the categorical covariates in catNames
%
% [AUTHOR]
% Henning Schmidt 
%
% [DATE]
% 25.08.2014

% Information:
% ============
% Copyright (c) 2012 Novartis Pharma AG
% 
% This program is Free Open Source Software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program. If not, see <http://www.gnu.org/licenses/>.

if isempty(projectPath),
    covNames = {};
    catNames = {};
    referenceCAT = [];
    return
end

% Handle NONMEM or MONOLIX
if isNONMEMfitSBPOP(projectPath),
    x = parseNONMEMresultsSBPOP(projectPath);
    y = sampleNONMEMpopulationParametersSBPOP(x,0,1);
elseif isMONOLIXfitSBPOP(projectPath),
    x = parseMONOLIXresultsSBPOP(projectPath);
    y = sampleMONOLIXpopulationParametersSBPOP(x,0,1);
else
    error('Wrong project path? Not an NLME project!');
end

% Get covariate names in cell arrays
covNames = unique([y.covariates.continuous.covariates]);
catNames = unique([y.covariates.categorical.covariates]);

% Get reference values for categorical covariates
referenceCAT = NaN(1,length(catNames));

for k=1:length(catNames),
    for k2=1:length(y.covariates.categorical),
        z = y.covariates.categorical(k2);
        % Check if covariate present
        ix = strmatchSB(catNames{k},z.covariates,'exact');
        if ~isempty(ix),
            values = z.information(ix).values;
            ix0 = find(values==0);
            reference = z.information(ix).categories(ix0(1));
            referenceCAT(k) = reference;
        end
    end
end
    
    
