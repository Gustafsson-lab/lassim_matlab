function [cost]=knock_fun(parameters, plot)
%% knock_fun
%
% A function that returnes the number of missed knocks based on a parameter
% vector
%

%% Variable declaration
global model
global param_names
global data_mean

data = data_mean; 

%% Variable declarations
sim_options                 = [];
sim_options.method          = 'stiff';
sim_options.maxnumsteps     = 1e8;

% The data from the knocks
meas_knock_data = ([-0.0629644616826732 -0.103518029533163 -0.0112601118596944 0.0103334015118748 0.385914941161763 0.0130570305329434 -0.144188540454513 ...
    -0.0808186784856657 0.939955618385281 0.00562478404745326 -0.466282583946805 -0.0379249060868032;-0.00387240842624259 -0.781832311611693 -0.100059109519814 ...
    -0.0455647663126699 -0.163856099130436 -0.324003955996035 -0.0544766307663589 -0.161927170153502 -0.349024899174864 0.0107225729828397 -0.340107399265324 ...
    0.00314014144673824;0.0147517671410482 -0.637667548996787 -0.0695949311874027 4.17047852598884e-05 -0.642306642518144 -0.615418871195019 -0.162256705691552 ...
    -0.647768530415412 0.459886127299471 0.00957049661508513 0.314788304281217 0.0140478052873900;-0.0238792986135631 -0.652993407267175 -0.0321850066277305 ...
    -0.147689107808936 -0.292866140757413 -0.716150975440056 -0.0511795436750508 -0.411272860799260 -0.478895706704823 -0.0154198311246870 -0.986278748136225 ...
    -0.0234082013515310;0.0382322739762759 -0.852949047426104 0.00274916051478069 0.0251835616336602 -0.908481260383104 -0.834442288687814 -0.197894771251459 ...
    -0.847346971332516 0.923896909076850 0.0171617789544525 -0.249489460858527 -0.00233594178050778;-0.0125789428916400 2 0.200471252188615 0.00123831035276467 ...
    1.87857246513710 -0.997602896946623 -0.0233673304956679 2 -0.991546252369602 -0.0141599389384889 -0.996807039980465 -0.0157617119576720;0.0323985400856177 ...
    -0.986091597818622 -0.0167802413697767 0.0325377211989792 -0.984865229456539 -0.983344097550467 -0.446087141972385 0.157613848416005 2 0.00385216404363842 ...
    -0.852804238646906 0.0161126287792943;-0.0507377794147915 -0.0963867345332556 -0.00860841357989439 0.0241929125581553 0.000203408283434570 0.0945591079606940 ...
    0.0265625176726376 -0.780927925754947 -0.102142217757595 0.00181807438886783 -0.0933482484811649 -0.0465066868646499;-0.00321082856743604 -0.642993314625959 ...
    0.0212260687542436 0.000712685404083757 -0.999267016298034 -0.834638960744190 -0.0602799737736812 -0.998893758052566 -0.193068483877446 0.00518052973385830 ...
    -0.0759845482111854 -0.0157731889870612;-0.0240987917453924 2 -0.00392183158748882 -0.0111801188504005 -0.218116750782263 2 -0.0210121968876288 2 0.304260594048405 ...
    -0.0872849544642798 -0.208758945401686 -0.0300408151312301;-0.0404039840697039 -0.384558238410848 -0.0437275254102199 -0.00137298512310235 -0.343345675636937 ...
    -0.289324908146543 -0.0573395605444285 -0.364677865149891 0.0878994350231792 -5.76073636933128e-05 -0.689625512058475 -0.0426740345147894;-0.0158647817798865 ...
    -0.445875301026121 0.00665087653282970 -0.0331297721081324 0.202208491601483 -0.388298061290647 0.0141090021928811 -0.989002426606203 -0.346167079195244 ...
    0.000404882651488414 2 -0.0947095039845287]);

% Get how much the lambdas should be increased
inhibition_param =(1./(diag(meas_knock_data)+1)).*ones(12,1);
sim_fold_change = zeros(12);
%% Control
% These are the reference simulations
sim                         = SBPDsimulate(model,[0 24], data(:,1),param_names,parameters,sim_options);
simulation_control24         = sim.statevalues(end,:)';

sim                         = SBPDsimulate(model,[0 12], data(:,1),param_names,parameters,sim_options);
simulation_control12         = sim.statevalues(end,:)';

%% Simulation of COPEB knock
sim                         = SBPDsimulate(model,[0 24], data(:,1),param_names,[inhibition_param(1)*parameters(1) parameters(2:end)],sim_options);
simulationCOPEB              = sim.statevalues(end,:)';
sim_fold_change(:,1)           = simulationCOPEB./simulation_control24;

%% Simulation of ELK1 knock
sim                         = SBPDsimulate(model,[0 24], data(:,1),param_names,[parameters(1) inhibition_param(2)*parameters(2) parameters(3:end)],sim_options);
simulationELK1              = sim.statevalues(end,:)';
sim_fold_change(:,2)           = simulationELK1./simulation_control24;
%% Simulation of GATA3 knock
sim                         = SBPDsimulate(model,[0 24], data(:,1),param_names,[parameters(1:2) inhibition_param(3)*parameters(3) parameters(4:end)],sim_options);
simulationGATA3             = sim.statevalues(end,:)';
sim_fold_change(:,3)           = simulationGATA3./simulation_control24;
%% Simulation of IRF4 knock
sim                         = SBPDsimulate(model,[0 12], data(:,1),param_names,[parameters(1:3) inhibition_param(4)*parameters(4) parameters(5:end)],sim_options);
simulationIRF4              = sim.statevalues(end,:)';
sim_fold_change(:,4)           = simulationIRF4./simulation_control12;
%% Simulation of JUN knock
sim                         = SBPDsimulate(model,[0 24], data(:,1),param_names,[parameters(1:4) inhibition_param(5)*parameters(5) parameters(6:end)],sim_options);
simulationJUN               = sim.statevalues(end,:)';
sim_fold_change(:,5)           = simulationJUN./simulation_control24;
%% Simulation of MAF knock
sim                         = SBPDsimulate(model,[0 24], data(:,1),param_names,[parameters(1:5) inhibition_param(6)*parameters(6) parameters(7:end)],sim_options);
simulationMAF               = sim.statevalues(end,:)';
sim_fold_change(:,6)        = simulationMAF./simulation_control24;
%% Simulation of MYB knock
sim                   = SBPDsimulate(model,[0 24], data(:,1),param_names,[parameters(1:6) inhibition_param(7)*parameters(7) parameters(8:end)],sim_options);
simulationMYB         = sim.statevalues(end,:)';
sim_fold_change(:,7)      = simulationMYB./simulation_control24;
%% Simulation of NFATC3 knock
sim                         = SBPDsimulate(model,[0 24], data(:,1),param_names,[parameters(1:7) inhibition_param(8)*parameters(8) parameters(9:end)],sim_options);
simulationNFATC3            = sim.statevalues(end,:)';
sim_fold_change(:,8)        = simulationNFATC3./simulation_control24;
%% Simulation of NFKB1 knock
sim                         = SBPDsimulate(model,[0 24], data(:,1),param_names,[parameters(1:8) inhibition_param(9)*parameters(9) parameters(10:end)],sim_options);
simulationNFKB1             = sim.statevalues(end,:)';
sim_fold_change(:,9)        = simulationNFKB1./simulation_control24;
%% Simulation of RELA knock
sim                         = SBPDsimulate(model,[0 24], data(:,1),param_names,[parameters(1:9) inhibition_param(10)*parameters(10) parameters(11:end)],sim_options);
simulationRELA              = sim.statevalues(end,:)';
sim_fold_change(:,10)           = simulationRELA./simulation_control24;
%% Simulation of STAT3 knock
sim                         = SBPDsimulate(model,[0 24], data(:,1),param_names,[parameters(1:10) inhibition_param(11)*parameters(11) parameters(12:end)],sim_options);
simulationSTAT3             = sim.statevalues(end,:)';
sim_fold_change(:,11)       = simulationSTAT3./simulation_control24;
%% Simulation of USF2 knock
sim                         = SBPDsimulate(model,[0 24], data(:,1),param_names,[parameters(1:11) inhibition_param(12)*parameters(12) parameters(13:end)],sim_options);
simulationUSF2             = sim.statevalues(end,:)';
sim_fold_change(:,12)          = simulationUSF2./simulation_control24;

%% Cost calculation
% quantitative_analysis is a vector containing all the measured knocks.
% Handle this in the same was as data has been handled
sim_fold_change = sim_fold_change-1;
sim_fold_change(sim_fold_change>2)=2;
sim_fold_change = sim_fold_change*lscov(sim_fold_change(:),meas_knock_data(:));
cost = sum(sum((meas_knock_data-sim_fold_change).^2));

%% Plot
if nargin>1 && plot == 1
    TF_name={'COPEB'
        'ELK1'
        'GATA3'
        'IRF4'
        'JUN'
        'MAF'
        'MYB'
        'NFATC3'
        'NFKB1'
        'RELA'
        'STAT3'
        'USF2'};
    figure()
    colormap('jet')
    imagesc((meas_knock_data-sim_fold_change).^2)
    colorbar
    set(gca, 'YTick',1:12, 'YTickLabel', TF_name(end:-1:1));
    set(gca, 'XTick',1:12, 'XTickLabel', TF_name) 
end
end
%%