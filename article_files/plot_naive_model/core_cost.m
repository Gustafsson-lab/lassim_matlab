%% A function that calculates the fit of a model with respect to the parameter vector "parameters".
%
% Created 150527

function [cost] = core_cost(parameters, logic_plot)
%% Variable declaration
global model
global param_names
global data_mean
global sigma_new
global time_new
global edges

parameters_expanded=zeros(1,75-12);
parameters_expanded(edges)=parameters(25:end);
parameters=[parameters(1:24) parameters_expanded];
%% Simulations
try
    sim_options                 = [];
    sim_options.method          = 'stiff';
    sim_options.maxnumsteps     = 1e8;
    
    % Do the numerical integration
    sim                     = SBPDsimulate(model,time_new, data_mean(:,1),param_names,parameters,sim_options);
    SimVal                  = sim.statevalues';
    
    % Handle in the same way as the data
    for i=1:12
        SimVal(i,:)=SimVal(i,:)./max(SimVal(i,:));%
    end
    
    % Calculate the cost
    cost=0;
    cost=cost+sum(sum(((data_mean-SimVal)./sigma_new).^2));
    cost=cost+((knock_fun(parameters))); %Also add the cost of the knocks
catch
    cost=inf;
    return
end

%% Do a plot
if nargin>1 
    if logic_plot==1
        close all
        knock_fun(parameters,1);
        simP1                     = SBPDsimulate(model,time_new(end), data_mean(:,1),param_names,parameters,sim_options);
        SimValP1                  = simP1.statevalues';
        for i=1:12
            SimValP1(i,:)=SimValP1(i,:)./max(SimValP1(i,:));
        end
        
        names=simP1.states;
        figure()
        for i=1:12
            subplot(3,4,i)
            hold on
            errorbar(time_new, data_mean(i,:),sigma_new(i,:), '*r','Linewidth', 1)
            plot(simP1.time, SimValP1(i,:),'-b', 'Linewidth', 2);
            ylim([-0.1 1.1])
            title(names(i))
            xlim([-1 (max(time_new)+1)])
            hold off
        end
        for j=1:3
            subplot(3,4,((j-1)*4)+1)
            xlabel('Time, h');
            ylabel('Response, a.u.')
        end
    end
    
end
end

