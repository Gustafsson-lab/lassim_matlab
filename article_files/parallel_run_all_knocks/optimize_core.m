function []=optimize_core(suffix_of_file)
%% Variable declaration
global model
global param_names
global sigma
global time
global edges
global data_mean

load data

edges=true(1,75-12);
%% Build the model
model         = strcat('CoreModel',num2str(suffix_of_file));
modelName     = SBmodel('CoreModel.txt');
SBPDmakeMEXmodel(modelName,model);
[param_names,start_guess] = SBparameters(modelName);


%%Knock
%%
start_guess=[0.0981455743234426 0.0636148888714481 0.00903984379891179 0.0589540074636830 18.8033452791367 0.0166051572414702 0.000176404659618044 1.03221656660800 0.728334717706295 2.06547107764205 12.6411458119668 5.46728284720036 0.0284600419585261 0.193673452802649 0.0271669360810143 13.9342541637353 3.83338317703518 0.113566646639346 0.00559066697504285 0.607706895193253 1.19494203785322 17.7585922921552 10.3813593416021 4.65856815802044 0.00188798373634433 0.00265196323716963 0.00247904746149072 0.00149496927855761 0.00344344928112015 0.00207055657595862 0.000802982628847357 -0.0230626992565181 0.369610532753544 0.00163343794270900 -0.217139918749890 8.68871642059989 0.00218672270343106 0.00231332434096426 0.00246355697100262 -1.11555471516757 -19.3372038907714 0.00251820839626219 -0.00162667101060758 -0.00125126665603414 0.00193284594043624 -18.9957882468817 0.00199049486932509 2.64047813207404 20 -0.0181914896656509 0.00254609960885328 0.00282469417183857 0.00187780023196894 0.00135553267327794 -0.00254041933169189 0.00267317385710059 18.6733854640531 0.00197350514876784 0.000723607264715266 0.00248905390901799 0.00210913555921218 -19.9929401277689 0.00242448701508741 8.99853760849919 -0.00196783240019893 -0.0259989340544224 -0.0497275260256093 -2.97672510896351 0.00266564662592831 2.20943370238223 0.000399426749470598 0.00197109228976113 0.00157787521895375 0.000985863583709303 0.00194975678005024 0.00124506679031356 -5.88544307411030 11.8906759100902 0.00187723106901938 1.60008898888793 -12.1840657799887 0.00175531250068331 -12.9557170231647 -0.834566318011530 -0.667562506376952 4.71145172071260 17.2985790189573]';
start_cost=core_cost(start_guess');
%% Optimization options
OPTIONS.tempstart           = start_cost*2;            % InitialTemp  %1e2*start_cost
OPTIONS.tempend             = 0.1;                        % EndTemp
OPTIONS.tempfactor          = 0.1;                      % tempfactor
OPTIONS.maxitertemp         = 10*length(start_guess);    % Max-iterations per temp
OPTIONS.maxitertemp0        = 10*length(start_guess);    % Max-iterations at temp0
OPTIONS.maxtime             = 10;                       % Max-time
OPTIONS.tolx                = 1e-10;                    % TolX
OPTIONS.tolfun              = 1e-10;                    % Tolfun
OPTIONS.MaxRestartPoints    = 0;                        % Number of parallel valleys which are searched through
OPTIONS.lowbounds           = [ zeros(24,1); -20*ones(size(start_guess(25:end)))];
OPTIONS.highbounds          = [ 20*ones(24,1); 20*ones(size(start_guess(25:end)))];
OPTIONS.outputFunction      = '';
OPTIONS.silent              = 1;

format long
format compact
%% Does the optimizaion
knocks=zeros(1,length(edges));
edges=true(size(start_guess(25:end)))';
solutions=zeros(75+12,length(edges));
costs=zeros(1,length(edges));


[optimized_param]         = simannealingSB(@core_cost,start_guess',OPTIONS);
for j=1:length(edges)
    for k=1%:6
        [optimized_param,optim_cost]         = simannealingSB(@core_cost,optimized_param',OPTIONS);
        OPTIONS.tempstart           = optim_cost*1.2;
    end
    costs(j)=core_cost(optimized_param);
    
    parameters_expanded=zeros(1,75-12);% Reconstructs param
    parameters_expanded(edges)=optimized_param(25:end);
    parameters_full=[optimized_param(1:24) parameters_expanded];
    solutions(:,j)=parameters_full;
    knocks(j)=knock_fun(parameters_full);

    abs_param=abs(parameters_full(25:end));
    abs_param(~edges)=inf;
    [~, indexOfLowest]=min(abs_param);
    edges(indexOfLowest)=false;
    
    OPTIONS.lowbounds           = [ zeros(24,1); -20*ones(sum(edges),1)];
    OPTIONS.highbounds          = [ 20*ones(24,1); 20*ones(sum(edges),1)];
    optimized_param=parameters_full([true(1,24) edges]);
end
cd results
save(strcat('results_of_edge_removal_from_',date,'_number_',num2str(suffix_of_file)),'knocks','solutions','costs')
cd ..
delete(strcat('CoreModel',num2str(suffix_of_file),'.mexw64'));
end
%
%
%
