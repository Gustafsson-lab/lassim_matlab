%% A function that calculates the fit of a model with respect to the parameter vector "parameters".
function [cost] = core_cost(parameters)
%% Variable declaration
global model
global param_names
global data_mean
global sigma
global time
global edges

parameters_expanded=zeros(1,75-12);
parameters_expanded(edges)=parameters(25:end);
parameters=[parameters(1:24) parameters_expanded];


%% Simulations
try
    sim_options                 = [];
    sim_options.method          = 'stiff';
    sim_options.maxnumsteps     = 1e8;
    
    sim                     = SBPDsimulate(model,time, data_mean(:,1),param_names,parameters,sim_options);
    SimVal                  = sim.statevalues';
    
    for i=1:12
        SimVal(i,:)=SimVal(i,:)./max(SimVal(i,:));
    end
    cost=0;
    cost=cost+sum(sum(((data_mean-SimVal)./sigma).^2));
    cost=cost+((knock_fun(parameters)));
catch
    cost=inf;
    return
end

end

