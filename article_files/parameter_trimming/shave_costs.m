% A function that takes the results from the optimization of parameters and
% handels the costs. A vector sent in will be returned strictely
% increasing.
% 
% Example:
% 
% a = [1 2 3 2 5];
% b = shave_costs(a)
% 
% b =
% 
%      1     2     2     2     5
%
% =========================================================================

function [ cost_shaved ] = shave_costs( costs )
% A function that shaves the costs to the lowest left value found to the
% right
cost_shaved=zeros(size(costs));
minimal_value=costs(end);
cost_shaved(end)=minimal_value;
for i=1:length(costs)-1
    if costs(end-i)>minimal_value
        cost_shaved(1:end-i)=minimal_value;
    else
        cost_shaved(end-i)=costs(end-i);
        minimal_value=costs(end-i);
    end
end
end

