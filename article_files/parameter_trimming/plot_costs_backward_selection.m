% This is a script that handles the results from the parallel forloop,
% where multiple tests of stepwise removal of edges were tested.
% 

load backwards_sel

figure()
subplot(2,2,1)
[~, index] = sort(median(set_to_zero));
ordered = set_to_zero(:,index);
boxplot(ordered)
grid on
grid minor
title('Iteration span of edge removal','FontSize',13)
xlabel('Edge (sorted)','FontSize',14,'FontWeight','bold')
ylabel('Iteration','FontSize',14,'FontWeight','bold')
set(gca, 'XTick',0:10:60, 'XTickLabel', 0:10:60)

subplot(2,2,3)
plot(cost_of_removal')
grid on
grid minor
title('Cost as a function of removed edges','FontSize',13)
xlabel('Iteration','FontSize',14,'FontWeight','bold')
xlim([0 64])
ylabel('Cost','FontSize',14,'FontWeight','bold')
set(gca, 'YTickLabel', '')
ax=gca;
%% Make the boxplot of set with knocks
cd results_with_knocks\
name_of_files = ls;
name_of_files = name_of_files(8:end-1,:);

set_to_zero = zeros(length(name_of_files(:,1)),length(solutions(25:end,1)));
cost_of_removal = zeros(length(name_of_files(:,1)),63);
for j = 1:length(name_of_files(:,1))
    load(name_of_files(j,:));
    set_to_zero(j,:)=sum(solutions(25:end,:)~=0,2);
    cost_of_removal(j,:) = costs/72; 
end
cd(original_dir)

for k = 1:length(name_of_files(:,1))
    cost_of_removal(k,:) = shave_costs(cost_of_removal(k,:));
end

subplot(2,2,2)
[~, index] = sort(median(set_to_zero));
ordered = set_to_zero(:,index);
boxplot(ordered)
grid on
grid minor
title('Iteration span of edge removal','FontSize',13)
xlabel('Edge (sorted)','FontSize',14,'FontWeight','bold')
ylabel('Iteration','FontSize',14,'FontWeight','bold')
set(gca, 'XTick',0:10:60, 'XTickLabel', 0:10:60)

subplot(2,2,4)
plot(cost_of_removal')
grid on
grid minor
xlim([0 64])
title('Cost as a function of removed edges','FontSize',13)
xlabel('Iteration','FontSize',14,'FontWeight','bold')
xlim([0 64])
ylabel('Cost','FontSize',14,'FontWeight','bold')
set(gca, 'YTickLabel', '')

figure()
[~, index] = sort(median(set_to_zero));
ordered = set_to_zero(:,index);
boxplot(ordered)
grid on

title('Iteration span of edge removal','FontSize',13)
xlabel('Edge (sorted)','FontSize',14,'FontWeight','bold')
ylabel('Iteration','FontSize',14,'FontWeight','bold')
set(gca, 'XTick',0:10:60, 'XTickLabel', 0:10:60)
