% This is a script that handles the results from the parallel forloop,
% where multiple tests of stepwise removal of edges were tested.


load cost_bootstrap

%% Normalize the new cost
cost_of_removal_b = cost_of_removal_b*(mean(cost_of_removal_b(:))/mean(cost_of_removal(:)))^-1;

% subplot(1,2,2)
hold on
p1 = plot(cost_of_removal_b','-r','LineWidth',0.5);
xlim([0 64])
title('Cost as a function of removed edges','FontSize',13)
xlabel('Iteration','FontSize',14,'FontWeight','bold')
xlim([0 64])
ylabel('Cost','FontSize',14,'FontWeight','bold')

hold on
p2 = plot(cost_of_removal', '-b','LineWidth',0.5);
xlim([0 64])
title('Cost as a function of removed edges','FontSize',13)
xlabel('Iteration','FontSize',14,'FontWeight','bold')
xlim([0 64])
ylabel('Cost','FontSize',14,'FontWeight','bold')
set(gca, 'YTickLabel', '')
cd(original_dir)

legend([p1(1), p2(1)],'Bootstrapped data','Control','Location','NorthWest')
