
%% Plot all
% This is a set of scripts that plots the solutions from LASSIM,
% together with the fit to data.

load('compiled_results','naive_data','costs')
naive_data = naive_data(isfinite(costs(1,:)),:);
costs = costs(:,isfinite(costs(1,:)));

[~, index] = sort(sum(costs)); %Find the index of the new groups
index = fliplr(index);
costs = sum(costs(:,index))'; % Arrange 'costs' correspondingly

%% Plot
figure()
red_color_map=zeros(100,3); % The map-variable is for the colour scale
red_color_map(:,1)=1;
red_color_map(:,2)=(100:-1:1)/100;
red_color_map(:,3)=(100:-1:1)/100;


sp1 = subplot(1,7,1:6);
colormap(sp1,'parula')
imagesc(naive_data(index,:))
set(gca, 'XTick',1:6, 'XTickLabel', [0 6 24 72 144 192])
set(gca, 'YTick',nan, 'YTickLabel', nan)
xlabel('Timepoint','FontSize',14)
sp2 = subplot(1,7,7);
imagesc(costs)
colormap(sp2,red_color_map)

set(gca, 'XTick',nan, 'XTickLabel', nan)
set(gca, 'YTick',nan, 'YTickLabel', nan)
xlabel('Cost','FontSize',14)
