
%% Plot all
% This is a set of scripts that plots the solutions from LASSIM,
% together with the fit to data.


%% Uncomment if other data than what's in 'compiled_results.mat'
% model         = ['core_model_expanded'];
% model_name     = SBmodel(('core_model_expanded.txt'));
% SBPDmakeMEXmodel(model_name,model);

%
%
% load results_LASSIM_160805.mat
%
% number_of_genes = length(entrez);
% costs = nan(6,number_of_genes);
% curves = nan(1001,number_of_genes);
% parfor i = 1:number_of_genes
%     try
%     [costs(:,i), curves(:,i)] = optimize_additional(entrez(i), LASSIM_solution(:,i));
%     disp(i)
%     catch
%         disp('Error')
%     end
% end

%% Do cluster
load('compiled_results_total')
total_tcell_data = double(ds.time_response);
costs = ds.total_cells_cost;

total_tcell_data = total_tcell_data(isfinite(costs),:);
costs = costs(isfinite(costs));

[~, index] = sort(costs); %Find the index of the new groups
index = fliplr(index')';
costs = (costs(index)); % Arrange 'costs' correspondingly

%% Plot
figure()
red_color_map=zeros(100,3); % The map-variable is for the colour scale
red_color_map(:,1)=1;
red_color_map(:,2)=(100:-1:1)/100;
red_color_map(:,3)=(100:-1:1)/100;


sp1 = subplot(1,7,1:6);
colormap(sp1,'parula')
imagesc(total_tcell_data(index,:))
set(gca, 'XTick',1:6, 'XTickLabel', [[0 6 48 96]])
set(gca, 'YTick',nan, 'YTickLabel', nan)
xlabel('Timepoint','FontSize',14)
sp2 = subplot(1,7,7);
imagesc(costs)
colormap(sp2,red_color_map)

set(gca, 'XTick',nan, 'XTickLabel', nan)
set(gca, 'YTick',nan, 'YTickLabel', nan)
xlabel('Cost','FontSize',14)

disp(strcat('The amount of genes that cannot be rejected is:', num2str(sum(costs<chi2inv(0.95,4))/length(costs))))