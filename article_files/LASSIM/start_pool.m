%% A script that starts the pool

%% Calculate the genes that are to be tested
load all_data
load is_regulated
genes_to_be_tested = 1:length(ordering.EntrezGene);
genes_to_be_tested=genes_to_be_tested(any(is_regulated,2));
%% Do the LASSIM
for i=1:length(genes_to_be_tested)
    run_number = genes_to_be_tested(i);
    try
        optimize_additional(run_number);
        disp(strcat('Gene index',{' '},num2str(run_number),{' '},'done...'))
    catch errorreport
        disp('Crash')
        disp(errorreport)
    end
end
exit
%% End of script