function [ data, sigma, time, knock_data, entrez, name] = load_data( gene_index, naive )
% Handels the data
% [ data, sigma, time ] = load_data( gene_index, naive )
%
% Where
%   * gene_index is the row corresponding to 'ordering' gene.
%   * naive is a logical determining weather the naive or total t cell meas
%     should be used
%
%% Loads data time series for naive T-cells
load all_data
%% Naive meas
if naive
    data = mean([double(ordering.x_naive_new(gene_index,1:6));double(ordering.x_naive_new(gene_index,7:12));double(ordering.x_naive_new(gene_index,13:18));double(ordering.x_naive_new(gene_index,19:24))]);
    data = data/max(data);
    sigma=std([double(ordering.x_naive_new(gene_index,1:6));double(ordering.x_naive_new(gene_index,7:12));double(ordering.x_naive_new(gene_index,13:18));double(ordering.x_naive_new(gene_index,19:24))]);
    time=tid.t_naive_new(1:6);
else
    data = mean([double(ordering.x_total_soren(gene_index4:7));double(ordering.x_total_soren(gene_index8:11));double(ordering.x_total_soren(gene_index12:15))]);
    data = data/max(data);
    sigma=std([double(ordering.x_total_soren(gene_index4:7));double(ordering.x_total_soren(gene_index8:11));double(ordering.x_total_soren(gene_index12:15))]);
    time=tid.t_total_soren(4:7);
end

knock_data = get_knock_data(ordering.EntrezGene(gene_index));
knock_data = knock_data-1;
knock_data(knock_data>2) = 2;

entrez = ordering.EntrezGene(gene_index);
name = ordering.GeneSymbol(gene_index);


end 
%% End of function