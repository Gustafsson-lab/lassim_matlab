
function [InhibParam]=get_knock_data(EntrezID)
% get_knock_data
%
% A function that returns a vector of

home_dir = cd;
cd knocks
load knock3TFsnew.mat 
load knock7TFs.mat     
load knockIRF4.mat         
load knock4newMLM.mat  
load knockETS1.mat     
load knockp65.mat   
cd(home_dir)


CoreKnocksRELA=p65(ismember(p65.Entrez, EntrezID),:);
RELAControl_tot_tcell_24h=CoreKnocksRELA.data(:,1:4);
RELAKnock_tot_tcell_24h=CoreKnocksRELA.data(:,5:8); 


CoreKnocksIRF4=IRF4(ismember(IRF4.Entrez, EntrezID),:);
IRF4Control_tot_tcell_12h=CoreKnocksIRF4.data(:,1:5); %The IRF4 knock was measured at 12 hours
IRF4Knock_tot_tcell_12h=CoreKnocksIRF4.data(:,6:10);

CoreKnocks7=knock7TFs(ismember(knock7TFs.Entrez, EntrezID),:);

Names7=CoreKnocks7.Entrez;
Control7_tot_tcell_24h=CoreKnocks7.data(:,1:8);
ELKknock_tot_tcell_24h=CoreKnocks7.data(:,9:12);
GATA3knock_tot_tcell_24h=CoreKnocks7.data(:,13:16);
JUNknock_tot_tcell_24h=CoreKnocks7.data(:,17:20);
MAFknock_tot_tcell_24h=CoreKnocks7.data(:,21:24);
NFATC3knock_tot_tcell_24h=CoreKnocks7.data(:,25:28);
NFKB1knock_tot_tcell_24h=CoreKnocks7.data(:,29:32);
STAT3knock_tot_tcell_24h=CoreKnocks7.data(:,33:36);

CoreKnocksNaive=knock3TFsnew(ismember(knock3TFsnew.Entrez, EntrezID),:);
KnockControlMYBGATA3=CoreKnocksNaive.x(:,[12 16 20 24 ]); %Uses 'MOCK' as a control
KnockvalMYB_naive_24h=CoreKnocksNaive.x(:,[9 13 17 21]);
KnockvalGATA3_naive_24h=CoreKnocksNaive.x(:,[11 15 19 21]);

KnockvalMAF_naive_24h=CoreKnocksNaive.x(:,[10 14 18 22]);
percentInhibNaiveMAF=mean(KnockvalMAF_naive_24h,2)./mean(KnockControlMYBGATA3,2); 

minimumDataPoint=min(min(knock4newMLM.data)); % The basal has been removed, is approximated with the lowest value
CoreKnocksWronglyExpressed=knock4newMLM(ismember(knock4newMLM.Entrez, EntrezID),:);
KnockControlUSF2_COPEB=CoreKnocksWronglyExpressed.data(:,(cell2mat(knock4newMLM_desc.type)=='0'))-minimumDataPoint; 
KnockvalUSF2=CoreKnocksWronglyExpressed.data(:,(cell2mat(knock4newMLM_desc.type)=='2'))-minimumDataPoint;           
KnockvalCOPEB=CoreKnocksWronglyExpressed.data(:,(cell2mat(knock4newMLM_desc.type)=='3'))-minimumDataPoint;         
indexUSF2_COPEB=ismember(EntrezID,knock4newMLM.Entrez);



Knockval                        = nan(1,12);
Knockval(indexUSF2_COPEB,1)     = mean(KnockvalCOPEB,2);
Knockval(2)           = mean(ELKknock_tot_tcell_24h,2);
Knockval(3)           = mean(KnockvalGATA3_naive_24h, 2); 
Knockval(4)           = mean(IRF4Knock_tot_tcell_12h,2);
Knockval(5)           = mean(JUNknock_tot_tcell_24h,2);
Knockval(6)           = mean(MAFknock_tot_tcell_24h,2);
Knockval(7)           = mean(KnockvalMYB_naive_24h,2);
Knockval(8)           = mean(NFATC3knock_tot_tcell_24h,2);
Knockval(9)           = mean(NFKB1knock_tot_tcell_24h,2);
Knockval(10)          = mean(RELAKnock_tot_tcell_24h,2);
Knockval(11)          = mean(STAT3knock_tot_tcell_24h,2);
Knockval(12)    = mean(KnockvalUSF2,2);

KnockControl                    = nan(length(EntrezID),12);
KnockControl(1) = mean(KnockControlUSF2_COPEB,2);
KnockControl(2)       = mean(Control7_tot_tcell_24h,2);
KnockControl(3)       = mean(KnockControlMYBGATA3,2);
KnockControl(4)       = mean(IRF4Control_tot_tcell_12h,2);
KnockControl(5)       = mean(Control7_tot_tcell_24h,2);
KnockControl(6)       = mean(Control7_tot_tcell_24h,2);
KnockControl(7)       = mean(KnockControlMYBGATA3,2);
KnockControl(8)       = mean(Control7_tot_tcell_24h,2);
KnockControl(9)       = mean(Control7_tot_tcell_24h,2);
KnockControl(10)      = mean(RELAControl_tot_tcell_24h,2);
KnockControl(11)      = mean(Control7_tot_tcell_24h,2);
KnockControl(12)= mean(KnockControlUSF2_COPEB,2);

percentInhib=Knockval./KnockControl;

InhibParam=percentInhib;
end

