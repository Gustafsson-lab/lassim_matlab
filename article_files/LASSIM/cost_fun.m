%% A function that calculates the fit of a model with respect to the parameter vector "parameters".
%
% Created 150527
% Created by Rasmus Magnusson

function [cost] = cost_fun(parameters)
%% Variable declaration
global model
global param_names
global edges
global data
global sigma
global time
global naive_sol
global knock_data
%% Set the compressed param vector to match the full parameter vector
[ parameters ] = restore_full_parameter_vector( parameters, naive_sol, edges );

%% Simulations
try
    sim_options                 = [];
    sim_options.method          = 'stiff';
    sim_options.maxnumsteps     = 1e8;
    
    simulation                  = SBPDsimulate(model,time, data(:,1),param_names,parameters,sim_options);
    sim_val                     = simulation.statevalues';

        sim_val(13,:) = sim_val(13,:)./max(sim_val(13,:));%

    cost = sum(((data(end,:)-sim_val(end,:))./mean(sigma(:))).^2);
    cost = cost + knock_fun(parameters)/3;

catch
    cost=inf;
    return
end
end

