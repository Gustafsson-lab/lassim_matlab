% function [genes] = select_genes_with_predicted_regulation()

load all_data
names = ordering.GeneSymbol;

is_regulated = zeros(length(names),12);

parfor i = 1:length(is_regulated)
    is_regulated(i,:) = get_core_interactions(names(i));
disp(i)
end
