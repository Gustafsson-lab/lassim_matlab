function [ is_used ] = get_core_interactions( gene_name )
% get_core_interactions 
% 
% This function returns a vector corresponding to the TFs that are predicted 
% to bind to the gene stated by the 'gene_name' input. 
%% Start of script
gene_name = upper(gene_name);
load predicted_core_to_gene_edges.mat
TFs_with_binding = all_TFs(ismember(all_TF_targets,gene_name));
TFs_with_binding(strcmp(TFs_with_binding,'KLF6')) = {'COPEB'};
TFs_with_binding = sort(TFs_with_binding);
TF_core={'COPEB';'ELK1';'GATA3';'IRF4';'JUN';'MAF';'MYB';'NFATC3';'NFKB1';'RELA';'STAT3';'USF2';};
is_used = ismember(TF_core, TFs_with_binding)';

if ~any(is_used)
   is_used(:) = true; 
end
end

