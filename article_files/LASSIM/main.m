%% main 
% This is the main script of LASSIM, as implemented in MATLAB.
% This script enables the algorithm to be run on a cluster

% install SBtoolbox
disp('Installing SBtoolbox')
 try
     restoredefaultpath
     current_directory = cd;
     cd sbtoolbox
     installSBPOPpackageInitial
     cd ..
 catch
     disp('Failed to install the SBtoolbox')
     cd(current_directory)
     restoredefaultpath
     addpath(genpath('sbtoolbox'))
 end

%Create the model
model         = ['core_model_expanded'];
model_name     = SBmodel(('core_model_expanded.txt'));
SBPDmakeMEXmodel(model_name,model);

% Submit a job of 500 cores
 job = batch('start_pool','pool',499); 
 exit