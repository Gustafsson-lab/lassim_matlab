function [ full_parameter_vector ] = restore_full_parameter_vector( parameters, naive_sol, edges )
% restore_full_parameter_vector
% This function takes a subset of parameters, i.e. the ones that are
% currently beeing optimized, and expands that vector to include all
% parameter elements that are to be included in the simulation. This is
% done in order let some edges be equal to zero, and not optimized.
% Furthermode, the parameters associated with the core-model should not be
% altered. 
%
% Created 160628
%
%
parameters_expanded = zeros(1,length(edges)); %The connections betwen the core model and the new state has a maximum of 'edges' long elements
parameters_expanded(edges) = parameters(3:end); % The interactions that are non zero, i.e. edges = 1, are assigned the active parameters 
parameters = [parameters(1:2) parameters_expanded]; % The new parameter vector is created, with the first two elements being the self-regulatory parameters (always non-zero)
full_parameter_vector = [naive_sol parameters]; % Naive_sol is the parameter solution corresponding to the core model
end

