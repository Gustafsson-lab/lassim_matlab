%% A script that solves one core-to-peripheral set of interactions

function [solutions, cost_of_step]=optimize_additional(gene_index )

%% Variable declaration
global model
global param_names
global sigma
global time
global edges
global data
global naive_sol
global knock_data


edges=true(1,12);

%% Get the data

try
    [additional_gene_data,additional_gene_std,time,knock_data,gene_entrez, gene_name] = load_data(gene_index, 1);
catch
    solutions = nan;
    cost_of_step = nan;
    disp('Data not_present')
    load all_data
    gene_entrez = ordering.EntrezGene(gene_index);
    save(strcat('results_number_', num2str(gene_index)),'solutions', 'cost_of_step','gene_entrez')
    return
end
load data_new
data = [data_mean; additional_gene_data];
sigma = [sigma_new; additional_gene_std];
%% Build the model
% model         = 'core_model_expanded';
model         = strcat('core_model_expanded');

[param_names] = SBparameters(model);

%% Set the chosen core model parameters
naive_sol = [0.0859775709786086 0.0614645114940208 0.00776806596364148 0.0655317810068344 19.7835890294585 0.0241739448046613 1.75169807169834e-05 1.07664480747446 0.734789342083222 1.16149584024346 12.2171758650986 5.39731920989453 0.0231782006104115 0.150055755462409 0.0263144509448575 13.6653055081299 3.80929074641520 0.147342455744122 0.00546270161966289 0.622639331107302 1.18783986455443 16.4886307447265 11.2052776878764 4.55246720551380 0 0.990796305525855 0 0 0 0 0 0 0.426213102736732 0 0 8.56415682456333 0 0 0.0678528703225236 -2.61261513765761 -19.9175066095241 0 0 0 0.0923881970061578 -19.9248656276523 0 2.19422218856815 20 0 -9.76666636139619e-05 0 0 0 0 5.91246109646829 20 0 0 0 0 -19.9571377590960 0 8.39766281794448 0 0 -0.171166566741348 -3.11960546487366 0 2.97911036202873 0 0 0 0 0 0 -6.25947780949389 10.6675845143744 0 0 -14.7746243133358 0 -14.8445309214718 0 0 2.70292230345614 17.6242436728099];

start_cost=cost_fun([1 1 zeros(1,12)]);
%% Optimization options
OPTIONS.tempstart           = start_cost*2;             % InitialTemp  %1e2*start_cost
OPTIONS.tempend             = 0.01;                    % EndTemp
OPTIONS.tempfactor          = 0.1;                      % tempfactor
OPTIONS.maxitertemp         = 8*length(edges);         % Max-iterations per temp
OPTIONS.maxitertemp0        = 8*length(edges);         % Max-iterations at temp0
OPTIONS.maxtime             = 2;                        % Max-time
OPTIONS.tolx                = 1e-10;                    % TolX
OPTIONS.tolfun              = 1e-10;                    % Tolfun
OPTIONS.MaxRestartPoints    = 0;                        % Number of parallel valleys which are searched through
OPTIONS.lowbounds           = [ zeros(2,1); -20*ones(12,1)];
OPTIONS.highbounds          = [ 20*ones(2,1); 20*ones(12,1)];
OPTIONS.outputFunction      = '';
OPTIONS.silent              = 1;

format long
format compact
%% Does the optimizaion
solutions=nan(length(naive_sol)+14,length(edges)+1);
cost_of_step=nan(1,length(edges)+1);


edges = get_core_interactions(gene_name);
predicted_bindings = get_core_interactions(gene_name);
[ full_parameter_solution ] = restore_full_parameter_vector( [true(1,2) edges(edges~=0)], naive_sol, edges );
OPTIONS.lowbounds           = [ zeros(2,1); -20*ones(sum(edges),1)];
    OPTIONS.highbounds          = [ 20*ones(2,1); 20*ones(sum(edges),1)];
    OPTIONS.maxitertemp         = 10*length(edges);    % Max-iterations per temp
    OPTIONS.maxitertemp0        = 10*length(edges);    % Max-iterations at temp0
    
    optimized_param=full_parameter_solution([true(1,2) edges]);
    
[optimized_param]         = simannealingSB(@cost_fun,optimized_param',OPTIONS);    
for k=1:2
    [optimized_param,optim_cost]         = simannealingSB(@cost_fun,optimized_param',OPTIONS);
    OPTIONS.tempstart           = optim_cost*1.2;
    
end
for j=1:sum(edges)
        [optimized_param,optim_cost]         = simannealingSB(@cost_fun,optimized_param',OPTIONS);
        OPTIONS.tempstart           = optim_cost*1.2;
    
    [ full_parameter_solution ] = restore_full_parameter_vector( optimized_param, naive_sol, edges );
    cost_of_step(j)=optim_cost;
    solutions(:,j) = full_parameter_solution;
    
    abs_param=abs(full_parameter_solution(end-length(edges)+1:end));
    abs_param(~edges)=inf;
    [~, indexOfLowest]=min(abs_param);
    edges(indexOfLowest)=false;
    
    OPTIONS.lowbounds           = [ zeros(2,1); -20*ones(sum(edges),1)];
    OPTIONS.highbounds          = [ 20*ones(2,1); 20*ones(sum(edges),1)];
    OPTIONS.maxitertemp         = 10*length(edges);    % Max-iterations per temp
    OPTIONS.maxitertemp0        = 10*length(edges);    % Max-iterations at temp0
    
    optimized_param=full_parameter_solution([true(1,2) edges]);
end

%% Calculate the null model
for k=1:2
    [optimized_param,optim_cost]         = simannealingSB(@cost_fun,optimized_param',OPTIONS);
    OPTIONS.tempstart           = optim_cost*1.2;
end
[ full_parameter_solution ] = restore_full_parameter_vector( optimized_param, naive_sol, edges );
cost_of_step(end)=cost_fun(optimized_param);
solutions(:,end) = full_parameter_solution;

save(strcat('results_number_', num2str(gene_index)),'solutions', 'cost_of_step','gene_entrez','predicted_bindings')

%
%
%
