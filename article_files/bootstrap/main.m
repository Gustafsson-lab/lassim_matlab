% This is a script that performs a bootstrab of 100 runs, with the core
% system cost calculations perturbed with weights belonging to [0,1].

for i = 1:100
    optimize_core(i)
end