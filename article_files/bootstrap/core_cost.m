%% A function that calculates the fit of a model with respect to the parameter vector "parameters".
%
% Created 150527
function [cost] = core_cost(parameters, logic_plot)
%% Variable declaration
global model
global param_names
global data_mean
global sigma_new
global time_new
global edges
global random_weights_ts
global random_weights_knocks

% Reconstruct the parameter set, to include 0 where an edge is removed
parameters_expanded=zeros(1,75-12);
parameters_expanded(edges)=parameters(25:end);
parameters=[parameters(1:24) parameters_expanded];


%% Simulations
try
    % options
    sim_options                 = [];
    sim_options.method          = 'stiff';
    sim_options.maxnumsteps     = 1e8;
    
    % numerical integration
    sim                     = SBPDsimulate(model,time_new, data_mean(:,1),param_names,parameters,sim_options);
    SimVal                  = sim.statevalues';
    
    % Handle results as data
    for i=1:12
        SimVal(i,:)=SimVal(i,:)./max(SimVal(i,:));%
    end
    
    % calculate cost (with bootstrap weights)
    cost=sum(sum((((data_mean-SimVal)./sigma_new).^2).*(random_weights_ts)./sum(random_weights_ts(:)))) ;
    cost=cost+knock_fun(parameters); % cost of knockfit with bootstrap weights
catch
    cost=inf;
    return
end

%% plot?
if nargin>1
    if logic_plot==1
        simP1                     = SBPDsimulate(model,time_new(end), data_mean(:,1),param_names,parameters,sim_options);
        SimValP1                  = simP1.statevalues';
        
        for i=1:12
            SimValP1(i,:)=SimValP1(i,:)./max([SimValP1(i,:)]);% SimValP2(i,:) SimValP3(i,:) SimValP4(i,:)]);
        end
        names=simP1.states;
        figure(1)
        for i=1:12
            subplot(3,4,i)
            hold on
            plot(time_new, data_mean(i,:), '*b','Linewidth', 2)
            plot(simP1.time, SimValP1(i,:),'-r', 'Linewidth', 2);
            title(names(i))
            xlabel('Time, h');
            xlim([-1 (max(time_new)+1)])
            ylabel('Response, a.u.')
            hold off
        end
    end
end
end

