function [log10p] = approximate_pvalues(z)
log10p =(.5*log(2*pi) - 0.5*z*z - log(z) + log(1 - z^-2 + 3*z^-4))/(log(10));
