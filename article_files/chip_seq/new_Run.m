
load('rank_of_removed.mat')
load('lassim_res.mat')
load('Chip_Seq_STM_mapped_to_protein_coding_genes.mat')

a= bsxfun(@ge,rank_removal,index) & isfinite(rank_removal);
p= isfinite(rank_removal);


lassim_gata3= entrez(a(:,find(ismember(rank_of_removed.desc,'GATA3'))));
predicted_edge_gata3= entrez(p(:,find(ismember(rank_of_removed.desc,'GATA3'))));
lassim_maf= entrez(a(:,find(ismember(rank_of_removed.desc,'MAF'))));
predicted_edge_maf= entrez(p(:,find(ismember(rank_of_removed.desc,'MAF'))));
lassim_myb= entrez(a(:,find(ismember(rank_of_removed.desc,'MYB'))));
predicted_edge_myb= entrez(p(:,find(ismember(rank_of_removed.desc,'MYB'))));

lassim_irf4= entrez(a(:,find(ismember(rank_of_removed.desc,'IRF4'))));
predicted_edge_irf4= entrez(p(:,find(ismember(rank_of_removed.desc,'IRF4'))));


for k=1:8
    [target_id{k},rank_tf{k}]=consolidator([ChipSeq.data{k}.EntrezId],[ChipSeq.data{k}.peakScore],@(x) sum(x,1));
    tmp = setdiff(entrez,target_id{k});
    target_id{k}(end+1:end+length(tmp)) = tmp;
    rank_tf{k}(end+1:end+length(tmp)) = 0;
    
    if k<3
        lassim=lassim_gata3;
        prior= predicted_edge_gata3;
    elseif k<6
        lassim=lassim_maf;
        prior= predicted_edge_maf;
    else
        lassim=lassim_myb;
        prior= predicted_edge_myb;
    end
[pv(k),~,z(k),fe(k),medel_lassim(k)] = Bootstrapping(log(1+rank_tf{k}(ismember(target_id{k},prior))),log(1+rank_tf{k}(ismember(target_id{k},lassim))),10000);
medel_prior(k)= mean(log(1+rank_tf{k}(ismember(target_id{k},prior)))); 
[pv2(k),~,z2(k),fe2(k),medel_lassim2(k)] = Bootstrapping(log(1+rank_tf{k}(ismember(target_id{k},entrez))),log(1+rank_tf{k}(ismember(target_id{k},lassim))),10000);
medel_prior2(k)= mean(log(1+rank_tf{k}(ismember(target_id{k},entrez)))); 
end
%



Chip1 = dataset('XLSFile','gata3_tbet.xls');
Chip1= Chip1(:,[2,5,6]);
Chip1(isfinite(Chip1.EntrezGeneID),:);
 tmp = setdiff(entrez,Chip1.EntrezGeneID);
 en = length(Chip1.EntrezGeneID);
 Chip1.EntrezGeneID(en+1:en+length(tmp)) = tmp;
 Chip1.NumberOfGATA3Sites_Th1_(en+1:en+length(tmp)) = 0;
 Chip1.NumberOfGATA3Sites_Th2_(en+1:en+length(tmp)) = 0;
 

[pv(9),yrand,z(9),fe(9),medel_lassim(9)] = Bootstrapping(log(1+Chip1.NumberOfGATA3Sites_Th1_(ismember(Chip1.EntrezGeneID,predicted_edge_gata3))),...
    log(1+Chip1.NumberOfGATA3Sites_Th1_(ismember(Chip1.EntrezGeneID,lassim_gata3))),10000);
medel_prior(9)= mean(log(1+Chip1.NumberOfGATA3Sites_Th1_(ismember(Chip1.EntrezGeneID,predicted_edge_gata3)))); 

[pv(10),yrand,z(10),fe(10),medel_lassim(10)] = Bootstrapping(log(1+Chip1.NumberOfGATA3Sites_Th2_(ismember(Chip1.EntrezGeneID,predicted_edge_gata3))),...
    log(1+Chip1.NumberOfGATA3Sites_Th2_(ismember(Chip1.EntrezGeneID,lassim_gata3))),10000);
medel_prior(10)= mean(log(1+Chip1.NumberOfGATA3Sites_Th2_(ismember(Chip1.EntrezGeneID,predicted_edge_gata3)))); 

medel_lassim./medel_prior

bar(-log10(pv))

texten= ChipSeq.desc.name;
texten{9}= 'NumberOfGATA3Sites_Th1_';
texten{10}= 'NumberOfGATA3Sites_Th2_';
save chipseq_plot texten pv medel_lassim medel_prior


irf4Chip = dataset('File',['if4_chipchip.txt'],'format','%f%s','Headerlines',1,'ReadVarNames',false,'Delimiter','\t');
type = irf4Chip.Var2;
upstream = false(size(type));
for k=1:length(type)
if ~isempty(strfind(type{k},'upstream'))
    upstream(k) = true;
end
end
irf4Chip = irf4Chip(upstream,:);
irf4Chip = irf4Chip(isfinite(irf4Chip.Var1),:);

irf4.entrez = entrez;
irf4.weights= 0*irf4.entrez ;
irf4.weights(ismember(irf4.entrez , irf4Chip.Var1))=1;

[pv(11),yrand,z(11),fe(11),medel_lassim(11)] = Bootstrapping(log(1+irf4.weights(ismember(irf4.entrez,predicted_edge_irf4))),...
    log(1+irf4.weights(ismember(irf4.entrez,lassim_irf4))),10000);
medel_prior(11)= mean(log(1+irf4.weights(ismember(irf4.entrez,predicted_edge_irf4)))); 



chip_stat3=dataset('XLSFile','NIHMS251352-supplement-Supplemental_2.xls');
chip_stat3.GeneName= upper(chip_stat3.GeneName);

load NCBI_with_ensembl.mat
NCBI=unique(NCBI(:,1:2));
NCBI.Properties.VarNames{1} = 'entrez';
NCBI.Properties.VarNames{2} = 'GeneName';
NCBI = NCBI(isfinite(NCBI.entrez),:);

chip_stat3 = join(NCBI,chip_stat3,'Keys','GeneName','MergeKeys',true,'Type','inner');
k=12;
[target_id{k},rank_tf{k}]=consolidator([chip_stat3.entrez],[chip_stat3.STAT3],@(x) sum(x,1));
tmp = setdiff(entrez,target_id{k});
target_id{k}(end+1:end+length(tmp)) = tmp;
rank_tf{k}(end+1:end+length(tmp)) = 0;
lassim= entrez(a(:,find(ismember(rank_of_removed.desc,'STAT3'))));
prior= entrez(p(:,find(ismember(rank_of_removed.desc,'STAT3'))));

[pv(k),~,z(k),fe(k),medel_lassim(k)] = Bootstrapping(log(1+rank_tf{k}(ismember(target_id{k},prior))),log(1+rank_tf{k}(ismember(target_id{k},lassim))),10000);
medel_prior(k)= mean(log(1+rank_tf{k}(ismember(target_id{k},prior)))); 

    


chip_rela=dataset('XLSFile','rela_chip.xlsx');
chip_rela = chip_rela(:,[8,4,5]);
chip_rela.Properties.VarNames{1} = 'GeneName';
chip_rela = join(NCBI,chip_rela,'Keys','GeneName','MergeKeys',true,'Type','inner');
k=13;
[target_id{k},rank_tf{k}]=consolidator([chip_rela.entrez],[chip_rela.RelADMSORPM],@(x) sum(x,1));
tmp = setdiff(entrez,target_id{k});
target_id{k}(end+1:end+length(tmp)) = tmp;
rank_tf{k}(end+1:end+length(tmp)) = 0;
lassim= entrez(a(:,find(ismember(rank_of_removed.desc,'RELA'))));
prior= entrez(p(:,find(ismember(rank_of_removed.desc,'RELA'))));

[pv(k),~,z(k),fe(k),medel_lassim(k)] = Bootstrapping(log(1+rank_tf{k}(ismember(target_id{k},prior))),log(1+rank_tf{k}(ismember(target_id{k},lassim))),10000);
medel_prior(k)= mean(log(1+rank_tf{k}(ismember(target_id{k},prior)))); 

k=14;
[target_id{k},rank_tf{k}]=consolidator([chip_rela.entrez],[chip_rela.RelABAYRPM],@(x) sum(x,1));
tmp = setdiff(entrez,target_id{k});
target_id{k}(end+1:end+length(tmp)) = tmp;
rank_tf{k}(end+1:end+length(tmp)) = 0;
lassim= entrez(a(:,find(ismember(rank_of_removed.desc,'RELA'))));
prior= entrez(p(:,find(ismember(rank_of_removed.desc,'RELA'))));

[pv(k),~,z(k),fe(k),medel_lassim(k)] = Bootstrapping(log(1+rank_tf{k}(ismember(target_id{k},prior))),log(1+rank_tf{k}(ismember(target_id{k},lassim))),10000);
medel_prior(k)= mean(log(1+rank_tf{k}(ismember(target_id{k},prior)))); 

% Fill in the missing text
texten{11} = 'IRF4, total T-cells';
texten{12} = 'STAT3, total T-cells';
texten{13} = 'RELA, RelADMSORPM, total T-cells';
texten{14} = 'RELA, RelABAYRPM, total T-cells';

% rearrange the p-values
p_val = nan(1,length(pv));
p_val(1:2) = pv(1:2);
p_val(3:5) = pv(9:11);
p_val(6:11) = pv(3:8);
p_val(12:13) = pv(13:14);
p_val(14) = pv(12);

% rearrage the text accordingly
text_of_ChIP{1} = texten{1};
text_of_ChIP{2} = texten{2};
text_of_ChIP{3} = texten{9};
text_of_ChIP{4} = texten{10};
text_of_ChIP{5} = texten{11};
text_of_ChIP{6} = texten{3};
text_of_ChIP{7} = texten{4};
text_of_ChIP{8} = texten{5};
text_of_ChIP{9} = texten{6};
text_of_ChIP{10} = texten{7};
text_of_ChIP{11} = texten{8};
text_of_ChIP{12} = texten{13};
text_of_ChIP{13} = texten{14};
text_of_ChIP{14} = texten{12};
text_of_ChIP = regexprep(text_of_ChIP, '_', ' ');

bar(-log10(p_val))
set(gca, 'XTickLabel', text_of_ChIP)


save chipseq_plot texten pv medel_lassim medel_prior




