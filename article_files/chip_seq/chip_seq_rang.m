% function [ number_of_targets ] = chip_seq_rang( used_tfs )
%% Calculate chip-seq rang
% A script that validates the chosen network in the removal. Input shout
%
% ========================================================================
%
% Created 160421
% COPYRIGHT Rasmus Magnusson
%
% ========================================================================

%% Core network and LASSIM results
% TF_Name={'COPEB'
%     'ELK1'  %declares the names of the TF:s
%     'GATA3'
%     'IRF4'
%     'JUN'
%     'MAF'
%     'MYB'
%     'NFATC3'
%     'NFKB1'
%     'RELA'
%     'STAT3'
%     'STAT6'
%     'USF2'};
%
% TF_Entrez=[1316
%     2002  %declares the entrez numbers of the TF:s
%     2625
%     3662
%     3725
%     4094
%     4602
%     4775
%     4790
%     5970
%     6774
%     6778
%     7392];

load results_LASSIM_160805.mat


%% Load the Chip-Seq data
load('Chip_Seq_STM_mapped_to_protein_coding_genes.mat')
[target_id_gata3,rank_gata3,z_gata3]=consolidator([ChipSeq.data{1}.EntrezId;ChipSeq.data{2}.EntrezId],[],'count'); %Gata3
[target_id_maf,rank_maf,z_maf]=consolidator([ChipSeq.data{3}.EntrezId;ChipSeq.data{4}.EntrezId;ChipSeq.data{5}.EntrezId],[],'count'); %MAF
[target_id_myb,rank_myb,z_myb]=consolidator([ChipSeq.data{6}.EntrezId;ChipSeq.data{7}.EntrezId;ChipSeq.data{8}.EntrezId],[],'count'); %MYB
%% Remove what's not in the measurement data from LASSIM, and set the genes that are not in the chip-seq to = 0
i = ismember(target_id_gata3,entrez);
target_id_gata3 = target_id_gata3(i);
rank_gata3 = rank_gata3(i);
ent2 = setdiff(entrez,target_id_gata3);
target_id_gata3(length(target_id_gata3)+1:length(target_id_gata3)+length(ent2)) = ent2;
rank_gata3(length(rank_gata3)+1:length(rank_gata3)+length(ent2)) = 0;

i=ismember(target_id_maf,entrez);
target_id_maf= target_id_maf(i);
rank_maf= rank_maf(i);
ent2= setdiff(entrez,target_id_maf);
target_id_maf(length(target_id_maf)+1:length(target_id_maf)+length(ent2))=ent2;
rank_maf(length(rank_maf)+1:length(rank_maf)+length(ent2))=0;

i=ismember(target_id_myb,entrez);
target_id_myb= target_id_myb(i);
rank_myb= rank_myb(i);
ent2= setdiff(entrez,target_id_myb);
target_id_myb(length(target_id_myb)+1:length(target_id_myb)+length(ent2))=ent2;
rank_myb(length(rank_myb)+1:length(rank_myb)+length(ent2))=0;

%% Randomize the solutions 1000 times
load predicted_edges.mat
predicted_edge_gata3 = predicted_edges(ismember(predicted_edges(:,1),2625),2);
predicted_edge_maf = predicted_edges(ismember(predicted_edges(:,1),4094),2);
predicted_edge_myb = predicted_edges(ismember(predicted_edges(:,1),4602),2);


number_of_targets_all_random = zeros(100,3);
for i =1:length(number_of_targets_all_random)
    number_of_LASSIM_predictions_gata3 = sum(optim_sol(3,:) ~=0);
    gata3_is_regulator = predicted_edge_gata3(randperm(length(predicted_edge_gata3),number_of_LASSIM_predictions_gata3));

    number_of_LASSIM_predictions_maf = sum(optim_sol(6,:) ~=0);
    maf_is_regulator = predicted_edge_maf(randperm(length(predicted_edge_maf),number_of_LASSIM_predictions_maf));

    number_of_LASSIM_predictions_myb = sum(optim_sol(7,:) ~=0);
    myb_is_regulator = predicted_edge_myb(randperm(length(predicted_edge_myb),number_of_LASSIM_predictions_myb));

    number_of_targets_gata3 = rank_gata3(ismember(target_id_gata3,gata3_is_regulator));
    number_of_targets_maf = rank_maf(ismember(target_id_maf,maf_is_regulator));
    number_of_targets_myb = rank_myb(ismember(target_id_myb,myb_is_regulator));
    
    number_of_targets_all_random(i,1:3) = [sum(number_of_targets_gata3), sum(number_of_targets_maf), sum(number_of_targets_myb)];
end
%% Test the solution chosen by LASSIM
load results_LASSIM_160805.mat
gata3_is_regulator = entrez(optim_sol(3,:) ~= 0);
maf_is_regulator = entrez(optim_sol(6,:) ~= 0);
myb_is_regulator = entrez(optim_sol(7,:) ~= 0);

number_of_targets_gata3=rank_gata3(ismember(target_id_gata3,gata3_is_regulator));
number_of_targets_maf=rank_maf(ismember(target_id_maf,maf_is_regulator));
number_of_targets_myb=rank_myb(ismember(target_id_myb,myb_is_regulator));

number_of_targets_all = [sum(number_of_targets_gata3), sum(number_of_targets_maf), sum(number_of_targets_myb)];
mean(bsxfun(@le,number_of_targets_all_random,number_of_targets_all))








