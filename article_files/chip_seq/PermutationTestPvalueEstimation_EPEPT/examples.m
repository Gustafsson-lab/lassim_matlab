% Some examples
%
% Theo Knijnenburg
% Institute for Systems Biology
%
% Jun 09 2009
%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Warranty Disclaimer and Copyright Notice
% 
% Copyright (C) 2003-2010 Institute for Systems Biology, Seattle, Washington, USA.
% 
% The Institute for Systems Biology and the authors make no representation about the suitability or accuracy of this software for any purpose, and makes no warranties, either express or implied, including merchantability and fitness for a particular purpose or that the use of this software will not infringe any third party patents, copyrights, trademarks, or other rights. The software is provided "as is". The Institute for Systems Biology and the authors disclaim any liability stemming from the use of this software. This software is provided to enhance knowledge and encourage progress in the scientific community. 
% 
% This is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation; either version 2.1 of the License, or (at your option) any later version.
% 
% You should have received a copy of the GNU Lesser General Public License along with this library; if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%% Pecdf
N = 1000;                                       %number of permutations
Pgoal = 1e-1;                                   %P_perm (correct P-value)
x0 = finv(1-Pgoal,5,10);                        %original statistic x0
y = frnd(5,10,N,1);                             %permutation values
M = sum(y>=x0);                                 %if M>=10, P_ecdf will be computed
Pest = Ppermest(x0,y);                          %P_ecdf       
[Pest,Pest_ci] = Ppermest(x0,y,0.05);           %P_ecdf with confidence bounds

%%  Pgpd
Pgoal = 1e-5;                                   %P_perm (correct P-value)
x0 = finv(1-Pgoal,5,10);                        %original statistic x0
M = sum(y>=x0);                                 %if M>=10, P_gpd will be computed
Pest = Ppermest(x0,y);                          %P_gpd (default method is ML)  
[Pest,Pest_ci] = Ppermest(x0,y,0.05);           %P_gpd with confidence bounds
[Pest,Pest_ci] = Ppermest(x0,y,0.05,'MOM');     %P_gpd using MOM method
[Pest] = Ppermest(x0,y,0.05,'MOM');             %P_gpd using MOM method without confidence bounds
[Pest,Pest_ci] = Ppermest(x0,y,0.05,'PWM');     %P_gpd using PWM method
[Pest,Pest_ci] = Ppermest(x0,y,0.05,'PWM',100); %P_gpd using different number of exceedances (N_exc)