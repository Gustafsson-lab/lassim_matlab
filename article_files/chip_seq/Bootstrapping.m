function [p,yrand,z,fe,aveX,Pest] = Bootstrapping(xrand,x,nrand,weights)

xrand= xrand(isfinite(xrand));

n = length(x);
if nargin<4
    weights = ones(n,1);
end
i = isfinite(x) & isfinite(weights);
x= x(i);
n = length(x);
weights = weights(i);
weights = weights/sum(weights);
aveX = sum(weights.*x);

Nxrand = length(xrand);
yrand= zeros(nrand,1);
parfor k=1:nrand
yrand(k) = sum(weights.*xrand(randsample(Nxrand,n)));
end
Pest = Ppermest(aveX,yrand);      

p = mean( yrand > aveX )  ;
z = (aveX  - mean(yrand))/std(yrand);
fe = aveX /mean(yrand);

if p<10/nrand
[~,p] = ztest(z,0,1,[],'right');
if p<10*eps    
    p = 10.^approximate_pvalues(z);
end
end
