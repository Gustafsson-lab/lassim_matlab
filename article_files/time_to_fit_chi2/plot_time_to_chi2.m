%% This is a scrip that plots the mean core-time needed to reach a chi2 acceptable fit

load time_22-Aug-2016.mat

figure()
plot(log(median(time)),'-*')
title('Median core time to \chi^{2}', 'FontSize',15)
set(gca, 'XTick',1:6, 'XTickLabel', 63:-10:13)
xlim([0 7])
ylim([5 6.5])
ylabel('log2 core time (s)', 'FontSize',13)
xlabel('Number of edges', 'FontSize',13)

figure()
boxplot(log(time))
title('Median core time to \chi^{2}', 'FontSize',15)
set(gca, 'XTick',1:6, 'XTickLabel', 63:-10:13)
ylabel('log2 core time (s)', 'FontSize',13)
xlabel('Number of edges', 'FontSize',13)