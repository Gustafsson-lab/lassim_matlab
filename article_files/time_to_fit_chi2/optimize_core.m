function [time_to_chi2]=optimize_core(remove_edges)
%% Variable declaration
global model
global param_names
global sigma_new
global time_new
global edges
global data_mean


core_time = cputime;

load data_new

edges = true(1,63);

if remove_edges > 0
    edges(1:10*remove_edges) = false;
    edges = edges(randperm(length(edges)));
end

%% Build the model
model         = 'CoreModel';
modelName     = SBmodel('CoreModel.txt');
[param_names] = SBparameters(modelName);

%% A good start guess, with the unactive edges removed

start_guess = [ones(24,1); zeros(sum(edges),1)];
start_cost=core_cost(start_guess');
%% Optimization options
OPTIONS.tempstart           = start_cost*2;            % InitialTemp  %1e2*start_cost
OPTIONS.tempend             = 0.1;                        % EndTemp
OPTIONS.tempfactor          = 0.1;                      % tempfactor
OPTIONS.maxitertemp         = 10*length(start_guess);    % Max-iterations per temp
OPTIONS.maxitertemp0        = 10*length(start_guess);    % Max-iterations at temp0
OPTIONS.maxtime             = 5;                       % Max-time
OPTIONS.tolx                = 1e-10;                    % TolX
OPTIONS.tolfun              = 1e-10;                    % Tolfun
OPTIONS.MaxRestartPoints    = 0;                        % Number of parallel valleys which are searched through
OPTIONS.lowbounds           = [ zeros(24,1); -20*ones(size(start_guess(25:end)))];
OPTIONS.highbounds          = [ 20*ones(24,1); 20*ones(size(start_guess(25:end)))];
OPTIONS.outputFunction      = '';
OPTIONS.silent              = 1;

format long
format compact
%% Does the optimizaion
[optimized_param]         = simannealingSB(@core_cost,start_guess',OPTIONS);
for k=1:6
    [optimized_param,optim_cost]         = simannealingSB(@core_cost,optimized_param',OPTIONS);
    OPTIONS.tempstart           = optim_cost*1.2;
     
    if core_cost( optimized_param, true ) < chi2inv(0.95, 6*12)
        time_to_chi2 = cputime -core_time;
        return
    end
end
time_to_chi2 = inf; % If never reached chi2
end

