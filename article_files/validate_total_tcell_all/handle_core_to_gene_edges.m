function [] = handle_core_to_gene_edges( TF_core, name_of_file )
% handle_core_to_gene_edges( TF_core, name_of_file )
%
% This function saves the interactions of those TFs that are present in the
% core system. The files are read from the 'name_of_file' argument. If
% nothing is stated, the inputs from Magnusson et al 2016 are used.

if nargin == 0
predicted_interactions = tdfread('TFBN_Th2_link_list.tsv');
TF_core={
    'KLF6'
    'ELK1'
    'GATA3'
    'IRF4'
    'JUN'
    'MAF'
    'MYB'
    'NFATC3'
    'NFKB1'
    'RELA'
    'STAT3'
    'USF2'};
elseif nargin == 2
    predicted_interactions = tdfread(strcat(name_of_file,'.tsv'));
end

all_TFs = deblank(cellstr(predicted_interactions.TF));
all_TF_targets = deblank(cellstr(predicted_interactions.target));


all_TF_targets = all_TF_targets(ismember(all_TFs,TF_core),:);
all_TFs = all_TFs(ismember(all_TFs,TF_core),:);


save('predicted_core_to_gene_edges','all_TFs','all_TF_targets')

