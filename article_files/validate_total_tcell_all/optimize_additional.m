function [optimized_param, cost]=optimize_additional(gene_entrez, solution, random )

%% Variable declaration

global model
global param_names
global sigma
global time
global data
global core_sol
global edges

if any(isnan(solution)) %Check if there is a LASSIM result
    optimized_param = nan;
    cost = nan;
    return
end

if random
    solution = solution(randperm(length(solution)));
    solution = randn(size(solution)).*solution;
end

% Make sure that all solutions have the same premisses
solution(solution<0) = -0.5;
solution(solution>0) = 0.5;


naive = false;  %The genes to be tested are the total t-cells
try
    [additional_gene_data,additional_gene_std,time, gene_name] = load_data(gene_entrez, naive);
catch
    optimized_param = nan;
    cost = nan;
    disp('Data not_present')
    load all_data
    gene_entrez = ordering.EntrezGene(gene_index);
    save(strcat('results_number_', num2str(gene_index)),'solutions', 'cost_of_step','gene_entrez')
    return
end
load data_tot
data = [data_tot; additional_gene_data];
sigma = [std_tot; additional_gene_std];

%% Build the model
model         = strcat('core_model_expanded');
[param_names] = SBparameters(model);
edges = solution~=0;
%%
core_sol = [0.0442104504570580;0.0408143233600834;1.79168031129305e-06;0.0677035015705644;9.90832602352007;0.00673465681339131;0.0502137937030691;0.364752363656985;0.383336853127052;0.0500340718385870;0.122368759728948;2.09596748409861;0.0222638633915854;0.0839995902991789;0.0139919373940257;14.3046279353122;3.90845189728967;6.49551294187261e-07;14.8527023903199;0.107231995226743;2.74859384016750;3.39929076580883;0.174478996780057;1.86472068634569;0;0.836316552198452;0;0;0;0;0;0;0.00135759826179116;0;0;19.2213736934611;0;0;8.05640945707158;-17.0811879094546;-16.8809001730488;0;0;0;0.0163952139302752;-0.163565739882736;0;13.2775684738583;19.9384572211998;0;-17.2623835122710;0;0;0;0;0.0323569976051642;9.43708716751759;0;0;0;0;-19.3304697832983;0;3.26550059069026;0;0;-0.0517525835769529;-19.9929167822298;0;4.55529555275948;0;0;0;0;0;0;-6.74014043725042;0.00103939433059579;0;0;-13.3254561697335;0;-8.19923616418146;0;0;0.250971797920263;19.8762449379250]';
start_guess = [1; 1; solution(edges)];
start_cost = cost_fun(start_guess');
%% Optimization options
OPTIONS.tempstart           = start_cost;             % InitialTemp  %1e2*start_cost
OPTIONS.tempend             = 0.01;                    % EndTemp
OPTIONS.tempfactor          = 0.1;                      % tempfactor
OPTIONS.maxitertemp         = 10*length(start_guess);         % Max-iterations per temp
OPTIONS.maxitertemp0        = 20*length(start_guess);         % Max-iterations at temp0
OPTIONS.maxtime             = 0.0000000000005;                        % Max-time
OPTIONS.tolx                = 1e-10;                    % TolX
OPTIONS.tolfun              = 1e-10;                    % Tolfun
OPTIONS.MaxRestartPoints    = 0;                        % Number of parallel valleys which are searched through
OPTIONS.lowbounds           = [ zeros(2,1); -20*(start_guess(3:end)<0)];
OPTIONS.highbounds          = [ 20*ones(2,1); 20*(start_guess(3:end)>0)];
OPTIONS.outputFunction      = '';
OPTIONS.silent              = 1;

format long
format compact

    
[optimized_param] = simannealingSB(@cost_fun,start_guess,OPTIONS);
[optimized_param, cost] = simannealingSB(@cost_fun,optimized_param,OPTIONS);
[ optimized_param ] = restore_full_parameter_vector( optimized_param, [], edges );
end
