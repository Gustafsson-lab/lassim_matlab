function [ data, sigma, time, name, knock_data] = load_data( gene_entrez, naive )
% Handels the data
% [ data, sigma, time ] = load_data( gene_index, naive )
%
% Where
%   * gene_index is the row corresponding to 'ordering' gene.
%   * naive is a logical determining weather the naive or total t cell meas
%     should be used
%
%% Loads data time series for naive T-cells
load all_data
%% Naive meas
gene_index = (ordering.EntrezGene == gene_entrez);
if naive
    data = mean([double(ordering.x_naive_new(gene_index,1:6));double(ordering.x_naive_new(gene_index,7:12));double(ordering.x_naive_new(gene_index,13:18));double(ordering.x_naive_new(gene_index,19:24))]);
    data = data/max(data);
    sigma=std([double(ordering.x_naive_new(gene_index,1:6));double(ordering.x_naive_new(gene_index,7:12));double(ordering.x_naive_new(gene_index,13:18));double(ordering.x_naive_new(gene_index,19:24))]);
    time=tid.t_naive_new(1:6);
    
    knock_data = get_knock_data(ordering.EntrezGene(gene_index));
    knock_data = knock_data-1;
    knock_data(knock_data>2) = 2;
        name = ordering.GeneSymbol(gene_index);
else
    data = mean([double(ordering.x_total_soren(gene_index,4:7));double(ordering.x_total_soren(gene_index,8:11));double(ordering.x_total_soren(gene_index,12:15))]);
    data = data/max(data);
    sigma=std([double(ordering.x_total_soren(gene_index,4:7));double(ordering.x_total_soren(gene_index,8:11));double(ordering.x_total_soren(gene_index,12:15))]);
    time=tid.t_total_soren(4:7);
    
    name = ordering.GeneSymbol(gene_index);
end




end
%% End of function