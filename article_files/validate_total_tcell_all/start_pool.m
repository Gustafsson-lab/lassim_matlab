

load results_LASSIM_160805.mat

random_param = nan(length(entrez),100,14);
random_cost = nan(length(entrez),100);

chosen_param = nan(length(entrez),14);
chosen_cost = nan(length(entrez),1);

for i = 1:length(entrez)
    try
        [chosen_param(i,:), chosen_cost(i)] = optimize_additional(entrez(i),optim_sol(:,i),false);
        parfor j = 1:100
            [random_param(i,j,:), random_cost(i,j)] = optimize_additional(entrez(i),optim_sol(:,i),true);
        end
        entrez_of_gene = entrez(i);
        disp(i)
    catch
    end
end
save('final_results')
disp('Done')
%% End of script