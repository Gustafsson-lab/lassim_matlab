%% A function that calculates the fit of a model with respect to the parameter vector "parameters".
%
% Created 150527

function [cost] = cost_fun(parameters, logic_plot)
%% Variable declaration
global model
global param_names
global data
global sigma
global time
global core_sol
global edges

%% Restore full parameter vector
%% Set the compressed param vector to match the full parameter vector
[ parameters ] = restore_full_parameter_vector( parameters, core_sol, edges );

%% Simulations
try
    sim_options                 = [];
    sim_options.method          = 'stiff';
    sim_options.maxnumsteps     = 1e8;
    
    simulation                  = SBPDsimulate(model,time, data(:,1),param_names,parameters,sim_options);
    sim_val                     = simulation.statevalues';

    sim_val(13,:) = sim_val(13,:)./max(sim_val(13,:)); 
    cost = sum(((data(end,:)-sim_val(end,:))./mean(sigma(:))).^2);
catch
    cost=inf;
    return
end
if nargin>1
    if logic_plot == 1
        sim_continuous            = SBPDsimulate(model,time(end), data(:,1),param_names,parameters,sim_options);
        sim_val_continuous        = sim_continuous.statevalues';
        
        for i=13
            sim_val_continuous(i,:) = sim_val_continuous(i,:)./max([sim_val_continuous(i,:)]);
        end
        names_of_tfs = sim_continuous.states;
        figure(1)
        hold on
        errorbar(time, data(end,:), sigma(end,:), '*b','Linewidth', 2)
        plot(sim_continuous.time, sim_val_continuous(end,:),'-r', 'Linewidth', 2);
        xlabel('Time, h');
        xlim([-1 (max(time)+1)])
        ylabel('Response, a.u.')
        hold off
        
    end
end
end

