

% disp('Installing SBtoolbox')
% try
%     restoredefaultpath
%     current_directory = cd;
%     cd sbtoolbox
%     installSBPOPpackageInitial
%     cd ..
% catch
%     disp('Failed to install the SBtoolbox')
%     cd(current_directory)
%     restoredefaultpath
%     addpath(genpath('sbtoolbox'))
% end

%% Create model
model         = ['core_model_expanded'];
model_name     = SBmodel(('core_model_expanded.txt'));
SBPDmakeMEXmodel(model_name,model);

start_pool
% job = batch('start_pool','pool',32)
% exit