%% A script that



function [optimized_param, optim_cost]=test_solution(start_guess, run_number)

%% Variable declaration

global model
global param_names
global edges
global data_tot
global std_tot
global time_tot

load data_tot


%% Build the model

model         = ['core_model'];
[param_names] = SBparameters(model);
%%


interactions = start_guess(25:end);
start_guess = [start_guess(1:24); interactions(randperm(length(interactions)))];

edges=[start_guess(25:end)~=0]';
start_guess=start_guess([true(1,24) edges]);
start_cost=cost_fun(start_guess');
%% Optimization options
OPTIONS.tempstart           = start_cost*3;            % InitialTemp  %1e2*start_cost
OPTIONS.tempend             = 1;                        % EndTemp
OPTIONS.tempfactor          = 0.1;                      % tempfactor
OPTIONS.maxitertemp         = 200*length(start_guess);    % Max-iterations per temp
OPTIONS.maxitertemp0        = 150*length(start_guess);    % Max-iterations at temp0
OPTIONS.maxtime             = 15;                       % Max-time
OPTIONS.tolx                = 1e-10;                    % TolX
OPTIONS.tolfun              = 1e-10;                    % Tolfun
OPTIONS.MaxRestartPoints    = 0;                        % Number of parallel valleys which are searched through
OPTIONS.lowbounds           = [ zeros(24,1); -20*(start_guess(25:end)<0)];
OPTIONS.highbounds          = [ 20*ones(24,1); 20*(start_guess(25:end)>0)];
OPTIONS.outputFunction      = '';
OPTIONS.silent              = 1;

format long
format compact
%% Do the optimizaion
[optimized_param,optim_cost]         = simannealingSB(@cost_fun,start_guess',OPTIONS);
for k=1:5
    [optimized_param,optim_cost]         = simannealingSB(@cost_fun,optimized_param',OPTIONS);
    OPTIONS.tempstart           = start_cost*2;
end

parameters_expanded=zeros(1,75-12);
parameters_expanded(edges)=optimized_param(25:end);
optimized_param=[optimized_param(1:24) parameters_expanded]';
%% delete mex file
% delete(strcat('core_model', num2str(run_number),'.mexw64'))
try
    cd results_folder
    save(strcat('results_number_', num2str(run_number)), 'optimized_param', 'optim_cost')
    cd ..
    
    load results
    solution = [solution optimized_param];
    cost = [cost; optim_cost];
    save('results', 'solution', 'cost');
    disp('Save sucecssfull')
catch
%     disp('Error when saving, does backup')
%     cd results_folder\
%     save(strcat('results_number_',  num2str(run_number)), 'optimized_param', 'optimized_param')
%     cd ..
end

%% End of function
