function [] = checkDependenciesSB()
% checkDependenciesSB: checks if functions / scripts (*.m files) in the 
% NON _ACADEMIC/_DEPRECATED/_EXPLORATORY folders depend on functions or scripts
% (*.m files) in the _ACADEMIC/_DEPRECATED/_EXPLORATORY folders.
% If such non-allowed dependencies are found, the user is warned.
% ".svn" folders are not considered
% The following function names are excluded from consideration:
%       'disp.m','set.m','display.m','SBPOPstruct.m','SBimportSBML.m','SBimportSBMLCD.m'

% Information:
% ============
% Copyright � 2012 Novartis Pharma AG
% 
% This program is Free Open Source Software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program. If not, see <http://www.gnu.org/licenses/>.

clear all;

global mfiles_ACADEMIC_DEPRECATED_EXPLORATORY dependencies_found
mfiles_ACADEMIC_DEPRECATED_EXPLORATORY = {};
dependencies_found = 0;

disp('checkDependenciesSB: The following function names in the _ACADEMIC/_DEPRECATED/_EXPLORATORY folders are excluded from consideration:');
disp('                     disp.m,set.m,display.m,SBPOPstruct.m,SBimportSBML.m,SBimportSBMLCD.m');

old = pwd;
SBPOP_FOLDER = fileparts(which(('installSBPOPpackage')));
cd([SBPOP_FOLDER '/..']);
% Get folder name of SBPOP PACKAGE
[xdummyx,SBPOP_FOLDER_NAME] = fileparts(SBPOP_FOLDER);
recurseFolderGET_ACADEMIC_mfiles(SBPOP_FOLDER_NAME);
cd(old);

% Remove some entries from the mfiles_ACADEMIC_DEPRECATED_EXPLORATORY list
mfiles_ACADEMIC_DEPRECATED_EXPLORATORY = unique(mfiles_ACADEMIC_DEPRECATED_EXPLORATORY);
remove = {'disp.m','set.m','display.m','SBPOPstruct.m','SBimportSBML.m','SBimportSBMLCD.m'};
for k=1:length(remove),
    ix = strmatch(remove{k},mfiles_ACADEMIC_DEPRECATED_EXPLORATORY,'exact');
    if ~isempty(ix),
        mfiles_ACADEMIC_DEPRECATED_EXPLORATORY{ix} = 'xxxxxxxxxxxxxxxxxxxxxxxxx.xxx';
    end
end

old = pwd;
cd([SBPOP_FOLDER '/..']);
check_NONACADEMIC_files(SBPOP_FOLDER_NAME);
cd(old);

if dependencies_found == 0,
    disp('No dependencies of normal functions on _ACADEMIC or _DEPRECATED or _EXPLORATORY functions found.');
end
return

function check_NONACADEMIC_files(folder)
global mfiles_ACADEMIC_DEPRECATED_EXPLORATORY dependencies_found
% change folder
cd(folder);
% get list of all m files if ACADEMIC or DEPRECATED folders
if isempty(strfind(pwd,'_ACADEMIC')) && isempty(strfind(pwd,'_DEPRECATED')) && isempty(strfind(pwd,'_EXPLORATORY')),
    files = dir('*.m');
    for k=1:length(files),
        content = fileread(files(k).name);
        for k2=1:length(mfiles_ACADEMIC_DEPRECATED_EXPLORATORY),
            ix = regexp(content,['\<' strrep(mfiles_ACADEMIC_DEPRECATED_EXPLORATORY{k2},'.m','') '[\s]*\(']);
            if ~isempty(ix),
                fprintf('Call to _ACADEMIC or _DEPRECATED or _EXPLORATORY function "%s" found in function "%s" in path "%s".\n',mfiles_ACADEMIC_DEPRECATED_EXPLORATORY{k2},files(k).name,pwd);
                dependencies_found = 1;
            end
        end
    end
end
% recurse in all subfolders
allfiles = dir;
for k = 1:length(allfiles),
    if ~strcmp(allfiles(k).name,'.svn') && ~strcmp(allfiles(k).name,'..') && ~strcmp(allfiles(k).name,'.') && allfiles(k).isdir == 1,
        check_NONACADEMIC_files(allfiles(k).name);
    end
end
% up we go
cd ..
return


function recurseFolderGET_ACADEMIC_mfiles(folder)
global mfiles_ACADEMIC_DEPRECATED_EXPLORATORY
% change folder
cd(folder);
% get list of all m files if ACADEMIC or DEPRECATED folders
if ~isempty(strfind(pwd,'_ACADEMIC')) || ~isempty(strfind(pwd,'_DEPRECATED')) || ~isempty(strfind(pwd,'_EXPLORATORY')),
    files = dir('*.m');
    mfiles_ACADEMIC_DEPRECATED_EXPLORATORY = {mfiles_ACADEMIC_DEPRECATED_EXPLORATORY{:} files.name};
end
% recurse in all subfolders
allfiles = dir;
for k = 1:length(allfiles),
    if ~strcmp(allfiles(k).name,'.svn') && ~strcmp(allfiles(k).name,'..') && ~strcmp(allfiles(k).name,'.') && allfiles(k).isdir == 1,
        recurseFolderGET_ACADEMIC_mfiles(allfiles(k).name);
    end
end
% up we go
cd ..
return
