function [output] = mat2datasetSB(x,colNames)
% mat2datasetSB: converts a double matrix to a dataset
% Function required to allow SBPOP be compatible with pre R2013 versions of MATLAB
%
%   d = mat2datasetSB(x)
%   d = mat2datasetSB(x,colNames)
%
% x:        matlab matrix
% colNames: cell-array with columnnames

% Information:
% ============
% Copyright (C) 2013 Henning Schmidt, henning@sbtoolbox2.org
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  
% USA.

if nargin == 1,
    colNames = {};
    for k=1:size(x,2),
        colNames{k} = sprintf('x%d',k);
    end
end

if length(colNames) ~= size(x,2),
    error('Incorrect number of column names.');
end

output = dataset();
for k=1:size(x,2),
    output.(colNames{k}) = x(:,k);
end

    
