function [] = parsaveSB(filename,variableName,variable)
% parsaveSB: Allows saving from within a parfor loop
%
% USAGE:
% ======
% [] = parsaveSB(filename,variableName,variable)
%
% Output Arguments:
% =================
% filename:         filename for the mat file
% variableName:     name how the variable is to be saved
% variable:         variable value

% Information:
% ============
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  
% USA.

eval(sprintf('%s = variable;',variableName));

save(filename, variableName)

