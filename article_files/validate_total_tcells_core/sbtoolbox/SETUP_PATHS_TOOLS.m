% In this file you need to provide the names and potentially the paths to
% the executables for tools, such as NONMEM, MONOLIX, SAS, etc.
% If executables in the system
% path, then only the names of the executables are needed.
%
% It is possible to define paths for Windows and Unix independently,
% allowing to use the package on different systems without the need to
% re-edit the paths. If a tool is not available on a system, then just
% leave the path empty.

% NONMEM (currently tested version: 7.2)
PATH_SYSTEM_NONMEM_WINDOWS          = '';
PATH_SYSTEM_NONMEM_UNIX             = 'nmfe72';

% NONMEM PARALLEL
PATH_SYSTEM_NONMEM_PARALLEL_WINDOWS = '';
PATH_SYSTEM_NONMEM_PARALLEL_UNIX    = 'nmfe72par';

% MONOLIX STANDALONE (tested versions 4.2.0-4.3.2)
PATH_SYSTEM_MONOLIX_WINDOWS         = 'C:\LOCAL\Monolix\Monolix432s\bin\Monolix.bat';
PATH_SYSTEM_MONOLIX_UNIX            = '/CHBS/apps/dev_apps/Monolix/4.3.2/Standalone/Monolix432grid-Matlab2008/bin/Monolix.sh';

% SAS
PATH_SYSTEM_SAS_WINDOWS             = '';
PATH_SYSTEM_SAS_UNIX                = 'sas-9.4';