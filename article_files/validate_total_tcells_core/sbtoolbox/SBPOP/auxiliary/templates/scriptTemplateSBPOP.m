% [DESCRIPTION]
% In this section the purpose of the script should be described.
% Additionally, the different possible syntaxes. 
%
% [INPUT]
% Define the input arguments or other inputs (e.g. required files, etc.)
% Please do NOT use hardcoded data in MATLAB scripts.
%
% [OUTPUT]
% Define the output arguments or other outputs (e.g. produced log files,
% figures, etc.) 
%
% [ASSUMPTIONS]
% Here you can add any additional background information about assumptions.
%
% [AUTHOR]
% Original author, ideally with email address
%
% [DATE]
% Date of first creation
%
% [PLATFORM]
% Windows XP, MATLAB R2012a, ...
%
% [KEYWORDS]
% Any keyword you like ... comma separated
% 
% [TOOLBOXES USED]
% Any commercial and other MATLAB toolboxes that need to be available for
% this script to be run. 
%
% [VALIDATION HISTORY]
% �
% [MODIFICATION HISTORY]
% including version numbering, changes, author and date of changes

% Here your script starts. The above header should be ended by an empty
% line.

