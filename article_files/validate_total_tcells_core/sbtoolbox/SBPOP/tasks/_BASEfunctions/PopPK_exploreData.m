function [] = PopPK_exploreData( dataPath,outputPath,varargin )
% [SYNTAX]
% [] = PopPK_exploreData( dataPath,outputPath )
% [] = PopPK_exploreData( dataPath,outputPath,covariateInfo )
% dataPath:         Path to the dataset in the general data format
% outputPath:       Path to where to write the exploratory plots and results
% covariateInfo:    MATLAB cell-array, defining which readouts in the
%                       general dataset should be added as time independent
%                       covariates. The format for this argument is as
%                       follows (documented by example):
% 
%                       covariateInfo = {
%                           % NAME              USENAME      CONTINUOUS
%                            'Gender'            'SEX'          0
%                            'Age'               'AGE0'         1
%                            'Bodyweight'        'WT0'          1
%                            'Height'            'HT0'          1
%                            'BMI'               'BMI0'         1
%                       };
%
%                       The CONTINUOUS column is optional and will not be used in this 
%                       function yet. It is used somewhere else to identify what the user
%                       desires to use as continuous and what as categorical covariate.
%
%                       The values for the covariates will be determined as
%                       follows:
%                        - Use mean of BASEline assessments by default. 
%                        - If BASE not defined then use mean of SCREEN assessments.
%                        - BASE and SCREEN not defined then use mean of pre-first-dose assessments.
%
% [OUTPUT]
% PDFs and text files are generated. Standard covariates are found automatically.
% If desired, the user can define the covariates to use manually.

% Handle automatic or user-supplied covariate information
if nargin == 2,
    disp('No "covariateInfo" specified. Trying to identify general covariates.');
    covariateInfo       = {
                        % NAME          COVARIATE NAME      CONTINUOUS
                        'Weight'        'WT0'                   1   
                        'Height'        'HT0'                   1
                        'Age'           'AGE0'                  1
                        'BMI'           'BMI0'                  1
                        'Gender'        'SEX'                   0 
                        'Ethnicity'     'ETHN'                  0
                        'Race'          'RACE'                  0
                          };
elseif nargin == 3,
    covariateInfo = varargin{1};
    if size(covariateInfo,2) ~= 3,
        error('Please make sure to correctly define the "covariateInfo" input argument.');
    end
else
    error('Incorrect number of input arguments.');
end

% Get continuous and categorical covariate names
covNames            = covariateInfo(find([covariateInfo{:,3}]==1),2);
catNames            = covariateInfo(find([covariateInfo{:,3}]==0),2);

% Augment the general dataset 
dataAugmented       = SBPOPconvertGeneralDataFormat(dataPath,covariateInfo);

% Define the options (just the output folder)
options             = [];
options.outputPath  = [outputPath '/Output/DataExploration'];

% Run the PopPK graphical plots
SBPOPexplorePopPKdata(dataAugmented,covNames,catNames,options)

