function [] = PopPK_baseModel( dataPath,outputPath,varargin )

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Handle input arguments
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

FLAG_ANTIBODY   = 0;
NONLINEAR    	= 0;
N_TESTS         = 1;
TOOL            = 'MONOLIX';

% Handle options
if nargin >= 3,
    options = varargin{1};
    try FLAG_ANTIBODY   = options.FLAG_ANTIBODY;    catch; end
    try NONLINEAR		= options.NONLINEAR;        catch; end
    try TOOL            = options.TOOL;             catch; end
    try 
        N_TESTS         = options.N_TESTS;          
    catch
        % Set N_TESTS=5 by default (if not given otherwise) for NONMEM
        if strcmp(TOOL,'NONMEM'), N_TESTS = 5; end
    end
end

% Handle automatic or user-supplied covariate information
if nargin < 4,
    disp('No "covariateInfo" specified. Trying to identify general covariates.');
    covariateInfo       = {
                        % NAME          COVARIATE NAME      CONTINUOUS
                        'Weight'        'WT0'                   1   
                        'Height'        'HT0'                   1
                        'Age'           'AGE0'                  1
                        'BMI'           'BMI0'                  1
                        'Gender'        'SEX'                   0 
                        'Ethnicity'     'ETHN'                  0
                        'Race'          'RACE'                  0
                          };
elseif nargin == 4,
    covariateInfo = varargin{2};
    if size(covariateInfo,2) ~= 3,
        error('Please make sure to correctly define the "covariateInfo" input argument.');
    end
else
    error('Incorrect number of input arguments.');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Load data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Augment the general dataset and get cov and cat names
[dataAugmented,covNames,catNames] = SBPOPconvertGeneralDataFormat(dataPath,covariateInfo);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Data cleaning
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Define default categorical covariate imputation values
catImputationValues = 999*ones(size(catNames));

% By default do not remove subjects
removeSUBJECT  = {
    % Unique subject identifier     Reason for removal of subject
};

% By default do not remove records
removeREC = {
    % Record number in dataset      Reason for removal
};

% By default only remove subjects with not more than one PK sample
Nobs = 1;

% By default remove all BLOQ data
optionsDataCleaning                 = [];
optionsDataCleaning.outputPath      = [outputPath '/Output/DataCleaning/'];
optionsDataCleaning.FLAG_LLOQ       = 0; % Remove all LLOQ data by default

% Do the cleaning
dataCleaned = SBPOPcleanPopPKdata(dataAugmented,removeSUBJECT,removeREC,Nobs,covNames,catNames,catImputationValues,optionsDataCleaning);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Generate popPK modeling dataset
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

modelingDatasetFile = [outputPath '/Data/popPK_NLME_dataset.csv'];
dataheaderNLME = SBPOPconvert2popPKdataset(dataCleaned,covNames,catNames,modelingDatasetFile);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Determine data information for different things
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

dataNLME = SBPOPloadCSVdataset(modelingDatasetFile);

% Number of subjects
Nsubjects = length(unique(dataNLME.ID));

% IV data present
FLAG_IV_data = ~isempty(find(dataNLME.ADM==2));

% Absorption1 data present 
FLAG_ABS1_data = ~isempty(find(dataNLME.ADM==1));

% Get values of data - 5% quantile as starting guess for additive error
ADD_ERROR_0 = quantile(dataNLME.DV(dataNLME.EVID==0),0.05);

% Determine FACTOR_UNITS
UNIT_DOSE = unique(dataNLME.UNIT(dataNLME.TYPE==0)); UNIT_DOSE = UNIT_DOSE{1};
UNIT_CONC = unique(dataNLME.UNIT(dataNLME.TYPE==1)); UNIT_CONC = UNIT_CONC{1};
if strcmp(lower(UNIT_DOSE),'mg') && strcmp(lower(UNIT_CONC),'ug/ml'),
    FACTOR_UNITS = 1;
elseif strcmp(lower(UNIT_DOSE),'ug') && strcmp(lower(UNIT_CONC),'ng/ml'),
    FACTOR_UNITS = 1;
elseif strcmp(lower(UNIT_DOSE),'mg') && strcmp(lower(UNIT_CONC),'ng/ml'),
    FACTOR_UNITS = 1000;
else
    error('Incorrect definitions of dose and concentration units.');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Define default settings for parameter estimation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

optionsNLME                                 = [];                      

optionsNLME.parameterEstimationTool         = TOOL;
optionsNLME.N_PROCESSORS_PAR                = 10;
optionsNLME.N_PROCESSORS_SINGLE             = 5;

optionsNLME.algorithm.SEED                  = 123456;                  
optionsNLME.algorithm.K1                    = 500;                    
optionsNLME.algorithm.K2                    = 200;                     
optionsNLME.algorithm.NRCHAINS              = ceil(100/Nsubjects);   

% Set NONMEM specific options
optionsNLME.algorithm.METHOD                = 'SAEM';
optionsNLME.algorithm.ITS                   = 1;
optionsNLME.algorithm.ITS_ITERATIONS        = 10;
optionsNLME.algorithm.IMPORTANCESAMPLING    = 1;
optionsNLME.algorithm.IMP_ITERATIONS        = 5;

% Set MONOLIX specific options
optionsNLME.algorithm.LLsetting             = 'linearization';     

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Define Fabs1 and ka estimation based on availability of ABS1 and IV data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if FLAG_ABS1_data,
    EST_ka = 1;
else
    EST_ka = 0;
end

if FLAG_ABS1_data && FLAG_IV_data,
    EST_Fabs1 = 1;
    Fabs1     = 0.5;
else
    EST_Fabs1 = 0;
    Fabs1     = 1;
end    

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Run 1 compartmental distribution model (linear) to obtain initial guesses
% Separate between antibody and not-antibody
% No Tlag
% Fabs1 and ka if needed
% Linear elimination always, nonlinear only if desired
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if ~FLAG_ANTIBODY || (FLAG_ANTIBODY && NONLINEAR),
    % Change into a folder to allow for popPK workflow tools to work
    oldpath = pwd();
    cd([outputPath '/Data']);
    
    % Run a first simple model 
    modeltest                           = [];
    modeltest.numberCompartments        = 1;
    modeltest.errorModels               = 'comb1';
    modeltest.errorParam0               = [ADD_ERROR_0 0.3];
    if ~NONLINEAR,
        modeltest.saturableClearance    = 0;
    else
        modeltest.saturableClearance    = [0 1];
    end        
    modeltest.lagTime                   = 0;
    modeltest.FACTOR_UNITS              = FACTOR_UNITS;
    %                                       CL    Vc    Q1    Vp1    Q2    Vp2    Fiv    Fabs1      ka        Tlag_input1    VMAX    KM
    if ~FLAG_ANTIBODY,
        modeltest.POPvalues0            = [ 5     10    5     100    5     100    1      Fabs1      0.5       0.5             10      10];
    else        
        modeltest.POPvalues0            = [ 0.2   3     0.8   5      NaN   NaN    1      Fabs1      0.5       0.5             10      10];
    end
    modeltest.POPestimate               = [ 1     1     1     1      1     1      0      EST_Fabs1  EST_ka    0               1       1];
    modeltest.IIVestimate               = [ 1     1     1     1      1     1      0      EST_Fabs1  EST_ka    0               1       1];
    
    optionsModelSpace                   = [];
    optionsModelSpace.Ntests            = N_TESTS;
    
    [MODELINFO,RESULTS] = SBPOPbuildPopPKModelSpace('MODEL_00_INITIAL_GUESSES', modeltest, modelingDatasetFile, dataheaderNLME, optionsNLME, optionsModelSpace);

    % Sort results by BIC
    ranking_var         = sortrows([[1:length(RESULTS)]' [RESULTS.BIC]'],2); 
    RANKING             = ranking_var(:,1);
    RESULTS_ORDERD      = RESULTS(RANKING);
    
    % Define initial guesses
    CL0                 =  RESULTS_ORDERD(1).rawParameterInfo.fixedEffects.values(1);
    Vc0                 =  RESULTS_ORDERD(1).rawParameterInfo.fixedEffects.values(2);
    Fabs10              =  RESULTS_ORDERD(1).rawParameterInfo.fixedEffects.values(8);
    ka0                 =  RESULTS_ORDERD(1).rawParameterInfo.fixedEffects.values(9);
    Tlag0               =  RESULTS_ORDERD(1).rawParameterInfo.fixedEffects.values(10);
    try
        VMAX0           =  RESULTS_ORDERD(1).rawParameterInfo.fixedEffects.values(11);
        KM0             =  RESULTS_ORDERD(1).rawParameterInfo.fixedEffects.values(12);
    catch
        VMAX0           =  10;
        KM0             =  10;
    end
    
    %                       CL    Vc    Q1    Vp1    Q2    Vp2    Fiv    Fabs1      ka        Tlag_input1    VMAX    KM
    if ~FLAG_ANTIBODY,
        POPvalues0      = [ CL0   Vc0   CL0   10*Vc0 CL0   10*Vc0 1      Fabs10     ka0       Tlag0          VMAX0   KM0];
    else
        POPvalues0      = [ 0.2   3     0.8   3      0.8   3      1      Fabs10     ka0       Tlag0          VMAX0   KM0];
    end
    
    % Get starting guesses for error model parameters
    errorParam0         = abs(RESULTS_ORDERD(1).rawParameterInfo.errorParameter.values);
    
    % Change out of path
    cd(oldpath);
else
    % Define starting guesses for antibody models
    %                             CL    Vc    Q1    Vp1    Q2    Vp2    Fiv    Fabs1      ka        Tlag_input1    VMAX    KM
    POPvalues0                = [ 0.2   3     0.8   3      0.8   3      1      0.5        0.5       0.5            1e-10   10];
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Now run the base models of interest
% Separate between Antibody and other models
%
% Non-antibodies: 
%   Run a 1,2,3 compartmental distribution model
%   Additive/proportional error model
%   Linear elimination and if desired also saturable elimination
%   No lagtime for now
%
% Antibodies
%   Run a 1,2 compartmental distribution model
%   Additive/proportional error model
%   Linear elimination and if desired also saturable elimination
%   No lagtime for now
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Change into a folder to allow for popPK workflow tools to work
oldpath = pwd();
cd([outputPath '/Data']);

modeltest                           = [];
if ~FLAG_ANTIBODY,
    modeltest.numberCompartments    = [1 2 3];
else
    modeltest.numberCompartments    = [1 2];
end
modeltest.errorModels               = 'comb1';
% Use previously determined error model parameters (crucial for NONMEM)
modeltest.errorParam0               = errorParam0;
if ~NONLINEAR,
    modeltest.saturableClearance    = 0;
else
    modeltest.saturableClearance    = [0 1];
end
modeltest.lagTime                   = 0;
modeltest.FACTOR_UNITS              = FACTOR_UNITS;
%                                       CL    Vc    Q1    Vp1    Q2    Vp2    Fiv    Fabs1      ka        Tlag_input1    VMAX    KM
% Use previously derived initial guesses
modeltest.POPvalues0                = POPvalues0;
modeltest.POPestimate               = [ 1     1     1     1      1     1      0      EST_Fabs1  EST_ka    0               1       1];
modeltest.IIVestimate               = [ 1     1     1     1      1     1      0      EST_Fabs1  EST_ka    0               1       1];

optionsModelSpace                   = [];
optionsModelSpace.Ntests            = N_TESTS;

[MODELINFO,RESULTS] = SBPOPbuildPopPKModelSpace('MODEL_01_BASE', modeltest, modelingDatasetFile, dataheaderNLME, optionsNLME, optionsModelSpace);

% Change out of path
cd(oldpath);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Load and display results 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

oldpath = pwd();
cd([outputPath '/Output/FitAnalysis/MODEL_01_BASE']);

edit fitInfoParameters.txt

cd(oldpath);
