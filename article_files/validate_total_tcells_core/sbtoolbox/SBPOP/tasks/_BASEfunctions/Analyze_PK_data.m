%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Setup
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clc; clear all; close all; restoredefaultpath();
oldpath = pwd();
cd('/home/schmihek/TOOLS/SBPOP PACKAGE');
installSBPOPpackage
cd(oldpath)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Define dataset to use for analysis and output folder for results
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
dataPath    = 'Example Data/Data Compound 3.csv';
outputPath  = 'TEST3';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% STEP1: Sanity check if general data format is correct
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
PopPK_checkData(dataPath);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% STEP2: Do some first plots
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
PopPK_exploreData(dataPath,outputPath)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% STEP3: Do some simple popPK base model assessment
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
options                 = [];

% If nonlinear elimination expected based on graphical exploration, then 
% set the flag to 1. Otherwise set to 0
options.NONLINEAR       = 0;

% Define if 'NONMEM' or 'MONOLIX' should be used (better is MONOLIX, faster
% is NONMEM)
options.TOOL            = 'NONMEM';

% If repetition of individual fits desired, please state the number of fits
% to be performed. When using MONOLIX it is ok to take N_TESTS=1. If using
% NONMEM it is very important to use more 5-10. NONMEM has huge problems to
% estimate correctly the additive part of the residual error model.
options.N_TESTS         = 1;

% Run the model based PK analysis
PopPK_baseModel(dataPath,outputPath,options)



  
 
 