function [] = SBPOPscm(TOOL,optionsSCM,model,dosing,data,projectPath,options)
% SBPOPscm: Stepwise covariate search, using forward inclusion / backward
% elimination. Decision can be made based on objective function value or confidence interval
% of estimated covariate coefficients.
%
% Two modes/types of search:
%   - type='OBJ': Behaves like the traditional PsN SCM, considering the
%                 objective function value.
%   - type='P':   Considers the probabilities that estimated covariate
%                 coefficients are different from 0. In each forward step
%                 the covariate coefficient is retained that is most
%                 statistically significant.
%
% Although the algorithm is in principle applicable to categorical
% covariates with more than 2 categories, it does not really make sense and
% might introduce to many parameters that are not significant. A better
% approach is to get the modeler to think a little more and reduce the
% number of categories to 2. Covariates with multiple categories can be
% assessed after the SCM has been done by adding them one at a time.
%
% This function here will only allow for categorical covariates with 2
% categories - not more.
%
% USAGE:
% ======
% SBPOPscm(TOOL,optionsSCM,model,dosing,data,projectPath,options)
%
% Input arguments model, dosing, data, options are identical to the ones
% documented in SBPOPcreateNONMEMproject and SBPOPcreateMONOLIXproject and
% are not repeated here.
%
% If covariateModel is defined in modeltest, then these covariates are
% included by default and are not subject to the SCM algorithm.
%
% TOOL:         'NONMEM' or 'MONOLIX'
% projectPath:  Path to which all the generated NLME projects will be saved
%
% In case NONMEM/SAEM is used it will be checked that IMPORTANCESAMPLING
% option is set to 1, in order to determine the true objective function
% that is suitable for statistical testing.
%
% optionsSCM: structure with optional settings for the covariate search.
%   optionsSCM.type:            Define if decisions based on objective function value or based on
%                               probability that covariate coefficients are different from 0.
%                               'OBJ' or 'P' (default: 'OBJ'). For categorical covariates with multiple
%                               levels the delta OFV is adjusted and for 'P' the lowest p-value is used.
%   optionsSCM.outputPath:      Path where to store the log file (default: projectPath)
%
%   optionsSCM.N_PROCESSORS_PAR:     Number of parallel model runs (default: 1)
%   optionsSCM.N_PROCESSORS_SINGLE:  Number of processors to parallelize single run (if NONMEM and MONOLIX allow for it) (default: 1)
%
%   optionsSCM.p_forward:       p-value for forward inclusion step (default: 0.05)
%                               Either for OFV or for probability that 0 included in CI for covariate coefficient.
%   optionsSCM.p_backward:      p-value for forward inclusion step (default: 0.001)
%                               Either for OFV or for probability that 0 included in CI for covariate coefficient.
%   optionsSCM.covariateTests:  cell-array with parameter/covariate combinations to test
%                               First element in each sub-array is the parameter name,
%                               following are the covariates to test.
%                               Example:
%                                          {
%                                             {'EMAX','WT0','SEX','HT0','ETHN'}
%                                             {'EC50','WT0','SEX','HT0','ETHN'}
%                                             {'fS'  ,'WT0','SEX','HT0','ETHN'}
%                                           }
%                               If not specified or empty, then all covariates will be
%                               tested on all parameters.

% Information:
% ============
% Copyright (c) 2012 Novartis Pharma AG
%
% This program is Free Open Source Software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program. If not, see <http://www.gnu.org/licenses/>.

%% %%%%%%%%%%%%%%%%%%%%%%%%%
% Check TOOL definition
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if ~strcmpi(TOOL,'nonmem') && ~strcmpi(TOOL,'monolix'),
    error('Please select as first input argument either ''NONMEM'' or ''MONOLIX''');
end

%% %%%%%%%%%%%%%%%%%%%%%%%%%
% Handle optionsSCM
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
try p_forward           = optionsSCM.p_forward;            catch, p_forward              = 0.05;         end
try p_backward          = optionsSCM.p_backward;           catch, p_backward             = 0.001;        end
try covariateTests      = optionsSCM.covariateTests;       catch, covariateTests         = {};           end
try outputPath          = optionsSCM.outputPath;           catch, outputPath             = projectPath;  end
try type                = optionsSCM.type;                 catch, type                   = 'OBJ';        end
try N_PROCESSORS_PAR    = optionsSCM.N_PROCESSORS_PAR;     catch, N_PROCESSORS_PAR       = 1;            end
try N_PROCESSORS_SINGLE = optionsSCM.N_PROCESSORS_SINGLE;  catch, N_PROCESSORS_SINGLE    = 1;            end

%% %%%%%%%%%%%%%%%%%%%%%%%%%
% Get previously defined covariate model
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Get fixed covariate model
try covariateModel          = options.covariateModel;               catch, covariateModel        = ''; end
try covariateModelValues    = options.covariateModelValues;         catch, covariateModelValues  = {}; end
try COVestimate             = options.COVestimate;                  catch, COVestimate           = {}; end

%% %%%%%%%%%%%%%%%%%%%%%%%%%
% Create the projectFolder
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
try rmdir(projectPath,'s'); catch end
mkdir(projectPath);

%% %%%%%%%%%%%%%%%%%%%%%%%%%
% Load dataset to assess number of categorical elements
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
oldpath = pwd();
cd(projectPath);
dataContents = SBPOPloadCSVdataset([data.dataRelPathFromProject '/' data.dataFileName]);
cd(oldpath);

%% %%%%%%%%%%%%%%%%%%%%%%%%%
% Change some paths
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
data.dataRelPathFromProject = ['../' data.dataRelPathFromProject];

%% %%%%%%%%%%%%%%%%%%%%%%%%%
% Create the BASE model (MODEL_00) and load header
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
BASEmodelFolder = [projectPath '/MODEL_BASE'];
if strcmpi(TOOL,'nonmem')
    % Check if IMP set to 1
    if strcmpi(options.algorithm.METHOD,'saem') && options.algorithm.IMPORTANCESAMPLING == 0,
        error('When using NONMEM/SAEM, please set the options.algorithm.IMPORTANCESAMPLING=1.');
    end
    SBPOPcreateNONMEMproject(model,dosing,data,BASEmodelFolder,options);
    projectInfo = parseProjectHeaderNONMEMSBPOP(BASEmodelFolder);
elseif strcmpi(TOOL,'monolix'),
    SBPOPcreateMONOLIXproject(model,dosing,data,BASEmodelFolder,options);
    projectInfo = parseProjectHeaderMONOLIXSBPOP(BASEmodelFolder);
end

%% %%%%%%%%%%%%%%%%%%%%%%%%%
% Get param cov and cat
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
parNames    = projectInfo.PARAMNAMES;
covNames    = projectInfo.COVNAMES;
catNames    = projectInfo.CATNAMES;
covcatNames = [covNames catNames];

%% %%%%%%%%%%%%%%%%%%%%%%%%%
% If list of covariates empty then generate it
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if isempty(covariateTests)
    covariateTests = cell(length(parNames),1);
    for k1=1:length(parNames),
        cTk{1} = parNames{k1};
        for k2=1:length(covcatNames),
            cTk{k2+1} = covcatNames{k2};
        end
        covariateTests{k1} = cTk(:)';
    end
end

%% %%%%%%%%%%%%%%%%%%%%%%%%%
% Get covariate list for parameters and covariate names
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
covSearch = [];
covSearch.paramNamesTest  = {};
covSearch.covcatNamesTest = {};
% Get param and covcat names from covariateTests
for k1=1:length(covariateTests),
    for k2=2:length(covariateTests{k1}),
        covSearch.paramNamesTest{end+1}  = covariateTests{k1}{1};
        covSearch.covcatNamesTest{end+1} = covariateTests{k1}{k2};
    end
end

%% %%%%%%%%%%%%%%%%%%%%%%%%%
% Check covariate list for parameters and covariate names
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for k=1:length(covSearch.paramNamesTest),
    ix = strmatchSB(covSearch.paramNamesTest{k},parNames,'exact');
    if isempty(ix),
        error('Parameter %s is not estimated in the model.',covSearch.paramNamesTest{k});
    end
end

for k=1:length(covSearch.covcatNamesTest),
    ix = strmatchSB(covSearch.covcatNamesTest{k},covcatNames,'exact');
    if isempty(ix),
        error('Covariate %s is not present in the dataset.',covSearch.covcatNamesTest{k});
    end
end

%% %%%%%%%%%%%%%%%%%%%%%%%%%
% Determine structural covariate information
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
covStructural = [];
covStructural.paramNames  = {};
covStructural.covcatNames = {};
covStructural.value       = [];
covStructural.estimate    = [];
terms = strrep(strrep(explodePCSB(covariateModel,',','{','}'),'{',''),'}','');
for k=1:length(terms),
    x = explodePCSB(terms{k});
    for k2=2:length(x),
        covStructural.paramNames{end+1} = x{1};
        covStructural.covcatNames{end+1} = x{k2};
        if isempty(covariateModelValues),
            covStructural.value(end+1) = 0.1;
        else
            covStructural.value(end+1) = covariateModelValues{k}(k2-1);
        end
        if isempty(COVestimate),
            covStructural.estimate(end+1) = 1;
        else
            covStructural.estimate(end+1) = COVestimate{k}(k2-1);
        end
    end
    
end

%% %%%%%%%%%%%%%%%%%%%%%%%%%
% Remove structural covariates from covSearch
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ix_remove = [];
for k=1:length(covStructural.paramNames),
    par = covStructural.paramNames{k};
    cov = covStructural.covcatNames{k};
    ix1 = strmatchSB(par,covSearch.paramNamesTest,'exact');
    ix2 = strmatchSB(cov,covSearch.covcatNamesTest,'exact');
    if ~isempty(intersect(ix1,ix2)),
        ix_remove(end+1) = intersect(ix1,ix2);
    end
end
if ~isempty(ix_remove),
    covSearch.paramNamesTest(ix_remove) = [];
    covSearch.covcatNamesTest(ix_remove) = [];
end

%% %%%%%%%%%%%%%%%%%%%%%%%%%
% Determine delta Objective functions required for covariates
% DF: 1 for continuous, N-1 for categorical
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for k=1:length(covSearch.covcatNamesTest),
    % Check if categorical
    if ~isempty(strmatchSB(covSearch.covcatNamesTest{k},covNames,'exact')),
        % Continuous covariate
        N_DF = 1;
    else
        % Categorical covariate
        N_categories = length(unique(dataContents.(covSearch.covcatNamesTest{k})));
        if N_categories > 2,
            error('Categorical covariate "%s" has more than 2 categories - this is not allowed.',covSearch.covcatNamesTest{k});
            N_DF = length(unique(dataContents.(covSearch.covcatNamesTest{k}))) - 1;
        end
    end
    covSearch.delta_forward(k)   = chi2inv(1-p_forward,N_DF);
    covSearch.delta_backward(k)  = chi2inv(1-p_backward,N_DF);
end

% For later
covSearchBackUp = covSearch;

%% %%%%%%%%%%%%%%%%%%%%%%%%%
% Open report file
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
mkdir(outputPath);
fid = fopen([outputPath '/SCMlogfile.txt'],'w');

fprintf(fid,'****************************************************************************\n');
fprintf(fid,'* Covariate search using forward inclusion and backward elimination method *\n');
fprintf(fid,'****************************************************************************\n');;
fprintf(fid,'p-value forward:      %g\n',p_forward);
fprintf(fid,'p-value backward:     %g\n',p_backward);
fprintf(fid,'\n');
if strcmpi(type,'P'),
    fprintf(fid,'Decisions based on probability of CI of covariate coefficient including 0.\n');
elseif strcmpi(type,'OBJ'),
    fprintf(fid,'Decisions based on delta objective function.\n');
else
    error('Incorrect definition of "type"');
end
fprintf(fid,'\n');

fprintf(fid,'************************************\n');
fprintf(fid,'* RUNNING BASE MODEL (MODEL_BASE)  *\n');
fprintf(fid,'************************************\n');
fprintf(fid,'\n');

%% %%%%%%%%%%%%%%%%%%%%%%%%%
% Run the BASE model (MODEL_BASE)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
BASEmodelFolder = [projectPath '/MODEL_BASE'];
if strcmpi(TOOL,'nonmem')
    SBPOPrunNONMEMproject(BASEmodelFolder,N_PROCESSORS_SINGLE);
    RESULTS = parseNONMEMresultsSBPOP(BASEmodelFolder,1);
elseif strcmpi(TOOL,'monolix'),
    SBPOPrunMONOLIXproject(BASEmodelFolder,N_PROCESSORS_SINGLE);
    RESULTS = parseMONOLIXresultsSBPOP(BASEmodelFolder);
end
close all; drawnow();

%% %%%%%%%%%%%%%%%%%%%%%%%%%
% Get OFV and optimal parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
BASE_OFV            = RESULTS.objectivefunction.OBJ;

%% %%%%%%%%%%%%%%%%%%%%%%%%%
% Update options with optimal BASE model parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
options.POPvalues0  = RESULTS.rawParameterInfo.fixedEffects.values;
options.IIVvalues0  = RESULTS.rawParameterInfo.randomEffects.values;
options.errorParam0 = RESULTS.rawParameterInfo.errorParameter.values;

%% %%%%%%%%%%%%%%%%%%%%%%%%%
% Update data path again for the level models
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
data.dataRelPathFromProject = ['../' data.dataRelPathFromProject];

%% %%%%%%%%%%%%%%%%%%%%%%%%%
% Forward inclusion
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fprintf(fid,'************************************\n');
fprintf(fid,'* FORWARD INCLUSION                *\n');
fprintf(fid,'************************************\n');
fprintf(fid,'\n');

OLD_OFV                 = BASE_OFV;
covariatesForward       = {};
%%%
covariateModelValuesForward = {};
COVestimateForward          = {};
%%%
countModel              = 1;
Nmaxmodels              = (length(covSearch.covcatNamesTest)^2-length(covSearch.covcatNamesTest))/2+length(covSearch.covcatNamesTest);
continueForwardSearch   = 1;
levelCount              = 1;
levelModel              = '';
ForwardModelName        = BASEmodelFolder;
ModelNameAll            = {};

while continueForwardSearch,
    fprintf(fid,'* Level %d: ',levelCount);
    
    % Run through remaining parameter/covariate combinations
    % Generate all models first and then run all at once
    covariatesTestForward = {};
    %%%
    covariateModelValuesTESTForward = {};
    COVestimateTESTForward          = {};
    %%%
    covariateModelAll     = {};
    for k1=1:length(covSearch.paramNamesTest),
        
        % Reset covariate setting to previous level
        covariatesTest = covariatesForward;
        %%%
        covariateModelValues = covariateModelValuesForward;
        COVestimate = COVestimateForward;
        %%%
        
        % Get new parameter/covariate combination to test
        param = covSearch.paramNamesTest{k1};
        cov   = covSearch.covcatNamesTest{k1};
        delta = covSearch.delta_forward(k1);
        
        %%%
        value = 0.1; % Setting default value to 0.1 (NONMEM does not allow 0)
        
        % Build covariate test structure and covariateModelValues and COVestimate
        ix = [];
        for k2=1:length(covariatesTest),
            if strcmp(covariatesTest{k2}{1},param),
                ix = k2;
            end
        end
        if isempty(ix),
            covariatesTest{end+1}{1} = param;
            covariatesTest{end}{2} = cov;
            covariateModelValues{end+1} = value;
            COVestimate{end+1} = 1;
        else
            covariatesTest{ix}{end+1} = cov;
            covariateModelValues{ix}(end+1) = value;
            COVestimate{ix}(end+1) = 1;
        end
        covariatesTest = covariatesTest(:);
        %%%
        
        % Save for later
        covariatesTestForward{k1} = covariatesTest;
        %%%
        covariateModelValuesTESTForward{k1} = covariateModelValues;
        COVestimateTESTForward{k1}          = COVestimate;
        %%%
        
        % Convert to required format
        covariateModel                      = '';
        for k3=1:size(covariatesTest,1),
            if ~isempty(covariatesTest{k3}),
                text                        = sprintf('%s,',covariatesTest{k3}{:});
                covariateModel              = [covariateModel '{' text(1:end-1) '},'];
            end
        end
        covariateModel                      = covariateModel(1:end-1);
        covariateModelAll{k1}               = covariateModel;
        
        % Determine COVestimate and covariateModelValues
        COVestimate = {};
        covariateModelValues = {};
        for k3=1:size(covariatesTest,1),
            COVestimate{k3} = ones(1,length(covariatesTest{k3})-1);
            covariateModelValues{k3} = 0.1*ones(1,length(covariatesTest{k3})-1);
        end
        
        % Add structural covariates to covariate model
        for k3=1:length(covStructural.paramNames),
            param    = covStructural.paramNames{k3};
            cov      = covStructural.covcatNames{k3};
            value    = covStructural.value(k3);
            estimate = covStructural.estimate(k3);
            % Find where to add
            ix = [];
            for k4=1:size(covariatesTest,1),
                if strcmp(param,covariatesTest{k4}{1}),
                    ix = k4;
                    break;
                end
            end
            if ~isempty(ix),
                % add info
                covariatesTest{ix}{end+1} = cov;
                covariateModelValues{ix}(end+1) = value;
                COVestimate{ix}(end+1) = estimate;
            else
                % new param
                covariatesTest{end+1}{1} = param;
                covariatesTest{end}{2} = cov;
                covariateModelValues{end+1}(1) = value;
                COVestimate{end+1}(1) = estimate;
            end
            covariatesTest = covariatesTest(:);
        end
        
        % Convert to required format for the search things
        covariateModel                      = '';
        for k3=1:size(covariatesTest,1),
            if ~isempty(covariatesTest{k3}),
                text                        = sprintf('%s,',covariatesTest{k3}{:});
                covariateModel              = [covariateModel '{' text(1:end-1) '},'];
            end
        end
        covariateModel                      = covariateModel(1:end-1);
        
        % Set covariateModel
        options.covariateModel              = covariateModel;
        options.covariateModelValues        = covariateModelValues;
        options.COVestimate                 = COVestimate;
        
        % Create model
        ModelName                           = sprintf('MODEL_%s',preFillCharSB(countModel,length(num2str(Nmaxmodels)),'0'));
        FolderName                          = [projectPath sprintf('/FW_LEVEL_%d',levelCount)];
        ModelFolder                         = [FolderName '/' ModelName];
        ModelNameAll{k1}                    = ModelFolder;
        if strcmpi(TOOL,'nonmem')
            SBPOPcreateNONMEMproject(model,dosing,data,ModelFolder,options);
        elseif strcmpi(TOOL,'monolix'),
            SBPOPcreateMONOLIXproject(model,dosing,data,ModelFolder,options);
        end
        countModel                          = countModel + 1;
    end
    
    % Write out level base cov model
    fprintf(fid,'"%s"\n',levelModel);
    fprintf(fid,'\n');
    
    % Run the level models - do not create GoF plots
    SBPOPrunNLMEprojectFolder(FolderName,N_PROCESSORS_PAR,N_PROCESSORS_SINGLE,1);
    
    % Read the results
    LEVEL_RESULTS = SBPOPfitanalysisProjectsFolderInfo(FolderName,FolderName);
    
    % Determine significance in different ways, defined by "type"
    LEVEL_SIGNIFICANCE = [];
    % Get OBJ
    LEVEL_OBJ = [LEVEL_RESULTS.OBJ];
    
    % Handle P or OBJ type
    if strcmpi(type,'OBJ'),
        % BASED ON OBJECTIVE FUNCTION VALUE
        % Print out info and determine significance
        for k1=1:length(covSearch.paramNamesTest),
            fprintf(fid,'%s (%s on %s): DeltaOFV=%g Goal>=%g ... ',LEVEL_RESULTS(k1).model,covSearch.covcatNamesTest{k1},covSearch.paramNamesTest{k1},OLD_OFV-LEVEL_OBJ(k1),covSearch.delta_forward(k1));
            if OLD_OFV-LEVEL_OBJ(k1) >= covSearch.delta_forward(k1),
                LEVEL_SIGNIFICANCE(k1) = 1;
                fprintf(fid,'significant');
            else
                LEVEL_SIGNIFICANCE(k1) = 0;
                fprintf(fid,'not significant');
            end
            fprintf(fid,'\n');
        end
        fprintf(fid,'\n');
        
        % Determine which covariate leads to the largest drop in OBJ
        [xdummyx,ixBEST] = max(OLD_OFV-LEVEL_OBJ);
        
    else
        % BASED ON ESTIMATED COVARIATE COEFFICIENTS
        LEVEL_PVALUES = Inf(1,length(covSearch.paramNamesTest));
        % Get covariate estimate informations for the current fit
        for k1=1:length(covSearch.paramNamesTest),
            param = covSearch.paramNamesTest{k1};
            cov   = covSearch.covcatNamesTest{k1};
            for k2=1:length(LEVEL_RESULTS(k1).rawParameterInfo.covariate.names),
                xxx = strrep(LEVEL_RESULTS(k1).rawParameterInfo.covariate.names{k2},'(','_');
                xxx = strrep(xxx,')','_');
                test1 = strfind(xxx,['_' param '_']);
                test2 = strfind(xxx,['_' cov]);
                if ~isempty(test1) && ~isempty(test2),
                    % Yes, this was a tested covariate
                    % Calculate the p-value
                    mu = LEVEL_RESULTS(k1).rawParameterInfo.covariate.values(k2);
                    sigma = LEVEL_RESULTS(k1).rawParameterInfo.covariate.stderr(k2);
                    p = min(normcdf(0,abs(mu),abs(sigma))*2);
                    LEVEL_PVALUES(k1) = min(p,LEVEL_PVALUES(k1)); % Minimum handles multiple levels categorical covs
                end
            end
        end
        for k1=1:length(LEVEL_PVALUES),
            fprintf(fid,'%s (%s on %s): P=%g Goal<=%g ... ',LEVEL_RESULTS(k1).model,covSearch.covcatNamesTest{k1},covSearch.paramNamesTest{k1},LEVEL_PVALUES(k1),p_forward);
            if LEVEL_PVALUES(k1)<=p_forward,
                LEVEL_SIGNIFICANCE(k1) = 1;
                fprintf(fid,'significant');
            else
                LEVEL_SIGNIFICANCE(k1) = 0;
                fprintf(fid,'not significant');
            end
            fprintf(fid,'\n');
        end
        fprintf(fid,'\n');
        
        % Determine which covariate leads to the lowest p-value
        [xdummyx,ixBEST] = min(LEVEL_PVALUES);
    end
    
    % If none significant then stop forward inclusion here
    if sum(LEVEL_SIGNIFICANCE) == 0,
        continueForwardSearch = 0;
    else
        % Message
        fprintf(fid,'\n');
        fprintf(fid,'Retained covariate for next level: %s on %s\n',covSearch.covcatNamesTest{ixBEST},covSearch.paramNamesTest{ixBEST});
        fprintf(fid,'\n');
        % Setup for new level
        OLD_OFV = LEVEL_OBJ(ixBEST);
        % Update options with optimal model parameters from last run
        options.POPvalues0  = LEVEL_RESULTS(ixBEST).rawParameterInfo.fixedEffects.values;
        options.IIVvalues0  = LEVEL_RESULTS(ixBEST).rawParameterInfo.randomEffects.values;
        options.errorParam0 = LEVEL_RESULTS(ixBEST).rawParameterInfo.errorParameter.values;
        % Remove covariate from search
        covSearch.covcatNamesTest(ixBEST) = [];
        covSearch.delta_forward(ixBEST)  = [];
        covSearch.delta_backward(ixBEST)  = [];
        covSearch.paramNamesTest(ixBEST)  = [];
        % Set new base covariates
        covariatesForward = covariatesTestForward{ixBEST};
        
        %%%
        covariateModelValuesForward = covariateModelValuesTESTForward{ixBEST};
        COVestimateForward = COVestimateTESTForward{ixBEST};
        
        % Update estimated covariate coefficient for next level - with all new
        % estimates!
        covInfo = LEVEL_RESULTS(ixBEST).rawParameterInfo.covariate;
        
        for k1x = 1:length(covariatesForward),
            paramUpdate = covariatesForward{k1x}{1};
            for k2x = 2:length(covariatesForward{k1x}),
                covUpdate = covariatesForward{k1x}{k2x};
                % Find parameter and covariate
                matchIX = [];
                for kkkx=1:length(covInfo.names),
                    ixParam = strfind(covInfo.names{kkkx},paramUpdate);
                    ixCov = strfind(covInfo.names{kkkx},covUpdate);
                    if ~isempty(ixParam) && ~isempty(ixCov),
                        matchIX(end+1) = kkkx;
                    end
                end
                if length(matchIX) ~= 1,
                    error('Problem with getting covariate coefficient value.');
                end
                % Get the estimated value
                value = covInfo.values(matchIX);
                % Add value to covariateModelValuesForward
                ix = [];
                for k2=1:length(covariatesForward),
                    if strcmp(covariatesForward{k2}{1},paramUpdate),
                        ix = k2;
                    end
                end
                covariateModelValuesForward{ix}(strmatchSB(covUpdate,covariatesForward{ix},'exact')-1) = value;
            end
        end
        %%%
        
        
        levelModel = covariateModelAll{ixBEST};
        ForwardModelName = ModelNameAll{ixBEST};
        levelCount = levelCount+1;
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Get forward results
%%%%%%%%%%%%%%%%%%%%%%%%%%%
FORWARD_OFV = OLD_OFV;
forwardModel = levelModel;

%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Report forward model
%%%%%%%%%%%%%%%%%%%%%%%%%%%

fprintf(fid,'************************************\n');
fprintf(fid,'* FORWARD MODEL RESULTS            *\n');
fprintf(fid,'************************************\n');
fprintf(fid,'\n');

fprintf(fid,'Forward model:             %s\n',ForwardModelName);
fprintf(fid,'Covariates:                %s\n',forwardModel);
fprintf(fid,'Objective function value:  %g\n',FORWARD_OFV);
fprintf(fid,'\n');


%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Backward elimination
% Handle P and OBJ differently
%%%%%%%%%%%%%%%%%%%%%%%%%%%
fprintf(fid,'************************************\n');
fprintf(fid,'* BACKWARD ELIMINATION             *\n');
fprintf(fid,'************************************\n');
fprintf(fid,'\n');

%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Create covSearch structure for backward search
%%%%%%%%%%%%%%%%%%%%%%%%%%%
covSearchBackward = covSearchBackUp;
ix_keep = [];
for k1=1:size(covariatesForward),
    param = covariatesForward{k1}{1};
    for k2=2:length(covariatesForward{k1}),
        cov = covariatesForward{k1}{k2};
        ix_keep(end+1) = intersect(strmatchSB(param,covSearchBackward.paramNamesTest,'exact'),strmatchSB(cov,covSearchBackward.covcatNamesTest,'exact'));
    end
end
covSearchBackward.covcatNamesTest = covSearchBackward.covcatNamesTest(ix_keep);
covSearchBackward.paramNamesTest = covSearchBackward.paramNamesTest(ix_keep);
covSearchBackward.delta_forward = covSearchBackward.delta_forward(ix_keep);
covSearchBackward.delta_backward = covSearchBackward.delta_backward(ix_keep);

%%%
% Add parameter values
for k1=1:length(covSearchBackward.paramNamesTest),
    paramName = covSearchBackward.paramNamesTest{k1};
    covName   = covSearchBackward.covcatNamesTest{k1};
    for k2=1:size(covariatesForward),
        if strcmp(paramName,covariatesForward{k2}{1}),
            ix = k2;
            break;
        end
    end
    ix2 = strmatchSB(covName,covariatesForward{ix}(2:end),'exact');
    value = covariateModelValuesForward{ix}(ix2);
    covSearchBackward.value(k1) = value;
end
%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Do the search
%%%%%%%%%%%%%%%%%%%%%%%%%%%
covariateModelAll  = {};
levelCount         = 1;
countModel         = 1;
BackwardModelName  = ForwardModelName;
ModelNameAll       = {};

% Check if backward search needed
if ~isempty(covariatesForward),
    continueBackwardSearch = 1;
else
    continueBackwardSearch = 0;
end

if strcmpi(type,'OBJ'),
    % OBJ:  Start with FORWARD_OFV and remove one by one ... until deltaOFV>... for
    
    while continueBackwardSearch,
        fprintf(fid,'* Level %d: ',levelCount);
        
        % Run through remaining parameter/covariate combinations
        % Generate all models first and then run all at once
        
        for k1=1:length(covSearchBackward.paramNamesTest),
            
            % Reset covariate setting to previous level
            covariatesTest = covSearchBackward;
            
            % Remove parameter to test
            %%%
            param = covariatesTest.paramNamesTest;
            cov   = covariatesTest.covcatNamesTest;
            value = covariatesTest.value;
            param(k1) = [];
            cov(k1)   = [];
            value(k1) = [];
            %%%
            
            % Build covariate test structure
            x = unique(param);
            covTestStructure = {};
            %%%
            covariateModelValues = cell(1,length(x));
            COVestimate = cell(1,length(x));
            %%%
            
            for k2=1:length(x),
                covTestStructure{k2}{1} = x{k2};
            end
            covTestStructure = covTestStructure(:);
            for k2=1:length(param),
                ix = strmatchSB(param{k2},x,'exact');
                covTestStructure{ix}{end+1} = cov{k2};
                %%%
                covariateModelValues{ix}(end+1) = value(k2);
                COVestimate{ix}(end+1) = 1;
                %%%
                
            end
            
            % Convert to required format
            covariateModel                      = '';
            for k3=1:size(covTestStructure,1),
                if ~isempty(covTestStructure{k3}),
                    text                        = sprintf('%s,',covTestStructure{k3}{:});
                    covariateModel              = [covariateModel '{' text(1:end-1) '},'];
                end
            end
            covariateModel                      = covariateModel(1:end-1);
            covariateModelAll{k1}               = covariateModel;
            
            % Add structural covariates to covariate model
            for k3=1:length(covStructural.paramNames),
                param    = covStructural.paramNames{k3};
                cov      = covStructural.covcatNames{k3};
                value    = covStructural.value(k3);
                estimate = covStructural.estimate(k3);
                % Find where to add
                ix = [];
                for k4=1:size(covTestStructure,1),
                    if strcmp(param,covTestStructure{k4}{1}),
                        ix = k4;
                        break;
                    end
                end
                if ~isempty(ix),
                    % add info
                    covTestStructure{ix}{end+1} = cov;
                    covariateModelValues{ix}(end+1) = value;
                    COVestimate{ix}(end+1) = estimate;
                else
                    % new param
                    covTestStructure{end+1}{1} = param;
                    covTestStructure{end}{2} = cov;
                    covariateModelValues{end+1}(1) = value;
                    COVestimate{end+1}(1) = estimate;
                end
                covTestStructure = covTestStructure(:);
            end
            
            % Convert to required format for the search things
            covariateModel                      = '';
            for k3=1:size(covTestStructure,1),
                if ~isempty(covTestStructure{k3}),
                    text                        = sprintf('%s,',covTestStructure{k3}{:});
                    covariateModel              = [covariateModel '{' text(1:end-1) '},'];
                end
            end
            covariateModel                      = covariateModel(1:end-1);
            
            % Set covariateModel
            options.covariateModel              = covariateModel;
            options.covariateModelValues        = covariateModelValues;
            options.COVestimate                 = COVestimate;
            
            % Create model
            ModelName                           = sprintf('MODEL_%s',preFillCharSB(countModel,length(num2str(Nmaxmodels)),'0'));
            FolderName                          = [projectPath sprintf('/BW_LEVEL_%d',levelCount)];
            ModelFolder                         = [FolderName '/' ModelName];
            ModelNameAll{k1}                    = ModelFolder;
            if strcmpi(TOOL,'nonmem')
                SBPOPcreateNONMEMproject(model,dosing,data,ModelFolder,options);
            elseif strcmpi(TOOL,'monolix'),
                SBPOPcreateMONOLIXproject(model,dosing,data,ModelFolder,options);
            end
            countModel                          = countModel + 1;
        end
        
        % Write out level base cov model
        fprintf(fid,'"%s"\n',levelModel);
        fprintf(fid,'\n');
        
        % Run the level models - do not create GoF plots
        SBPOPrunNLMEprojectFolder(FolderName,N_PROCESSORS_PAR,N_PROCESSORS_SINGLE,1);
        
        % Read the results
        LEVEL_RESULTS = SBPOPfitanalysisProjectsFolderInfo(FolderName,FolderName);
        
        % Determine significance in different ways, defined by "type"
        LEVEL_SIGNIFICANCE = [];
        % Get OBJ
        LEVEL_OBJ = [LEVEL_RESULTS.OBJ];
        
        % Print out info and determine significance
        for k1=1:length(covSearchBackward.paramNamesTest),
            fprintf(fid,'%s (%s on %s): DeltaOFV=%g Goal>=%g ... ',LEVEL_RESULTS(k1).model,covSearchBackward.covcatNamesTest{k1},covSearchBackward.paramNamesTest{k1},-OLD_OFV+LEVEL_OBJ(k1),covSearchBackward.delta_backward(k1));
            if -OLD_OFV+LEVEL_OBJ(k1) >= covSearchBackward.delta_backward(k1),
                LEVEL_SIGNIFICANCE(k1) = 1;
                fprintf(fid,'significant');
            else
                LEVEL_SIGNIFICANCE(k1) = 0;
                fprintf(fid,'not significant');
            end
            fprintf(fid,'\n');
        end
        fprintf(fid,'\n');
        
        % Determine which covariate leads to the largest increase in OBJ
        [xdummyx,ixBEST] = min(-OLD_OFV+LEVEL_OBJ);
        
        % If all significant then stop backward elimination here
        if sum(LEVEL_SIGNIFICANCE~=1) == 0,
            continueBackwardSearch = 0;
        else
            % Message
            fprintf(fid,'\n');
            fprintf(fid,'Removed covariate for next level: %s on %s\n',covSearchBackward.covcatNamesTest{ixBEST},covSearchBackward.paramNamesTest{ixBEST});
            fprintf(fid,'\n');
            % Setup for new level
            OLD_OFV = LEVEL_OBJ(ixBEST);
            % Update options with optimal model parameters from last run
            options.POPvalues0  = LEVEL_RESULTS(ixBEST).rawParameterInfo.fixedEffects.values;
            options.IIVvalues0  = LEVEL_RESULTS(ixBEST).rawParameterInfo.randomEffects.values;
            options.errorParam0 = LEVEL_RESULTS(ixBEST).rawParameterInfo.errorParameter.values;
            % Remove covariate from search
            covSearchBackward.covcatNamesTest(ixBEST) = [];
            covSearchBackward.delta_forward(ixBEST)  = [];
            covSearchBackward.delta_backward(ixBEST)  = [];
            covSearchBackward.paramNamesTest(ixBEST)  = [];
            levelModel = covariateModelAll{ixBEST};
            levelCount = levelCount+1;
            BackwardModelName = ModelNameAll{ixBEST};
            %%%
            % Update values in covSearchBackward to estimated ones in best model
            covInfo = LEVEL_RESULTS(ixBEST).rawParameterInfo.covariate;
            
            for k1x = 1:length(covSearchBackward.paramNamesTest),
                paramUpdate = covSearchBackward.paramNamesTest{k1x};
                covUpdate   = covSearchBackward.covcatNamesTest{k1x};
                
                % Find parameter and covariate
                matchIX = [];
                for kkkx=1:length(covInfo.names),
                    ixParam = strfind(covInfo.names{kkkx},paramUpdate);
                    ixCov = strfind(covInfo.names{kkkx},covUpdate);
                    if ~isempty(ixParam) && ~isempty(ixCov),
                        matchIX(end+1) = kkkx;
                    end
                end
                if length(matchIX) ~= 1,
                    error('Problem with getting covariate coefficient value.');
                end
                % Get the estimated value
                value = covInfo.values(matchIX);
                % Add value to covSearchBackward
                covSearchBackward.value(k1x) = value;
            end
            %%%
        end
        
        if  isempty(covSearchBackward.paramNamesTest),
            continueBackwardSearch = 0;
        end
    end
else
    % P:    Remove largest of the p-values>p_backward then rerun until all p-values<=p_backward
    
    while continueBackwardSearch,
        if  isempty(covSearchBackward.paramNamesTest),
            continueBackwardSearch = 0;
            break
        end
        
        % Get all P values for the last model
        if strcmpi(TOOL,'nonmem')
            RESULTS = parseNONMEMresultsSBPOP(BackwardModelName,1);
        elseif strcmpi(TOOL,'monolix'),
            RESULTS = parseMONOLIXresultsSBPOP(BackwardModelName);
        end
        P_VALUES = Inf(1,length(covSearchBackward.paramNamesTest));
        for k1=1:length(covSearchBackward.paramNamesTest),
            param = covSearchBackward.paramNamesTest{k1};
            cov   = covSearchBackward.covcatNamesTest{k1};
            for k2=1:length(RESULTS.rawParameterInfo.covariate.names),
                xxx = strrep(RESULTS.rawParameterInfo.covariate.names{k2},'(','_');
                xxx = strrep(xxx,')','_');
                test1 = strfind(xxx,['_' param '_']);
                test2 = strfind(xxx,['_' cov]);
                if ~isempty(test1) && ~isempty(test2),
                    % Yes, this was a tested covariate
                    % Calculate the p-value
                    mu = RESULTS.rawParameterInfo.covariate.values(k2);
                    sigma = RESULTS.rawParameterInfo.covariate.stderr(k2);
                    p = min(normcdf(0,abs(mu),abs(sigma))*2);
                    P_VALUES(k1) = min(p,P_VALUES(k1)); % Minimum handles multiple levels categorical covs
                end
            end
        end
        
        % Check if all P values <= p_backward
        if sum(P_VALUES > p_backward) ~= 0,
            continueBackwardSearch = 1;
            
            % Message
            fprintf(fid,'* Level %d: "%s"\n',levelCount,levelModel);
            fprintf(fid,'\n');
            
            for k1=1:length(covSearchBackward.paramNamesTest),
                fprintf(fid,'%s on %s: P=%g Goal<=%g ... ',covSearchBackward.covcatNamesTest{k1},covSearchBackward.paramNamesTest{k1},P_VALUES(k1),p_backward);
                if P_VALUES(k1)<=p_backward,
                    fprintf(fid,'significant');
                else
                    fprintf(fid,'not significant');
                end
                fprintf(fid,'\n');
            end
            
            % Remove the largest P_value
            [xdummyx,ix_remove] = max(P_VALUES);
            
            % Message
            fprintf(fid,'\n');
            fprintf(fid,'Removed covariate for next level: %s on %s\n',covSearchBackward.covcatNamesTest{ix_remove},covSearchBackward.paramNamesTest{ix_remove});
            fprintf(fid,'\n');
            
            % Remove covariate
            covSearchBackward.paramNamesTest(ix_remove) = [];
            covSearchBackward.covcatNamesTest(ix_remove) = [];
            covSearchBackward.delta_forward(ix_remove) = [];
            covSearchBackward.delta_backward(ix_remove) = [];
            %%%
            covSearchBackward.value(ix_remove) = [];
            %%%
            
            % Build covariate test structure
            x = unique(covSearchBackward.paramNamesTest);
            covTestStructure = {};
            %%%
            covariateModelValues = cell(1,length(x));
            COVestimate = cell(1,length(x));
            %%%
            for k2=1:length(x),
                covTestStructure{k2}{1} = x{k2};
            end
            covTestStructure = covTestStructure(:);
            for k2=1:length(covSearchBackward.paramNamesTest),
                ix = strmatchSB(covSearchBackward.paramNamesTest{k2},x,'exact');
                covTestStructure{ix}{end+1} = covSearchBackward.covcatNamesTest{k2};
                %%%
                covariateModelValues{ix}(end+1) = covSearchBackward.value(k2);
                COVestimate{ix}(end+1) = 1;
                %%%
            end
            
            % Convert to required format
            covariateModel                      = '';
            for k3=1:size(covTestStructure,1),
                if ~isempty(covTestStructure{k3}),
                    text                        = sprintf('%s,',covTestStructure{k3}{:});
                    covariateModel              = [covariateModel '{' text(1:end-1) '},'];
                end
            end
            covariateModel                      = covariateModel(1:end-1);
            
            % Add structural covariates to covariate model
            for k3=1:length(covStructural.paramNames),
                param    = covStructural.paramNames{k3};
                cov      = covStructural.covcatNames{k3};
                value    = covStructural.value(k3);
                estimate = covStructural.estimate(k3);
                % Find where to add
                ix = [];
                for k4=1:size(covTestStructure,1),
                    if strcmp(param,covTestStructure{k4}{1}),
                        ix = k4;
                        break;
                    end
                end
                if ~isempty(ix),
                    % add info
                    covTestStructure{ix}{end+1} = cov;
                    covariateModelValues{ix}(end+1) = value;
                    COVestimate{ix}(end+1) = estimate;
                else
                    % new param
                    covTestStructure{end+1}{1} = param;
                    covTestStructure{end}{2} = cov;
                    covariateModelValues{end+1}(1) = value;
                    COVestimate{end+1}(1) = estimate;
                end
                covTestStructure = covTestStructure(:);
            end
            
            % Convert to required format for the search things
            covariateModel                      = '';
            for k3=1:size(covTestStructure,1),
                if ~isempty(covTestStructure{k3}),
                    text                        = sprintf('%s,',covTestStructure{k3}{:});
                    covariateModel              = [covariateModel '{' text(1:end-1) '},'];
                end
            end
            covariateModel                      = covariateModel(1:end-1);
            
            % Set covariateModel
            options.covariateModel              = covariateModel;
            options.covariateModelValues        = covariateModelValues;
            options.COVestimate                 = COVestimate;
            
            
            % Create model
            ModelName                           = sprintf('MODEL_%s',preFillCharSB(countModel,length(num2str(Nmaxmodels)),'0'));
            FolderName                          = [projectPath sprintf('/BW_LEVEL_%d',levelCount)];
            ModelFolder                         = [FolderName '/' ModelName];
            if strcmpi(TOOL,'nonmem')
                SBPOPcreateNONMEMproject(model,dosing,data,ModelFolder,options);
            elseif strcmpi(TOOL,'monolix'),
                SBPOPcreateMONOLIXproject(model,dosing,data,ModelFolder,options);
            end
            countModel                          = countModel + 1;
            
            % Run the model - do not create GoF plots
            SBPOPrunNLMEprojectFolder(FolderName,N_PROCESSORS_PAR,N_PROCESSORS_SINGLE,1);
            
            % Read information
            LEVEL_RESULTS = SBPOPfitanalysisProjectsFolderInfo(FolderName,FolderName);
            OLD_OFV = LEVEL_RESULTS.OBJ;
            
            % Define things for next level
            options.POPvalues0  = LEVEL_RESULTS.rawParameterInfo.fixedEffects.values;
            options.IIVvalues0  = LEVEL_RESULTS.rawParameterInfo.randomEffects.values;
            options.errorParam0 = LEVEL_RESULTS.rawParameterInfo.errorParameter.values;
            levelModel = covariateModel;
            levelCount = levelCount+1;
            BackwardModelName = ModelFolder;
            %%%
            % Update values in covSearchBackward to estimated ones in best model
            covInfo = LEVEL_RESULTS.rawParameterInfo.covariate;
            
            for k1x = 1:length(covSearchBackward.paramNamesTest),
                paramUpdate = covSearchBackward.paramNamesTest{k1x};
                covUpdate   = covSearchBackward.covcatNamesTest{k1x};
                
                % Find parameter and covariate
                matchIX = [];
                for kkkx=1:length(covInfo.names),
                    ixParam = strfind(covInfo.names{kkkx},paramUpdate);
                    ixCov = strfind(covInfo.names{kkkx},covUpdate);
                    if ~isempty(ixParam) && ~isempty(ixCov),
                        matchIX(end+1) = kkkx;
                    end
                end
                if length(matchIX) ~= 1,
                    error('Problem with getting covariate coefficient value.');
                end
                % Get the estimated value
                value = covInfo.values(matchIX);
                % Add value to covSearchBackward
                covSearchBackward.value(k1x) = value;
            end
            %%%
        else
            continueBackwardSearch = 0;
            
            % Message
            fprintf(fid,'* Level %d: "%s"\n',levelCount,levelModel);
            fprintf(fid,'\n');
            
            for k1=1:length(covSearchBackward.paramNamesTest),
                fprintf(fid,'%s on %s: P=%g Goal<=%g ... ',covSearchBackward.covcatNamesTest{k1},covSearchBackward.paramNamesTest{k1},P_VALUES(k1),p_backward);
                if P_VALUES(k1)<=p_backward,
                    fprintf(fid,'significant');
                else
                    fprintf(fid,'not significant');
                end
                fprintf(fid,'\n');
            end
            fprintf(fid,'\n');
            
        end
        
    end
    
    
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Get backward results
%%%%%%%%%%%%%%%%%%%%%%%%%%%
BACKWARD_OFV = OLD_OFV;
backwardModel = levelModel;

%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Report backward model
%%%%%%%%%%%%%%%%%%%%%%%%%%%

fprintf(fid,'************************************\n');
fprintf(fid,'* BACKWARD MODEL RESULTS           *\n');
fprintf(fid,'************************************\n');
fprintf(fid,'\n');

fprintf(fid,'Backward model:            %s\n',BackwardModelName);
fprintf(fid,'Covariates:                "%s"\n',backwardModel);
fprintf(fid,'Objective function value:  %g\n',BACKWARD_OFV);
fprintf(fid,'\n');


%% %%%%%%%%%%%%%%%%%%%%%%%%%
% Close report file
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fclose(fid);
