%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% PD model development
% Note, this code will not run as is - it is more a reminder of things to do
% and not to have to type everything from scratch.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Setup
clc; clear all; close all; restoredefaultpath();

path2SBPOP          = 'SBPOP PACKAGE';
oldpath             = pwd; cd(path2SBPOP);      installSBPOPpackageInitial();           cd(oldpath);

%% Load data
data        = SBPOPloadCSVdataset('../Data/data_cleaned.csv');
dataPlacebo = SBPOPloadCSVdataset('../Data/data_cleaned_PLACEBO.csv');

%% Definition of general settings
covNames            = {'WT0','XYZ0'};
catNames            = {'STUDY' 'TRT'};
catImputationValues = [ 999     999 ];

PD_NAME             = 'XYZ';

%% Data cleaning
removeSUBJECT       = {};
removeREC           = {};
Nobs                = 1;
options             = [];
options.FLAG_LLOQ   = 0;
options.outputPath  = '../Output/DataCleaning/All/';
datacleaned = SBPOPcleanPopPDdata(data,PD_NAME,removeSUBJECT,removeREC,Nobs,covNames,catNames,catImputationValues,options);

options.outputPath  = '../Output/DataCleaning/Placebo/';
dataPlacebocleaned = SBPOPcleanPopPDdata(dataPlacebo,PD_NAME,removeSUBJECT,removeREC,Nobs,covNames,catNames,catImputationValues,options);

%% Data exploration
options                     = [];
options.outputPath          = '../Output/Graphical_Exploration/All';
options.plotIndividualData  = 0;
options.plotStratifiedData  = 1;
options.plotCovariateData   = 1;
SBPOPexplorePopPDdata(datacleaned,7,covNames,catNames,options)

options                     = [];
options.outputPath          = '../Output/Graphical_Exploration/Placebo';
options.plotIndividualData  = 0;
options.plotStratifiedData  = 1;
options.plotCovariateData   = 1;
SBPOPexplorePopPDdata(dataPlacebocleaned,7,covNames,catNames,options)

%% Convert to modeling datasets
model                       = SBmodel('../Models/PKPDmodel_01.txt');
dosing                      = SBPOPdosing('../Models/PKPDdosing.dos');
pathNLMEproject             = '../Models/MODEL_01_PK_NONMEM/FITMODEL_01_PK_NONMEM_006';

analysisDataset             = '../Data/popPKPD.csv';
dataheader                  = SBPOPconvert2popPDparametersDataset(model,dosing,pathNLMEproject,datacleaned,PD_NAME,covNames,catNames,analysisDataset)

analysisDatasetPlacebo      = '../Data/popPKPD_placebo.csv';
dataheader_placebo          = SBPOPconvert2popPDparametersDataset(model,dosing,pathNLMEproject,dataPlacebocleaned,PD_NAME,covNames,catNames,analysisDatasetPlacebo)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% PLACEBO MODELING
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

model                                   = SBmodel('../Models/PKPDmodel_01.txt');
dosing                                  = SBPOPdosing('../Models/PKPDdosing.dos');
regressionNames                         = {'Fabs1' 'CL' 'Vc' 'Q1' 'Vp1' 'ka'};

dataFIT                                 = [];
dataFIT.dataRelPathFromProject          = '../../../Data';
dataFIT.dataHeaderIdent                 = SBPOPgetNONMEMdataHeader(SBPOPloadCSVdataset('../Data/popPKPD_placebo.csv'),covNames,catNames,regressionNames)
dataFIT.dataFileName                    = 'popPKPD_placebo.csv';

optionsFIT                              = [];

%                                             BASELINE      kout      fS        EMAX    EC50    hill
optionsFIT.POPvalues0                   = [      5          0.01      0.1       0.6     50      8   ];
optionsFIT.POPestimate                  = [      1          1         1         0       0       0   ];
optionsFIT.IIVvalues0                   = [      1          0         1         0       0       0   ];
optionsFIT.IIVestimate                  = [      1          0         1         0       0       0   ];
optionsFIT.IIVdistribution              = {     'N'        'L'       'N'       'L'     'L'     'L'  };
optionsFIT.errorModels                  = 'const';   % additive only
optionsFIT.covarianceModel              = '';
optionsFIT.covariateModel               = '';

optionsFIT.algorithm.SEED               = 123456;
optionsFIT.algorithm.K1                 = 500;
optionsFIT.algorithm.K2                 = 200;
optionsFIT.algorithm.NRCHAINS           = 1;
optionsFIT.algorithm.METHOD             = 'SAEM';
optionsFIT.algorithm.ITS                = 1;
optionsFIT.algorithm.IMPORTANCESAMPLING = 1;

projectPath                             = '../Models/MODEL_02_PD_PLACEBO/FIT_01';

SBPOPcreateNONMEMproject(model,dosing,dataFIT,projectPath,optionsFIT)
SBPOPrunNONMEMproject(projectPath,'nmfe72',10)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% COMPLETE MODELING
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% First model
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
model                                   = SBmodel('../Models/PKPDmodel_01.txt');
dosing                                  = SBPOPdosing('../Models/PKPDdosing.dos');
regressionNames                         = {'Fabs1' 'CL' 'Vc' 'Q1' 'Vp1' 'ka'};

dataFIT                                 = [];
dataFIT.dataRelPathFromProject          = '../../../Data';
dataFIT.dataHeaderIdent                 = SBPOPgetNONMEMdataHeader(SBPOPloadCSVdataset('../Data/popPKPD.csv'),covNames,catNames,regressionNames)
dataFIT.dataFileName                    = 'popPKPD.csv';

optionsFIT                              = [];

%                                             BASELINE      kout      fS        EMAX    EC50    hill
optionsFIT.POPvalues0                   = [      4.8        0.03      0.15      0.3     12      3   ];
optionsFIT.POPestimate                  = [      1          1         0         0       1       0   ];
optionsFIT.IIVvalues0                   = [      1.1        0         0.21      0       1       0   ];
optionsFIT.IIVestimate                  = [      1          0         2         0       1       0   ];
optionsFIT.IIVdistribution              = {     'N'        'L'       'N'       'L'     'L'     'L'  };
optionsFIT.errorModels                  = 'const';   % additive only
optionsFIT.covarianceModel              = '';
optionsFIT.covariateModel               = '';

optionsFIT.algorithm.SEED               = 123456;
optionsFIT.algorithm.K1                 = 500;
optionsFIT.algorithm.K2                 = 200;
optionsFIT.algorithm.NRCHAINS           = 1;
optionsFIT.algorithm.METHOD             = 'SAEM';
optionsFIT.algorithm.ITS                = 1;
optionsFIT.algorithm.IMPORTANCESAMPLING = 1;

projectPath                             = '../Models/MODEL_03_PD/FIT_01';

SBPOPcreateNONMEMproject(model,dosing,dataFIT,projectPath,optionsFIT)
SBPOPrunNONMEMproject(projectPath,'nmfe72',20)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Summary of results 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

SBPOPfitanalysisProjectsFolderInfo('../Models/MODEL_03_PD','../Output/FitAnalysis/MODEL_03_PD')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% VPCs
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

dataVPC                                 = SBPOPloadCSVdataset('../Data/popPKPD.csv');

projectFolder                           = '../Models/MODEL_03_PD/FIT_01';
VPC( projectFolder, dataVPC, 5 )

