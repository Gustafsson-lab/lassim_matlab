function [] = SBPOPexplorePopPDdata(data,TYPE_NAME,covNames,catNames,options)
% [DESCRIPTION]
% This function allows to plot standard data exploration plots focusing on
% a popPD analysis. The data need to be provided, following the standard
% dataspec, defined in the help to the function SBPOPcheckDataFormat, so
% please look there for more information.  
%
% [SYNTAX]
% [] = SBPOPexplorePopPDdata(data,TYPE,covNames,catNames)
% [] = SBPOPexplorePopPDdata(data,TYPE,covNames,catNames,options)
% [] = SBPOPexplorePopPDdata(data,NAME,covNames,catNames)
% [] = SBPOPexplorePopPDdata(data,NAME,covNames,catNames,options)
%
% [INPUT]
% data:         MATLAB PKPD dataset in standard data spec format  
% TYPE:         Numeric value, determining the TYPE to be plotted. TYPE
%               should be a value from the elements in the TYPE column.
%               Standard assumes dose record has TYPE 0 and PK observation 
%               record has TYPE=1, all other things are up to you.
%               IMPORTANT: It assumes that a single SUBTYPE value is present
%               in the dataset. This will be checked. 
% NAME:         Instead of a TYPE the NAME of the component to plot can be
%               specified as a string. NAME needs to be the name as entered 
%               in the NAME column of the dataset.
% covNames:     Cell-array with the names of the continuous covariates, as
%               defined in the dataset
% catNames:     Cell-array with the names of the categorical covariates, as
%               defined in the dataset
% options:      MATLAB structure with additional options
%
%               options.color:    =0: use black and white where necessary,
%                                 =1: use color (default)
%               options.outputPath: path where
%                                 outputs are exported to. Default:
%                                 '../Output/DataExploration/';
%               options.plotIndividualData: =1: yes (default), =0: no
%               options.plotStratifiedData: =1: yes (default), =0: no
%               options.plotCovariateData:  =1: yes (default), =0: no
%
% [OUTPUT]
% Several PDF documents with plots. The names of the
% files tell what is shown. also a summary statistic of the data as an
% exported file. Covariate information, etc.
%
% [ASSUMPTIONS]
%
% [AUTHOR]
% Henning Schmidt, henning.schmidt@novartis.com
%
% [DATE]
% 15th April 2013
%
% [PLATFORM]
% Windows, Unix, MATLAB
%
% [TOOLBOXES USED]
% Statistics Toolbox

% Information:
% ============
% Copyright (c) 2012 Novartis Pharma AG
%
% This program is Free Open Source Software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program. If not, see <http://www.gnu.org/licenses/>.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Check input arguments
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if ~isa(data,'dataset'),
    error('First input argument is not a MATLAB dataset.');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% If dataset empty then return
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if isempty(data),
    disp('Empty dataset.');
    return;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Check
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
SBPOPcheckDataFormat(data);
datanames = get(data,'VarNames');
for k=1:length(covNames),
    if isempty(strmatchSB(covNames{k},datanames,'exact')), error('The dataset does not contain the covariate ''%s''.',covNames{k}); end
end
for k=1:length(catNames),
    if isempty(strmatchSB(catNames{k},datanames,'exact')), error('The dataset does not contain the covariate ''%s''.',catNames{k}); end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Check if TYPE or NAME
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if isnumeric(TYPE_NAME),
    % Check if single SUBTYPE value
    if length(unique(data.SUBTYPE(data.TYPE==TYPE_NAME))) > 1,
        error('For this TYPE several SUBTYPE values are present.');
    end
    FLAG_TYPE = 1;
else
    % Check if name present
    if ~ismember(TYPE_NAME,unique(data.NAME)),
        error('Specified NAME not present in the dataset.');
    end
    FLAG_TYPE = 0;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Handle options
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
try outputPath          = [options.outputPath '/'];     catch, outputPath           = '../Output/DataExploration/';     end; %#ok<*CTCH>
try color               = options.color;                catch, color                = 1;                                end; %#ok<*CTCH>
try plotIndividualData  = options.plotIndividualData;   catch, plotIndividualData   = 1;                                end; %#ok<*CTCH>
try plotStratifiedData  = options.plotStratifiedData;   catch, plotStratifiedData   = 1;                                end; %#ok<*CTCH>
try plotCovariateData   = options.plotCovariateData;    catch, plotCovariateData    = 1;                                end; %#ok<*CTCH>

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Create output folder
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[p,f,e] = fileparts(outputPath);
warning off
mkdir(p);
warning on

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Get colors etc - for color=0 option (only in plots where it is needed)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[colors,lines,dots,bwcolors] = getcolorsSBPOP();

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Export individual data - summary - linear Y axis
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
dataPlot            = data;
options             = [];
filename            = [outputPath '01_individual_PD_data_summary_linearY'];
options.logY        = 0;
options.showDose    = 0;
options.showText    = 0;
options.nIDperPage  = 36;
options.sameaxes    = 1;
options.nameGroup   = 'STYSID1A';
SBPOPexploreIndivData(dataPlot,TYPE_NAME,filename,options)
    
if plotIndividualData,
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Export individual data - 1 page per ID - linear Y axis 
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    dataPlot            = data;
    filename            = [outputPath '02_individual_PD_data_SINGLE_linearY'];
    options             = [];
    options.logY        = 0;
    options.showDose    = 1;
    options.showText    = 1;
    options.nIDperPage  = 1;
    options.sameaxes    = 0;
    options.nameGroup   = 'STYSID1A';
    SBPOPexploreIndivData(dataPlot,TYPE_NAME,filename,options)
end

if plotStratifiedData,
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Assessment of data availability
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    filename    = [outputPath '03_data_availability'];
    startNewPrintFigureSBPOP(filename);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Assessment of data availability TRT/STUDY
    % Get PK data
    if FLAG_TYPE,
        dataPlot    = data(data.TYPE==TYPE_NAME,:);
    else
        dataPlot  = data(strcmp(data.NAME,TYPE_NAME),:);
    end

    % Do the plot
    nameGroupX              = 'TRT';
    nameGroupY              = 'STUDY';
    nameY                   = 'DV';
    nameX                   = 'TIME';
    options                 = [];
    options.nameSubGroup    = 'ID';
    options.nameColorGroup  = 'ID';
    options.linetype        = '--';
    options.linewidth       = 1;
    options.xlabelText      = sprintf('Time [%s]',dataPlot.TIME_UNIT{1});
    nY                      = length(unique(dataPlot.(nameGroupY)));
    options.ylabelText      = {};
    for k=1:nY, options.ylabelText{k} = ''; end
    if nY==1,
        options.ylabelText{1} = sprintf('%s [%s]',dataPlot.NAME{1},dataPlot.UNIT{1});
    else
        options.ylabelText{floor(nY/2)} = sprintf('%s [%s]',dataPlot.NAME{1},dataPlot.UNIT{1});
    end
    options.logY            = 0;
    options.sameaxes        = 1;
    if ~color,
        options.showmarkers      = 1;
        options.linecolorsCustom = bwcolors;
        options.markersize       = 3;
    end
    options.maxlegendentries = 20;
    options.heighttitlebarX = 0.12;
    SBPOPplotfacetgrid(dataPlot,nameX,nameY,nameGroupX,nameGroupY,options)
    printFigureSBPOP(gcf,filename);
    close(gcf);
    convert2pdfSBPOP(filename);
end

if plotCovariateData,
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Summary statistics covariates
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    filename    = [outputPath '04_summary_statistics'];
    SBPOPexploreSummaryStats(data,covNames,catNames,filename);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Graphical exploration of covariates
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    filename    = [outputPath '05_covariates_relationship'];
    SBPOPexploreCovariateCorrelations(data,covNames,catNames,filename);
end