function [ X ] = invtransformXparametersSBPOP( xtrans, transformation )
% Transforms parameters for the optimization from the "normal" domain into
% the real one. Used only in the median fitting approach.
%
% Handle normal, lognormal, and logitnormal transformation.

X = NaN(1,length(xtrans));

for k=1:length(xtrans),
    if strcmpi(transformation{k},'N'),
        X(k) = xtrans(k);
    elseif strcmpi(transformation{k},'L'),
        X(k) = exp(xtrans(k));
    elseif strcmpi(transformation{k},'G'),
        X(k) = exp(xtrans(k))/(1+exp(xtrans(k)));
    end
end


