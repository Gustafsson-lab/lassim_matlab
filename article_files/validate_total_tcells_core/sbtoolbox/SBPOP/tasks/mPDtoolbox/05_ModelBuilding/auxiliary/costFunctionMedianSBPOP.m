function [ COST, OUTPUT] = costFunctionMedianSBPOP( Xtrans , varargin)
% costFunctionMedianSBPOP: default costfunction for the median modeling approach,
% called by optimizeModelMedianSBPOP.
%
% No further documentation, since auxiliary to SBPOPrunMEDIANoptimization

% Information:
% ============
% Copyright (c) 2012 Novartis Pharma AG
% 
% This program is Free Open Source Software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program. If not, see <http://www.gnu.org/licenses/>.

global simulationTRTinfo 

%% Determine index of estimated parameters
ix_estimated = find(simulationTRTinfo(1).parameterInformation.estimate);

%% Transform the transformed parameters Xtrans into the correct range
X = invtransformXparametersSBPOP(Xtrans,simulationTRTinfo(1).parameterInformation.trans(ix_estimated));

%% Get the parameter names and values which are considered by the median modeling (estimated or kept fixed)
% Estimated parameters need to be set to the X value, not estimated ones kept on their initial value
paramNames      = simulationTRTinfo(1).parameterInformation.names;          
paramValues     = simulationTRTinfo(1).parameterInformation.values0;         
% Set estimated parameters to their values in "X"
paramValues(ix_estimated) = X; 
    
%% Cycle through the different treatment groups
for kTRT = 1:length(simulationTRTinfo),
    
    %% Get simulation info structure for TRT group
    simInfo = simulationTRTinfo(kTRT);

    %% Add the PK parameters, if present, to the parameter names and values (might be different per TRT group due to covariates)
    paramNamesSimulate  = [paramNames  simInfo.PKparamNames];
    paramValuesSimulate = [paramValues simInfo.PKparamValues];
    
    %% Add the regression parameters
    paramNamesSimulate  = [paramNames  simInfo.regressionNames];
    paramValuesSimulate = [paramValues simInfo.regressionValues(:)'];
    
    %% Get dosing information 
    dosing_sim = simInfo.DOSING;
    
    % Handle weight based dosing
    %   Check if weight based dosing and then change the dose
    %   Check for each input in the dosing scheme
    ds = struct(dosing_sim);
    for kx = 1:length(ds.inputs),
        if simInfo.WEIGHTBASED(kx),
            ds.inputs(kx).D = ds.inputs(kx).D*simInfo.WEIGHT;
        end    
    end
    dosing_sim = SBPOPdosing(ds);
    
    %% Initialize results variable
    NR_SIMTIMES = length(simInfo.SIMTIME);
    NR_OUTPUTS  = length(simInfo.modelInformation.modelOutput);
	SIMULATION_VALUES = Inf(NR_OUTPUTS,NR_SIMTIMES);
    
    %% Simulate
    try
        simres = SBPOPsimdosing(simInfo.mexModel,dosing_sim,simInfo.SIMTIME,[],paramNamesSimulate,paramValuesSimulate);
        
        % Read out the results
        for kres=1:length(simInfo.modelInformation.modelOutput),
            SIMULATION_VALUES(kres,:) = simres.variablevalues(:,variableindexSB(simInfo.moddos,simInfo.modelInformation.modelOutput{kres}))';
        end
    catch %#ok<CTCH>
        disp(lasterr)  %#ok<LERR>
        disp('Simulation error. Handled by large OFV');
    end

    % Save simulated median RR
    simulationTRTinfo(kTRT).SIMULATION_VALUES = SIMULATION_VALUES;
end

%% Determine error and cost
% Use weighted least square ... weighted by the standarderror of the mean
% for each time point. TRT groups are just added together. Should be Chi2 - after
% discussion with statisticians. Will include some user definable TRT weighting.
DV   = {};
PRED = {};
WRES = {};
RES  = {};
TIME = {};
TRT  = [];
COST_TRT = [];

try
    for kTRT = 1:length(simulationTRTinfo),
        simInfo = simulationTRTinfo(kTRT);

        % Save DV and PRED
        DV{kTRT}     = simInfo.REFERENCE_DATA;
        PRED{kTRT}   = simInfo.SIMULATION_VALUES;
        TIME{kTRT}   = simInfo.SIMTIME;
        TRT(kTRT)    = simInfo.TRT;
        
        % Determine differences between reference and simulation 
        % And weight by standard error
        if ~simInfo.options.LOG_TRANSFORM,
            % No transformation
            % Calculate difference
            diff = simInfo.REFERENCE_DATA-simInfo.SIMULATION_VALUES;
            % Weighting difference by standard error of the observations
            weighted_diff = diff./simInfo.REFERENCE_DATA_STDERR;
        else
            % Log transformation of data and simulation results
            % Determine offset in case of 0 elements are present ... as 1000th of maximum values
            offset = max([simInfo.REFERENCE_DATA(:); simInfo.SIMULATION_VALUES(:)])/1000;
            diff = log(simInfo.REFERENCE_DATA+offset)-log(simInfo.SIMULATION_VALUES+offset);
            % Weighting difference by standard error of the log of the observations
            weighted_diff = diff./simInfo.REFERENCE_DATA_STDERR_LOG;
            % For categorical data this will give NaN, but it is already checked that 
            % with categorical (responder rate) data log tranformation can not be used (leading to an error message).
        end

        % Save RES and WRES
        RES{kTRT}  = diff;
        WRES{kTRT} = weighted_diff;
        
        % Calculate sum of squared weighted error and multiply with TRTweighting
        WSOSerror = simInfo.TRTweighting*sum(sum(weighted_diff.^2));
        
        % Collect cost per treatment to use as output argument
        COST_TRT = [COST_TRT WSOSerror];
    end
    COST = sum(COST_TRT);
catch
    COST = Inf;
    COST_TRT = NaN;
end

%% Provide some output on the screen and save in log text
textxx = sprintf('\tCost=%1.5g%sX=[%s]',COST,char(32*ones(1,12-length(sprintf('%1.5g',COST)))),sprintf('%g ',X));
simulationTRTinfo(1).logText = [simulationTRTinfo(1).logText char(10) textxx];

%% Generate output argument
OUTPUT = [];
OUTPUT.X = X;
OUTPUT.COST = COST;
OUTPUT.parameterNames  = paramNames;
OUTPUT.parameterValues = paramValues;
OUTPUT.TRT = TRT;
OUTPUT.COST_TRT = COST_TRT;
OUTPUT.DV = DV;
OUTPUT.PRED = PRED;
OUTPUT.RES  = RES;
OUTPUT.WRES = WRES;
OUTPUT.TIME = TIME;
