function [projectfolder,modelInformation,dataInformation,parameterInformation,dosingInformation,options,COVCAT_INFORMATION] = checkProcessMedianModelingInputsSBPOP(projectfolder,modelInformation,dataInformation,parameterInformation,dosingInformation,options)
% Auxiliary function for SBPOPrunMEDIANoptimization to check and process the input arguments

%% Handle and check model information
try modelInformation.model;             catch, error('Please define "modelInformation.model".'); end
try modelInformation.modelOutput;       catch, error('Please define "modelInformation.modelOutput".'); end
try modelInformation.FIT_PK;            catch, modelInformation.FIT_PK = ''; end
try modelInformation.regressionPARA;    catch, modelInformation.regressionPARA = ''; end

if ischar(modelInformation.model),
    modelInformation.model = SBmodel(modelInformation.model);
end
if ~isSBmodel(modelInformation.model),
    error('"modelInformation.model" is not an SBmodel.');
end
if ischar(modelInformation.modelOutput),
    modelInformation.modelOutput = {modelInformation.modelOutput};
end
variables = SBvariables(modelInformation.model);
for k=1:length(modelInformation.modelOutput),
    ix = strmatchSB(modelInformation.modelOutput{k},variables,'exact');
    if isempty(ix),
        error('"%s" is not a variable in the model.',modelInformation.modelOutput{k});
    end
end
% Parse covariate information from potential FIT_PK model
COVCAT_INFORMATION = [];
[COVCAT_INFORMATION.covNames, COVCAT_INFORMATION.catNames, COVCAT_INFORMATION.referenceCAT] = getCovariateInformationSBPOP(modelInformation.FIT_PK);

% Check regressionPARA
if ischar(modelInformation.regressionPARA) && ~isempty(modelInformation.regressionPARA),
    modelInformation.regressionPARA = {modelInformation.regressionPARA};
end

% Check if regressionPARA in model
parameters = SBparameters(modelInformation.model);
for k=1:length(modelInformation.regressionPARA),
    ix = strmatchSB(modelInformation.regressionPARA{k},parameters,'exact');
    if isempty(ix),
        error('Regression parameter %s is not in the model.',modelInformation.regressionPARA{k});
    end
end

%% Handle and check data information
try dataInformation.data;               catch, error('Please define "dataInformation.data".');   end
try dataInformation.names;              catch, error('Please define "dataInformation.names".');  end
try dataInformation.type;               catch, error('Please define "dataInformation.type".');   end
try dataInformation.stratify;           catch, dataInformation.stratify = {''};                  end
try dataInformation.regressionDATA;     catch, dataInformation.regressionDATA = ''; end

if ischar(dataInformation.names),
    dataInformation.names = {dataInformation.names};
end
if ischar(dataInformation.stratify),
    dataInformation.stratify = {dataInformation.stratify};
end
if ischar(dataInformation.data),
    dataInformation.data = SBPOPloadCSVdataset(dataInformation.data);
end
if ~strcmp(class(dataInformation.data),'dataset'),
    error('"dataInformation.data" should be a dataset.');
end
% Check column names
varnames = get(dataInformation.data,'VarNames');    
% ID, TRT, NOMINAL_TIME
if isempty(strmatchSB('ID',varnames,'exact')),
    error('"dataInformation.data" does not contain an ID column.');
end
if isempty(strmatchSB('TRT',varnames,'exact')),
    error('"dataInformation.data" does not contain a TRT column.');
end
if isempty(strmatchSB('NOMINAL_TIME',varnames,'exact')),
    error('"dataInformation.data" does not contain a NOMINAL_TIME column.');
end
% Check dataInformation.names
if length(dataInformation.names) ~= length(modelInformation.modelOutput),
    error('Different number of dataInformation.names and modelInformation.modelOutput.');
end
for k=1:length(dataInformation.names),
    if isempty(strmatchSB(dataInformation.names{k},varnames,'exact')),
        error('"dataInformation.data" does not contain a %s column.',dataInformation.names{k});
    end
end
% Check covariates
covcatNames = [COVCAT_INFORMATION.covNames COVCAT_INFORMATION.catNames];
for k=1:length(covcatNames),
    if isempty(strmatchSB(covcatNames{k},varnames,'exact')),
        error('"dataInformation.data" does not contain a %s column (covariate in FIT_PK).',covcatNames{k});
    end
end
% Check type
if ~strcmpi(dataInformation.type,'continuous') && ~strcmpi(dataInformation.type,'categorical'),
    error('Incorrect definition of "dataInformation.type".');
end
% Check stratify ... check that elements either empty of available in dataset as columns
for k=1:length(dataInformation.stratify),
    x = dataInformation.stratify{k};
    if ~isempty(x),
        ix = strmatchSB(x,varnames,'exact');
        if isempty(ix),
            error('Stratification variable "%s" not in dataset.',x);
        end
    end
end

% Check regressionDATA
if ischar(dataInformation.regressionDATA) && ~isempty(dataInformation.regressionDATA),
    dataInformation.regressionDATA = {dataInformation.regressionDATA};
end

% Check if regressionDATA in dataset
for k=1:length(dataInformation.regressionDATA),
    ix = strmatchSB(dataInformation.regressionDATA{k},varnames,'exact');
    if isempty(ix),
        error('Regression data %s is not in the dataset.',dataInformation.regressionDATA{k});
    end
end

% check lengths
if length(dataInformation.regressionDATA) ~= length(modelInformation.regressionPARA),
    error('Different number of regression parameters in model and in dataset.');
end

%% Check parameterInformation
try parameterInformation.names;         catch, error('Please define "parameterInformation.names".');     end
try parameterInformation.values0;       catch, parameterInformation.values0 = [];                        end
try parameterInformation.estimate;      catch, parameterInformation.estimate = [];                       end
try parameterInformation.trans;         catch, parameterInformation.trans = {};                          end
        
if isempty(parameterInformation.values0),
    parameterInformation.values0 = SBparameters(modelInformation.model,parameterInformation.names);
    parameterInformation.values0 = parameterInformation.values0(:)';
end

if isempty(parameterInformation.estimate),
    parameterInformation.estimate = ones(1,length(parameterInformation.names));
end

if isempty(parameterInformation.trans),
    parameterInformation.trans = cell(1,length(parameterInformation.names));
    parameterInformation.trans(1:end) = {'L'}';
end

if length(parameterInformation.names) ~= length(parameterInformation.values0),
    error('Different lengths of parameterInformation.names and parameterInformation.values0.');
end
if length(parameterInformation.names) ~= length(parameterInformation.estimate),
    error('Different lengths of parameterInformation.names and parameterInformation.estimate.');
end
if length(parameterInformation.names) ~= length(parameterInformation.trans),
    error('Different lengths of parameterInformation.names and parameterInformation.trans.');
end

if length(parameterInformation.values0) ~= length(parameterInformation.estimate),
    error('Different lengths of parameterInformation.values0 and parameterInformation.estimate.');
end
if length(parameterInformation.values0) ~= length(parameterInformation.trans),
    error('Different lengths of parameterInformation.values0 and parameterInformation.trans.');
end

if length(parameterInformation.estimate) ~= length(parameterInformation.trans),
    error('Different lengths of parameterInformation.estimate and parameterInformation.trans.');
end

%% Check dosingInformation
try dosingInformation.TRT;          catch, error('Please define "dosingInformation.TRT".');          end
try dosingInformation.dosings;      catch, error('Please define "dosingInformation.dosings".');      end
try dosingInformation.name;         catch, error('Please define "dosingInformation.name".');         end
try dosingInformation.weightBased;  catch, error('Please define "dosingInformation.weightBased".');  end
try dosingInformation.TRTweighting; catch, dosingInformation.TRTweighting = ones(1,length(dosingInformation.TRT));  end

% Check length of dosingInformation.TRTweighting
if length(dosingInformation.TRTweighting) ~= length(dosingInformation.TRT),
    error('dosingInformation.TRTweighting needs to have same length as dosingInformation.TRT.');
end

% Check that for each TRT in the data a dosing definition is present. Otherwise error
TRTdata = unique(dataInformation.data.TRT);
for k=1:length(TRTdata),
    ix = find(dosingInformation.TRT == TRTdata(k), 1);
    if isempty(ix),
        error('No dosingInformation for TRT=%d in dataInformation.data.',TRTdata(k));
    end
end

% Handle cell-arrays
if isSBPOPdosing(dosingInformation.dosings),
    dosingInformation.dosings = {dosingInformation.dosings};
end
if ischar(dosingInformation.name),
    dosingInformation.name = {dosingInformation.name};
end

% Check size of dosingInformation.weightBased
ds = struct(dosingInformation.dosings{1});
Ninputs = length(ds.inputs);
if size(dosingInformation.weightBased,1) ~= Ninputs,
    error('dosingInformation.weightBased requires %d rows - one for each input in the dosing schemes.',Ninputs);
end
if size(dosingInformation.weightBased,2) ~= length(dosingInformation.TRT),
    error('dosingInformation.weightBased requires %d columns - one for each TRT group.',length(dosingInformation.TRT));
end

% Check that all dosings have same structure
dstypes1 = {ds.inputs.type};
for k=1:length(dosingInformation.dosings),
    dsk = struct(dosingInformation.dosings{k});
    dstypesk = {dsk.inputs.type};
    if length(dstypes1) ~= length(dstypesk),
        error('Not all dosings have same input types.');
    end
    for k2=1:length(dstypes1),
        if ~strcmp(dstypes1{k2},dstypesk{k2}),
            error('Not all dosings have same input types.');
        end            
    end
end

% Check weight based dosing .. if present then add WT0 to required covariates but only if present in dataset. Otherwise error
if sum(sum(dosingInformation.weightBased)) > 0,
    % Check if WT0 in dataset
    if isempty(strmatchSB('WT0',varnames,'exact')),
        error('Weight based dosing present but no "WT0" column with weight information in the dataset.');
    end
    COVCAT_INFORMATION.covNames = unique([{'WT0'} COVCAT_INFORMATION.covNames]);
end

%% Handle options
try options.maxtime;            catch, options.maxtime = Inf;                                        end
try options.maxiter;            catch, options.maxiter = Inf;                                        end
try options.maxfunevals;        catch, options.maxfunevals = 100*sum(parameterInformation.estimate); end
try options.OPTIMIZER;          catch, options.OPTIMIZER = 'simplexSB';                              end 
try options.N_BOOTSTRAP;        catch, options.N_BOOTSTRAP = 50;                                     end
try options.LOG_TRANSFORM;      catch, options.LOG_TRANSFORM = 0;                                    end
try options.SEED;               catch, options.SEED = 123456;                                        end
try options.N_PROCESSORS;       catch, options.N_PROCESSORS = 1;                                     end
