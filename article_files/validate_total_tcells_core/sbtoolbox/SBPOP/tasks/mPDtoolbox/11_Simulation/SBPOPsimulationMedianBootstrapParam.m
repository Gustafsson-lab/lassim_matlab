function [] = SBPOPsimulationMedianBootstrapParam(projectfolder,dosingInformation,data,SIMTIME,filename)
% SBPOPsimulationMedianBootstrapParam: Similar function as "SBPOPvpcMedianBootstrapParam"
% The difference to the VPC is that additional dosing schemes can be
% simulated. If data available then it is plotted. If not then just
% simulation. Additionally, a plot is generated, comparing the median/responder rate
% results without uncertainty for the different provided dosing schemes.
%
% dosingInformation is not used from the run_results of the
% project but from the user provided input argument. 
%
% The data is not used from the run_results, but from the provided input
% argument. Same transformations will be done as specified in the
% run_results.
%
% Simulations based on bootstrap parameter estimates.
%
% USAGE:
% ======
% SBPOPsimulationMedianBootstrapParam(projectfolder,dosingInformation,data,SIMTIME,filename)   
%
% projectfolder:        string with the path where the model results are stored 
%                       (e.g.: logfiles folder, run_results.data, ...)
% dosingInformation:	structure with information about the nominal treatment group dosings
%   dosingInformation.TRT:          Vector with treatment group identifiers to be used in optimization
%                                   TRT groups are optimized if dosings AND data are available - otherwise ignored!
%                                   It will be checked that for each TRT in the data a dosing definition is present. Otherwise error!
%   dosingInformation.dosings:      Cell-array defining nominal dosings for each treatment group
%   dosingInformation.name:         Cell-array defining names for each treatment group
%   dosingInformation.weightBased:  Matrix with as many rows as dosing inputs in the model and as many columns as treatment groups.
%                                   A "0" entry defines fixed dosing and a "1" entry defines weight based dosing.
%                                   If weight based dosing is used, the dataInformation.data dataset needs to contain a WT0 column.
%
% data:                 Matlab dataset in the median modeling format
% SIMTIME:              Time vector for the simulations
% filename:             Path, including name for the generated PDF

% Information:
% ============
% Copyright (c) 2012 Novartis Pharma AG
% 
% This program is Free Open Source Software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program. If not, see <http://www.gnu.org/licenses/>.

%% Read tun results
run_results = load([projectfolder '/run_results.data'],'-mat'); run_results = run_results.run_results;

%% Get colors
colors = getcolorsSBPOP();

%% Get covariate information
% Parse covariate information from potential FIT_PK model
COVCAT_INFORMATION = [];
[COVCAT_INFORMATION.covNames, COVCAT_INFORMATION.catNames, COVCAT_INFORMATION.referenceCAT] = getCovariateInformationSBPOP(run_results.modelInformation.FIT_PK);
% Check weight based dosing .. if present then add WT0 to required covariates but only if present in dataset. Otherwise error
if sum(sum(dosingInformation.weightBased)) > 0,
    % Check if WT0 in dataset
    if isempty(strmatchSB('WT0',get(data,'VarNames'),'exact')),
        error('Weight based dosing present but no "WT0" column with weight information in the dataset.');
    end
    COVCAT_INFORMATION.covNames = unique([{'WT0'} COVCAT_INFORMATION.covNames]);
end

%% Determine regression things
regressionPARA = run_results.modelInformation.regressionPARA;
regressionDATA = run_results.dataInformation.regressionDATA;

%% Transform the data (stratification and median calculation)
[dataStratified,stratificationTHRESHOLD] = createStratifiedDatasetSBPOP(data,'ID',run_results.stratificationSetting.NAME,run_results.stratificationSetting.RANGE);
DATAmedian = getMedianModelingDataStructSBPOP(dataStratified,run_results.dataInformation.names,run_results.dataInformation.type,COVCAT_INFORMATION.covNames,COVCAT_INFORMATION.catNames,COVCAT_INFORMATION.referenceCAT,regressionPARA,regressionDATA);

%% Produce MEX model
moddos                          = mergemoddosSBPOP(run_results.modelInformation.model,run_results.dosingInformation.dosings{1});
mexModelName                    = 'mexModel_simulation';
SBPDmakeMEXmodel(moddos,mexModelName);

%% Get PD parameters from bootstrap
parameterNames  = run_results.run_information.OUTPUTopt{1}.parameterNames;   
parameterValues = [];
for k=1:length(run_results.run_information),
    parameterValues = [parameterValues; run_results.run_information.OUTPUTopt{k}.parameterValues];
end
NSIM = length(run_results.run_information);

%% Get allTRTs for which dosings are defined - since we want to simulate all provided dosings!
allTRT = dosingInformation.TRT;
        
%% Simulate all TRTs that were in the dosingInformation
PK_ALL_TRT          = {};
READOUTS_ALL_TRT    = {};
SIMTIME_TRT         = {};

parfor kTRT = 1:length(allTRT),
    % kTRT is relative to dosingInformation!
    
    % Get treatment info
    TRT                                         = allTRT(kTRT);
    
    % Get index in the DATAmedian
    ixTRT_DATAmedian                            = find(DATAmedian.TRT==TRT);
       
    % Get default simulation parameters 
    paramNamesSim  = [parameterNames];
    paramValuesSim = [parameterValues];
    
    % Sample PK population parameters (include median covariate information)
    if ~isempty(run_results.modelInformation.FIT_PK),
        % For TRT groups not present in the data, no covariate information is availabel but might be needed for the sampling of the PK model.
        % In this case (identified by isempty(ixTRT_DATAmedian), the median of the covariates is used across the provided data.
        if ~isempty(ixTRT_DATAmedian),
            PKparam = SBPOPsampleNLMEfitParam(run_results.modelInformation.FIT_PK,3,1,DATAmedian.covNames,DATAmedian.covValues(:,ixTRT_DATAmedian)',DATAmedian.catNames,DATAmedian.catValues(:,ixTRT_DATAmedian)');
        else
            covValues = nanmedian(DATAmedian.covValues')';
            catValues = nanmedian(DATAmedian.catValues')'; % Also median for cat values ...
            PKparam = SBPOPsampleNLMEfitParam(run_results.modelInformation.FIT_PK,3,1,DATAmedian.covNames,covValues,DATAmedian.catNames,catValues);
        end
        % Need to check in the model and remove the PK parameters that ae
        % not present in the model
        PKparamNotInModel = setdiff(PKparam.parameterNames,SBparameters(moddos));
        ix_remove_PKparam = [];
        for kPKparam=1:length(PKparamNotInModel),
            ix_remove_PKparam = [ix_remove_PKparam strmatchSB(PKparamNotInModel{kPKparam},PKparam.parameterNames)];
        end
        PKparam.parameterNames(ix_remove_PKparam) = [];
        PKparam.parameterValuesPopulation(ix_remove_PKparam) = [];
        
        % Combine PK and PD parameters
        paramNamesSim  = [paramNamesSim   PKparam.parameterNames];
        paramValuesSim = [paramValuesSim  PKparam.parameterValuesPopulation(ones(1,NSIM),:)];
    end
    
    % Add regression parameters
    % For TRT groups not present in the data, no regression information is available but might be needed
    % In this case (identified by isempty(ixTRT_DATAmedian), the median of the regression information is used across the provided data.
    paramNamesSim  = [paramNamesSim DATAmedian.regressionNames];
    if ~isempty(ixTRT_DATAmedian),
        regvalues = DATAmedian.regressionValues(:,ixTRT_DATAmedian)';
    else
        regvalues = nanmedian(DATAmedian.regressionValues')';
    end        
    paramValuesSim = [paramValuesSim  regvalues(ones(1,NSIM),:)];
    
    % Get dosing information and handle weight based dosing
    dosing_sim          = dosingInformation.dosings{kTRT};
    
    % Handle weight based dosing
    %   Check if weight based dosing and then change the dose
    %   Check for each input in the dosing scheme
    ds = struct(dosing_sim);
    for kx = 1:length(ds.inputs),
        if dosingInformation.weightBased(kx,kTRT),
            medianWEIGHT = DATAmedian.covValues(strmatchSB(DATAmedian.covNames,'WT0','exact'),ixTRT_DATAmedian);
            ds.inputs(kx).D = ds.inputs(kx).D*medianWEIGHT;
        end    
    end
    dosing_sim = SBPOPdosing(ds);
        
    % Simulate
    PK_ALL = NaN(length(SIMTIME),NSIM);
    READOUTS_ALL = cell(1,length(run_results.modelInformation.modelOutput));
    for kx=1:length(run_results.modelInformation.modelOutput),
        READOUTS_ALL{kx} = NaN(length(SIMTIME),NSIM);
    end
    for kSIM=1:NSIM,
        kSIM
        try
            simres = SBPOPsimdosing(mexModelName,dosing_sim,SIMTIME,[],paramNamesSim,paramValuesSim(kSIM,:));
            
            % Get the PK ... assume Cc is the one.
            if ~isempty(variableindexSB(moddos,'Cc')),
                PK_ALL(:,kSIM) = simres.variablevalues(:,variableindexSB(moddos,'Cc'));
            end
            
            % Get the other readouts
            for kx=1:length(run_results.modelInformation.modelOutput),
                READOUTS_ALL{kx}(:,kSIM) = simres.variablevalues(:,variableindexSB(moddos,run_results.modelInformation.modelOutput{kx}));
            end
        catch
        end
    end
   
    PK_ALL_TRT{kTRT}            = PK_ALL;
    READOUTS_ALL_TRT{kTRT}      = READOUTS_ALL;
    SIMTIME_TRT{kTRT} = SIMTIME;    
end

%% start output
startNewPrintFigureSBPOP(filename)

%% Plot PK results
figure(1); clf;
nrows = ceil(sqrt(length(allTRT)));
ncols = ceil(length(allTRT)/nrows);
PK_X = [];
for kTRT=1:length(allTRT),
    PK_X  = [PK_X; PK_ALL_TRT{kTRT}(:)];
end
minY = min(log10(PK_X));
maxY = max(log10(PK_X));
for kTRT=1:length(allTRT),
    subplot(nrows,ncols,kTRT);
    PK_TRT  = PK_ALL_TRT{kTRT};
    SIMTME  = SIMTIME_TRT{kTRT};
    plot(SIMTIME,log10(nanmedian(PK_TRT')),'k-','LineWidth',2)
    % Annotate
    if kTRT>length(allTRT)-ncols,
        xlabel('Time')
    end
    if mod(kTRT,ncols) == 1,
        ylabel('Conc')
    end
    grid on;
    set(gca,'YTick',[0 1 2 3 4 5 6]);
    set(gca,'YTickLabel',10.^get(gca,'YTick'));
    set(gca,'XLim',[min(SIMTIME) max(SIMTIME)]);
    set(gca,'YLim',[minY maxY]);
    
    % Get treatment name
    title(dosingInformation.name{kTRT},'Interpreter','none');
end
printFigureSBPOP(gcf,filename)

%% Plot Readout results with uncertainty - one plot per TRT
SIMTIME_X = [];
RO_Y = [];
for k=1:length(PK_ALL_TRT),
    SIMTIME_X = [SIMTIME_X SIMTIME_TRT{kTRT}];
    for kRO=1:length(run_results.modelInformation.modelOutput),
        RO_Y      = [RO_Y; READOUTS_ALL_TRT{kTRT}{kRO}(:)]; 
    end
end


for kRO = 1:length(run_results.modelInformation.modelOutput),
    
    figure(kRO+1); clf;
    nrows = ceil(sqrt(length(PK_ALL_TRT)));
    ncols = ceil(length(PK_ALL_TRT)/nrows);

    for kTRT=1:length(allTRT),
        subplot(nrows,ncols,kTRT);
        
        RO_TRT = READOUTS_ALL_TRT{kTRT}{kRO};
        SIMTIME = SIMTIME_TRT{kTRT};
        
        % Plot the data if available
        % Get index in the DATAmedian
        ixTRT_DATAmedian = find(DATAmedian.TRT==allTRT(kTRT));
        if ~isempty(ixTRT_DATAmedian),
            data = DATAmedian.DATA{ixTRT_DATAmedian}(kRO,:);
            time = DATAmedian.NT{ixTRT_DATAmedian};
            plot(time,data,'.-','MarkerSize',25,'Color',0.3*[1 1 1]); hold on
        else
            plot(-Inf,-Inf,'.-','MarkerSize',25,'Color',0.3*[1 1 1]); hold on
        end
        
        plot(SIMTIME,nanmedian(RO_TRT'),'b-','LineWidth',2,'Color',0.8*colors(kRO,:)); hold on
        
        ranges      = [90 75 50 25];
        colorfactor = [0.65 0.5 0.35 0.2];
        legendText = {'Observations',sprintf('Simulated median (N=%d)',run_results.options.N_BOOTSTRAP)};
        for kplot=1:length(ranges)
            qlow = (1-ranges(kplot)/100)/2;
            qhigh = 1-(1-ranges(kplot)/100)/2;
            SBPOPplotfill(SIMTIME,quantile(RO_TRT',qlow),quantile(RO_TRT',qhigh),min(colorfactor(kplot)+colors(kRO,:),1),1,min(colorfactor(kplot)+colors(kRO,:),1)); hold on;
            legendText{end+1} = sprintf('%d %% CI',ranges(kplot));
        end
        
        plot(SIMTIME,nanmedian(RO_TRT'),'b-','LineWidth',2,'Color',0.8*colors(kRO,:)); hold on
        
        % Annotate
        if kTRT>length(PK_ALL_TRT)-ncols,
            xlabel('Time')
        end
        if mod(kTRT,ncols) == 1,
            ylabel(sprintf('%s',run_results.dataInformation.names{kRO}),'Interpreter','none')
        end
        grid on;
        set(gca,'XLim',[min(SIMTIME_X) max(SIMTIME_X)]);
        set(gca,'YLim',[min(RO_Y)*0.9 max(RO_Y)*1.1]);
        title(dosingInformation.name{kTRT},'Interpreter','none');
        
        % Plot the data if available
        % Get index in the DATAmedian
        ixTRT_DATAmedian = find(DATAmedian.TRT==allTRT(kTRT));
        if ~isempty(ixTRT_DATAmedian),
            data = DATAmedian.DATA{ixTRT_DATAmedian}(kRO,:);
            time = DATAmedian.NT{ixTRT_DATAmedian};
            plot(time,data,'.-','MarkerSize',25,'Color',0.3*[1 1 1]); hold on
            % Plot errorbars on data
            data_stderr = DATAmedian.DATA_STDERR{ixTRT_DATAmedian}(kRO,:);
            errorbar(time,data,data_stderr,'Color',0.3*[1 1 1])
        end
    end
    % Add legend
    subplot(nrows,ncols,1);
    legend(legendText,'Location','NorthEast')
    printFigureSBPOP(gcf,filename)
end

%% Plot Readout results without uncertainty - compare all TRTs in a single plot ... no data plotting
SIMTIME_X = [];
RO_Y = [];
for k=1:length(PK_ALL_TRT),
    SIMTIME_X = [SIMTIME_X SIMTIME_TRT{kTRT}];
    for kRO=1:length(run_results.modelInformation.modelOutput),
        RO_Y      = [RO_Y; READOUTS_ALL_TRT{kTRT}{kRO}(:)]; 
    end
end

for kRO = 1:length(run_results.modelInformation.modelOutput),
    figure(kRO+1); clf;
    legendText = {};
    for kTRT=1:length(allTRT),
        RO_TRT = READOUTS_ALL_TRT{kTRT}{kRO};
        SIMTIME = SIMTIME_TRT{kTRT};
        plot(SIMTIME,nanmedian(RO_TRT'),'b-','LineWidth',3,'Color',colors(kTRT,:)); hold on
        legendText{kTRT} = sprintf('%s (median)',dosingInformation.name{kTRT});
    end
    
    % Annotate
    if kTRT>length(PK_ALL_TRT)-ncols,
        xlabel('Time')
    end
    if mod(kTRT,ncols) == 1,
        ylabel(sprintf('%s',run_results.dataInformation.names{kRO}),'Interpreter','none')
    end
    grid on;
    set(gca,'XLim',[min(SIMTIME_X) max(SIMTIME_X)]);
    set(gca,'YLim',[min(RO_Y)*0.9 max(RO_Y)*1.1]);
    set(gca,'FontSize',12);
    xlabel('Time','FontSize',14)
    ylabel(run_results.dataInformation.names{kRO},'FontSize',14,'Interpreter','none');
    title('Comparison of median responses for all TRT groups','FontSize',14,'Interpreter','none')
    
    legend(legendText,'Location','NorthEast')
    printFigureSBPOP(gcf,filename)
end

%%
convert2pdfSBPOP(filename);
close all

% Delete mex file
clear mex
delete([mexModelName '.' mexext]);
