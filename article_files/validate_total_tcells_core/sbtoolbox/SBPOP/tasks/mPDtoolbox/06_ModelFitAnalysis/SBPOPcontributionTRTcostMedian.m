function [] = SBPOPcontributionTRTcostMedian(projectfolder)
% SBPOPcontributionTRTcostMedian: Assess contribution of different TRT groups to the optimal cost 
%
% USAGE:
% ======
% SBPOPcontributionTRTcostMedian(projectfolder)       
%
% projectfolder:        string with the path where the model results are stored 
%                       (e.g.: logfiles folder, run_results.data, ...)
%
% Results are generated as PNG file and stored in the projectfolder as 
% "OUTPUT_02_contribution_TRT_total_cost.png".

% Information:
% ============
% Copyright (c) 2012 Novartis Pharma AG
% 
% This program is Free Open Source Software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program. If not, see <http://www.gnu.org/licenses/>.

%% Read tun results
run_results = load([projectfolder '/run_results.data'],'-mat'); run_results = run_results.run_results;

%% Get contributions to cost function
COST_TRT = [];
for k=1:length(run_results.run_information.OUTPUTopt),
    COST_TRT = [COST_TRT; run_results.run_information.OUTPUTopt{k}.COST_TRT];
end
COST_TRT = abs(COST_TRT);

%% Get TRT names
TRTnames = {};
allTRT = run_results.run_information.OUTPUTopt{1}.TRT;
for k=1:length(allTRT),
    TRTnames{k} = run_results.dosingInformation.name{find(run_results.dosingInformation.TRT==allTRT(k))};
end

%% Plot the mean 
figure(1); clf;
mean_Y = 100*mean(COST_TRT,1)/sum(mean(COST_TRT,1));
if run_results.options.N_BOOTSTRAP>1,
    std_Y  = 100*std(COST_TRT)/sum(mean(COST_TRT));
else
    std_Y = zeros(1,length(mean_Y));
end
SBbarplotErrors(mean_Y, std_Y);
grid on;
set(gca,'XTickLabel',TRTnames)
set(gca,'FontSize',12);
ylabel('Contribution to total cost [%]','FontSize',14)
title('Contribution of TRT groups total cost','FontSize',14,'Interpreter','none')
legend('Mean','Standard deviation');
printFigureSBPOP(gcf,[projectfolder '/OUTPUT_02_contribution_TRT_total_cost'],'png');
close all