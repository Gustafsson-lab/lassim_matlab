function [] = SBPOPcreateAllFitAssessmentsMedian(projectfolder,NSAMPLES_VPC)
% SBPOPcreateAllFitAssessmentsMedian: This function generates all currently 
% available GoF and fit assessment plots for the median modeling
%
% USAGE:
% ======
% SBPOPcreateAllFitAssessmentsMedian(projectfolder)
% SBPOPcreateAllFitAssessmentsMedian(projectfolder,NSAMPLES_VPC)
% 
% projectfolder:    path to the project folder (including the stratification path)
% NSAMPLES_VPC:     number of samples when simulating from the parametric
%                   parameter distribution (Default: 500)
%
% Output: The output is stored in the projectfolder as PDF, PNG and TXT files

% Information:
% ============
% Copyright (c) 2012 Novartis Pharma AG
% 
% This program is Free Open Source Software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program. If not, see <http://www.gnu.org/licenses/>.


if nargin==1,
    NSAMPLES_VPC = 500;
end

SBPOPassessConvergenceMedian(projectfolder)
SBPOPcontributionTRTcostMedian(projectfolder)
SBPOPbootstrapFitsMedian(projectfolder)
SBPOPuncertaintyDistributionMedian(projectfolder)
SBPOPvpcMedianBootstrapParam(projectfolder)
SBPOPvpcMedianSampledParam(projectfolder,NSAMPLES_VPC)
