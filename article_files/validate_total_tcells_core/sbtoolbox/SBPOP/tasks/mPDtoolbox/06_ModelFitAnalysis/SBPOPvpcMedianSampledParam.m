function [] = SBPOPvpcMedianSampledParam(projectfolder,varargin)
% SBPOPvpcMedianSampledParam: Do VPC based on sampled parameters from the 
% uncertainty distribution. Use transformed parameters to determine the 
% distribution to sample from. Transformed parameters ensure better results
% e.g. due to correct sign etc.
%
% For sampling, the mean, the std, and the correlation between the parameters are used.
%
% USAGE:
% ======
% SBPOPvpcMedianSampledParam(projectfolder)       
% SBPOPvpcMedianSampledParam(projectfolder,NSAMPLES)       
%
% projectfolder:        string with the path where the model results are stored 
%                       (e.g.: logfiles folder, run_results.data, ...)
% NSAMPLES:             Number of samples to simulate for the VPC (default: 200)
%
% Results are generated as PDF file and stored in the projectfolder as 
% "OUTPUT_07_VPC_sampledParameters.pdf". 

% Information:
% ============
% Copyright (c) 2012 Novartis Pharma AG
% 
% This program is Free Open Source Software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program. If not, see <http://www.gnu.org/licenses/>.

%% Handle variable input arguments
NSAMPLES = 200;
if nargin == 2,
    NSAMPLES = varargin{1};
end

%% Read tun results
run_results = load([projectfolder '/run_results.data'],'-mat'); run_results = run_results.run_results;

%% Get colors
colors = getcolorsSBPOP();

%% Get the original data structure for the median fitting
DATAmedian = run_results.dataOriginalStratifiedMedian;

%% Produce MEX model
moddos                          = mergemoddosSBPOP(run_results.modelInformation.model,run_results.dosingInformation.dosings{1});
mexModelName                    = 'mexModel_VPC';
SBPDmakeMEXmodel(moddos,mexModelName);

%% Get PD parameters from bootstrap and sample from the distribution
% Only get the estimated ones (not the fixed ones)
[xdummyx, parameters_bootstrap_all_TRANS] = SBPOPgetParametersMedian(projectfolder);
% Get names and numeric values
parameterNames = get(parameters_bootstrap_all_TRANS,'VarNames');
parameters_bootstrap_all_TRANS = double(parameters_bootstrap_all_TRANS);
% Use the transformed parameters for the sampling!

% Determine the distribution of the parameterValues and sample from it
mean_PD  = mean(parameters_bootstrap_all_TRANS);
std_PD   = std(parameters_bootstrap_all_TRANS);
corr_PD  = corr(parameters_bootstrap_all_TRANS);
sigma_PD = std_PD'*std_PD;
% Handle non pos-semidefinite sigma_PD
[eigV,eigD] = eig(sigma_PD);
if min(diag(eigD)) > -1e-3,
    % It is not positive semidefinite, but the smallest eigenvalue
    % is so close to zero that we are going to put it on zero
    eigD(eigD<0) = 0;
    sigma_PD = eigV*eigD*inv(eigV);
    disp('Covariance matrix not positive semidefinite. Smallest eigenvalue is closer to 0 than -1e-3 => making it positive semidefinite.');
end
cov_PD   = corr_PD.*sigma_PD;
parameterValuesSAMPLED_TRANS = mvnrnd(mean_PD,cov_PD,NSAMPLES);

% Inverse transform the sampled parameters
parameterValuesSAMPLED = [];
for k=1:NSAMPLES,
    parameterValuesSAMPLED = [parameterValuesSAMPLED; invtransformXparametersSBPOP(parameterValuesSAMPLED_TRANS(k,:),run_results.parameterInformation.trans(find(run_results.parameterInformation.estimate)))];
end

% Add missing not estimated parameters
parameterNamesAdd   = run_results.parameterInformation.names(find(~run_results.parameterInformation.estimate));
parameterValuesAdd  = run_results.parameterInformation.values0(find(~run_results.parameterInformation.estimate));

parameterNames          = [parameterNames parameterNamesAdd];
parameterValuesSAMPLED  = [parameterValuesSAMPLED parameterValuesAdd(ones(1,size(parameterValuesSAMPLED,1)),:)];

% Assign sampled PD parameters to the parameters used
parameterValues = parameterValuesSAMPLED;
NSIM = NSAMPLES;

%% Get allTRTs that were in the fit
allTRT = DATAmedian.TRT;
        
%% Simulate
PK_ALL_TRT          = {};
READOUTS_ALL_TRT    = {};
SIMTIME_TRT         = {};

parfor kTRT = 1:length(allTRT),
    
    % Get treatment info
    TRT                                         = allTRT(kTRT);
    
    % Get index in dosingInfomation
    ix                                          = find(run_results.dosingInformation.TRT==TRT);
       
    % Get default simulation parameters 
    paramNamesSim  = [parameterNames];
    paramValuesSim = [parameterValues];
    
    % Sample PK population parameters (include median covariate information)
    if ~isempty(run_results.modelInformation.FIT_PK),
        PKparam = SBPOPsampleNLMEfitParam(run_results.modelInformation.FIT_PK,3,1,DATAmedian.covNames,DATAmedian.covValues(:,kTRT)',DATAmedian.catNames,DATAmedian.catValues(:,kTRT)');
        % Need to check in the model and remove the PK parameters that ae
        % not present in the model
        PKparamNotInModel = setdiff(PKparam.parameterNames,SBparameters(moddos));
        ix_remove_PKparam = [];
        for kPKparam=1:length(PKparamNotInModel),
            ix_remove_PKparam = [ix_remove_PKparam strmatchSB(PKparamNotInModel{kPKparam},PKparam.parameterNames)];
        end
        PKparam.parameterNames(ix_remove_PKparam) = [];
        PKparam.parameterValuesPopulation(ix_remove_PKparam) = [];
        
        % Combine PK and PD parameters
        paramNamesSim  = [paramNamesSim   PKparam.parameterNames];
        paramValuesSim = [paramValuesSim  PKparam.parameterValuesPopulation(ones(1,NSIM),:)];
    end
    
    % Add regression parameters
    paramNamesSim  = [paramNamesSim run_results.dataOriginalStratifiedMedian.regressionNames];
    regvalues = run_results.dataOriginalStratifiedMedian.regressionValues(:,kTRT)';
    paramValuesSim = [paramValuesSim  regvalues(ones(1,NSIM),:)];
    
    % Get dosing information 
    dosing_sim          = run_results.dosingInformation.dosings{ix};
    
    % Handle weight based dosing
    %   Check if weight based dosing and then change the dose
    %   Check for each input in the dosing scheme
    ds = struct(dosing_sim);
    for kx = 1:length(ds.inputs),
        if run_results.dosingInformation.weightBased(kx,ix),
            medianWEIGHT = DATAmedian.covValues(strmatchSB(DATAmedian.covNames,'WT0','exact'),kTRT);
            ds.inputs(kx).D = ds.inputs(kx).D*medianWEIGHT;
        end    
    end
    dosing_sim = SBPOPdosing(ds);
        
    % Define SIMTIME
    SIMTIME = unique([0; run_results.dataOriginalStratifiedMedian.NT{kTRT}]);
    SIMTIME = linspace(min(SIMTIME),max(SIMTIME),100);
    
    % Simulate
    PK_ALL = NaN(length(SIMTIME),NSIM);
    READOUTS_ALL = cell(1,length(run_results.modelInformation.modelOutput));
    for kx=1:length(run_results.modelInformation.modelOutput),
        READOUTS_ALL{kx} = NaN(length(SIMTIME),NSIM);
    end
    for kSIM=1:NSIM,
        kSIM
        try
            simres = SBPOPsimdosing(mexModelName,dosing_sim,SIMTIME,[],paramNamesSim,paramValuesSim(kSIM,:));
            
            % Get the PK ... assume Cc is the one.
            if ~isempty(variableindexSB(moddos,'Cc')),
                PK_ALL(:,kSIM) = simres.variablevalues(:,variableindexSB(moddos,'Cc'));
            end
            
            % Get the other readouts
            for kx=1:length(run_results.modelInformation.modelOutput),
                READOUTS_ALL{kx}(:,kSIM) = simres.variablevalues(:,variableindexSB(moddos,run_results.modelInformation.modelOutput{kx}));
            end
        catch
        end
    end
   
    PK_ALL_TRT{kTRT}            = PK_ALL;
    READOUTS_ALL_TRT{kTRT}      = READOUTS_ALL;
    SIMTIME_TRT{kTRT} = SIMTIME;    
end

%% start output
filename = [projectfolder '/OUTPUT_07_VPC_sampledParameters'];
startNewPrintFigureSBPOP(filename)

%% Plot PK results
if ~isempty(variableindexSB(moddos,'Cc')),
    % Get max and min conc
    PKx = [];
    for k=1:length(PK_ALL_TRT),
        PKx = [PKx; PK_ALL_TRT{k}(:)];
    end
    PKx(PKx==0) = [];
    minPK = min(PKx);
    maxPK = max(PKx);

    figure(1); clf;
    nrows = ceil(sqrt(length(PK_ALL_TRT)));
    ncols = ceil(length(PK_ALL_TRT)/nrows);
    PK_X = [];
    for kTRT=1:length(PK_ALL_TRT),
        PK_X  = [PK_X; PK_ALL_TRT{kTRT}(:)];
    end
    minY = min(log10(PK_X));
    maxY = max(log10(PK_X));
    for kTRT=1:length(allTRT),
        subplot(nrows,ncols,kTRT);
        PK_TRT  = PK_ALL_TRT{kTRT};
        SIMTIME  = SIMTIME_TRT{kTRT};
        plot(SIMTIME,log10(nanmedian(PK_TRT')),'k-','LineWidth',2)
        % Annotate
        if kTRT>length(PK_ALL_TRT)-ncols,
            xlabel('Time')
        end
        if mod(kTRT,ncols) == 1,
            ylabel('Conc')
        end
        grid on;
        set(gca,'YTick',[0 1 2 3 4 5 6]);
        set(gca,'YTickLabel',10.^get(gca,'YTick'));
        set(gca,'XLim',[min(SIMTIME) max(SIMTIME)]);
        set(gca,'YLim',[log10(minPK) log10(maxPK)]);
        
        % Get treatment name
        ix = find(run_results.dosingInformation.TRT==allTRT(kTRT));
        title(run_results.dosingInformation.name{ix},'Interpreter','none');
    end
    printFigureSBPOP(gcf,filename)
end

%% Plot Readout results
SIMTIME_X = [];
RO_Y = [];
for k=1:length(allTRT),
    SIMTIME_X = [SIMTIME_X SIMTIME_TRT{k}];
    for kRO=1:length(run_results.modelInformation.modelOutput),
        RO_Y      = [RO_Y; READOUTS_ALL_TRT{k}{kRO}(:)]; 
    end
end
% Postprocess RO_Y
RO_Y(isnan(RO_Y)) = [];
RO_Y = [min(RO_Y) min(max(RO_Y),median(RO_Y)*10)];

for kRO = 1:length(run_results.modelInformation.modelOutput),
    
    figure(kRO+1); clf;
    nrows = ceil(sqrt(length(PK_ALL_TRT)));
    ncols = ceil(length(PK_ALL_TRT)/nrows);

    for kTRT=1:length(PK_ALL_TRT),
        subplot(nrows,ncols,kTRT);
        
        RO_TRT = READOUTS_ALL_TRT{kTRT}{kRO};
        SIMTIME = SIMTIME_TRT{kTRT};
        
        % Plot the data
        data = run_results.dataOriginalStratifiedMedian.DATA{kTRT}(kRO,:);
        time = run_results.dataOriginalStratifiedMedian.NT{kTRT};
        plot(time,data,'.-','MarkerSize',25,'Color',0.3*[1 1 1]); hold on

        plot(SIMTIME,nanmedian(RO_TRT'),'-','LineWidth',2,'Color',0.8*colors(kRO,:)); hold on
        
        ranges      = [90 75 50 25];
        colorfactor = [0.65 0.5 0.35 0.2];
        legendText = {'Observations',sprintf('Simulated median (N=%d)',NSAMPLES)};
        for kplot=1:length(ranges)
            qlow = (1-ranges(kplot)/100)/2;
            qhigh = 1-(1-ranges(kplot)/100)/2;
            SBPOPplotfill(SIMTIME,quantile(RO_TRT',qlow),quantile(RO_TRT',qhigh),min(colorfactor(kplot)+colors(kRO,:),1),1,min(colorfactor(kplot)+colors(kRO,:),1)); hold on;
            legendText{end+1} = sprintf('%d %% CI',ranges(kplot));
        end
        
        plot(SIMTIME,nanmedian(RO_TRT'),'-','LineWidth',2,'Color',0.8*colors(kRO,:)); hold on
        
        % Plot errorbars on data
        data_stderr = run_results.dataOriginalStratifiedMedian.DATA_STDERR{kTRT}(kRO,:);
        errorbar(time,data,data_stderr,'Color',0.3*[1 1 1])
        
        % Annotate
        if kTRT>length(PK_ALL_TRT)-ncols,
            xlabel('Time')
        end
        if mod(kTRT,ncols) == 1,
            ylabel(sprintf('%s',run_results.dataInformation.names{kRO}),'Interpreter','none')
        end
        grid on;
        set(gca,'XLim',[min(SIMTIME_X) max(SIMTIME_X)]);
        set(gca,'YLim',[min(RO_Y)*0.9 max(RO_Y)*1.1]);
        ix = find(run_results.dosingInformation.TRT==allTRT(kTRT));
        title(run_results.dosingInformation.name{ix},'Interpreter','none');
        
        % Plot the data
        data = run_results.dataOriginalStratifiedMedian.DATA{kTRT}(kRO,:);
        time = run_results.dataOriginalStratifiedMedian.NT{kTRT};
        plot(time,data,'.-','MarkerSize',25,'Color',0.3*[1 1 1]);
    end
    % Add legend
    subplot(nrows,ncols,1);
    legend(legendText,'Location','NorthEast')
    printFigureSBPOP(gcf,filename)
end

convert2pdfSBPOP(filename);
close all

% Delete mex file
clear mex
delete([mexModelName '.' mexext]);

