function [] = SBPOPuncertaintyDistributionMedian(projectfolder)
% SBPOPuncertaintyDistributionMedian: Plot parameter uncertainty distribution 
% and correlations. Determine statistics.
%
% USAGE:
% ======
% SBPOPuncertaintyDistributionMedian(projectfolder)       
%
% projectfolder:        string with the path where the model results are stored 
%                       (e.g.: logfiles folder, run_results.data, ...)
%
% Results are generated as PDF file and stored in the projectfolder as 
% "OUTPUT_04_parameter_distributions.pdf". Additionally a table with the
% parameter values and their correlations (both transformed and un-transformed) 
% is generated as: OUTPUT_05_Parameter_Values.txt

% Information:
% ============
% Copyright (c) 2012 Novartis Pharma AG
% 
% This program is Free Open Source Software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program. If not, see <http://www.gnu.org/licenses/>.

%% Read tun results
run_results = load([projectfolder '/run_results.data'],'-mat'); run_results = run_results.run_results;

%% Read in parameters from bootstraps (getting normal and transformed values)
[parameters_bootstrap_all, parameters_bootstrap_all_TRANS] = SBPOPgetParametersMedian( projectfolder );

%% Start figure
filename = [projectfolder '/OUTPUT_04_parameter_distributions'];
startNewPrintFigureSBPOP(filename);

%% Define number of bins for histograms
NBINS = max(5,0.1*run_results.options.N_BOOTSTRAP);

%% Plot histograms of parameters
figure(1); clf;
N_paramEstimates = size(parameters_bootstrap_all,2);
nrows = ceil(sqrt(N_paramEstimates));
ncols = ceil(N_paramEstimates/nrows);
PDparamNamesEst = get(parameters_bootstrap_all,'VarNames');
for k=1:N_paramEstimates,
    subplot(nrows,ncols,k);
    hist(double(parameters_bootstrap_all(:,k)),NBINS);
    title(PDparamNamesEst{k},'FontSize',14,'Interpreter','none');
    grid on;
end
printFigureSBPOP(gcf,filename);

%% Plot histograms of transformed parameters
figure(1); clf;
N_paramEstimates = size(parameters_bootstrap_all,2);
nrows = ceil(sqrt(N_paramEstimates));
ncols = ceil(N_paramEstimates/nrows);
% Rename parameters
PDparamNamesEst_trans = {};
transInfo = run_results.parameterInformation.trans(find(run_results.parameterInformation.estimate));
for k=1:length(PDparamNamesEst),
    if strcmp(transInfo{k},'N'),
        PDparamNamesEst_trans{k} = PDparamNamesEst{k};
    elseif strcmp(transInfo{k},'L'),
        PDparamNamesEst_trans{k} = ['log(' PDparamNamesEst{k} ')'];
    elseif strcmp(transInfo{k},'G'),
        PDparamNamesEst_trans{k} = ['logit(' PDparamNamesEst{k} ')'];
    end
end
for k=1:N_paramEstimates,
    subplot(nrows,ncols,k);
    hist(double(parameters_bootstrap_all_TRANS(:,k)),20);
    title(PDparamNamesEst_trans{k},'FontSize',14,'Interpreter','none');
    grid on;    
end
printFigureSBPOP(gcf,filename);

%% Plot correlation information for parameters
gplotmatrix(double(parameters_bootstrap_all),[],[],[],[],[],[],[],PDparamNamesEst,PDparamNamesEst);
printFigureSBPOP(gcf,filename);

%% Plot correlation information for transformed parameters
gplotmatrix(double(parameters_bootstrap_all_TRANS),[],[],[],[],[],[],[],PDparamNamesEst_trans,PDparamNamesEst_trans);
printFigureSBPOP(gcf,filename);

%% Close figure
convert2pdfSBPOP(filename)

%% Calculate mean, median, stderror and RSE of mean
median_param = median(double(parameters_bootstrap_all),1);
mean_param   = mean(double(parameters_bootstrap_all),1);
if run_results.options.N_BOOTSTRAP>1,
    stderr_param    = std(double(parameters_bootstrap_all))/sqrt(run_results.options.N_BOOTSTRAP);
else
    stderr_param    = zeros(1,length(median_param));
end
RSEmean_param = 100*stderr_param./mean_param;
% Determine correlation of untransformed parameters
corrx = num2str(round(100*corr(double(parameters_bootstrap_all)))/100);
% Get parameter names
PDparamNamesEst = get(parameters_bootstrap_all,'VarNames');

% Write out the results in a table
fid = fopen([projectfolder '/OUTPUT_05_Parameter_Values.txt'],'w');
fprintf(fid,'==================================================================\n');
fprintf(fid,'=                     Parameter estimates                        =\n');
fprintf(fid,'==================================================================\n');
fprintf(fid,'\n');
text = sprintf('Parameter       MEDIAN      MEAN        STD         RSE(MEAN)\n');
text = sprintf('%s------------------------------------------------------------------',text);
for k=1:sum(run_results.parameterInformation.estimate),
    text = sprintf('%s\n%s %s%1.3g%s%1.3g%s%1.3g%s%1.3g%%',text,...
        PDparamNamesEst{k},...
        char(32*ones(1,15-length(PDparamNamesEst{k}))),...
        median_param(k),...
        char(32*ones(1,12-length(sprintf('%1.3g',median_param(k))))),...
        mean_param(k),...
        char(32*ones(1,12-length(sprintf('%1.3g',mean_param(k))))),...
        stderr_param(k),...
        char(32*ones(1,12-length(sprintf('%1.3g',stderr_param(k))))),...
        RSEmean_param(k));
end
fprintf(fid,'%s',text);

text = fprintf(fid,'\n\nCorrelations\n------------------------------------------------------------------\n');

for k=1:size(corrx,1),
    fprintf(fid,'%s\n',corrx(k,:));
end


fclose(fid);

close all
