function [] = SBPOPassessConvergenceMedian(projectfolder)
% SBPOPassessConvergenceMedian: Load the convergence information from a 
% provided median modeling project folder and plot the trajectories for parameters,
% and costfunction.
%
% USAGE:
% ======
% SBPOPassessConvergenceMedian(projectfolder)       
%
% projectfolder:        string with the path where the model results are stored 
%                       (e.g.: logfiles folder, run_results.data, ...)
%
% Results are generated as PDF file and stored in the projectfolder as 
% "OUTPUT_01_Convergence.pdf".

% Information:
% ============
% Copyright (c) 2012 Novartis Pharma AG
% 
% This program is Free Open Source Software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program. If not, see <http://www.gnu.org/licenses/>.

% Read run results MAT file
run_results = load([projectfolder '/run_results.data'],'-mat'); run_results = run_results.run_results;

% Read folder with logfiles
x = dir([projectfolder '/logfiles/*.log']);

% Get OFV and parameter data to assess convergence from the logfiles
OFV_ALL = [];
PARAMETERS_ALL = {};
for k=1:sum(run_results.parameterInformation.estimate),
    PARAMETERS_ALL{k} = [];
end

for k=1:length(x),
    contents = fileread([projectfolder '/logfiles/' x(k).name]);
    contents = strrep(contents,'Cost=','');
    contents = strrep(contents,'X=[','');
    contents = strrep(contents,']','');
    contents = ['[' strtrim(contents) ']'];
    values   = eval(contents);

    nrelements  = min(run_results.options.maxfunevals,size(values,1));
    
    OFV         = NaN(length(1:run_results.options.maxfunevals),1);
    OFV(1:nrelements) = values(1:nrelements,1);
    OFV_normalized = OFV/OFV(1);
    OFV_ALL  = [OFV_ALL OFV_normalized(:)];
    
    PARAMETERS = NaN(length(1:run_results.options.maxfunevals),size(values,2)-1);
    PARAMETERS(1:nrelements,:) = values(1:nrelements,2:end);
    
    for k2=1:size(PARAMETERS,2),
        PARAMETERS_ALL{k2} = [PARAMETERS_ALL{k2} PARAMETERS(1:run_results.options.maxfunevals,k2)];
    end
end

% Start output
filename = [projectfolder '/OUTPUT_01_Convergence'];
startNewPrintFigureSBPOP(filename);

% Do plot of OFV to assess convergence
figure(1); clf
plot(OFV_ALL); hold on
grid on;
title('Normalized OFV','FontSize',16,'Interpreter','none');
xlabel('# cost function evaluation','FontSize',16);
set(gca,'FontSize',14);
set(gca,'YLim',[0 1.5]);
printFigureSBPOP(gcf,filename);   

% Do plot parameter trajectories - also for convergence
figure(2); clf
nrows = ceil(sqrt(sum(run_results.parameterInformation.estimate)));
ncols = ceil(size(PARAMETERS,2)/nrows);
PDparamNames_plot = run_results.parameterInformation.names(find(run_results.parameterInformation.estimate));
for k2=1:sum(run_results.parameterInformation.estimate),
    subplot(nrows,ncols,k2);
    plot(PARAMETERS_ALL{k2}); hold on
    grid on;
    title(PDparamNames_plot{k2},'FontSize',14,'Interpreter','none');
    xlabel('# cost function evaluation','FontSize',14);
    set(gca,'FontSize',12);
end
printFigureSBPOP(gcf,filename);   

%% Do plot median parameter trajectory and 5% and 95% quantiles
if run_results.options.N_BOOTSTRAP > 1,
    figure(3); clf
    nrows = ceil(sqrt(sum(run_results.parameterInformation.estimate)));
    ncols = ceil(size(PARAMETERS,2)/nrows);
    PDparamNames_plot = run_results.parameterInformation.names(find(run_results.parameterInformation.estimate));
    for k2=1:sum(run_results.parameterInformation.estimate),
        subplot(nrows,ncols,k2);
        plot(median(PARAMETERS_ALL{k2}'),'k','LineWidth',3); hold on
        ranges      = [90 75 50 25];
        colorfactor = [0.95 0.85 0.75 0.65];
        legendText = {'Median'};
        for kplot=1:length(ranges)
            qlow = (1-ranges(kplot)/100)/2;
            qhigh = 1-(1-ranges(kplot)/100)/2;
            SBPOPplotfill(1:run_results.options.maxfunevals,quantile(PARAMETERS_ALL{k2}',qlow),quantile(PARAMETERS_ALL{k2}',qhigh),colorfactor(kplot)*[1 1 1],1,colorfactor(kplot)*[1 1 1]); hold on
            legendText{end+1} = sprintf('%d %%',ranges(kplot));
        end
        plot(median(PARAMETERS_ALL{k2}'),'k','LineWidth',3); hold on
        grid on;
        title(PDparamNames_plot{k2},'FontSize',14,'Interpreter','none');
        xlabel('# cost function evaluation','FontSize',14);
        set(gca,'FontSize',12);
        if k2==1,
            legend(legendText,'Location','SouthEast')
        end
    end
    printFigureSBPOP(gcf,filename);
end

convert2pdfSBPOP(filename);
close all