%% SCRIPT 01: for graphical exploration (no data included ... just code)
%
% In this script the data is explored and the dataset for fitting of the 
% responder rates is built 

%% ===Setup
clc; clear all; close all; restoredefaultpath();
path2SBPOP          = '../../../SBPOP_REV_1289';
oldpath             = pwd; cd(path2SBPOP);      installSBPOPpackage();                  cd(oldpath);

%% ===Data
data = SBPOPloadCSVdataset('../Data/data.csv');

%% ===Data exploration

%% Continuous PD readouts
NAMES               = {'ABC',   'XYZVAS'};
BASELINENAMES       = {'ABC0',  'XYZ0'};
COVARIATES          = {'AGE0','SEX','HT0','WT0','BMI0','CRP0'};
TIMEPOINT_CHANGE    = 100;
PD_IMPROVEMENT      = [-20 -40];  % in percent

options             = [];
options.filename    = '../Output/Graphical_Exploration/01_PD_data_continuous';
options.fontsize    = 14;

SBPOPgraphicalExplorationContinuousPD(data,NAMES,BASELINENAMES,COVARIATES,TIMEPOINT_CHANGE,PD_IMPROVEMENT,options)

%% Categorical PD readouts (Responder Rates)
NAMES               = {'MNO20','MNO40'};
COVARIATES          = {'AGE0','SEX','HT0','WT0','BMI0','CRP0','ABC0','XYZ0'};

options             = [];
options.filename    = '../Output/Graphical_Exploration/02_PD_data_categorical';
options.fontsize    = 14;

SBPOPgraphicalExplorationResponderRatePD(data,NAMES,COVARIATES,options)

%% Summary statistics
covNames = {'ABC0','XYZ0','AGE0','HT0','WT0','BMI0','CRP0'};
catNames = {'SEX'};
SBPOPexploreSummaryStats(data,covNames,catNames,'../Output/Graphical_Exploration/03_Summary_Statistics')

%% Covariate exploration
covNames = {'ABC0','XYZ0','AGE0','HT0','WT0','BMI0','CRP0'};
catNames = {'SEX'};
SBPOPexploreCovariateCorrelations(data,covNames,catNames,'../Output/Graphical_Exploration/04_Covariate_Exploration')

%% ===Generate dataset for optimization
NAMES               = {'ABC',   'XYZVAS', 'MNO20', 'MNO40'};
COVARIATES          = {'AGE0','SEX','HT0','WT0','BMI0','CRP0','ABC0','XYZ0'};
SBPOPcreateDatasetMedianOptimization(data,NAMES,COVARIATES,'../Data/data_median_optimization.csv')

