function [] = SBPOPcreateDRUGligandTargetModel(filename)
% SBPOPcreateDRUGligandTargetModel: this function simply copies the file
% "standard_Drug_LigandTarget_model.txtbc" under the given filename in the current
% folder. 
%
% USAGE:
% ======
% SBPOPcreateDRUGligandTargetModel(filename)
% 
% filename: name of the model file to create (extension: .txtbc)

% Information:
% ============
% Copyright � 2012 Novartis Pharma AG
% 
% This program is Free Open Source Software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program. If not, see <http://www.gnu.org/licenses/>.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CHECK THE INPUT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if ~ischar(filename),
    error('Incorrect input argument.');
end
% remove extensions from input filename
[pathstr,name] = fileparts(filename);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% COPY THE FILE TO NEW LOCATION AND NAME
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
copyfile(which('standard_Drug_LigandTarget_model.txtbc'),[name '.txtbc']);
