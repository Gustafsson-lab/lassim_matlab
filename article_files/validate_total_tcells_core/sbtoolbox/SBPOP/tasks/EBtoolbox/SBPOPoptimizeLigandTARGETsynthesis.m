function [popt] = SBPOPoptimizeLigandTARGETsynthesis(model,pnames_in,pvalues0,referenceValues_in,varargin)
% SBPOPoptimizeLigandTARGETsynthesis: This function will estimate parameters
% of the standard model to adjust the steady-state concentrations of a
% target (ligand) to desired properties (plasma/disease tissue/peripheral tissue
% concentrations) 
%
% These desired properties are:
%      - plasma target steady-state concentration              
%      - disease tissue target steady-state concentration      
%      - peripheral tissue target steady-state concentration   
%
% Before using this function you need to fit the target kinetics (clearance
% and surface exchange coefficient) using the
% "SBPOPoptimizeLigandTARGETkinetics" function.
% 
% IMPORTANT:
% ==========
% This function assumes the use of the standard model and by default the use of
% "T1_plasma"/"T1_itst_tissue"/"T1_itst_peri" as the statenames for the
% target concentration in the different tissues. Additionally, it is
% assumed that the reference values are provided in exactly that order
% (plasma / tissue / peripheral tissue).
% The user can define different names for the target state in plasma /
% tissue / peripheral tissue (that order!), which is useful if a second
% soluble target is available in the model which also should  be fitted.
% The user can provide the parameters to be estimated and initial guesses. 
%
% The results are shown in the MATLAB command window.
%
% USAGE:
% ======
% [popt] = SBPOPoptimizeLigandTARGETsynthesis(model,pnames,pvalues0,referenceValues)
% [popt] = SBPOPoptimizeLigandTARGETsynthesis(model,pnames,pvalues0,referenceValues,parametervector)
% [popt] = SBPOPoptimizeLigandTARGETsynthesis(model,pnames,pvalues0,referenceValues,parametervector,targetparam)
% [popt] = SBPOPoptimizeLigandTARGETsynthesis(model,pnames,pvalues0,referenceValues,parametervector,targetparam,weights)
% [popt] = SBPOPoptimizeLigandTARGETsynthesis(model,pnames,pvalues0,referenceValues,parametervector,targetparam,weights,timevector)
%
% model: SBmodel or MEX simulation model
% pnames: cell-array with parameters to estimate
% pvalues0: vector with initial guesses for the parameters
% referenceValues: vector with 2 entries:
%   referenceValues(1): desired plasma steady-state target concentration (NOT ALLOWED TO BE ZERO)              
%   referenceValues(2): desired disease tissue steady-state target concentration (NOT ALLOWED TO BE ZERO)      
%   referenceValues(3): desired peripheral tissue steady-state target concentration (NOT ALLOWED TO BE ZERO)   
% parametervector: full parameter vector for model simulation. If not given, the parameters
%   defined in the model are used.
% weights: vector with weights for the residuals for the three different
%   objectives, defined in the referenceValues vector. This is very useful
%   in the case that not all of the steady state concentrations for the 
%   tissues are known. Set zero as weight for the reference value you do
%   not have a reasonable guess for.
% targetparam: names of the target state variables in plasma, disease
%   tissue, peripheral tissue (that order).
% timevector: timevector for simulation during optimization (needs to
%   capture the system until its steady-state).
%
% DEFAULT VALUES:
% ===============
% parametervector: nominal vector defined in the model
% targetparam: {'T1_plasma','T1_itst_tissue','T1_itst_peri'}
% weights: [1 1 1]
% timevector: [0:0.01:30]
%
% Output Arguments:
% =================
% popt: vector with optimized parameter values

% Information:
% ============
% Copyright � 2012 Novartis Pharma AG
% 
% This program is Free Open Source Software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program. If not, see <http://www.gnu.org/licenses/>.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Make parameter names and reference values 
% available to the cost function
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
warning off;
global pnames referenceValues MEXmodel weights opttimevector plotFlag targetparam parameternames parametervector
warning on;
pnames = pnames_in;
referenceValues = referenceValues_in;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Handle variable input arguments
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
opttimevector = [0:0.01:30];
targetparam = {'T1_plasma','T1_itst_tissue','T1_itst_peri'};
weights = [1 1 1];
[parameternames,parametervector_nominal] = SBparameters(model); % get values stored in the model
parametervector = parametervector_nominal;
if nargin >= 5,
    parametervector = varargin{1};
    if isempty(parametervector),
        parametervector = parametervector_nominal;
    else
        if length(parametervector) ~= length(SBparameters(model)),
            error('The length of the provided parameter vector does not match the number of parameters in the model.');
        end
    end
end
if nargin >= 6,
    targetparam = varargin{2};
end
if nargin >= 7,
    weights = varargin{3};
end
if nargin >= 8,
    opttimevector = varargin{4};
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Get MEX model
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if isSBmodel(model),
    [MEXmodel, MEXmodelfullpath] = makeTempMEXmodelSBPD(model);
else 
    MEXmodel = model;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define parameter bounds
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
plb = zeros(1,length(pvalues0));       % lower bounds
pub = 1000*pvalues0;                   % upper bounds

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Run the optimization
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
OPTOPTIONS.lowbounds = plb;
OPTOPTIONS.highbounds = pub;
OPTOPTIONS.maxfunevals = 200;
OPTOPTIONS.silent = 1;
plotFlag = 0;
[popt, FVAL] = simplexSB(@target_steady_state, pvalues0, OPTOPTIONS);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Check "optimal" parameters and plot a check
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% REF_T_plasma      = referenceValues(1);   % Plasma SS conc Target
% REF_T_itst_tissue = referenceValues(2);   % Disease tissue SS conc Target
% REF_T_itst_peri   = referenceValues(3);   % Peripheral tissue SS conc Target
plotFlag = 1;
[cost,residuals] = target_steady_state(popt);
disp(' ');
disp('Optimized Target Synthesis:');
disp('===========================');
disp(sprintf('achieved cost = %g',cost));
disp('Optimal parameters:');
for k=1:length(pnames),
    disp(sprintf('\t%s = %g',pnames{k},popt(k)));
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Remove MEX model if created here
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if isSBmodel(model),
    clear mex; delete(MEXmodelfullpath);
end
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Cost function for optimization of the parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [cost,res] = target_steady_state(x)
% target_steady_state: cost function for the optimization of given model
% parameters with the goal to achieve a certain (and provided):
%
%   - Plasma SS conc Target
%   - Disease tissue SS conc Target
%   - Peripheral tissue SS conc Target
%
% The user needs to check that the global parameter "opttimevector" is set
% such that steady-state can be achieved in the second experiment
% simulation below.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% get the global variables
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
global pnames referenceValues MEXmodel weights opttimevector plotFlag targetparam parameternames parametervector

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % Use the reference T_plasma value as initial condition. At final
% % simulation time it needs to be the same => equilibrium between target
% % removal and production
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% icvector = makeinicondvectorSBPD(MEXmodel,targetparam,referenceValues);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Get the parametervector
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
parametervector_sim = updateparamvecSBPD(parameternames, parametervector, pnames, x);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Simulate
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
options = [];
options.abstol = 1e-10;
options.reltol = 1e-10;
simdata = feval(MEXmodel, opttimevector, [], parametervector_sim,options);
% get the T_concentrations matrix (targetparam can be states or variables)
T_concentrations = zeros(length(simdata.time),length(targetparam));
T_concentrations_end = zeros(1,length(targetparam));
for k=1:length(targetparam),
    param = targetparam{k};
    % figure out if state of variable
    index = strmatchSB(param,simdata.states,'exact');
    if ~isempty(index),
        % state
        T_concentrations(:,k) = simdata.statevalues(:,index);
        T_concentrations_end(k) = simdata.statevalues(end,index);
    else
        % variable
        index = strmatchSB(param,simdata.variables,'exact');
        if isempty(index),
            error('''%s'' is neither a state nor a variable in the model.',param);
        end
        T_concentrations(:,k) = simdata.variablevalues(:,index);
        T_concentrations_end(k) = simdata.variablevalues(end,index);
    end
end        
        
% Plot to check 
if plotFlag
    figure; clf;
    plot(simdata.time, T_concentrations); hold on;
    plot([0 simdata.time(end)],[referenceValues(:)';referenceValues(:)'],'--'); 
    set(gca,'YScale','log'); 
    legend(targetparam);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Determine the residuals
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
res = T_concentrations_end(:) - referenceValues(:);

% Get the cost
weightedres = res(:)./referenceValues(:).*weights(:);
cost = sum(weightedres.^2);
return







