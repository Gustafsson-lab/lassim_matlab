function SBPOPplotDoseParamCurve(effect, dosevector, paramchangevector, titletext, effectLevelSteps, targetEffect)
% SBPOPplotDoseParamCurve: This function plots a dose/parameter curve for
% given input arguments. Different effect levels are used as parameters for
% the different curves.
%
% USAGE:
% ======
% SBPOPplotDoseParamCurve(effect, dosevector, paramchangevector)
% SBPOPplotDoseParamCurve(effect, dosevector, paramchangevector, titletext)
% SBPOPplotDoseParamCurve(effect, dosevector, paramchangevector, titletext, effectLevelSteps)
% SBPOPplotDoseParamCurve(effect, dosevector, paramchangevector, titletext, effectLevelSteps, targetEffect)
%
% effect: matrix with determined effect values (columns correspond to
%   different parameter settings (parameterchangevector) and rows correspond
%    to different dose amount settings (dosevector)).
% dosevector: vector with different dosing amounts, corresponding to  the
%   effect matrix.
% paramchangevector: vector with values for an additional parameter that
%   was varied.
% effectLevelSteps: effect level steps to consider. Default: [5,10,20,30,40,50,60,70,80,90,95]
% targetEffect: special effect levels that are plotted in a different color.
% titletext: text to use as figure title

% Information:
% ============
% Copyright � 2012 Novartis Pharma AG
% 
% This program is Free Open Source Software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program. If not, see <http://www.gnu.org/licenses/>.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% HANDLE VARIABLE INPUT ARGUMENTS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if ~exist('titletext','var')
    titletext  = 'Untitled';
end
if ~exist('effectLevelSteps','var')
    effectLevelSteps  = [5,10,20,30,40,50,60,70,80,90,95];
end
if isempty(effectLevelSteps),
    effectLevelSteps  = [5,10,20,30,40,50,60,70,80,90,95];
end    
if ~exist('targetEffect','var')
    targetEffect  = []; % means undefined!
end
% add targetEffect to effectLevelSteps (as last effect level)
effectLevelSteps = [effectLevelSteps targetEffect];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% INTERPLOATE TO OBTAIN DOSE value for given
% effect level and parametervalue
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
doseMatrix = [];
for k1=1:length(paramchangevector),
    for k2=1:length(effectLevelSteps),
        % we need to find the dose that is needed to obtain with current
        % parameter the current effect level. The splines not always
        % invertible => use bisection.
        effectLevel = effectLevelSteps(k2);
        dose = getDose4Effect(dosevector, effect(:,k1), effectLevel);
        doseMatrix(k2,k1) = dose;
    end
end
% doseMatrix: entries: doses to obtain desired effect level with given parameter
%             columns: each column different parameter
%             rows: each row different desired effect level

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Split up target Effect Level if present
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if ~isempty(targetEffect),
    effectLevelSteps = effectLevelSteps(1:end-length(targetEffect));
    targetDoseMatrix = doseMatrix(end-length(targetEffect)+1:end,:);
    doseMatrix = doseMatrix(1:end-length(targetEffect),:);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plot the curves (for effect levels)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure; clf;
for k=1:size(doseMatrix,1)
    effectLevel = effectLevelSteps(k);
    dose = doseMatrix(k,:);
    loglog(dose,paramchangevector, 'LineWidth', 2); hold on;
    text(mean(dose), mean(paramchangevector), num2str(effectLevel),'EdgeColor','black','BackgroundColor','white');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plot the curves (for target levels)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if ~isempty(targetEffect),
    for k=1:size(targetDoseMatrix,1)
        effectLevel = targetEffect(k);
        dose = targetDoseMatrix(k,:);
        loglog(dose,paramchangevector, 'LineWidth', 2, 'Color', 'red'); hold on;
        text(mean(dose), mean(paramchangevector), num2str(effectLevel),'EdgeColor','red','BackgroundColor','white');
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Decorate the plot
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
xlabel('Dose');
hlhlx = title(titletext,'Interpreter','none');
set(hlhlx,'Interpreter','none');
ylabel('Parameter');
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Do bisection to get the dose for given effect and parameter
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [dose] = getDose4Effect(dosevector, effect, effectLevel)
lowDose = min(dosevector);
highDose = max(dosevector);
currentDose = (highDose+lowDose)/2;
stopDeltaDose = 1e-3*lowDose;
while abs(highDose-lowDose) > stopDeltaDose,
    doseeffect = interpcsSB(dosevector,effect,currentDose);
    if doseeffect > effectLevel,
        lowDose = currentDose;
        highDose = highDose;
        currentDose = (highDose+lowDose)/2;
    elseif doseeffect < effectLevel,
        lowDose = lowDose;
        highDose = currentDose;
        currentDose = (highDose+lowDose)/2;
    else
        break;
    end
end
dose = currentDose;
return



