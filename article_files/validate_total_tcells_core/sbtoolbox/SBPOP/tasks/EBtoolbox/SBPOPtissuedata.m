function SBPOPtissuedata()
% SBPOPtissuedata: This function is simply a collection of literature data
% from two references (see below), that stores the volumes of different
% tissues and the corresponding volumes of the interstitial spaces.
%
% PBPK parameters (organ weight and volume and interstitial space volume)
% for the 70kg, 20% fat "standardhuman" from reference [1] and [2] (see
% below). 
%
% USAGE: 
% ======
% SBPOPtissuedata()
% Just run the function and look at the output. In order to learn how the
% values have been determined, please read the complete function (this
% here).
%
% References:
% ===========
% [1] D.G. Levitt (2003) The pharmacokinetics of the interstitial space in
% humans, BMC Clinical Pharmacology, 3
% [2] R.P. Brown et al. (1997) Physiological parameter values for
% physiologically based pharmacokinetic models, Toxicology and Industrial
% Health, 13(4), 407-484 

% Information:
% ============
% Copyright � 2012 Novartis Pharma AG
% 
% This program is Free Open Source Software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program. If not, see <http://www.gnu.org/licenses/>.




% The following matrices define the considered tissues, their weight and volume 
% of their interstitial space that is relevant to the antibody/drug distribution.
% The factor represents the conversion factor from weight to volume for the different
% organs. It is taken from reference [2]. Some weights needed to be
% inferred, for more information, please see below. Some of the factors
% were not defined in the references. Then they have been set to 1 (e.g. blood, 
% liver, tendon, other) or inferred (e.g. portal, which was determined from
% the organs involved in it).
PBPK_data = {
% Tissue            Weight (kg)       Interstitial tissue volume (L)    factor (L/kg) 
'blood          '   5.5               2.68345                           1 % not given in [2]  
'liver          '   1.8               0.2898                            1 % not given in [2]
% "portal" in reference [1] refers to the organs drained by the portal vein (stomach, small and large
% intestine, spleen, pancreas)
'portal         '   1.5               0.351                             1.045 % factor assumed as approx mean from different components factors, defined in [2]
'muscle         '   26                3.042                             1.04
'kidney         '   0.31              0.04092                           1.05
% "brain": blood-brain barrier will prevent the distribution into the interstitial space
'brain          '  1.4                0                                 1.04
'heart          '  0.33               0.066                             1.03
'lung           '  0.536              0.08576                           1.05
'skin           '  2.6                1.092                             1.45    % manually chosen factor => fits nicely with value used by Matthias in a previous project
'tendon         '  3                  2.55                              1       % factor assumed (not given in [2])
% "bone" refers to the inert solid component of bone that has no volume of distribution or blood flow
'bone           '  4                  0                                 1.99    % factor not including marrow
'adipose        '  17.5               3.5                               0.916   
'other          '  5.524              3.75632                           1       % factor assumed (not given in [2])
};

% Determine the tissue volumes from the weights and factors, defined in the
% PBPK_data matrix and write the result in the 5th column.
for k=1:size(PBPK_data,1),
    weight = PBPK_data{k,2};
    factor = PBPK_data{k,4};
    volume = weight*factor;
    PBPK_data{k,5} = volume;
end

% Above, "portal" includes 5 different organs: stomach, small and large
% intestine, spleen, pancreas. Reference [2] tells us the relative weights
% for a reference man (stomach: 0.21%, small intestine: 0.91%, large
% intestine: 0.53%, spleen: 0.26%, pancreas: 0.14%). Using these
% definitions and a 70kg standard human the sum of the weights is 1.435kg.
% This is in contrast to the 1.5kg for "portal" in reference [1]. We just
% scale the relative weights to obtain a total weight of 1.5kg and
% determine the volume of the organ tissues using these weights and the
% factors, given in [2]. Furthermore, we split the interstitial space
% volume, defined in [1] for portal (0.351) according to the ratio tissue
% weight / portal tissue weight. This is all just an approximation, but a
% reasonable one :-)
PBPK_data_portal = {
'stomach        '   0.154             0.036                             1.05  
'small intestine'   0.666             0.1558                            1.045 
'large intestine'   0.388             0.0907                            1.042 
'spleen         '   0.190             0.0445                            1.054 
'pancreas       '   0.102             0.024                             1.045 
};

% Determine the tissue volumes for the portal organ tissues
for k=1:size(PBPK_data_portal,1),
    weight = PBPK_data_portal{k,2};
    factor = PBPK_data_portal{k,4};
    volume = weight*factor;
    PBPK_data_portal{k,5} = volume;
end

% Write out the information:
clc;
disp('Tissue information: organ volumes and interstitial spaces');
disp('=========================================================');
disp('Assumption: 70kg, 20% fat, standardhuman');
disp(' ');
disp('Tissue              Volume (L)          Interstitial space (L)');
disp('---------------------------------------------------------------');
totalvolume = 0;
totalitstspace = 0;
for k=1:size(PBPK_data,1),
    organ = PBPK_data{k,1};
    tissuevolume = PBPK_data{k,5};
    interstitialspace = PBPK_data{k,3};
    disp(sprintf('%s\t\t%1.4f\t\t\t\t%g',organ,tissuevolume,interstitialspace));
    totalvolume = totalvolume + tissuevolume;
    totalitstspace = totalitstspace + interstitialspace;
end
disp('---------------------------------------------------------------');
disp(sprintf('Total:\t\t\t\t%1.4f\t\t\t\t%g',totalvolume,totalitstspace));
disp('---------------------------------------------------------------');
disp(' ');
disp('"Portal" tissue above contains several different organs:');
disp(' ');
disp('Tissue              Volume (L)          Interstitial space (L)');
disp('---------------------------------------------------------------');
for k=1:size(PBPK_data_portal,1),
    organ = PBPK_data_portal{k,1};
    tissuevolume = PBPK_data_portal{k,5};
    interstitialspace = PBPK_data_portal{k,3};
    disp(sprintf('%s\t\t%1.4f\t\t\t\t%g',organ,tissuevolume,interstitialspace));
end
disp('---------------------------------------------------------------');
disp('For information about the derivation of these values, please type:');
disp('               >> type SBPOPtissuedata');

 