function [popt] = SBPOPoptimizeDRUGkinetics(model,pnames_in,pvalues0,referenceValues_in,varargin)
% SBPOPoptimizeDRUGkinetics: This function will estimate parameters
% of the standard model to adjust the kinetics of a drug molecule (mAb,
% nanobody, etc.) to desired properties (alpha and beta halflife and
% percentage of steady-state distribution between disease tissue and
% plasma) 
%
% These desired properties are:
%   - alpha-halftime of drug in plasma                     (referenceValue(1)) 
%   - beta-halftime of drug in plasma                      (referenceValue(2)) 
%   - steady-state concentration ration of mAb             (referenceValue(3))
%     between tissue and plasma
%
% IMPORTANT:
% ==========
% This function assumes the use of the standard model and the use of
% "INPUT1" identifier for adding the drug. Nevertheless, the user can provide the
% parameters to be estimated and initial guesses. 
%
% The results are summarized in the MATLAB command window.
%
% USAGE:
% ======
% [popt] = SBPOPoptimizeDRUGkinetics(model,pnames,pvalues0,referenceValues)
% [popt] = SBPOPoptimizeDRUGkinetics(model,pnames,pvalues0,referenceValues,parametervector)
% [popt] = SBPOPoptimizeDRUGkinetics(model,pnames,pvalues0,referenceValues,parametervector,weights)
% [popt] = SBPOPoptimizeDRUGkinetics(model,pnames,pvalues0,referenceValues,parametervector,weights,input)
% [popt] = SBPOPoptimizeDRUGkinetics(model,pnames,pvalues0,referenceValues,parametervector,weights,input,timevector)
%
% model: SBmodel or MEX simulation model
% pnames: cell-array with parameters to estimate
% pvalues0: vector with initial guesses for the parameters
% referenceValues: vector with 3 entries:
%   referenceValues(1): desired alpha-halftime of drug in plasma (NOT ALLOWED TO BE ZERO)
%   referenceValues(2): desired beta-halftime of drug in plasma (NOT ALLOWED TO BE ZERO) 
%   referenceValues(3): desired ratio between steady-state drug in tissue and plasma (NOT ALLOWED TO BE ZERO)  
% parametervector: full parameter vector for model simulation. If not
%   given, the parameters defined in the model are used. The definition of
%   this parameter vector makes most sense if the function is called on a
%   MEX simulation function, rather than an SBmodel. If empty vector is
%   passed then the parameter vector in the model will be used.
% weights: vector with weights for the residuals for the three different
%   objectives, defined in the referenceValues vector. This is very useful
%   in the case that the lymph flow rate is known in advance (for nanobody
%   drugs the mAb lymph flow is set fixed). Then this function can be used
%   also to only fit the alpha and beta halflives.
% input: "INPUT1" or "INPUT2", etc. Depending on the input in your model
%   that you want to apply the drug to (default: INPUT1).
% timevector: timevector for simulation during optimization (needs to
%   capture both the fast and the slow phase of drug distribution and
%   elimination.
%
% DEFAULT VALUES:
% ===============
% parametervector: nominal vector defined in the model
% weights: [1 1 1]
% timevector: from 0 to referenceValues(2)*10 in 5000 steps
%
% Output Arguments:
% =================
% popt: vector with optimized parameter values

% Information:
% ============
% Copyright � 2012 Novartis Pharma AG
% 
% This program is Free Open Source Software: you can redistribute it and/or modify 
% it under the terms of the GNU General Public License as published by 
% the Free Software Foundation, either version 3 of the License, or 
% (at your option) any later version. 
% 
% This program is distributed in the hope that it will be useful, 
% but WITHOUT ANY WARRANTY; without even the implied warranty of 
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
% GNU General Public License for more details. 
% 
% You should have received a copy of the GNU General Public License 
% along with this program. If not, see <http://www.gnu.org/licenses/>.

% Display warning message
disp(' ');
disp('SBPOPoptimizeDRUGkinetics: Please make sure you have disabled drug/target binding in the model when calling this function.');
disp(' ');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Make parameter names and reference values 
% available to the cost function
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
warning off;
global pnames referenceValues MEXmodel opttimevector plotFlag parameternames parametervector weights input
warning on;
pnames = pnames_in;
referenceValues = referenceValues_in;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Handle variable input arguments
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ts = 0;
te = referenceValues(2)*10;
opttimevector = [ts:(te-ts)/5000:te];
weights = [1 1 1];
input = 'INPUT1';
[parameternames,parametervector_nominal] = SBparameters(model); % get values stored in the model
parametervector = parametervector_nominal;
if nargin >= 5,
    parametervector = varargin{1};
    if isempty(parametervector),
        parametervector = parametervector_nominal;
    else
        if length(parametervector) ~= length(SBparameters(model)),
            error('The length of the provided parameter vector does not match the number of parameters in the model.');
        end
    end
end
if nargin >= 6,
    weights = varargin{2};
end
if nargin >= 7,
    input = varargin{3};
end
if nargin >= 8,
    opttimevector = varargin{4};
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Get MEX model if not already provided as mex model
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if isSBmodel(model),
    [MEXmodel, MEXmodelfullpath] = makeTempMEXmodelSBPD(model);
else 
    MEXmodel = model;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define parameter bounds
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
plb = zeros(1,length(pvalues0));       % lower bounds
pub = 1000*pvalues0;                   % upper bounds

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Run the optimization
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
OPTOPTIONS.lowbounds = plb;
OPTOPTIONS.highbounds = pub;
OPTOPTIONS.maxfunevals = 200;
OPTOPTIONS.silent = 1;
if isempty(plotFlag)
    plotFlag = 0;
end
plotFlagOld = plotFlag;
plotFlag = 0;
[popt, FVAL] = simplexSB(@Drug_exp, pvalues0, OPTOPTIONS);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Check "optimal" parameters and plot a check
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
REF_T12_a         = referenceValues(1);   % Drug Alpha half time
REF_T12_b         = referenceValues(2);   % Drug Beta half time
REF_delta_c       = referenceValues(3);   % SS-ratio of mAb concentration tissue/plasma
plotFlag = plotFlagOld;
[cost,residuals] = Drug_exp(popt);
disp(' ');
disp('Optimized drug kinetics:');
disp('========================');
disp(sprintf('achieved cost = %g',cost));
disp(sprintf('achieved_T12_a = %g',residuals(1) + REF_T12_a))
disp(sprintf('achieved_T12_b = %g',residuals(2) + REF_T12_b))
disp(sprintf('achieved_delta_c = %g',residuals(3) + REF_delta_c))
disp('Optimal parameters:');
for k=1:length(pnames),
    disp(sprintf('\t%s = %g',pnames{k},popt(k)));
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Remove MEX model if created here
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if isSBmodel(model),
    clear mex; delete(MEXmodelfullpath);
end
return

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Cost function for optimization of the parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [cost,residuals] = Drug_exp(x)
% Drug_exp: cost function for the optimization of given model parameters
% with the goal to achieve a certain (and provided):
%
%   - Alpha half-time of mAb
%   - Beta half-time of mAb
%   - percent mAb steady state concentration difference plasma interstitium

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% get the global variables
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
global pnames referenceValues MEXmodel opttimevector plotFlag parameternames parametervector weights input

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Get the reference values
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
REF_T12_a   = referenceValues(1);
REF_T12_b   = referenceValues(2);
REF_delta_c = referenceValues(3);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Perform an experiment to determine the alpha 
% and beta halflife of the drug in plasma 
% for given parameters to be optimized
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% set initial concentration of drug in plasma to a value
icvector = makeinicondvectorSBPD(MEXmodel,{'D_plasma'},1000);
% set the parameters to be optimized
parametervector_sim = updateparamvecSBPD(parameternames, parametervector, pnames, x);
% simulate                                     
options = [];
options.abstol = 1e-10;
options.reltol = 1e-10;
simdata = feval(MEXmodel, opttimevector, icvector, parametervector_sim, options);
% Get the interesting data from simulation results (timevector and drug concentration in plasma)
time     = simdata.time;
D_plasma = simdata.statevalues(:,getstateindicesSBPD(MEXmodel,'D_plasma'));

% Compute the Alpha half life in plasma (T12_a)
t1_indx = 10;
T1_a = time(1);
T2_a = time(t1_indx);
Y1_a = D_plasma(1);
Y2_a = D_plasma(t1_indx);
T12_a = log(0.5)*(T2_a - T1_a) / (log(Y2_a) - log(Y1_a));

% Compute the Beta half life in plasma (T12_b)
t2_indx = 100;
T1_b = time(end-t2_indx);
T2_b = time(end);
Y1_b = D_plasma(end-t2_indx);
Y2_b = D_plasma(end);
T12_b = log(0.5)*(T2_b - T1_b) / (log(Y2_b) - log(Y1_b));

if plotFlag,
    figure; clf;
    plot(time,D_plasma); hold on; drawnow;  
    plot(T1_a,Y1_a,'ro');
    plot(T2_a,Y2_a,'go');
    plot(T1_b,Y1_b,'rx');
    plot(T2_b,Y2_b,'gx');
    set(gca,'YScale','log');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Experiment: constant drug input (until 
% steadystate/final simulation time)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
doseparamnames = {input};
doseparamvalues = [1000];
% Apply dosing parameters and parameters to be optimized
parametervector_sim = updateparamvecSBPD(parameternames, parametervector, {pnames{:}, doseparamnames{:}}, [x(:)' doseparamvalues(:)']);
% nominal initial conditions
icvector = [];
% Simulate      
options = [];
options.abstol = 1e-10;
options.reltol = 1e-10;
simdata = feval(MEXmodel, opttimevector, icvector, parametervector_sim,options);
% Get the interesting data from simulation results
time        = simdata.time;
D_plasma    = simdata.statevalues(:,getstateindicesSBPD(MEXmodel,'D_plasma'));
% Which tissue is considered does not really make an important difference. 
% We select the disease tissue for our calculations (makes more sense than
% the peripheral tissue)
D_itst_tissue = simdata.statevalues(:,getstateindicesSBPD(MEXmodel,'D_itst_tissue'));

% Determine ration between drug concentrations in tissue and drug in plasma
delta_c = D_itst_tissue(end)/D_plasma(end)*100;

if plotFlag,
    figure; clf;
    plot(time,[D_plasma D_itst_tissue], '--'); drawnow
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Determine the residuals
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
res(1) = T12_a - REF_T12_a;
res(2) = T12_b - REF_T12_b;
res(3) = delta_c - REF_delta_c;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Determine the costfunction 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
residuals = [res(1) res(2) res(3)];
scaledresiduals = residuals./[REF_T12_a REF_T12_b REF_delta_c];
weightedresiduals = scaledresiduals.*weights;
cost = sum(weightedresiduals.^2);
return