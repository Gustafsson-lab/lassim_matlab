function [ indiv_param ] = parseNONMEMindivparamSBPOP( projectPath,numberParameters )
% parseNONMEMindivparamSBPOP: Returns the individual parameters from a
% MONOLIX fit. numberParameters needs to be provided to know how many they
% are, since this can change depending on the settings.

indiv_param             = SBPOPloadNONCSVdataset([projectPath '/RESULTS/project.indiv'],1);
indiv_param             = indiv_param(:,1:numberParameters+1);

% Check case of parameter names (might be changed by really cute NONMEM
% program ...)
X = parseProjectHeaderNONMEMSBPOP(projectPath);
PN = X.PARAMNAMES;
VN = get(indiv_param,'VarNames');
for k=1:length(VN),
    ix = strmatch(lower(VN{k}),lower(PN),'exact');
    if ~isempty(ix),
        VN{k} = PN{ix};
    end
end
indiv_param = set(indiv_param,'VarNames',VN);