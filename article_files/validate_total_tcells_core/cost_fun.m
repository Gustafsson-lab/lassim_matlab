%% A function that calculates the fit of a model with respect to the parameter vector "parameters".
%
% Created 150527
function [cost] = cost_fun(parameters, logic_plot)
%% Variable declaration
global model
global param_names
global edges
global data_tot
global std_tot
global time_tot


%% Set the compressed param vector to match the full parameter vector
parameters_expanded=zeros(1,75-12);
parameters_expanded(edges)=parameters(25:end);
parameters=[parameters(1:24) parameters_expanded];


%% Simulations
try
    sim_options                 = [];
    sim_options.method          = 'stiff';
    sim_options.maxnumsteps     = 1e8;
    
    simulation                  = SBPDsimulate(model,time_tot, data_tot(:,1),param_names,parameters,sim_options);
    sim_val                     = simulation.statevalues';
    for i=1:12
        sim_val(i,:) = sim_val(i,:)./max(sim_val(i,:));%
    end
    cost = sum(sum(((data_tot-sim_val)./mean(std_tot(:))).^2));
    
catch
    cost=inf;
    return
end
if nargin>1
    if logic_plot == 1
        sim_continuous            = SBPDsimulate(model,time_tot(end), data_tot(:,1),param_names,parameters,sim_options);
        sim_val_continuous        = sim_continuous.statevalues';
        
        for i=1:12
            sim_val_continuous(i,:) = sim_val_continuous(i,:)./max([sim_val_continuous(i,:)]);
        end
        names_of_tfs = sim_continuous.states;
        figure(1)
        for i=1:12
            subplot(3,4,i)
            hold on
            errorbar(time_tot, data_tot(i,:), std_tot(i,:), '*b','Linewidth', 2)
            plot(sim_continuous.time, sim_val_continuous(i,:),'-r', 'Linewidth', 2);
            title(names_of_tfs(i))
            xlabel('Time, h');
            xlim([-1 (max(time_tot)+1)])
            ylabel('Response, a.u.')
            hold off
        end
    end
end
end

