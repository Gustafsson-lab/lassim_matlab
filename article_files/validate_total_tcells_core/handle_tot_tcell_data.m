%% A script used to handle the data of transcription factors used in LASSIM
% All TF:s have knock data, and used for the new core model in the LASSIM
% paper.
%
% Created: 160621
%% Declarations
TF_name={'COPEB' %declares the names of the TF:s
    'ELK1'
    'GATA3'
    'IRF4'
    'JUN'
    'MAF'
    'MYB'
    'NFATC3'
    'NFKB1'
    'RELA'
    'STAT3'
    'USF2'};
TF_entrez=[1316 %declares the entrez numbers of the TF:s
    2002
    2625
    3662
    3725
    4094
    4602
    4775
    4790
    5970
    6774
    7392];

core_set=dataset(TF_name, TF_entrez); % Creates a dataset
%% Loads data time series for naive T-cells
load all_data
meas_core=ordering(ismember(ordering.EntrezGene, core_set.TF_entrez), :);

%% Loads data time series for all T-cells
data_tot  = zeros(12,4);
std_tot   = zeros(12,4);

for i=1:length(meas_core)
    data_tot(i,:) = mean([double(meas_core.x_total_soren(i,4:7)); double(meas_core.x_total_soren(i,8:11)); double(meas_core.x_total_soren(i,12:15))]);
    data_tot(i,:) = data_tot(i,:)./max(mean([double(meas_core.x_total_soren(i,4:7)); double(meas_core.x_total_soren(i,8:11)); double(meas_core.x_total_soren(i,12:15))]));
end
for i=1:length(meas_core)
    std_tot(i,:) = std([double(meas_core.x_total_soren(i,4:7)); double(meas_core.x_total_soren(i,8:11)); double(meas_core.x_total_soren(i,12:15))]);
    std_tot(i,:) = std_tot(i,:)./max(mean([double(meas_core.x_total_soren(i,4:7)); double(meas_core.x_total_soren(i,8:11)); double(meas_core.x_total_soren(i,12:15))]));
end

time_tot = tid.t_total_soren(4:7);

save('data_tot', 'data_tot','std_tot','time_tot')
%% End of script