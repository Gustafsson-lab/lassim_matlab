%% This is a script that loads and handles the data saved in 'results_folder'

start_dir = cd;
cd results_folder
name_of_files = ls;
name_of_testing_run = name_of_files(end,:);
name_of_files = name_of_files(3:end-1,:); % Removes two first ('.' and '..') rows, and the last one which is the run that is being validated

cost_vector = nan(1,length(name_of_files(:,1)));

for i = 1:length(name_of_files(:,1))
    load(name_of_files(i,:));
    cost_vector(i) = optim_cost;
end
cost_vector = cost_vector(~isnan(cost_vector)); %Removes any failed optimizations

load(name_of_testing_run);
p_value = sum(cost_vector<optim_cost)/length(cost_vector);

disp(strcat('The p-value =',{' '},num2str(p_value)))
figure()
hist(cost_vector,20)
title('Distribution of fit for 1000 Monte-Carlo models')
xlabel('Cost in \chi^{2}')
ylabel('Frequency')
cd(start_dir)