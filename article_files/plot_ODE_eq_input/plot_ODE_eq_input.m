%% plot_ODE_eq_input
% ================================================================
% This script plots the output of the input term in the LASSIM ODE
% ================================================================
% 
% Author: Rasmus Magnusson
% COPYRIGHT (C) Rasmus Magnusson
% Linkoping 03-Feb-2017
% 
% ================================================================
%% Begin 
close all

figure()
sum_input = -20:0.01:20;
expression_val = (1 + exp(-(sum_input))).^-1;
plot(sum_input,expression_val)

ylim([-0.05,1.05])
xlabel('Value of \Sigma_j w_i_,_jx_j', 'FontSize', 15)
ylabel('Resulting effect on state', 'FontSize', 15)
title('The saturation of the input effect', 'FontSize', 15)

% ================================================================
%% End of file
